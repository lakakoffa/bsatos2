#!/usr/bin/env perl

=head1 NAME
main_peak_int_v2.pl
=head1 Description
get the main peak from the polished or profile data
=head1 Usage
perl main_peak_int_v2.pl --int <minimum interval length of QTL> --th1 <threshold value of P data> --th2 <threshold value of M data> --th3 <the threshold value of H data> --p <the P data from polish step> --m <the M data from polish step> --pm <the H data from polish step>
--int: minimum interval length of QTL
--th1: the threshold value of P data
--th2: the threshold value of M data
--th3: the threshold value of H data
--p: the P data from polish step
--m: the M data from polish step
--pm: the H data from polish step
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
use FindBin qw($Bin);

my ($int, $th1, $th2, $th3, $p, $m, $pm, $o, $h);

GetOptions("p=s"=>\$p, "m=s"=>\$m, "pm=s"=>\$pm, "int=s"=>\$int, "th1=s"=>\$th1, "th2=s"=>\$th2, "th3=s"=>\$th3, "help"=>\$h);
die `pod2text $0` if ($h);

unless(defined($int)) {$int = 0;}

my $get_max_R = "$Bin/../R/get_max.R";
my $get_max_py = "$Bin/../Python/get_max.py";
my $get_interval_R = "$Bin/../R/get_interval.R";
my $get_interval_py = "$Bin/../Python/get_interval.py";


sub get_peak {
    my @paras = @_;   my $file = $paras[0];   my $thres = $paras[1];   my $origin = $paras[2];;
    print "#Origin\tChrom\tQTL_Start\tQTL_End\tPeak_Pos\tPeak\tAccession\n";

    open AFD, "<$file";
    my @lines = <AFD>;

    my @chrs = `cut -f 1 $file | uniq | grep -v "#"`;
    for my $chr (@chrs) {
        chomp($chr); chomp($chr);
        my ($start, $end) = (-1, -1);
        my (@chr_pos, @arr_g1m, @arr_pos) = ((), (), ());
        # my $interval = `Rscript $get_interval_R $file $chr`;
        my $interval = `python $get_interval_py $file $chr`;
        $interval =~ /(\d*)\t(\d*)/;
        $start = $1 + 1;   # 若用Python获取区间索引需要+1，R不需要
        $end = $2 + 1;   # 若用Python获取区间索引需要+1，R不需要

        my $first_line = $lines[$start];
        my @first_line_array = split /\t/, $first_line;
        my $first_line_pos = $first_line_array[1];
        my $first_line_g1m = $first_line_array[11];
        if ($first_line_g1m > $thres){
            push @arr_g1m, $first_line_g1m;
            push @arr_pos, $first_line_pos;
            push @chr_pos, "up";
            push @chr_pos, $first_line_pos;
        }

        foreach (@lines[($start+1)..($end-1)]) {
            my @line = split /\t/, $_;   # 0-CHROM 1-POS 2-H_REF 3-H_ALT 4-L_REF 5-L_ALT 6-H_REF_AF 7-H_ALT_AF 8-L_REF_AF 9-L_ALT_AF 10-G_Value 11-Gprimer_1M
            my $chr = $line[0];
            my $pos = $line[1];
            my $g1m = $line[11];
            push @arr_g1m, $g1m;
            push @arr_pos, $pos;
            if($#arr_g1m == 0) {next;}
            if($#arr_g1m == 1){
                if(($arr_g1m[0] >= $thres) && ($arr_g1m[1] < $thres)){
                    push @chr_pos, ("down", $arr_pos[0]);
                }
                if(($arr_g1m[0] < $thres) && ($arr_g1m[1] >= $thres)){
                    push @chr_pos, ("up", $arr_pos[1]);
                }
                shift @arr_g1m;
                shift @arr_pos;
            }
        }

        my $last_line = $lines[$end];
        my @last_line_array = split /\t/, $last_line;
        my $last_line_pos = $last_line_array[1];
        my $last_line_g1m = $last_line_array[11];
        if ($last_line_g1m > $thres){
            push @arr_g1m, $last_line_g1m;
            push @arr_pos, $last_line_pos;
            push @chr_pos, "down";
            push @chr_pos, $last_line_pos;
        }

        my ($cnt, $count, @tmp) = (0, 0, ());

        foreach my $index (@chr_pos){
            $count++;
            push @tmp, $index;
            if ($count == 4){
                if(($tmp[0] eq "up") && ($tmp[2] eq "down")){
                    my $up = $tmp[0];
                    my $lower = $tmp[1];
                    my $down = $tmp[2];
                    my $upper = $tmp[3];
                    my $value = `python $get_max_py $file $chr $lower $upper`;
                    # my $value = `Rscript $get_max_R $file $chr $lower $upper`;
                    if(($upper - $lower) >= ($int * 1000)){
                        unless($value =~ /NA/){
                            $value =~ m/(\d.*\d)\t(\d*)/;
                            my $G1M = sprintf("%.2f", $1);
                            my $pos = $2;
                            $cnt++;
                            $chr =~ /Chr(\d\d)/i;
                            my $chr_num = $1;
                            my $accession = $origin . $chr_num . "." . $cnt;
                            print "$origin\t$chr\t$lower\t$upper\t$pos\t$G1M\t$accession\n";
                        }
                    }
                }
                @tmp = ();
                $count = 0;
            }
        }

        # my $length = int(($#chr_pos + 1)/4);
        # for (my $i = 0; $i < $length; $i++){
        #     my $up_index = $i * 4 + 0;
        #     my $start_index = $i * 4 + 1;
        #     my $down_index = $i * 4 + 2;
        #     my $end_index = $i * 4 + 3;
        #     my $up = $chr_pos[$up_index];
        #     my $lower = $chr_pos[$start_index];
        #     my $down = $chr_pos[$down_index];
        #     my $upper = $chr_pos[$end_index];
        #     if(($up eq "up") && ($down eq "down")){
        #         my $value = `python $get_max_py $file $chr $lower $upper`;
        #         # my $value = `Rscript $get_max_R $file $chr $lower $upper`;
        #         if(($upper - $lower) >= ($int * 1000)){
        #             unless($value =~ /NA/){
        #                 $value =~ m/(\d.*\d)\t(\d*)/;
        #                 my $G1M = $1;
        #                 my $pos = $2;
        #                 $cnt++;
        #                 $chr =~ /Chr(\d\d)/;
        #                 my $chr_num = $1;
        #                 my $accession = $origin . $chr_num . "." . $cnt;
        #                 print "$origin\t$chr\t$lower\t$upper\t$pos\t$G1M\t$accession\n";
        #             }
        #         }
        #     }
        #     @tmp = ();
        #     $count = 0;
        # }
    }
}


$th1 = sprintf("%.2f", $th1);
$th2 = sprintf("%.2f", $th2);
$th3 = sprintf("%.2f", $th3);

&get_peak($p, $th1, "P");     # polish_dir/P.polished.afd 2.77 P
&get_peak($m, $th2, "M");     # polish_dir/M.polished.afd 7.30 M
&get_peak($pm, $th3, "H");     # polish_dir/PM.polished.afd 3.39 H
