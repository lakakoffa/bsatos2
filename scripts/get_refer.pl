#!/usr/bin/env perl

=head1 NAME
get_ref.pl
=head1 Description
get reference genotype from mass of phase files
=head1 Usage
perl get_ref.pl <phase file> <genome file>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, %hash, $chr, $pos);
GetOptions("help" => \$h);
die `pod2text $0` if ($h);

open PHASE, "<$ARGV[0]";
open FA, "<$ARGV[1]";

while(<PHASE>){
    if($_ =~ /^M/) {
        my @a = split /\t/, $_;
        $hash{$a[1]}{$a[3]}++;
    }
}

while(<FA>){
    if($_ =~ />(\S+)/){
        $chr = $1;
        $pos = 0;
    }else{
        chomp;
        my @seq = split //, $_;
        foreach my $index (@seq){
            $pos++;
            if(defined($hash{$chr}{$pos})){
                print "$chr\t$pos\t$index\n";
            }
        }
    }
}
close PHASE;
close FA;
