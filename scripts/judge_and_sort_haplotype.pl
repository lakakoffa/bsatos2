#!/usr/bin/env perl

=head1 NAME
judge_and_sort_haplotype.pl
=head1 Description
judge and sort haplotype file
=head1 Usage
perl judge_and_sort_haplotype.pl <STDIN>
=cut

use strict;
use warnings;
use Pod::Usage;

my (@c, %hash) = ((), ());

while(<>){
    chomp;
    my @a = split /\t/, $_;
    if($a[4] eq "0") {$hash{$a[6]}{"0"}++;}
    if($a[4] eq "1") {$hash{$a[6]}{"1"}++;}
    push @c, $_;
}

foreach (@c){
    my @b = split /\t/, $_;
    unless(defined($hash{$b[6]}{"0"})) {$hash{$b[6]}{"0"} = 0;}
    unless(defined($hash{$b[6]}{"1"})) {$hash{$b[6]}{"1"} = 0;}
    if($hash{$b[6]}{"0"} > $hash{$b[6]}{"1"}){
        print "$b[0]\t$b[1]\t$b[2]\t$b[3]\t$b[4]\t$b[5]\t$b[6]\n";
    }else{
        print "$b[0]\t$b[1]\t$b[2]\t$b[3]\t$b[5]\t$b[4]\t$b[6]\n";
    }
}
