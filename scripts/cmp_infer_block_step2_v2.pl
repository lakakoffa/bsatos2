#!/usr/bin/env perl

=head1 NAME
perl cmp_infer_block_step2_v2.pl
=head1 Description
compare and infer phase blocks from progenies to parents
=head1 Usage
perl cmp_infer_block_step2_v2.pl <merged file>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, @a, @b, %hash) = (undef, (), (), ());
GetOptions("help" => \$h);
die `pod2text $0` if ($h);

### second step infer missing

open MYTMP, "<$ARGV[0]";
while(<MYTMP>){
    chomp;
    if($_ =~ /#/) {next;}
    push @a, $_;

    if($#a == 1){
        my %pb = ();
        my ($p_s1, $p_s2, $m_s1, $m_s2, $h_s1, $h_s2, $l_s1, $l_s2);

        my @first = split /\t/, $a[0];
        my @second = split /\t/, $a[1];

        foreach my $index (@a){
            @b = split /\t/, $index;
            $hash{$b[0]}{$b[1]} = [@b];     # 匿名数组的引用
            $p_s1 .= $b[4];
            $p_s2 .= $b[5];
            $m_s1 .= $b[7];
            $m_s2 .= $b[8];
            $h_s1 .= $b[10];
            $h_s2 .= $b[11];
            $l_s1 .= $b[13];
            $l_s2 .= $b[14];
        }

        if(($first[6] eq $second[6]) && ($first[6] ne "-") && ($first[9] eq $second[9]) && ($first[9] ne "-")){
            if(($p_s1 ne $m_s1) && ($p_s1 ne $m_s2)){
                shift @a;
                next;
            }
        }

        if(($first[6] eq $second[6]) && ($first[6] ne "-") && ($first[9] eq $second[9]) && ($first[9] ne "-")){
            if(($first[10] eq $first[4]) && (($second[12] eq "-") || ($second[12] ne $first[12]))){
                $second[10] = $second[4];
                $second[11] = $second[5];
                $second[12] = $first[6];
            }
            if(($first[10] eq $first[5]) && (($second[12] eq "-") || ($second[12] ne $first[12]))){
                $second[10] = $second[5];
                $second[11] = $second[4];
                $second[12] = $first[6];
            }
            if(($first[13] eq $first[4]) && (($second[15] eq "-") || ($second[15] ne $first[15]))){
                $second[13] = $second[4];
                $second[14] = $second[5];
                $second[15] = $first[6];
            }
            if(($first[13] eq $first[5]) && (($second[15] eq "-") || ($second[15] ne $first[15]))){
                $second[13] = $second[5];
                $second[14] = $second[4];
                $second[15] = $first[6];
            }
            $hash{$second[0]}{$second[1]} = [@second];     # 匿名数组的引用
        }
        $a[1] = join("\t", @second);
        shift @a;
    }
}
close MYTMP;

my @chr = keys %hash;
foreach my $index (@chr){
    my @pos = sort {$a <=> $b} keys %{$hash{$index}};
    foreach my $i (@pos){
        print join("\t", @{$hash{$index}{$i}})."\n";
    }
}
