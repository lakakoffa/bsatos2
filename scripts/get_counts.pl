#!/usr/bin/env perl

=head1 NAME
perl get_counts.pl
=head1 Description
get reads counts data based on the SNP calling result
=head1 Usage
perl get_counts.pl --d <dep> --q <quality> <VCF file>
--d: the theshold of coverage depth
--q: the log-phred SNP/InDel quality
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, $d, $q);
GetOptions("d=i" => \$d, "q=i" => \$q, "help" => \$h);
die `pod2text $0` if ($h);

unless(defined($d)) {$d = 10;}
unless(defined($q)) {$q = 10;}

while(<>){
    chomp;
    # unless(($_ =~ /(?:INDEL)|#/){
    unless(($_ =~ /#/) || ($_ =~ /INDEL/)){     # 过滤开头注释行以及（在FILTER列中）标注INDEL的行
        my @line = split /\t/, $_;     # CHROM、POS、ID、REF、ALT、QUAL、FILTER、INFO、FORMAT、Parent1、Parent2
        my $chr = $line[0];
        my $pos = $line[1];
        # if(($line[5] =~ /\d+\.?\d*/) && ($line[5] >= $q) && ($line[4] !~ /,/)){    # 第6个字段$line[5]匹配小数(小数点前后至少有一个数字)或正整数，不可仅为一个.字符，否则无法与数值$q比较大小
        if(($line[5] >= $q) && ($line[4] !~ /,/)){
            $_ =~ /DP4=(\d+),(\d+),(\d+),(\d+)/;     # DP4: Number of high-quality ref-forward , ref-reverse, alt-forward and alt-reverse bases
            my $ref = $1 + $2;
            my $alt = $3 + $4;
            my $all = $ref + $alt;
            # my $snp_index = $alt / $all;
            if($all >= $d) {print "$chr\t$pos\t$ref\t$alt\n";}
        }
    }
}
