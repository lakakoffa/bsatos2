#!/usr/bin/env perl

=head1 NAME
get_block.pl
=head1 Description
get the phase blocks from the phase file
=head1 Usage
perl get_block.pl <BAM phase file> <BAM ref file from get_ref.pl>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my $count=0;
my ($name, $ref, $alt1, $alt2, $q, $g, $d, $h, %hash);
GetOptions("help" => \$h);
die `pod2text $0` if ($h);

open PHASE, "<$ARGV[0]";
open REF, "<$ARGV[1]";

while(<REF>){
    chomp;
    my @c = split /\t/, $_;     # chrom  pos  ref
    $hash{$c[0]}{$c[1]} = $c[2];
}

print "\#CHROM\tPOS\tREF\tALT\tHAP1\tHAP2\tNAME\n";

while(<PHASE>){
    chomp;
    if($_ =~ /^PS/){     # PS: strat of a phased set
        $count++;
        $name = "block".$count;
    }
    if($_ =~ /^M/){     # M[012]: markers; 0 for singletons, 1 for phased and 2 for filtered
        my @a = split /\t/, $_;     # M?  chr  PS  pos  allele0  allele1  hetIndex  #supports0  #errors0  #supp1  #err1
        if (exists $hash{$a[1]}{$a[3]}) {
            $ref = $hash{$a[1]}{$a[3]};
            if($ref eq $a[4]){
                $alt1 = "0";
                $alt2 = "1";
                print "$a[1]\t$a[3]\t$ref\t$a[5]\t$alt1\t$alt2\t$name\n";
            }
            if($ref eq $a[5]){
                $alt1 = "1";
                $alt2 = "0";
                print "$a[1]\t$a[3]\t$ref\t$a[4]\t$alt1\t$alt2\t$name\n";
            }
        }

    }
}

close PHASE;
close REF;
