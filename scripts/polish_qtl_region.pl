#!/usr/bin/env perl

=head1 NAME
polish_qtl_region.pl
=head1 Description
polish QTL region and correct the QTL peaks
=head1 Usage
perl polish_qtl_region.pl <afd> <theshold 1M> <threhold 0.75M> <threshold 0.5M> <threshold 0.25M>
=cut


use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my $h;
GetOptions("help" => \$h);
die `pod2text $0` if ($h);

my ($c, %p, %m, %b1, %k1, %b2, %k2, %b3, %k3, %b4, %k4);

open AFD, "<$ARGV[0]";
while(<AFD>){
    if($_ =~ /#/) {next;}
    my @a = split /\t/, $_;
    #0-CHROM  1-POS  2-H_REF  3-H_ALT  4-L_REF  5-L_ALT  6-H_REF_AF  7-H_ALT_AF  8-L_REF_AF  9-L_ALT_AF  10-G_VALUE  11-Gprimer_1M  12-Gprimer_750K  13-Gprimer_500K  14-Gprimer_250K
    # 15-CHROM  16-POS  17-REF  18-ALT  19-P_HAP1  20-P_HAP2  21-P_NAME  22-M_HAP1  23-M_HAP2  24-M_NAME  25-H_HAP1  26-H_HAP2  27-H_NAME  28-L_HAP1  29-L_HAP2  30-L_NAME
    $c = $a[7] - $a[9];

    if($a[10] >= $ARGV[1]) {$b1{$a[22]}{"T"}++; $k1{$a[25]}{"T"}++;} else {$b1{$a[22]}{"F"}++; $k1{$a[25]}{"F"}++;}
    if($a[11] >= $ARGV[2]) {$b2{$a[22]}{"T"}++; $k2{$a[25]}{"T"}++;} else {$b2{$a[22]}{"F"}++; $k2{$a[25]}{"F"}++;}
    if($a[12] >= $ARGV[3]) {$b3{$a[22]}{"T"}++; $k3{$a[25]}{"T"}++;} else {$b3{$a[22]}{"F"}++; $k3{$a[25]}{"F"}++;}
    if($a[13] >= $ARGV[4]) {$b4{$a[22]}{"T"}++; $k4{$a[25]}{"T"}++;} else {$b4{$a[22]}{"F"}++; $k4{$a[25]}{"F"}++;}

    if($a[21] eq "1") {if($c > 0) {$p{$a[22]}{"H"}++;} else {$p{$a[22]}{"L"}++;}}
    if($a[21] eq "0") {if($c < 0) {$p{$a[22]}{"H"}++;} else {$p{$a[22]}{"L"}++;}}
    if($a[24] eq "1") {if($c > 0) {$m{$a[25]}{"H"}++;} else {$m{$a[25]}{"L"}++;}}
    if($a[24] eq "0") {if($c < 0) {$m{$a[25]}{"H"}++;} else {$m{$a[25]}{"L"}++;}}
}
close(AFD);


my ($fp, $lp, $fm, $mp, $c2, $n1, $n2, $n3, $n4, $n5, $n6, $n7, $n8, %remove);

open AFD, "<$ARGV[0]";
while(<AFD>){
    chomp;
    if($_ =~ /#/) {next;}
    my @b = split /\t/, $_;

    unless(defined($b1{$b[22]}{"T"})) {$b1{$b[22]}{"T"} = 0;}
    unless(defined($b1{$b[22]}{"F"})) {$b1{$b[22]}{"F"} = 0;}
    unless(defined($b2{$b[22]}{"T"})) {$b2{$b[22]}{"T"} = 0;}
    unless(defined($b2{$b[22]}{"F"})) {$b2{$b[22]}{"F"} = 0;}
    unless(defined($b3{$b[22]}{"T"})) {$b3{$b[22]}{"T"} = 0;}
    unless(defined($b3{$b[22]}{"F"})) {$b3{$b[22]}{"F"} = 0;}
    unless(defined($b4{$b[22]}{"T"})) {$b4{$b[22]}{"T"} = 0;}
    unless(defined($b4{$b[22]}{"F"})) {$b4{$b[22]}{"F"} = 0;}
    unless(defined($k1{$b[25]}{"T"})) {$k1{$b[25]}{"T"} = 0;}
    unless(defined($k1{$b[25]}{"F"})) {$k1{$b[25]}{"F"} = 0;}
    unless(defined($k2{$b[25]}{"T"})) {$k2{$b[25]}{"T"} = 0;}
    unless(defined($k2{$b[25]}{"F"})) {$k2{$b[25]}{"F"} = 0;}
    unless(defined($k3{$b[25]}{"T"})) {$k3{$b[25]}{"T"} = 0;}
    unless(defined($k3{$b[25]}{"F"})) {$k3{$b[25]}{"F"} = 0;}
    unless(defined($k4{$b[25]}{"T"})) {$k4{$b[25]}{"T"} = 0;}
    unless(defined($k4{$b[25]}{"F"})) {$k4{$b[25]}{"F"} = 0;}

    $n1 = $b1{$b[22]}{"T"} / ($b1{$b[22]}{"T"} + $b1{$b[22]}{"F"});
    $n2 = $k1{$b[25]}{"T"} / ($k1{$b[25]}{"T"} + $k1{$b[25]}{"F"});
    $n3 = $b2{$b[22]}{"T"} / ($b2{$b[22]}{"T"} + $b2{$b[22]}{"F"});
    $n4 = $k2{$b[25]}{"T"} / ($k2{$b[25]}{"T"} + $k2{$b[25]}{"F"});
    $n5 = $b3{$b[22]}{"T"} / ($b3{$b[22]}{"T"} + $b3{$b[22]}{"F"});
    $n6 = $k3{$b[25]}{"T"} / ($k3{$b[25]}{"T"} + $k3{$b[25]}{"F"});
    $n7 = $b4{$b[22]}{"T"} / ($b4{$b[22]}{"T"} + $b4{$b[22]}{"F"});
    $n8 = $k4{$b[25]}{"T"} / ($k4{$b[25]}{"T"} + $k4{$b[25]}{"F"});

    unless(($n1 < 0.7) && ($n2 < 0.7) && ($n3 < 0.7) && ($n4 < 0.7) && ($n5 < 0.7) && ($n6 < 0.7) && ($n8 < 0.7)) {next;}

    $c2 = $b[7] - $b[9];

    unless($p{$b[22]}{"H"}) {$p{$b[22]}{"H"} = 0;}
    unless($p{$b[22]}{"L"}) {$p{$b[22]}{"L"} = 0;}
    unless($m{$b[25]}{"H"}) {$m{$b[25]}{"H"} = 0;}
    unless($m{$b[25]}{"L"}) {$m{$b[25]}{"L"} = 0;}

    $lp = $p{$b[22]}{"L"} + $p{$b[22]}{"H"};
    $mp = $m{$b[25]}{"L"} + $m{$b[25]}{"H"};

    if($lp > 5) {$fp = $p{$b[22]}{"H"} / ($p{$b[22]}{"L"} + $p{$b[22]}{"H"});} else {$fp = 0;}
    if($mp > 5) {$fm = $m{$b[25]}{"H"} / ($m{$b[25]}{"L"} + $m{$b[25]}{"H"});} else {$fm = 0;}

    if($b[21] eq "1"){
        if($fp > 0.7) {if($c2 < 0) {$remove{$b[0]}{$b[1]}++;}}
        if(($fp < 0.3) && ($fp > 0)) {if($c2 > 0) {$remove{$b[0]}{$b[1]}++;}}
    }

    if($b[21] eq "0"){
        if($fp > 0.7) {if($c2 > 0) {$remove{$b[0]}{$b[1]}++;}}
        if(($fp < 0.3) && ($fp > 0)) {if($c2 < 0) {$remove{$b[0]}{$b[1]}++;}}
    }

    if($b[24] eq "1"){
        if($fm > 0.7) {if($c2 < 0) {$remove{$b[0]}{$b[1]}++;}}
        if(($fm < 0.3) && ($fm > 0)) {if($c2 > 0) {$remove{$b[0]}{$b[1]}++;}}
    }

    if($b[24] eq "0"){
        if($fm > 0.7) {if($c2 > 0) {$remove{$b[0]}{$b[1]}++;}}
        if(($fm < 0.3) && ($fm > 0)) {if($c2 < 0) {$remove{$b[0]}{$b[1]}++;}}
    }
}
close(AFD);

open AFD, "<$ARGV[0]";
while(<AFD>){
    if($_ =~ /#/) {next;}
    my @c = split /\t/, $_;
    unless(defined($remove{$c[0]}{$c[1]})){
        print join("\t", @c[0..5]), "\n";     # print "$c[0]\t$c[1]\t$c[2]\t$c[3]\t$c[4]\t$c[5]\n";
    }
}
close AFD;
