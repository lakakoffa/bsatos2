#!/usr/bin/env perl

=head1 NAME
generate_vcf_and_maf.pl
=head1 Description
generate VCF and MAF files based on the variation and annotation file
=head1 Usage
perl generate_vcf_and_maf.pl --var <variation type>  --header <yes> --in <variation and annotation file file>
--h: help information
--in: variation and annotation file file
--var: variation type snv or sv [snv]
--header: add header information (yes/no) [yes]
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($count, $h, $in, $header, $var);
GetOptions("var=s" => \$var, "header=s" => \$header, "in=s" => \$in, "help" => \$h);
die `pod2text $0` if ($h);
unless(defined($var)) {$var = "snv";}
unless(defined($header)) {$header = "yes";}

my $vcf_out = $in.".igv.vcf";
my $mut_out = $in.".igv.mut";

open IN, "<$in";
open VCF, ">$vcf_out";
open MUT, ">$mut_out";

while(<IN>){
    chomp;
    $count++;
    if(($count == 1) && ($header eq "yes")){
        if($var eq "snv") {print VCF "##fileformat=VCFv4.2\n#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tP_SNV\tM_SNV\n";}
        if($var eq "sv") {print VCF "##fileformat=VCFv4.2\n#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tP_SV\tM_SV\n";}
        print MUT "#chr\tstart\tend\tsample\ttype\n";
    }else{
        my @line = split /\t/, $_;
        print VCF "$line[13]\t$line[14]\t$line[15]\t$line[16]\t$line[17]\t$line[18]\t$line[19]\t$line[20]\t$line[21]\t$line[22]\t$line[23]\n";
        print MUT "$line[0]\t$line[1]\t$line[2]\tparents\t$line[8]\n";
    }
}
close IN;
close VCF;
close MUT;
