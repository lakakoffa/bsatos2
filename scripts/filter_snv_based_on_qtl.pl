#!/usr/bin/env perl

=head1 NAME
filter_snv_based_on_qtl.pl
=head1 Description
filter SNVs based on the QTL type
=head1 Usage
perl filter_snv_based_on_qtl.pl --i <QTL type> --d <promoter region> --q <SNV quality> --cov <read depth coverage> --hap <enriched haplotype file> <STDIN>
--i: QTL type
--d: promoter region
--q: SNV quality
--cov: read depth coverage
--hap: enriched haplotype file
<STDIN>: annotated SNVs file from ANNOVAR
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, $i, $hap, $q, $d, $cov, %hap, $parent1, $parent2);
GetOptions("hap=s" => \$hap, "i=s" => \$i, "q=i" => \$q, "d=i" => \$d, "cov=i" => \$cov, "help" => \$h);
die `pod2text $0` if ($h || (!defined($i)));

unless(defined($d)) {$d = 2000;}
unless(defined($q)) {$q = 10;}
unless(defined($cov)) {$cov = 10;}

open HAP, "<$hap";
while(<HAP>){
    chomp;
    my @p = split /\t/, $_;
    $hap{$p[0]}{$p[1]} = $_;
}

print "#Chr\tStart\tEnd\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tOtherinfo\tQual\t-\tChr\tPos\t-\tRef\tAlt\tQual\t-\tInfo\tFormat\tPollen Parent\tMaternal Parent\tChr\tEnd\tRef\tAlt\tP_HAP1\tP_HAP2\tP_NAME\tP_EN\tP_G\tM_HAP1\tM_HAP2\tM_NAME\tM_EN\tM_G\n";

while(<>){
    chomp;
    my @a = split /\t/, $_;   # 0-Chr  1-Start  2-End  3-Ref  4-Alt  5-Func.refGene  6-Gene.refGene  7-GeneDetail.refGene  8-ExonicFunc.refGene  9-AAChange.refGene  10-Otherinfo
    # Chr16  38991855  38991855  C  A  intergenic  gene:MD16G1283000;gene:MD16G1283100  dist=71513;dist=614900  .  .  0.5  455  252  Chr16  38991855  .  C  A  455  .  DP=252;......;AN=4;DP4=108,59,38,18;MQ=50  GT:PL  0/1:255,0,255  0/1:235,0,255
    $_ =~ /DP=(\d+);/;
    my $dp = $1;
    if($a[18] >= $q && $dp >= $cov){
        if($a[22] =~ /(\d\/\d):/) {$parent1 = $1;}   # 0/1:255,0,255
        if($a[23] =~ /(\d\/\d):/) {$parent2 = $1;}   # 0/1:235,0,255
        if($i eq "P"){
            if(($parent1 eq "0/1") && ($parent2 eq "0/0" || $parent2 eq "1/1")){
                if($_ =~ /dist=(\d+)\;dist=(\d+)/) {if(($1 > $d) && ($2 > $d)) {next;}}   # upstream distance;downstream distance
                unless(($_ =~ /\tsynonymous SNV/) || ($_ =~ /intronic/) || ($_ =~ /downstream/)){
                    unless(defined($hap{$a[0]}{$a[1]})) {$hap{$a[0]}{$a[1]}="\t\t\t\t\t\t\t\t\t\t\t\t\t";}
                    # print "$a[0]\t$a[1]\t$a[3]\t$a[4]\t$a[5]\t$a[6]\t$a[7]\t$a[8]\t$a[18]\t$a[22]\t$a[23]\t$hap{$a[0]}{$a[1]}\n";
                    print "$_\t$hap{$a[0]}{$a[1]}\n";
                }
            }
        }
        if($i eq "M"){
            if(($parent2 eq "0/1") && ($parent1 eq "0/0" || $parent1 eq "1/1")){
                if($_ =~ /dist=(\d+)\;dist=(\d+)/) {if(($1 > $d) && ($2 > $d)) {next;}}   # upstream distance;downstream distance
                unless(($_ =~ /\tsynonymous SNV/) || ($_ =~ /intronic/) || ($_ =~ /downstream/)){
                    unless(defined($hap{$a[0]}{$a[1]})) {$hap{$a[0]}{$a[1]}="\t\t\t\t\t\t\t\t\t\t\t\t\t";}
                    #print "$a[0]\t$a[1]\t$a[3]\t$a[4]\t$a[5]\t$a[6]\t$a[7]\t$a[8]\t$a[18]\t$a[22]\t$a[23]\t$hap{$a[0]}{$a[1]}\n";
                    print "$_\t$hap{$a[0]}{$a[1]}\n";
                }
            }
        }
        if($i eq "H"){
            if(($parent1 eq "0/1") && ($parent2 eq "0/1")){
                if($_ =~ /dist=(\d+)\;dist=(\d+)/) {if(($1 > $d) && ($2 > $d)) {next;}}   # upstream distance;downstream distance
                unless(($_ =~ /\tsynonymous SNV/) || ($_ =~ /intronic/) || ($_ =~ /downstream/)){
                    unless(defined($hap{$a[0]}{$a[1]})) {$hap{$a[0]}{$a[1]}="\t\t\t\t\t\t\t\t\t\t\t\t\t";}
                    # print "$a[0]\t$a[1]\t$a[3]\t$a[4]\t$a[5]\t$a[6]\t$a[7]\t$a[8]\t$a[18]\t$a[22]\t$a[23]\t$hap{$a[0]}{$a[1]}\n";
                    print "$_\t$hap{$a[0]}{$a[1]}\n";
                }
            }
        }
    }
}
