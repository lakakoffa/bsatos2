#!/usr/bin/env perl

=head1 NAME
get_bed.pl
=head1 Description
get genes in the QTL region
=head1 Usage
perl get_bed.pl <start> <end> <STDIN>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, $header);
GetOptions( "header=s" => \$header, "help" => \$h);
die `pod2text $0` if ($h);
unless(defined($header)) {$header = "F";}

while(<>){
    my @a = split /\t/, $_;   # Chr Start End Ref Alt Func.refGene Gene.refGene GeneDetail.refGene ExonicFunc.refGene AAChange.refGene Otherinfo
    if($_ =~ /#/) {if($header eq "F") {next;} else {print $_;}}
    if(($a[1] >= $ARGV[0]) && ($a[1] <= $ARGV[1])){
        print $_;
    }
}
