#!/usr/bin/env perl

=head1 NAME
get_bed.pl
=head1 Description
get genes in the QTL region
=head1 Usage
perl get_bed.pl <gene bed file> <chromosome> <start> <end>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, $header);
GetOptions( "header=s" => \$header, "help" => \$h);
die `pod2text $0` if ($h);
unless(defined($header)) {$header = "F";}

open LOD, "<$ARGV[0]";
while(<LOD>){
    my @a = split /\t/, $_;   # Chr Start End Ref Alt Func.refGene Gene.refGene GeneDetail.refGene ExonicFunc.refGene AAChange.refGene Otherinfo
    if($_ =~ /#/) {if($header eq "F") {next;} else {print $_;}}
    if(($a[0] eq $ARGV[1]) && ($a[1] >= $ARGV[2]) && ($a[1] <= $ARGV[3])){
        print $_;
    }
}
close LOD;
