#!/usr/bin/env perl

=head1 NAME
get_hap.pl
=head1 Description
get haplotype from block file
=head1 Usage
perl get_hap.pl <block file>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($name, $count) = (undef, 0);

while(<>){
    chomp;
    unless($_ =~ /\*/){
        if($_ =~ /BLOCK/){
            $count++;
            $name = "block".$count;
        }else{
            my @a = split /\t/, $_;
            print "$a[3]\t$a[4]\t$a[5]\t$a[6]\t$a[1]\t$a[2]\t$name\n";
        }
    }
}
