#!/usr/bin/env perl

=head1 NAME
merge_haplotype.pl
=head1 Description
merge haplotype of the files
=head1 Usage
perl merge_haplotype.pl --Pb <> --Mb <> --Hb <> --Lb <>
--Pb: block file from pollen parent
--Mb: block file from maternal parent
--Hb: block file from high pool
--Lb: block file from low pool
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, $Pb, $Mb, $Hb, $Lb, %hash);
GetOptions("Pb=s" => \$Pb, "Mb=s" => \$Mb, "Hb=s" => \$Hb, "Lb=s" => \$Lb, "help" => \$h);
die `pod2text $0` if ($h);

open LOP, "<$Pb";
open LOM, "<$Mb";
open LOH, "<$Hb";
open LOL, "<$Lb";

while(<LOP>){
    unless($_ =~ /#/){
        my @a = split /\t/, $_;     # CHROM   POS   REF   ALT   HAP2   HAP1   NAME
        $hash{$a[0]}{$a[1]}++;
    }
}

while(<LOM>){
    unless($_ =~ /#/){
        my @a = split /\t/, $_;     # CHROM   POS   REF   ALT   HAP2   HAP1   NAME
        $hash{$a[0]}{$a[1]}++;
    }
}

while(<LOH>){
    unless($_ =~ /#/){
        my @a = split /\t/, $_;     # CHROM   POS   REF   ALT   HAP2   HAP1   NAME
        $hash{$a[0]}{$a[1]}++;
    }
}

while(<LOL>){
    unless($_ =~ /#/){
        my @a = split /\t/, $_;     # CHROM   POS   REF   ALT   HAP2   HAP1   NAME
        $hash{$a[0]}{$a[1]}++;
    }
}

my @chrs = sort %hash;
foreach my $chr (@chrs){
    my @positon = sort {$a <=> $b} keys %{$hash{$chr}};
    foreach my $pos (@positon){
        print "$chr\t$pos\n";
    }
}


close LOP;
close LOM;
close LOH;
close LOL;
