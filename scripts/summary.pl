#!/usr/bin/env perl

=head1 NAME
summary.pl
=head1 Description
get summary information from the QTL SNV file and QTL SV file
=head1 Usage
perl summary.pl --dir qtl_pick_dir --qtl QTL.txt --snv QTL_SNV_genes.txt --sv QTL_SV_genes.txt --vcfdir qtl_pick_dir/SNV_SV --prepar prepar_dir --out QTL_summary.txt
=cut

use utf8;
use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
use FindBin qw($Bin);

my ($dir, $qtl, $snv, $sv, $vcfdir, $out, $prepar, $h);
GetOptions("dir=s" => \$dir, "qtl=s" => \$qtl, "snv=s" => \$snv, "sv=s" => \$sv, "vcfdir=s" => \$vcfdir, "prepar=s" => \$prepar, "out=s" => \$out, "help" => \$h);
die `pod2text $0` if ($h);

open (OUT, ">$out") or die "Error: can't open $out\n";

my $rg = "$FindBin::Bin/tools/bin/rg";
my $pthres = $dir . "/p.thres";
my $mthres = $dir . "/m.thres";
my $pmthres = $dir . "/pm.thres";
open (PTHRES, $pthres) or die "Error: can't open $pthres\n";
open (MTHRES, $mthres) or die "Error: can't open $mthres\n";
open (PMTHRES, $pmthres) or die "Error: can't open $pmthres\n";
my @p_thres_lines = <PTHRES>;
my @m_thres_lines = <MTHRES>;
my @pm_thres_lines = <PMTHRES>;
close (PTHRES);
close (MTHRES);
close (PMTHRES);

open (QTL, $qtl) or die "Error: can't open $qtl\n";
my @qtl = <QTL>;
close (QTL);

my $max_peak_value;
my $max_qtl_line;
foreach (@qtl) {
    if ($_ !~ /Accession/) {
        my @qtl_line = split /\t/,$_;   # 0-Origin 1-Chrom 2-QTL_Start 3-QTL_End 4-Peak_Pos 5-Peak 6-Accession
        if ($qtl_line[5] > $max_peak_value) {
            $max_qtl_line = $_;
        }
    }
}
print OUT "Major QTL:\t$max_qtl_line\n\n";

print OUT "Threshold\tPollen_Parent(blue)\tMaternal_Parent(red)\tHeterozygote(black)\n";
chomp($p_thres_lines[0], $m_thres_lines[0], $pm_thres_lines[0]);
print OUT "1M\t" . $p_thres_lines[0] . "\t" . $m_thres_lines[0] . "\t" . $pm_thres_lines[0] . "\n";
chomp($p_thres_lines[1], $m_thres_lines[1], $pm_thres_lines[1]);
print OUT "750K\t" . $p_thres_lines[1] . "\t" . $m_thres_lines[1] . "\t" . $pm_thres_lines[1] . "\n";
chomp($p_thres_lines[2], $m_thres_lines[2], $pm_thres_lines[2]);
print OUT "500K\t" . $p_thres_lines[2] . "\t" . $m_thres_lines[2] . "\t" . $pm_thres_lines[2] . "\n";
chomp($p_thres_lines[3], $m_thres_lines[3], $pm_thres_lines[3]);
print OUT "250K\t" . $p_thres_lines[3] . "\t" . $m_thres_lines[3] . "\t" . $pm_thres_lines[3] . "\n\n";


print OUT "QTL_total_number\tSNV_total_number\tSNV_total_gene_number\tSV_total_number\tSV_total_gene_number\t\n";
my $qtl_total_number = `wc -l $qtl`;
my $snv_total_number = `$rg -oNP "^Chr\\d\\d" $snv | wc -l`;
my $snv_total_gene_number = `$rg -oNP "MD\\d\\dG\\d*" $snv | uniq | wc -l`;
my $sv_total_number = `$rg -oNP "^Chr\\d\\d" $sv | wc -l`;
my $sv_total_gene_number = `$rg -oNP "MD\\d\\dG\\d*" $sv | uniq | wc -l`;
my @qtl_total_number_array = split /\s/, $qtl_total_number;
$qtl_total_number = $qtl_total_number_array[0] - 3;
chomp($snv_total_number, $snv_total_gene_number, $sv_total_number, $sv_total_gene_number);
print OUT "$qtl_total_number\t$snv_total_number\t$snv_total_gene_number\t$sv_total_number\t$sv_total_gene_number\n\n";

print OUT "Population\tQTL_number\tSNV_number\tSNV_gene_number\tSV_number\tSV_gene_number\n";
my $qtl_p_number = `$rg -oNP "^P" $qtl | wc -l`;
my $p_snv_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/P*.snv | wc -l`;
my $p_snv_gene_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/P*.snv | uniq | wc -l`;
my $p_sv_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/P*.sv | wc -l`;
my $p_sv_gene_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/P*.sv | uniq | wc -l`;
chomp($qtl_p_number, $p_snv_number, $p_snv_gene_number, $p_sv_number, $p_sv_gene_number);
print OUT "P\t$qtl_p_number\t$p_snv_number\t$p_snv_gene_number\t$p_sv_number\t$p_sv_gene_number\n";

my $qtl_m_number = `$rg -oNP "^M" $qtl | wc -l`;
my $m_snv_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/M*.snv | wc -l`;
my $m_snv_gene_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/M*.snv | uniq | wc -l`;
my $m_sv_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/M*.sv | wc -l`;
my $m_sv_gene_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/M*.sv | uniq | wc -l`;
chomp($qtl_m_number, $m_snv_number, $m_snv_gene_number, $m_sv_number, $m_sv_gene_number);
print OUT "M\t$qtl_m_number\t$m_snv_number\t$m_snv_gene_number\t$m_sv_number\t$m_sv_gene_number\n";

my $qtl_h_number = `$rg -oNP "^H" $qtl | wc -l`;
my $h_snv_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/H*.snv | wc -l`;
my $h_snv_gene_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/H*.snv | uniq | wc -l`;
my $h_sv_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/H*.sv | wc -l`;
my $h_sv_gene_number = `$rg -oNP "MD\\d\\dG\\d*" $vcfdir/H*.sv | uniq | wc -l`;
chomp($qtl_h_number, $h_snv_number, $h_snv_gene_number, $h_sv_number, $h_sv_gene_number);
print OUT "H\t$qtl_h_number\t$h_snv_number\t$h_snv_gene_number\t$h_sv_number\t$h_sv_gene_number\n\n";

print OUT "Chrom\tQTL_number\tSNV_number\tSNV_gene_number\tSV_number\tSV_gene_number\n";
for my $chr ("00".."17") {
    my $qtl_chr_number = `$rg -oNP "Chr${chr}" $qtl | wc -l`;
    my $chr_snv_number = `$rg -oNP "MD${chr}G\\d*" $snv | wc -l`;
    my $chr_snv_gene_number = `$rg -oNP "MD${chr}G\\d*" $snv | uniq | wc -l`;
    my $chr_sv_number = `$rg -oNP "MD${chr}G\\d*" $sv | wc -l`;
    my $chr_sv_gene_number = `$rg -oNP "MD${chr}G\\d*" $sv | uniq | wc -l`;
    chomp($qtl_chr_number, $chr_snv_number, $chr_snv_gene_number, $chr_sv_number, $chr_sv_gene_number);
    print OUT "Chr$chr\t$qtl_chr_number\t$chr_snv_number\t$chr_snv_gene_number\t$chr_sv_number\t$chr_sv_gene_number\n";
}

print OUT "\nSNV VCF Format:";
my $snv_info = "
##fileformat=VCFv4.2
##FILTER=<ID=PASS,Description=\"All filters passed\">
##ALT=<ID=*,Description=\"Represents allele(s) other than observed.\">
##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">
##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"List of Phred-scaled genotype likelihoods\">
##INFO=<ID=INDEL,Number=0,Type=Flag,Description=\"Indicates that the variant is an INDEL.\">
##INFO=<ID=IDV,Number=1,Type=Integer,Description=\"Maximum number of reads supporting an indel\">
##INFO=<ID=IMF,Number=1,Type=Float,Description=\"Maximum fraction of reads supporting an indel\">
##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Raw read depth\">
##INFO=<ID=VDB,Number=1,Type=Float,Description=\"Variant Distance Bias for filtering splice-site artefacts in RNA-seq data (bigger is better)\",Version=\"3\">
##INFO=<ID=RPB,Number=1,Type=Float,Description=\"Mann-Whitney U test of Read Position Bias (bigger is better)\">
##INFO=<ID=MQB,Number=1,Type=Float,Description=\"Mann-Whitney U test of Mapping Quality Bias (bigger is better)\">
##INFO=<ID=BQB,Number=1,Type=Float,Description=\"Mann-Whitney U test of Base Quality Bias (bigger is better)\">
##INFO=<ID=MQSB,Number=1,Type=Float,Description=\"Mann-Whitney U test of Mapping Quality vs Strand Bias (bigger is better)\">
##INFO=<ID=SGB,Number=1,Type=Float,Description=\"Segregation based metric.\">
##INFO=<ID=MQ0F,Number=1,Type=Float,Description=\"Fraction of MQ0 reads (smaller is better)\">
##INFO=<ID=ICB,Number=1,Type=Float,Description=\"Inbreeding Coefficient Binomial test (bigger is better)\">
##INFO=<ID=HOB,Number=1,Type=Float,Description=\"Bias in the number of HOMs number (smaller is better)\">
##INFO=<ID=AC,Number=A,Type=Integer,Description=\"Allele count in genotypes for each ALT allele, in the same order as listed\">
##INFO=<ID=AN,Number=1,Type=Integer,Description=\"Total number of alleles in called genotypes\">
##INFO=<ID=DP4,Number=4,Type=Integer,Description=\"Number of high-quality ref-forward , ref-reverse, alt-forward and alt-reverse bases\">
##INFO=<ID=MQ,Number=1,Type=Integer,Description=\"Average mapping quality\">
";
if (defined($prepar)) {if (-e $prepar."/snv_header") {my $snv_info = `cat $prepar/snv_header`;}}
print OUT "$snv_info\n";

print OUT "SV VCF Format:";
my $sv_info = "
##fileformat=VCFv4.2
##ALT=<ID=DEL,Description=\"Deletion\">
##ALT=<ID=DUP,Description=\"Duplication\">
##ALT=<ID=INV,Description=\"Inversion\">
##ALT=<ID=BND,Description=\"Translocation\">
##ALT=<ID=INS,Description=\"Insertion\">
##FILTER=<ID=PASS,Description=\"All filters passed\">
##FILTER=<ID=LowQual,Description=\"Poor quality and insufficient number of PEs and SRs.\">
##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">
##FORMAT=<ID=GL,Number=G,Type=Float,Description=\"Log10-scaled genotype likelihoods for RR,RA,AA genotypes\">
##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">
##FORMAT=<ID=FT,Number=1,Type=String,Description=\"Per-sample genotype filter\">
##FORMAT=<ID=RC,Number=1,Type=Integer,Description=\"Raw high-quality read counts or base counts for the SV\">
##FORMAT=<ID=RCL,Number=1,Type=Integer,Description=\"Raw high-quality read counts or base counts for the left control region\">
##FORMAT=<ID=RCR,Number=1,Type=Integer,Description=\"Raw high-quality read counts or base counts for the right control region\">
##FORMAT=<ID=RDCN,Number=1,Type=Integer,Description=\"Read-depth based copy-number estimate for autosomal sites\">
##FORMAT=<ID=DR,Number=1,Type=Integer,Description=\"# high-quality reference pairs\">
##FORMAT=<ID=DV,Number=1,Type=Integer,Description=\"# high-quality variant pairs\">
##FORMAT=<ID=RR,Number=1,Type=Integer,Description=\"# high-quality reference junction reads\">
##FORMAT=<ID=RV,Number=1,Type=Integer,Description=\"# high-quality variant junction reads\">
##INFO=<ID=CIEND,Number=2,Type=Integer,Description=\"PE confidence interval around END\">
##INFO=<ID=CIPOS,Number=2,Type=Integer,Description=\"PE confidence interval around POS\">
##INFO=<ID=CHR2,Number=1,Type=String,Description=\"Chromosome for POS2 coordinate in case of an inter-chromosomal translocation\">
##INFO=<ID=POS2,Number=1,Type=Integer,Description=\"Genomic position for CHR2 in case of an inter-chromosomal translocation\">
##INFO=<ID=END,Number=1,Type=Integer,Description=\"End position of the structural variant\">
##INFO=<ID=PE,Number=1,Type=Integer,Description=\"Paired-end support of the structural variant\">
##INFO=<ID=MAPQ,Number=1,Type=Integer,Description=\"Median mapping quality of paired-ends\">
##INFO=<ID=SRMAPQ,Number=1,Type=Integer,Description=\"Median mapping quality of split-reads\">
##INFO=<ID=SR,Number=1,Type=Integer,Description=\"Split-read support\">
##INFO=<ID=SRQ,Number=1,Type=Float,Description=\"Split-read consensus alignment quality\">
##INFO=<ID=CONSENSUS,Number=1,Type=String,Description=\"Split-read consensus sequence\">
##INFO=<ID=CE,Number=1,Type=Float,Description=\"Consensus sequence entropy\">
##INFO=<ID=CT,Number=1,Type=String,Description=\"Paired-end signature induced connection type\">
##INFO=<ID=SVLEN,Number=1,Type=Integer,Description=\"Insertion length for SVTYPE=INS.\">
##INFO=<ID=IMPRECISE,Number=0,Type=Flag,Description=\"Imprecise structural variation\">
##INFO=<ID=PRECISE,Number=0,Type=Flag,Description=\"Precise structural variation\">
##INFO=<ID=SVTYPE,Number=1,Type=String,Description=\"Type of structural variant\">
##INFO=<ID=SVMETHOD,Number=1,Type=String,Description=\"Type of approach used to detect SV\">
##INFO=<ID=INSLEN,Number=1,Type=Integer,Description=\"Predicted length of the insertion\">
##INFO=<ID=HOMLEN,Number=1,Type=Integer,Description=\"Predicted microhomology length using a max. edit distance of 2\">
";
if (defined($prepar)) {if (-e $prepar."/indel_header") {my $sv_info = `cat $prepar/indel_header`;}}
print OUT "$sv_info\n";

close OUT;
