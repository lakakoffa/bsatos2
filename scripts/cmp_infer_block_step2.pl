#!/usr/bin/env perl

=head1 NAME
perl cmp_infer_block_step2.pl
=head1 Description
compare and infer phase blocks from progenies to parents
=head1 Usage
perl cmp_infer_block_step2.pl <merged file>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, @a, @b, @z, @x, %hash) = (undef, (), (), (), (), ());
GetOptions("help" => \$h);
die `pod2text $0` if ($h);

### second step infer missing

while(<>){
    chomp;
    if($_ =~ /#/) {next;}
    push @a, $_;

    if($#a == 1){
        my %pb = ();
        my ($p_s1, $p_s2, $m_s1, $m_s2, $h_s1, $h_s2, $l_s1, $l_s2) = ("", "", "", "", "", "", "", "");

        @z = split /\t/, $a[0];
        @x = split /\t/, $a[1];

        foreach my $index (@a){
            @b = split /\t/, $index;
            $hash{$b[0]}{$b[1]} = [@b];
            $p_s1 .= $b[4];
            $p_s2 .= $b[5];
            $m_s1 .= $b[7];
            $m_s2 .= $b[8];
            $h_s1 .= $b[10];
            $h_s2 .= $b[11];
            $l_s1 .= $b[13];
            $l_s2 .= $b[14];
        }

        if(($z[6] eq $x[6]) && ($z[6] ne "-") && ($z[9] eq $x[9]) && ($z[9] ne "-")){
            if(($p_s1 ne $m_s1) && ($p_s1 ne $m_s2)){
                shift @a;
                next;
            }
        }

        if(($z[6] eq $x[6]) && ($z[6] ne "-") && ($z[9] eq $x[9]) && ($z[9] ne "-")){
            if(($z[10] eq $z[4]) && (($x[12] eq "-") || ($x[12] ne $z[12]))){
                $x[10] = $x[4];
                $x[11] = $x[5];
                $x[12] = $z[6];
            }
            if(($z[10] eq $z[5]) && (($x[12] eq "-") || ($x[12] ne $z[12]))){
                $x[10] = $x[5];
                $x[11] = $x[4];
                $x[12] = $z[6];
            }
            if(($z[13] eq $z[4]) && (($x[15] eq "-") || ($x[15] ne $z[15]))){
                $x[13] = $x[4];
                $x[14] = $x[5];
                $x[15] = $z[6];
            }
            if(($z[13] eq $z[5]) && (($x[15] eq "-") || ($x[15] ne $z[15]))){
                $x[13] = $x[5];
                $x[14] = $x[4];
                $x[15] = $z[6];
            }
            $hash{$x[0]}{$x[1]} = [@x];
        }
        my $op = join("\t", @x);
        $a[1] = $op;
        shift @a;
    }
}

my @chrs = keys %hash;
foreach my $chr (@chrs){
    my @pos = sort {$a <=> $b} keys %{$hash{$chr}};
    foreach my $i (@pos){
        my $out = join("\t", @{$hash{$chr}{$i}});
        print "$out\n";
    }
}
