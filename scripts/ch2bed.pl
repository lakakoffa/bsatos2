#!/usr/bin/env perl

=head1 NAME
ch2bed.pl
=head1 Description
change haplotype block file to BED file
=head1 Usage
perl ch2bed.pl haplotype.block
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($q, $g, $d, $h, $o);
GetOptions("help" => \$h);
die `pod2text $0` if ($h);

while(<>){
    chomp;
    my @a = split /\t/, $_;
    if ($_ =~ /#/) {next;}
    my $s = $a[1] - 1;
    print "$a[0]\t$s\t" . join("\t", @a[1..15]) . "\n";
}
