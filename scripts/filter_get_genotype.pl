#!/usr/bin/env perl

=head1 NAME
filter_get_genotype.pl
=head1 Description
filter SNV based on reads depth and phred-scaled quality
=head1 Usage
perl filter_get_genotype.pl --d <depth> --q <quality> --o <prefix> <SNP VCF file>
--d: the theshold of coverage depth [10]
--q: the log-phred SNP/Indel quality [10]
--o: <out prefix> [P_M]
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($indel_ref, $snp_ref, $indel_alt, $snp_alt, $snv_m, $snv_p, $snv_pm) = (0, 0, 0, 0, 0, 0, 0);
my ($q, $d, $h, $o);
GetOptions("d=i" => \$d, "q=i" => \$q, "o=s" => \$o, "help" => \$h);
die `pod2text $0` if ($h);

unless(defined($d)) {$d = 10;}
unless(defined($q)) {$q = 10;}
unless(defined($o)) {$o = "P_M";}

my $op = $o.".p";
my $om = $o.".m";
my $opm = $o.".pm";

open LOP, ">$op";
open LOM, ">$om";
open LOPM, ">$opm";

print LOP "#CHROM\tPOS\tREF\tALT\n";
print LOM "#CHROM\tPOS\tREF\tALT\n";
print LOPM "#CHROM\tPOS\tREF\tALT\n";

while(<>){
    # chomp;
    if ($_ !~ /#/) {
        my @a = split /\t/, $_;
        # CHROM   POS   ID   REF   ALT   QUAL   FILTER   INFO   FORMAT   Golden.bam   Jonathan.bam   Chr11   980   .   G   A   221   .
        # DP=101;VDB=0.811203;SGB=20.6245;ICB=0.3;HOB=0.125;AC=1;AN=4;DP4=32,31,17,15;MQ=55   GT:PL   0/0:0,102,255   0/1:255,0,255
        ($_ =~ /INDEL/) ? $indel_ref++ : $snp_ref++;
        $_ =~ /DP=(\d+);/;
        my $dep = $1;

        if (($a[5] >= $q) && ($dep >= $d)) {
            ($_ =~ /INDEL/) ? $indel_alt++ : $snp_alt++;
            if (($a[9] =~ /0\/1/) && ($a[10] =~ /0\/0/)) {
                $snv_p++;
                print LOP "$a[0]\t$a[1]\t$a[3]\t$a[4]\n";
            }
            if (($a[9] =~ /0\/0/) && ($a[10] =~ /0\/1/)) {
                $snv_m++;
                print LOM "$a[0]\t$a[1]\t$a[3]\t$a[4]\n";
            }
            if (($a[9] =~ /0\/1/) && ($a[10] =~ /0\/1/)) {
                $snv_pm++;
                print LOPM "$a[0]\t$a[1]\t$a[3]\t$a[4]\n";
            }
        }
    }
}
print "\n# SNVs and INDELs summary data:\n";
print "SNPs(before filtering):\t$snp_ref\n";
print "INDELs(before filtering):\t$indel_ref\n";
print "SNPs(after filtering):\t$snp_alt\n";
print "INDELs(after filtering):\t$indel_alt\n";
print "P type SNVs:\t$snv_p\n";
print "M type SNVs:\t$snv_m\n";
print "PM type SNVs:\t$snv_pm\n";

close LOP;
close LOM;
close LOPM;
