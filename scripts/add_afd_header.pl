#!/usr/bin/env perl

=head1 NAME
perl add_afd_header.pl
=head1 Description
add header to the AFD file
=head1 Usage
perl add_afd_header.pl <merged block file> <VCF file>
=cut

use strict;
use warnings;
use Pod::Usage;

# print "#CHROM\tPOS\tH_REF\tH_ALT\tL_REF\tL_ALT\tH_REF_AF\tH_ALT_AF\tL_REF_AF\tL_ALT_AF\tG_VALUE\tGprimer_1M\tGprimer_750K\tGprimer_500K\tGprimer_250K\tCHROM\tPOS\tREF\tALT\tP_HAP1\tP_HAP2\tP_NAME\tM_HAP1\tM_HAP2\tM_NAME\tH_HAP1\tH_HAP2\tH_NAME\tL_HAP1\tL_HAP2\tL_NAME\n";
print "#CHROM\tPOS\tH_REF\tH_ALT\tL_REF\tL_ALT\tH_REF_AF\tH_ALT_AF\tL_REF_AF\tL_ALT_AF\tG\tG1M\tG750K\tG500K\tG250K\tCHR\tPOSITION\tREF\tALT\tP_HAP1\tP_HAP2\tP_NAME\tM_HAP1\tM_HAP2\tM_NAME\tH_HAP1\tH_HAP2\tH_NAME\tL_HAP1\tL_HAP2\tL_NAME\n";
# while(<>) {print $_;}
