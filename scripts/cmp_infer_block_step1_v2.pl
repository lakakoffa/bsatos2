#!/usr/bin/env perl

=head1 NAME
perl cmp_infer_block_step1_v2.pl
=head1 Description
compare and infer phase blocks from parents to progenies
=head1 Usage
perl cmp_infer_block_step1_v2.pl <merged file>
=cut


use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, @a, @b, %hash) = (undef, (), (), ());
GetOptions("help" => \$h);
die `pod2text $0` if ($h);

### first step infer missing

open MYTMP, "<$ARGV[0]";
while(<MYTMP>){
    chomp;
    if($_ =~ /#/) {next;}
    push @a, $_;

    if($#a == 1){     # 数组a中有两行
        my ($p_s1, $p_s2, $m_s1, $m_s2, $h_s1, $h_s2, $l_s1, $l_s2);

        my @first = split /\t/, $a[0];     # Chr17   34744795   A   T   {0   1   block138490}   {-   -   -}   {0   1   block52951}   {-   -   -}
        my @second = split /\t/, $a[1];

        foreach my $index (@a){
            my @b = split /\t/, $index;
            $hash{$b[0]}{$b[1]} = [@b];     # 匿名数组的引用
            $p_s1 .= $b[4];
            $p_s2 .= $b[5];
            $m_s1 .= $b[7];
            $m_s2 .= $b[8];
            $h_s1 .= $b[10];
            $h_s2 .= $b[11];
            $l_s1 .= $b[13];
            $l_s2 .= $b[14];
        }

        if(($first[12] eq $second[12]) && ($first[12] ne "-") && ($first[15] eq $second[15]) && ($first[15] ne "-")){
            if(($h_s1 ne $l_s1) && ($h_s1 ne $l_s2)){
                shift @a;
                next;
            }
        }

        if(($first[12] eq $second[12]) && ($first[12] ne "-")){
            if(($first[4] eq $first[10]) && (($second[4] eq "-") || ($second[6] ne $first[6]))){
                $second[4] = $second[10];
                $second[5] = $second[11];
                $second[6] = $first[6];
            }
            if(($first[4] eq $first[11]) && (($second[4] eq "-") || ($second[6] ne $first[6]))){
                $second[4] = $second[11];
                $second[5] = $second[10];
                $second[6] = $first[6];
            }
            if(($first[7] eq $first[10]) && (($second[7] eq "-") || ($second[9] ne $first[9]))){
                $second[7] = $second[10];
                $second[8] = $second[11];
                $second[9] = $first[9];
            }
            if(($first[7] eq $first[11]) && (($second[7] eq "-") || ($second[9] ne $first[9]))){
                $second[7] = $second[11];
                $second[8] = $second[10];
                $second[9] = $first[9];
            }
        }

        if(($first[15] eq $second[15]) && ($first[15] ne "-")){
            if(($first[4] eq $first[13]) && (($second[4] eq "-") ||($second[6] ne $first[6]))){
                $second[4] = $second[13];
                $second[5] = $second[14];
                $second[6] = $first[6];
            }
            if(($first[4] eq $first[14]) && (($second[4] eq "-") || ($second[6] ne $first[6]))){
                $second[4] = $second[14];
                $second[5] = $second[13];
                $second[6] = $first[6];
            }
            if(($first[7] eq $first[13]) && (($second[7] eq "-") || ($second[9] ne $first[9]))){
                $second[7] = $second[13];
                $second[8] = $second[14];
                $second[9] = $first[9];
            }
            if(($first[7] eq $first[14]) && (($second[7] eq "-") || ($second[9] ne $first[9]))){
                $second[7] = $second[14];
                $second[8] = $second[13];
                $second[9] = $first[9];
            }
        }

        $hash{$second[0]}{$second[1]} = [@second];     # 匿名数组的引用
        $a[1] = join("\t", @second);
        shift @a;
    }
}
close MYTMP;

my @chrs = keys %hash;
foreach my $chr (@chrs){
    my @pos = sort {$a <=> $b} keys %{$hash{$chr}};
    foreach my $i (@pos){
        print join("\t", @{$hash{$chr}{$i}})."\n";
    }
}
