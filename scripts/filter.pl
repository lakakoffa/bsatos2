#!/usr/bin/env perl

=head1 NAME
perl filter.pl
=head1 Description
filter SNPs based on the reads counts of minor allele
=head1 Usage
perl filter.pl --d <threshold> <STDIN (reads counts file)>
=cut


use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, $d);
GetOptions("d=i" => \$d, "help" => \$h);
die `pod2text $0` if ($h);
unless(defined($d)) {$d = 3;}

print "#CHROM\tPOS\tH_REF\tH_ALT\tL_REF\tL_ALT\n";
while(<>){
    chomp;
    my @a = split /\t/, $_;
    unless(($a[2] <= $d) && ($a[4] <= $d)){
        unless(($a[3] <= $d) && ($a[5] <= $d)){
            print "$_\n";
        }
    }
}
