#!/usr/bin/env perl

=head1 NAME
bsa2igv.pl
=head1 Description
convert afd file to IGV format file
=head1 Usage
perl bsa2igv.pl --w <window size> --i <P/M/PM> <polished afd file>
--h: help information
--w: 1 or 0.75 or 0.5 or 0.25 [1]
--i: maker type <P/M/PM>
=cut


use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, $w, $i) = (undef, undef, undef);
GetOptions("w=i" => \$w, "i=s" => \$i, "h" => \$h);
die `pod2text $0` if ($h || (!defined($i)));
unless(defined($w)) {$w = 1;}

print "CHROM\tSTART\tEND\tFEATURE\t$i\n";
open IN, "<$ARGV[0]";
while(<IN>){
    # chomp;
    if($_ =~ /#/) {next;}
    my @a = split /\t/, $_;   # CHROM  POS  H_REF  H_ALT  L_REF  L_ALT  H_REF_F  H_ALT_F  L_REF_F  L_ALT_F  G  G1M  G750K  G500K  G250K  CHR  POSITION  REF  ALT  P_HAP1  P_HAP2  P_NAME  M_HAP1  M_HAP2  M_NAME  H_HAP1  H_HAP2  H_NAME  L_HAP1  L_HAP2  L_NAME
    my $start = $a[1] - 1;
    if($w == 1) {print "$a[0]\t$start\t$a[1]\t$i\t$a[11]\n";}
    if($w == 0.75) {print "$a[0]\t$start\t$a[1]\t$i\t$a[12]\n";}
    if($w == 0.5) {print "$a[0]\t$start\t$a[1]\t$i\t$a[13]\n";}
    if($w == 0.25) {print "$a[0]\t$start\t$a[1]\t$i\t$a[14]\n";}
}
close IN;
