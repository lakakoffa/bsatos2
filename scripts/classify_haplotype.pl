#!/usr/bin/env perl

=head1 NAME
classify_haplotype.pl
=head1 Description
compare and classify phase blocks from the merged file
=head1 Usage
perl classify_haplotype.pl <haplotype_block> <P.haplotype> <M.haplotype>
=cut


use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, %hash1, %hash2, %hash3, %hash4) = (undef, (), (), (), ());
GetOptions("help" => \$h);
die `pod2text $0` if ($h);

open HAP,"<$ARGV[0]";
while(<HAP>){
    if($_ =~ /#/) {next;}
    chomp;
    my @a = split /\t/,$_;
    unless($a[6] eq "-") {$hash1{$a[6]} = $a[0];}
    unless($a[9] eq "-") {$hash3{$a[9]} = $a[0];}
    unless($a[6] eq "-") {push @{$hash2{$a[6]}},$a[1];}
    unless($a[9] eq "-") {push @{$hash4{$a[9]}},$a[1];}
}
close HAP;

my @k1 = keys %hash1;
my @k2 = keys %hash3;

my $P_hap = $ARGV[1];
my $M_hap = $ARGV[2];

open PHAP,">$P_hap";
open MHAP,">$M_hap";

foreach my $index (sort @k1){
    my @d = sort {$a <=> $b} @{$hash2{$index}};
    if($#d > 5){
        my $start = shift @d;
        my $end = pop @d;
        print PHAP "$hash1{$index}\t$start\t$end\t$index\n";
    }
}

foreach my $index1 (sort @k2){
    my @e = sort {$a <=> $b} @{$hash4{$index1}};
    if($#e > 5){
        my $start1 = shift @e;
        my $end1 = pop @e;
        print MHAP "$hash3{$index1}\t$start1\t$end1\t$index1\n";
    }
}
close PHAP;
close MHAP;
