#!/usr/bin/env perl

=head1 NAME
fetch_qtl_vcf.pl
=head1 Description
fetch genes in the QTL region
=head1 Usage
perl fetch_qtl_vcf.pl <gene bed file> <chromosome> <start> <end>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my $h;
GetOptions("help" => \$h);
die `pod2text $0` if ($h);

open LOD, "<$ARGV[0]";
while(<LOD>){
    my @a = split /\t/, $_;
    if(($a[0] eq $ARGV[1]) && ($a[1] >= $ARGV[2]) && ($a[1] <= $ARGV[3])){
        print $_;
    }
}
close LOD;
