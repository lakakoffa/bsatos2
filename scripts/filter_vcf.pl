#!/usr/bin/env perl

=head1 NAME
filter_vcf.pl
=head1 Description
filter the VCF file
=head1 Usage
perl filter_vcf.pl  --d <depth> --q <quality> <VCF file>
--d: the theshold of coverage depth [10]
--q: the log-phred SNP/InDel quality [10]
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($q, $d, $h);
GetOptions("d=i" => \$d, "q=i" => \$q, "help" => \$h);
die `pod2text $0` if ($h);

unless(defined($d)) {$d = 10;}
unless(defined($q)) {$q = 10;}

while(<>){
    unless($_ =~ /#/){
        my @line = split /\t/, $_;     # CHROM、POS、ID、REF、ALT、QUAL、FILTER、INFO、FORMAT、P.bam、M.bam
        my $qual = $line[5];
        $_ =~ /DP=(\d+);/;
        my $dep = $1;
        if(($qual >= $q) && ($dep >= $d)){
            if(($_ !~ /\.\/\./) && ($_ =~ /0\/1/)){
                print $_;
            }
        }
    }
}
