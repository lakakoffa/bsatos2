#!/usr/bin/env perl

=head1 NAME
filter_sv_based_on_qtl.pl
=head1 Description
filter SVs based on the QTL type
=head1 Usage
perl filter_sv_based_on_qtl.pl --i <QTL type> --d <promoter region> <STDIN>
--i: QTL type
--d: promoter region
<STDIN>: annotated SNVs file from ANNOVAR
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($h, $i, $d, $parent1, $parent2) = (undef, undef, undef, undef, undef);
GetOptions("i=s" => \$i, "d=i" => \$d, "help" => \$h);
die `pod2text $0` if ($h || (!defined($i)));

unless(defined($d)) {$d = 2000;}

print "#Chr\tStart\tEnd\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tOtherinfo\tQual\t-\tChr\tStart\t-\tRef\tAlt\tQual\t-\tInfo\tFormat\tPollen Parent\tMaternal Parent\tChr\tEnd\tRef\tAlt\tP_HAP1\tP_HAP2\tP_NAME\tP_EN\tP_G\tM_HAP1\tM_HAP2\tM_NAME\tM_EN\tM_G\n";

while(<>){
    chomp;
    my @lines = split /\t/, $_;
    # 0-Chr03  1-20440548  2-20441352  3-0  4--  5-upstream  6-gene:MD03G1163900  7-dist=451  8-.  9-.  10-0.5  11-3726  12-.  13-Chr03  14-20440548  15-DEL00042776  16-A  17-<DEL>  18-3726  19-PASS
    # 20-PRECISE;SVTYPE=DEL;SVMETHOD=EMBL.DELLYv1.1.6;END=20441352;PE=46;MAPQ=60;CT=3to5;CIPOS=-9,9;CIEND=-9,9;SRMAPQ=60;INSLEN=0;HOMLEN=8;SR=17;SRQ=0.983957;CONSENSUS=TACAATCTATGAATTTCTGAT;CE=1.85025
    # 21-GT:GL:GQ:FT:RCL:RC:RCR:RDCN:DR:DV:RR:RV  22-0/1:-41.7728,0,-47.272:10000:PASS:19745:19901:18833:1:41:41:15:13  23-0/1:-46.3787,0,-25.8789:10000:PASS:16267:19399:18816:1:5:6:8:13
    if($lines[19] eq "PASS"){
        if($lines[22] =~ /(\d\/\d):/) {$parent1 = $1;}   # 0/1
        if($lines[23] =~ /(\d\/\d):/) {$parent2 = $1;}   # 0/1

        if($i eq "P"){
            if(($parent1 eq "0/1") && ($parent2 eq "0/0" || $parent2 eq "1/1")){
                if($_ =~ /dist=(\d+)\;dist=(\d+)/) {if(($1 > $d) && ($2 > $d)) {next;}}   # upstream distance;downstream distance
                unless(($_ =~ /(\t)synonymous SNV/) || ($_ =~ /intronic/) || ($_ =~ /downstream/)){
                    # print "$lines[0]\t$lines[1]\t$lines[3]\t$lines[4]\t$lines[5]\t$lines[6]\t$lines[7]\t$lines[8]\t$lines[18]\t$lines[22]\t$lines[23]\n";
                    print "$_\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n";
                }
            }
        }

        if($i eq "M"){
            if(($parent2 eq "0/1") && ($parent1 eq "0/0" || $parent1 eq "1/1")){
                if($_ =~ /dist=(\d+)\;dist=(\d+)/) {if(($1 > $d) && ($2 > $d)) {next;}}   # upstream distance;downstream distance
                unless(($_ =~ /(\t)synonymous SNV/) || ($_ =~ /intronic/) || ($_ =~ /downstream/)){
                    # print "$lines[0]\t$lines[1]\t$lines[3]\t$lines[4]\t$lines[5]\t$lines[6]\t$lines[7]\t$lines[8]\t$lines[18]\t$lines[22]\t$lines[23]\n";
                    print "$_\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n";
                }
            }
        }

        if($i eq "H"){
            if(($parent1 eq "0/1") && ($parent2 eq "0/1")){
                if($_ =~ /dist=(\d+)\;dist=(\d+)/) {if(($1 > $d) && ($2 > $d)) {next;}}   # upstream distance;downstream distance
                unless(($_ =~ /(\t)synonymous SNV/) || ($_ =~ /intronic/) || ($_ =~ /downstream/)){
                    # print "$lines[0]\t$lines[1]\t$lines[3]\t$lines[4]\t$lines[5]\t$lines[6]\t$lines[7]\t$lines[8]\t$lines[18]\t$lines[22]\t$lines[23]\n";
                    print "$_\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n";
                }
            }
        }
    }
}
