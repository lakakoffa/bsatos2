#!/usr/bin/env perl

=head1 NAME
get_rds.pl
=head1 Description
assign RDS (relative distance scores) to genes in the QTL region
=head1 Usage
perl get_rds.pl <gene bed file> <chromosome> <start> <end> <peak position> <window size>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my $h;
my $file = $ARGV[0];    # <gene bed file>
my $chr = $ARGV[1];     # <chromosome>
my $start = $ARGV[2];   # <start>
my $end = $ARGV[3];     # <end>
my $peak = $ARGV[4];    # <peak position>
my $w = $ARGV[5];       # <window size>

GetOptions("help" => \$h);
die `pod2text $0` if ($h);

open LOD, "<$file";
print "#CHROM\tSTART\tEND\tGENE\tPEAK\tRDS\n";

while(<LOD>){
    chomp;
    my @a = split /\t/, $_;   # chr start end gene
    if($_ =~ /#/) {next;}
    if(($a[0] eq $chr) && ($a[1] >= $start) && ($a[1] <= $end)){
        my $rds = (abs($a[1] - $peak) / ($w / 2)) * 100;   # RDS = (abs(start_pos - peak_pos) / (window_size / 2)) * 100
        print "$a[0]\t$a[1]\t$a[2]\t$a[3]\t$peak\t$rds\n";
    }
}
