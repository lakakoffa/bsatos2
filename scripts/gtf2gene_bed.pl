#!/usr/bin/env perl

=head1 NAME
gtf2gene_bed.pl
=head1 Description
convert gene GTF file to BED file
=head1 Usage
perl gtf2gene_bed.pl <GTF file>
=cut

use strict;
use warnings;
use Pod::Usage;

my ($gene, %hash, %chr);

while(<>){
    my @line = split /\t/, $_;
    if($_ =~ /gene_id "(\S+)";/){
        $gene = $1;
        $chr{$gene} = $line[0];
    }
    if($line[2] eq "exon"){
        push @{$hash{$gene}}, $line[3];
        push @{$hash{$gene}}, $line[4];
    }
}

my @gene = keys %hash;
foreach my $index (sort @gene){
    my @pos = sort {$a <=> $b} @{$hash{$index}};
    print "$chr{$index}\t$pos[0]\t$pos[$#pos]\t$index\n";
}
