#!/usr/bin/env perl

=head1 NAME
qtl_info.pl
=head1 Description
integrate variant infomation with QTLs
=head1 Usage
perl qtl_info.pl [options]
    --qtl     FILE    input file of QTLs
    --snv     FILE    output SNV file of QTLs
    --sv      FILE    output SV file of QTLs
    --dir     DIR     directory of SNV files
    --help            print this message
=head1 Example
perl qtl_info.pl --qtl QTLs.txt --snv QTL_SNV_genes.txt --sv QTL_SV_genes.txt --dir /home/user/bsa/qtl_pick_dir
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($qtl, $dir, $snv, $sv) = (undef, undef, undef, undef);
GetOptions ("qtl=s" => \$qtl, "dir=s" => \$dir, "snv=s" => \$snv, "sv=s" => \$sv);
die `pod2text $0` if ((!defined($qtl)) or (!defined($snv)) or (!defined($sv)) or (!defined($dir)));

open QTLFILE, "<$qtl" or die $!;
open QTLSNV, ">$snv" or die $!;
open QTLSV, ">$sv" or die $!;

print QTLSNV "#Chr\tQTL_Start\tQTL_End\tPeak_Pos\tPeak_Value\tAccession\tSNV_Start\tSNV_End\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tPos\tRef\tAlt\tQual\tInfo\tFormat\tPollen Parent\tMaternal Parent\n";
print QTLSV "#Chr\tQTL_Start\tQTL_End\tPeak_Pos\tPeak_Value\tAccession\tSV_Start\tSV_End\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tPos\tRef\tAlt\tQual\tInfo\tFormat\tPollen Parent\tMaternal Parent\n";

my @qtllines = <QTLFILE>;
# my $total_len = length($#qtllines - 2);
my @parr = map(/^P/, @qtllines);   # my @parr = grep(/^P/, @qtllines);
my @marr = map(/^M/, @qtllines);   # my @marr = grep(/^M/, @qtllines);
my @harr = map(/^H/, @qtllines);   # my @harr = grep(/^H/, @qtllines);

for my $qtlline (@qtllines) {
    chomp($qtlline);
    if ($qtlline !~ /Accession/) {
        my @qtl_line = split /\t/, $qtlline;   # 0-Origin 1-Chrom 2-QTL_Start 3-QTL_End 4-Peak_Pos 5-Peak 6-Accession
        @qtl_line[5] = sprintf("%.2f", $qtl_line[5]);   # 峰值保留两位小数
        my $qtl_accession = $qtl_line[6];
        my $snv_file = $dir."/".$qtl_accession.".snv";
        my $sv_file = $dir."/".$qtl_accession.".sv";
        my @accession_list = split /\./, $qtl_accession;
        if ($qtlline =~ /^P/) {@qtl_line[6] = $accession_list[0] . "." . sprintf("%0*d", length($#parr + 1), $accession_list[1]);}
        if ($qtlline =~ /^M/) {@qtl_line[6] = $accession_list[0] . "." . sprintf("%0*d", length($#marr + 1), $accession_list[1]);}
        if ($qtlline =~ /^H/) {@qtl_line[6] = $accession_list[0] . "." . sprintf("%0*d", length($#harr + 1), $accession_list[1]);}
        my $newqtlline = join("\t", @qtl_line[2..6]);
        # $newqtlline =~ s/(?=(\d{3})+\b)(?<=\d)/,/g;   # 千分位符格式化

        if(-e $snv_file) {
            open SNVFILE, "<$snv_file" or die $!;
            my @snvlines = <SNVFILE>;
            if($#snvlines > 0) {
                for my $snvline (@snvlines[1..$#snvlines]) {
                    if($snvline =~ /Chr\d*/i) {
                        my @fields = split /\t/, $snvline;
                        # $fields[1] =~ s/(?<=\d)(?=(\d{3})+$)/,/g;   # s/(?=(\d{3})+$)(?<=\d)/,/g     s/(?=(?!\b)(\d{3})+$)/,/g
                        # $fields[2] =~ s/(?<=\d)(?=(\d{3})+$)/,/g;   # s/(?=(\d{3})+$)(?<=\d)/,/g     s/(?=(?!\b)(\d{3})+$)/,/g
                        my $newsnvline = join("\t", @fields[1..9,14,16..18,20..23]);
                        print QTLSNV "$fields[0]\t$newqtlline\t$newsnvline\n";
                    }
                }
            }
        }
        if(-e $sv_file) {
            open SVFILE, "<$sv_file" or die $!;
            my @svlines = <SVFILE>;
            unless($#svlines == 0) {
                for my $svline (@svlines[1..$#svlines]) {
                    if($svline =~ /Chr\d+/i) {
                        my @fields = split /\t/, $svline;
                        # $fields[1] =~ s/(?<=\d)(?=(\d{3})+$)/,/g;   # s/(?=(\d{3})+$)(?<=\d)/,/g     s/(?=(?!\b)(\d{3})+$)/,/g
                        # $fields[2] =~ s/(?<=\d)(?=(\d{3})+$)/,/g;   # s/(?=(\d{3})+$)(?<=\d)/,/g     s/(?=(?!\b)(\d{3})+$)/,/g
                        my $newsvline = join("\t", @fields[1..9,14,16..18,20..23]);
                        print QTLSV "$fields[0]\t$newqtlline\t$newsvline\n";
                    }
                }
            }
        }
    }
}
close QTLFILE;
close QTLSNV;
close QTLSV;
close SNVFILE;
close SVFILE;
