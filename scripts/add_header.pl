#!/usr/bin/env perl

=head1 NAME
perl add_header.pl
=head1 Description
add header to the merged block file
=head1 Usage
perl add_header.pl <merged block file> <VCF file>
=cut

use strict;
use warnings;
use Pod::Usage;

print "\#CHROM\tPOS\tREF\tALT\tP_HAP1\tP_HAP2\tP_NAME\tM_HAP1\tM_HAP2\tM_NAME\tH_HAP1\tH_HAP2\tH_NAME\tL_HAP1\tL_HAP2\tL_NAME\n";
# while(<>) {print $_;}
