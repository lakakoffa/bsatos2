#!/usr/bin/env perl

=head1 NAME
main_peak.pl
=head1 Description
get the main peak from the polished or profile data
=head1 Usage
perl main_peak.pl --th1 <threshold value of G1 data> --th2 <threshold value of G2 data> --th3 <the threshold value of G3 data> --p <the G1 data from polish step> --m <the G2 data from polish step> --pm <the G3 data from polish step> --outprefix <out prefix>
--th1: the threshold value of G1 data
--th2: the threshold value of G2 data
--th3: the threshold value of G3 data
--p: the G1 data from polish step
--m: the G2 data from polish step
--pm: the G3 data from polish step
--outprefix <out prefix>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
use FindBin qw($Bin);

my ($th1, $th2, $th3, $p, $m, $pm, $h);

GetOptions("p=s" => \$p, "m=s" => \$m, "pm=s" => \$pm, "th1=s" => \$th1, "th2=s" => \$th2, "th3=s" => \$th3, "help" => \$h);
die `pod2text $0` if ($h);

my $gn1 = "P";
my $gn2 = "M";
my $gn3 = "H";

&get_peak($p, $th1, "P");
&get_peak($m, $th2, "M");
&get_peak($pm, $th3, "H");


sub get_peak {
    my @para = @_;  my $file = $para[0];  my $thres = $para[1];  my $origin = $para[2];
    my (@arr_g1m, @arr_pos, @chr_pos, @tmp, $count, $cnt, $chr) = ((), (), (), (), 0, 0, undef);
    my $get_max = "$Bin/../R/get_max_v1.R";
    print "#Origin\tChrom\tQTL_Start\tQTL_End\tPeak_Pos\tPeak\tAccession\n";

    open LOD, "<$file";
    while(<LOD>){
        chomp;
        if($_ =~ /#/){next;}
        my @line = split /\t/, $_;
        $chr = $line[0];
        push @arr_g1m, $line[11];
        push @arr_pos, $line[1];
        if($#arr_g1m == 0){next;}
        if($#arr_g1m == 1){
            if($arr_g1m[0] > $thres && $arr_g1m[1] < $thres){push @chr_pos, $arr_pos[0];}
            if($arr_g1m[0] < $thres && $arr_g1m[1] > $thres){push @chr_pos, $arr_pos[1];}
            shift @arr_g1m;
            shift @arr_pos;
        }
    }

    foreach my $index (@chr_pos){
        $count++;
        push @tmp, $index;
        if($count == 2){
            my $value = `Rscript $get_max $file $tmp[0] $tmp[1]`;
            unless($value =~ /NA/){
                my @interval = split /\t/, $value;
                if(($tmp[1] - $tmp[0]) > 250000){
                    $cnt++;
                    my $name = $origin.$cnt;
                    print "$origin\t$chr\t$tmp[0]\t$tmp[1]\t$interval[1]\t$interval[0]\t$name\n";
                }
            }
            $count = 0;
            @tmp = ();
        }
    }
}
