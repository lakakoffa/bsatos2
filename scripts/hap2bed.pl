#!/usr/bin/env perl

=head1 NAME
hap2bed.pl
=head1 Description
convert haplotype file to bed file
=head1 Usage
perl hap2bed.pl <haplotype file>
=cut

use strict;
use warnings;
use Pod::Usage;

while(<>){
    chomp;
    unless($_ =~ /#/){
        my @a = split /\t/, $_;
        my $start = $a[1] - 1;
        # print "$a[0]\t$start\t$a[1]\t$a[2]\t$a[3]\t$a[4]\t$a[5]\t$a[6]\t$a[7]\t$a[8]\t$a[9]\n";
        print "$a[0]\t$start\t" . join("\t", @a[1..9]) ."\n";
    }
}

while(<>){
    unless($_ =~ /#/){
        my @a = split /\t/, $_;
        print "$a[0]\t$a[1]\n";
    }
}
