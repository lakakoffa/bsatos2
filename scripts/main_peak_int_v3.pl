#!/usr/bin/env perl

=head1 NAME
main_peak_v2.pl
=head1 Description
get the main peak from the QTL file
=head1 Usage
perl main_peak_v2.pl --int <minimum interval length of QTL> --qtl <QTL file> --out <output file>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
my ($qtl, $out, $int) = (undef, undef, undef);
GetOptions ("qtl=s" => \$qtl, "out=s" => \$out, "int=i" => \$int);

open QTLFILE, "<$qtl" or die $!;
open OUTFILE, ">$out" or die $!;

for my $qtlline (<QTLFILE>) {
    if ($qtlline =~ /Accession/) {
        print OUTFILE $qtlline;
    } else {
        my @qtl_line = split /\t/, $qtlline;   # 0-Origin 1-Chrom 2-QTL_Start 3-QTL_End 4-Peak_Pos 5-Peak 6-Accession
        my $qtl_start = $qtl_line[2];
        my $qtl_end = $qtl_line[3];
        if (($qtl_end - $qtl_start) >= ($int * 1000)) {
            print OUTFILE $qtlline;
        }
    }
}
close QTLFILE;
close OUTFILE;
