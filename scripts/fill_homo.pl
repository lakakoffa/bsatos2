#!/usr/bin/env perl

=head1 NAME
perl fill_homo.pl
=head1 Description
fill in the homozygous blocks
=head1 Usage
perl fill_homo.pl <merged block file> <VCF file>
=cut

use strict;
use warnings;
use Pod::Usage;

my (%hash1, %hash2) = ((), ());
open VCF, "<$ARGV[1]";
while(<VCF>){
    chomp;
    my @a = split /\t/, $_;
    # Chr08   31609153   .   C   T   93   .   DP=39;VDB=0.00784;......;AN=4;DP4=2,24,6,0;MQ=56   GT:PL   0/0:0,27,179   0/1:128,0,255
    unless($_ =~ /#/){
        if($a[9] =~ /0\/0/) {$hash1{$a[0]}{$a[1]} = "0";}
        if($a[9] =~ /1\/1/) {$hash1{$a[0]}{$a[1]} = "1";}
        if($a[10] =~ /0\/0/) {$hash2{$a[0]}{$a[1]} = "0";}
        if($a[10] =~ /1\/1/) {$hash2{$a[0]}{$a[1]} = "1";}
    }
}
close VCF;


open BLOCK, "<$ARGV[0]";
while(<BLOCK>){
    chomp;
    my @b = split /\t/, $_;
    #   0        1       2   3      4   5      6                7    8   9         10   11     12               13  14       15
    # Chr17   34735807   A   G     {0   1   block138486}(P)     {-   -   -}(M)     {1   0   block52948}(H)     {0   1   block60876}(L)
    if(defined($hash1{$b[0]}{$b[1]}) && ($b[4] eq "-")){
        $b[4] = $hash1{$b[0]}{$b[1]};
        $b[5] = $hash1{$b[0]}{$b[1]};
    }
    if(defined($hash2{$b[0]}{$b[1]}) && ($b[7] eq "-")){
        $b[7] = $hash2{$b[0]}{$b[1]};
        $b[8] = $hash2{$b[0]}{$b[1]};
    }
    print join("\t", @b) . "\n";     # Chr17   34744795   A   T   {0   1   block138490}   {-   -   -}   {0   1   block52951}   {-   -   -}
}
close BLOCK;
