#!/usr/bin/env perl

=head1 NAME
merge.pl
=head1 Description
merge SNV, SV, gene, hap, snv_igv_mut, snv_igv_vcf, sv_igv_mut, sv_igv_vcf from all the files
=head1 Usage
perl merge.pl --dir <input directory>
=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
use FindBin qw($Bin);

my $dir = undef;
GetOptions ("dir=s" => \$dir);

my $xlsx = "$dir/QTL_Information.xlsx";
my $qtl = "$dir/QTL";
my $func_anno = "$dir/Functional_Annotation.txt";
my $snv = "$dir/snv_all";
my $sv = "$dir/sv_all";
my $gene = "$dir/gene_all";
my $hap = "$dir/hap_all";
my $snv_igv_mut = "$dir/snv_igv_mut_all";
my $snv_igv_vcf = "$dir/snv_igv_vcf_all";
my $sv_igv_mut = "$dir/sv_igv_mut_all";
my $sv_igv_vcf = "$dir/sv_igv_vcf_all";
# my $txt2excel = "$FindBin::Bin/../Python/txt2excel.py";


##############################################################

print STDOUT "merge gene files\n";
open(WH, ">$gene") or die "$!";
print WH "#CHROM\tSTART\tEND\tGENE\tPEAK\tRDS\n";
foreach (glob "$dir/*gene") {
    open(RH, $_) or die $!;
    my @lines = <RH>;
    for(my $i = 0; $i < @lines; $i++) {
        if ($lines[$i] !~ /^#/) {
            print WH $lines[$i];
        }
    }
}

print STDOUT "merge hap files\n";
open(WH, ">$hap") or die "$!";
print WH "#CHROM\tPOS\tREF\tALT\tP_HAP1\tP_HAP2\tP_NAME\tP_EN\tP_G\tM_HAP1\tM_HAP2\tM_NAME\tM_EN\tM_G\n";
foreach (glob "$dir/*hap") {
    open(RH, $_) or die $!;
    my @lines = <RH>;
    for(my $i = 0; $i < @lines; $i++) {
        if ($lines[$i] !~ /^#/) {
            print WH $lines[$i];
        }
    }
}

print STDOUT "merge snv.igv.mut files\n";
open(WH, ">$snv_igv_mut") or die "$!";
print WH "#chr\tstart\tend\tsample\ttype\n";
foreach (glob "$dir/*snv.igv.mut") {
    open(RH, $_) or die $!;
    my @lines = <RH>;
    for(my $i = 0; $i < @lines; $i++) {
        if ($lines[$i] !~ /^chr/) {
            print WH $lines[$i];
        }
    }
}

print STDOUT "merge snv.igv.vcf files\n";
open(WH, ">$snv_igv_vcf") or die "$!";
print WH "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tP_SNV\tM_SNV\n";
foreach (glob "$dir/*snv.igv.vcf") {
    open(RH, $_) or die $!;
    my @lines = <RH>;
    for(my $i = 0; $i < @lines; $i++) {
        if ($lines[$i] !~ /^#/) {
            print WH $lines[$i];
        }
    }
}

print STDOUT "merge sv files\n";
open(WH, ">$sv") or die "$!";
print WH "#Chr\tStart\tEnd\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tOtherinfo\t\t\t\t\t\t\t\t\t\t\t\t\t\n";
foreach (glob "$dir/*sv") {
    open(RH, $_) or die $!;
    my @lines = <RH>;
    for(my $i = 0; $i < @lines; $i++) {
        if ($lines[$i] !~ /^#/) {
            print WH $lines[$i];
        }
    }
}

print STDOUT "merge snv files\n";
open(WH, ">$snv") or die "$!";
print WH "#Chr\tStart\tEnd\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tOtherinfo\t\t\t\t\t\t\t\t\t\t\t\t\t\tCHROM\tPOS\tREF\tALT\tP_HAP1\tP_HAP2\tP_NAME\tP_EN\tP_G\tM_HAP1\tM_HAP2\tM_NAME\tM_EN\tM_G\n";
foreach (glob "$dir/*snv") {
    open(RH, $_) or die $!;
    my @lines = <RH>;
    for(my $i = 0; $i < @lines; $i++) {
        if ($lines[$i] !~ /^#/) {
            print WH $lines[$i];
        }
    }
}

print STDOUT "merge sv.igv.mut files\n";
open(WH, ">$sv_igv_mut") or die "$!";
print WH "#chr\tstart\tend\tsample\ttype\n";
foreach (glob "$dir/*sv.igv.mut") {
    open(RH, $_) or die $!;
    my @lines = <RH>;
    for(my $i = 0; $i < @lines; $i++) {
        if ($lines[$i] !~ /^chr/) {
            print WH $lines[$i];
        }
    }
}

print STDOUT "merge sv.igv.vcf files\n";
open(WH, ">$sv_igv_vcf") or die "$!";
print WH "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tP_SNV\tM_SNV\n";
foreach (glob "$dir/*sv.igv.vcf") {
    open(RH, $_) or die $!;
    my @lines = <RH>;
    for(my $i = 0; $i < @lines; $i++) {
        if ($lines[$i] !~ /^#/) {
            print WH $lines[$i];
        }
    }
}


# system("python $txt2excel $qtl $xlsx");
# system("python $txt2excel $func_anno $xlsx");
# system("python $txt2excel $gene $xlsx");
# ### system("python $txt2excel $hap $xlsx");
# system("python $txt2excel $snv_igv_mut $xlsx");
# system("python $txt2excel $snv_igv_vcf $xlsx");
# system("python $txt2excel $snv $xlsx");
# system("python $txt2excel $sv $xlsx");
# system("python $txt2excel $sv_igv_mut $xlsx");
# system("python $txt2excel $sv_igv_vcf $xlsx");
