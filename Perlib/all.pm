#!/usr/bin/env perl
package all;
BEGIN {my $VERSION = '2.0';}

use Cwd;
use strict;
use Getopt::Long;
use base 'Exporter';
use File::Basename;
use FindBin qw($Bin);
our @EXPORT = qw(runall);

my $G_USAGE = "
Usage: bsatos2 all [options]

Options:
    --o      [STR]      name prefix of output directory [all]
    --r      [FILE]     reference genome (fasta format) [Malus_domestica.fa]
    --gtf    [FILE]     GTF file of genes (GTF2 format; Not required if GFF file is provided) [Malus_domestica.gtf]
    --gff    [FILE]     GFF file of genes (GFF3 format; Not required if GTF file is provided) [Malus_domestica.gff3]
    --pf1    [FILE]     paired-end1 fastq file of pollen parent    |    | --pb  [FILE]  BAM file (.srt.rm.bam) of pollen parent
    --pf2    [FILE]     paired-end2 fastq file of pollen parent    |    |
    --mf1    [FILE]     paired-end1 fastq file of maternal parent  |    | --mb  [FILE]  BAM file (.srt.rm.bam) of maternal parent
    --mf2    [FILE]     paired-end2 fastq file of maternal parent  | OR |
    --hf1    [FILE]     paired-end1 fastq file of high pool reads  |    | --hb  [FILE]  BAM file (.srt.rm.bam) of high pool
    --hf2    [FILE]     paired-end2 fastq file of high pool reads  |    |
    --lf1    [FILE]     paired-end1 fastq file of low pool reads   |    | --lb  [FILE]  BAM file (.srt.rm.bam) of low pool
    --lf2    [FILE]     paired-end2 fastq file of low pool reads   |    |
    --log    [FILE]     all module log file [all.log]
    --db     [STR]      database directory of .fa & GTF & GFF files [Malus_domestica]

1) prepar step options:
    Aligment Options:
        --t   [INT]     number of threads [1]
    SNV Calling Options:
        --aq  [INT]     skip alignments with mapQ smaller than INT [10]
    SV Calling Options:
        --vq  [INT]     skip alignments with mapQ smaller than INT [10]
    SNV filtering Options:
        --d   [INT]     skip SNVs with reads depth smaller than INT [10]
        --sq  [INT]     skip SNVs with phred-scaled quality smaller than INT [10]

2) prep step options:
    Aligment Options:
        --t2  [INT]     number of threads [1]
    SNV Calling Options:
        --aq2 [INT]     skip alignments with mapQ smaller than INT [10]
    SNV filtering Options:
        --d2  [INT]     average sequencing coverage [20]
        --sq2 [INT]     skip SNVs with phred-scaled quality smaller than INT [10]
        --pn  [INT]     reads counts of minor allele should greater than INT [3]

3) haplotype step options:
    --hapcut2 [STR]     use samtools algorithm or HAPCUT2 algorithm to assembly haplotype [default: NO for HAPCUT2; YES for samtools]
    SNP genotyping Options:
        --t3  [INT]     number of threads [1]
        --dep [INT]     skip SNPs with read depth smaller than INT [10]
        --aq3 [INT]     skip alignment with mapQ smaller than INT [10]
        --vq2 [INT]     skip SNPs with phred-scaled quality smaller than INT [10]

4) afd step options:
    Statistics Options:
        --sd  [STR]     statistic method: ed/g/abs [g]
        --w   [INT]     sliding window size [1000000]
        --fn  [INT]     batches for smoothing; the smaller the faster, but more memory needed [20]

5) polish step options:
    Statistics Options:
        --sd2 [STR]     statistic method: ed/g/abs [g]
        --w2  [INT]     sliding window size [1000000]
        --fn2 [INT]     batches for smoothing; the smaller the faster, but more memory needed [20]

6) qtl_pick options:
    --q  [INT]          mininum phred-scaled quality score [10]
    --pr [INT]          promoter region [2000]

Example:
    1) Use paired-end fastq files to run BSATOS2
        bsatos2 all --o result --r Malus_domestica.fa --gtf Malus_domestica.gtf --pf1 P_1.fastq --pf2 P_2.fastq --mf1 M_1.fastq --mf2 M_2.fastq --hf1 H_1.fastq --hf2 H_2.fastq --lf1 L_1.fastq --lf2 L_2.fastq
    2) Use pre-aligned BAM files to run BSATOS2
        bsatos2 all --o result --r Malus_domestica.fa --gtf Malus_domestica.gtf --pb P.bam --mb M.bam --hb H.bam --lb L.bam
";


sub runall {
    my $genome = undef;     my $gtf = undef;        my $pf1 = undef;        my $pf2 = undef;
    my $mf1 = undef;        my $mf2 = undef;        my $hf1 = undef;        my $hf2 = undef;
    my $lf1 = undef;        my $lf2 = undef;        my $pb  = undef;        my $fn  = undef;
    my $mb  = undef;        my $hb  = undef;        my $lb  = undef;        my $t   = undef;
    my $aq  = undef;        my $vq  = undef;        my $d   = undef;        my $sq  = undef;
    my $t2  = undef;        my $aq2 = undef;        my $d2 = undef;         my $sq2 = undef;
    my $pn  = undef;        my $hapcut2 = undef;    my $dep = undef;        my $aq3 = undef;
    my $vq2 = undef;        my $sd  = undef;        my $w   = undef;        my $th  = undef;
    my $m   = undef;        my $q   = undef;        my $pr  = undef;        my $m2 = undef;
    my $sd2 = undef;        my $w2  = undef;        my $fn2 = undef;        my $th2 = undef;
    my $gff = undef;        my $t3 = undef;         my $db  = undef;        my $o = undef;
    my $log = undef;        my $help = undef;

    GetOptions (
        "r=s" => \$genome,   "gtf=s" => \$gtf,        "pf1=s" => \$pf1,        "pf2=s" => \$pf2,
        "mf1=s" => \$mf1,    "mf2=s" => \$mf2,        "hf1=s" => \$hf1,        "hf2=s" => \$hf2,
        "lf1=s" => \$lf1,    "fn=i" => \$fn,          "lf2=s" => \$lf2,        "pb=s" => \$pb,
        "mb=s" => \$mb,      "hb=s" => \$hb,          "lb=s" => \$lb,          "hapcut2=s" => \$hapcut2,
        "aq=i" => \$aq,      "vq=i" => \$vq,          "d=i" => \$d,            "sq=i" => \$sq,
        "t2=i" => \$t2,      "aq2=i" => \$aq2,        "d2=i" => \$d2,          "sq2=i" => \$sq2,
        "pn=i" => \$pn,      "t=i" => \$t,            "dep=i" => \$dep,        "aq3=i" => \$aq3,
        "vq2=i" => \$vq2,    "sd=s" => \$sd,          "w=i" => \$w,            "th=s" => \$th,
        "m=s" => \$m,        "q=i" => \$q,            "pr=i" => \$pr,          "db=s" => \$db,
        "log=s" => \$log,    "w2=i" => \$w2,          "sd2=s" => \$sd2,        "t3=i" => \$t3,
        "fn2=i" => \$fn2,    "th2=s" => \$th2,        "m2=s" => \$m2,          "o=s" => \$o,
        "gff=s" => \$gff,    "help=s" => \$help)
    or die("$G_USAGE");
    die "$G_USAGE" if ($help || !defined($genome));

##################################################################

    my $para_all_file = "$FindBin::Bin/Python/src/para_all_file";
    if (-e $para_all_file){
        open PARAALLFILE, "$para_all_file" or die $!;
        while(<PARAALLFILE>){
            chomp;
            my @para = split /\t/, $_;
            if (@para[0] =~ /^o\b/ && @para[1] =~ /\w/) {$o = @para[1];}
            if (@para[0] =~ /^r\b/ && @para[1] =~ /\w/) {$genome = @para[1];}
            if (@para[0] =~ /^gtf\b/ && @para[1] =~ /\w/) {$gtf = @para[1];}
            if (@para[0] =~ /^gff\b/ && @para[1] =~ /\w/) {$gff = @para[1];}
            if (@para[0] =~ /^pf1\b/ && @para[1] =~ /\w/) {$pf1 = @para[1];}
            if (@para[0] =~ /^pf2\b/ && @para[1] =~ /\w/) {$pf2 = @para[1];}
            if (@para[0] =~ /^mf1\b/ && @para[1] =~ /\w/) {$mf1 = @para[1];}
            if (@para[0] =~ /^mf2\b/ && @para[1] =~ /\w/) {$mf2 = @para[1];}
            if (@para[0] =~ /^hf1\b/ && @para[1] =~ /\w/) {$hf1 = @para[1];}
            if (@para[0] =~ /^hf2\b/ && @para[1] =~ /\w/) {$hf2 = @para[1];}
            if (@para[0] =~ /^lf1\b/ && @para[1] =~ /\w/) {$lf1 = @para[1];}
            if (@para[0] =~ /^lf2\b/ && @para[1] =~ /\w/) {$lf2 = @para[1];}
            if (@para[0] =~ /^pb\b/ && @para[1] =~ /\w/) {$pb = @para[1];}
            if (@para[0] =~ /^mb\b/ && @para[1] =~ /\w/) {$mb = @para[1];}
            if (@para[0] =~ /^hb\b/ && @para[1] =~ /\w/) {$hb = @para[1];}
            if (@para[0] =~ /^lb\b/ && @para[1] =~ /\w/) {$lb = @para[1];}
            if (@para[0] =~ /^db\b/ && @para[1] =~ /\w/) {$db = @para[1];}
        }
        close PARAALLFILE;
    }

    unless(defined($t)) {$t = 1;}
    unless(defined($t2)) {$t2 = 1;}
    unless(defined($t3)) {$t3 = 1;}
    unless(defined($aq)) {$aq = 10;}
    unless(defined($aq2)) {$aq2 = 10;}
    unless(defined($aq3)) {$aq3 = 10;}
    unless(defined($vq)) {$vq = 10;}
    unless(defined($vq2)) {$vq2 = 10;}
    unless(defined($sd)) {$sd = "g";}
    unless(defined($sd2)) {$sd2 = "g";}
    unless(defined($sq)) {$sq = 10;}
    unless(defined($sq2)) {$sq2 = 10;}
    unless(defined($w)) {$w = 1000000;}
    unless(defined($w2)) {$w2 = 1000000;}
    unless(defined($th)) {$th = "N";}
    unless(defined($th2)) {$th2 = "N";}
    unless(defined($m)) {$m = "N";}
    unless(defined($m2)) {$m2 = "N";}
    unless(defined($fn)) {$fn = 20;}
    unless(defined($fn2)) {$fn2 = 20;}
    unless(defined($d)) {$d = 10;}
    unless(defined($d2)) {$d2 = 10;}
    unless(defined($pn)) {$pn = 3;}
    unless(defined($hapcut2)) {$hapcut2 = "NO";}
    unless(defined($dep)) {$dep = 10;}
    unless(defined($q)) {$q = 10;}
    unless(defined($pr)) {$pr = 2000;}
    unless(defined($o)) {$o = "all";}
    unless(defined($db)) {$db = "Malus_domestica";}
    unless(defined($gtf)) {$gtf = "$FindBin::Bin/database/$db/$db.gtf";}                                    # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf
    unless(defined($genome)) {
        if (-e "$FindBin::Bin/database/$db/$db.fa"){$genome = "$FindBin::Bin/database/$db/$db.fa";}         # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa
        if (-e "$FindBin::Bin/database/$db/$db.fasta"){$genome = "$FindBin::Bin/database/$db/$db.fasta";}   # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fasta
    }
    unless(defined($gff)) {
        if (-e "$FindBin::Bin/database/$db/$db.gff"){$gff = "$FindBin::Bin/database/$db/$db.gff";}          # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff
        if (-e "$FindBin::Bin/database/$db/$db.gff3"){$gff = "$FindBin::Bin/database/$db/$db.gff3";}        # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff3
    }


    my $bsatos2 = "$FindBin::Bin/bsatos2";            # /home/user/BSATOS2/bsatos2
    my $work_dir = getcwd;                            # /home/user/bsa
    my $dir = $work_dir."/".$o."_dir";                # /home/user/bsa/all_dir
    my $prepar_dir = $work_dir."/prepar_dir";         # /home/user/bsa/prepar_dir
    my $prep_dir = $work_dir."/prep_dir";             # /home/user/bsa/prep_dir
    my $hap_dir = $work_dir."/haplotype_dir";         # /home/user/bsa/haplotype_dir
    my $afd_dir = $work_dir."/afd_dir";               # /home/user/bsa/afd_dir
    my $po_dir = $work_dir."/polish_dir";             # /home/user/bsa/polish_dir
    my $qtl_dir = $work_dir."/qtl_pick_dir";          # /home/user/bsa/qtl_pick_dir

    unless(defined($log)) {$log = $work_dir."/".$o.".log";}

##################################################################

    my %hlog = ();
    if(-e $log){
        open LOD, "<$log";
        my $count = 0;
        while(<LOD>){
            chomp;
            my @sp = split /\t/, $_;
            if($sp[2] eq "\#done") {$hlog{$sp[0]} = 1;}
            if($sp[2] eq "\#die") {$hlog{$sp[0]} = 0;}
            if($_ =~ /finish/) {$count++;}
        }
        if($count != 0){
            print "\n
                Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
                Version: 2.0
                BSATOS2 has been finished!!!!!\n\n
                 ▄▄▄▄▄▄▄▄▄▄   ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄
                ▐░░░░░░░░░░▌ ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
                ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀█░▌
                ▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌     ▐░▌     ▐░▌       ▐░▌▐░▌                    ▐░▌
                ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄           ▐░▌
                ▐░░░░░░░░░░▌ ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░▌       ▐░▌▐░░░░░░░░░░░▌ ▄▄▄▄▄▄▄▄▄█░▌
                ▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌     ▐░▌     ▐░▌       ▐░▌ ▀▀▀▀▀▀▀▀▀█░▌▐░░░░░░░░░░░▌
                ▐░▌       ▐░▌          ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌       ▐░▌          ▐░▌▐░█▀▀▀▀▀▀▀▀▀
                ▐░█▄▄▄▄▄▄▄█░▌ ▄▄▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌ ▄▄▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄
                ▐░░░░░░░░░░▌ ▐░░░░░░░░░░░▌▐░▌       ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
                 ▀▀▀▀▀▀▀▀▀▀   ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀\n\n";
            exit(0);
        }
    }
    open (my $lo, ">>$log") or die $!;

    print STDOUT "\n
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        all      run prepar, prep, haplotype, afd, polish, qtl_pick, igv in turn\n\n
        ██████╗ ███████╗ █████╗ ████████╗ ██████╗ ███████╗██████╗
        ██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██╔═══██╗██╔════╝╚════██╗
        ██████╔╝███████╗███████║   ██║   ██║   ██║███████╗ █████╔╝
        ██╔══██╗╚════██║██╔══██║   ██║   ██║   ██║╚════██║██╔═══╝
        ██████╔╝███████║██║  ██║   ██║   ╚██████╔╝███████║███████╗
        ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝╚══════╝\n\n";

    print $lo "\n
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        all      run prepar, prep, haplotype, afd, polish, qtl_pick, igv in turn\n\n";


    unless(-e $dir){
        print STDOUT "create all result dir\n";
        print $lo "create all result dir\nCMD1:\t";
        unless(defined($hlog{"CMD0:"})) {$hlog{"CMD0:"} = 0;}
        my $mx1c = $hlog{"CMD0:"};
        my $cd1c = "mkdir $dir";        # mkdir /home/user/bsa/all_dir
        &process_cmd($cd1c, $lo, $mx1c);
    }

##################################################################

    print STDOUT "\nThe first module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        prepar   prepare the parents data\n\n";

    print $lo "\nThe first module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        prepar   prepare the parents data\n\n";

    print STDOUT "stage1:\t";
    print $lo "stage1:\t";

    if(defined($pf1) && defined($pf2) && defined($mf1) && defined($mf2)){
        if(defined($gtf)){
            my $stage1 = "$bsatos2 prepar --r $genome --gtf $gtf --t $t --aq $aq --vq $vq --d $d --sq $sq --pf1 $pf1 --pf2 $pf2 --mf1 $mf1 --mf2 $mf2 --db $db --o prepar";
            unless(defined($hlog{"stage1"})) {$hlog{"stage1"} = 0;}
            my $m1 = $hlog{"stage1"};
            &process_cmd($stage1, $lo, $m1);
        }else{
            my $stage1 = "$bsatos2 prepar --r $genome --gff $gff --t $t --aq $aq --vq $vq --d $d --sq $sq --pf1 $pf1 --pf2 $pf2 --mf1 $mf1 --mf2 $mf2 --db $db --o prepar";
            unless(defined($hlog{"stage1"})) {$hlog{"stage1"} = 0;}
            my $m1 = $hlog{"stage1"};
            &process_cmd($stage1, $lo, $m1);
        }
    }else{
        if(defined($gtf)){
            my $stage1 = "$bsatos2 prepar --r $genome --gtf $gtf --t $t --aq $aq --vq $vq --d $d --sq $sq --pb $pb --mb $mb --db $db --o prepar";
            unless(defined($hlog{"stage1"})) {$hlog{"stage1"} = 0;}
            my $m1 = $hlog{"stage1"};
            &process_cmd($stage1, $lo, $m1);
        }else{
            my $stage1 = "$bsatos2 prepar --r $genome --gff $gff --t $t --aq $aq --vq $vq --d $d --sq $sq --pb $pb --mb $mb --db $db --o prepar";
            unless(defined($hlog{"stage1"})) {$hlog{"stage1"} = 0;}
            my $m1 = $hlog{"stage1"};
            &process_cmd($stage1, $lo, $m1);
        }
    }

##################################################################

    print STDOUT "\nThe second module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        prep     prepare the pool data\n\n";

    print $lo "\nThe second module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        prep     prepare the pool data\n\n";

    print STDOUT "stage2:\t";
    print $lo "stage2:\t";

    if(defined($hf1) && defined($hf2) && defined($lf1) && defined($lf2)){
        my $stage2 = "$bsatos2 prep --t2 $t2 --aq2 $aq2 --d2 $d2 --sq2 $sq2 --pn $pn --hf1 $hf1 --hf2 $hf2 --lf1 $lf1 --lf2 $lf2 --r $genome --gp prepar_dir/P_M.m --gm prepar_dir/P_M.m --gpm prepar_dir/P_M.pm --db $db --o prep";
        # /home/user/BSATOS2/bsatos2 prep --t2 1 --aq2 10 --d2 10 --sq2 10 --pn 3 --hf1 H_1.fq --hf2 H_2.fq --lf1 L_1.fq --lf2 L_2.fq --r /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa --gp prepar_dir/P_M.p --gm prepar_dir/P_M.m --gpm prepar_dir/P_M.pm --db Malus_domestica --o prep
        unless(defined($hlog{"stage2"})) {$hlog{"stage2"} = 0;}
        my $m2 = $hlog{"stage2"};
        &process_cmd($stage2, $lo, $m2);
    }else{
        my $stage2 = "$bsatos2 prep --t2 $t2 --aq2 $aq2 --d2 $d2 --sq2 $sq2 --pn $pn --hb $hb --lb $lb --r $genome --gp prepar_dir/P_M.p --gm prepar_dir/P_M.m --gpm prepar_dir/P_M.pm --db $db --o prep";
        # /home/user/BSATOS2/bsatos2 prep --t2 1 --aq2 10 --d2 10 --sq2 10 --pn 3 --hb H.bam --lb L.bam --r /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa --gp prepar_dir/P_M.p --gm prepar_dir/P_M.m --gpm prepar_dir/P_M.pm --db Malus_domestica --o prep
        unless(defined($hlog{"stage2"})) {$hlog{"stage2"} = 0;}
        my $m2 = $hlog{"stage2"};
        &process_cmd($stage2, $lo, $m2);
    }

##################################################################

    print STDOUT "\nThe third module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        haplotype   construct haplotype block\n\n";

    print $lo "\nThe third module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        haplotype   construct haplotype block\n\n";

    print STDOUT "stage3:\t";
    print $lo "stage3:\t";

    unless(defined($pb)) {$pb = "$FindBin::Bin/prepar_dir/prepar_P_rm.bam";}
    unless(defined($mb)) {$mb = "$FindBin::Bin/prepar_dir/prepar_M_rm.bam";}
    unless(defined($hb)) {$hb = "$FindBin::Bin/prep_dir/prep_H_rm.bam";}
    unless(defined($lb)) {$lb = "$FindBin::Bin/prep_dir/prep_L_rm.bam";}
    my $stage3 = "$bsatos2 haplotype --t3 $t3 --hapcut2 $hapcut2 --dep $dep --aq3 $aq3 --vq2 $vq2 --pb $pb --mb $mb --hb $hb --lb $lb --r $genome --var prepar_dir/P_M.snv --db $db --o haplotype";
    # /home/user/BSATOS2/bsatos2 haplotype --t3 1 --hapcut2 NO --dep 10 --aq3 10 --vq2 10 --pb P.bam --mb M.bam --hb H.bam --lb L.bam --r /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa --var prepar_dir/P_M.snv --db Malus_domestica --o haplotype
    unless(defined($hlog{"stage3"})) {$hlog{"stage3"} = 0;}
    my $m3 = $hlog{"stage3"};
    &process_cmd($stage3, $lo, $m3);

##################################################################

    print STDOUT "\nThe fourth module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        afd      calculate and filter allele frequency difference between two extreme pools\n\n";

    print $lo "\nThe fourth module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        afd      calculate and filter allele frequency difference between two extreme pools\n\n";

    print STDOUT "stage4:\t";
    print $lo "stage4:\t";

    my $stage4 = "$bsatos2 afd --sd $sd --w $w --th $th --m $m --pc prep_dir/P.counts --mc prep_dir/M.counts --pmc prep_dir/PM.counts --hap haplotype_dir/haplotype.block --db $db --o afd";
    # /home/user/BSATOS2/bsatos2 afd --sd g --w 1000000 --th N --m N --pc prep_dir/P.counts --mc prep_dir/M.counts --pmc prep_dir/PM.counts --hap haplotype_dir/haplotype.block --db Malus_domestica --o afd
    unless(defined($hlog{"stage4"})) {$hlog{"stage4"} = 0;}
    my $m4 = $hlog{"stage4"};
    &process_cmd($stage4, $lo, $m4);

##################################################################

    print STDOUT "\nThe fifth module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        polish   polish candidate QTLs region and remove nosiy makers based on haplotype information\n\n";

    print $lo "\nThe fifth module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        polish   polish candidate QTLs region and remove nosiy makers based on haplotype information\n\n";

    print STDOUT "stage5:\t";
    print $lo "stage5:\t";

    my $stage5 = "$bsatos2 polish --sd2 $sd2 --w2 $w2 --th2 $th2 --m2 $m2 --p afd_dir/P.AFD --m afd_dir/M.AFD --pm afd_dir/PM.AFD --hap haplotype_dir/haplotype.block --db $db --o polish";
    # /home/user/BSATOS2/bsatos2 polish --sd2 g --w2 1000000 --th2 N --m2 N --p afd_dir/P.AFD --m afd_dir/M.AFD --pm afd_dir/PM.AFD --hap haplotype_dir/haplotype.block --db Malus_domestica --o polish
    unless(defined($hlog{"stage5"})) {$hlog{"stage5"} = 0;}
    my $m5 = $hlog{"stage5"};
    &process_cmd($stage5, $lo, $m5);

##################################################################

    print STDOUT "\nThe sixth module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        qtl_pick  judge and pick up QTLs from three types of peaks\n\n";

    print $lo "\nThe sixth module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        qtl_pick  judge and pick up QTLs from three types of peaks\n\n";

    print STDOUT "stage6:\t";
    print $lo "stage6:\t";

    if(defined($gtf)){
        my $stage6 = "$bsatos2 qtl_pick --gtf $gtf --win $w --q $q --pr $pr --gp polish_dir/P.polished.afd --gm polish_dir/M.polished.afd --gpm polish_dir/PM.polished.afd --snv prepar_dir/anno/snv.AT_multianno.txt --sv prepar_dir/anno/sv.AT_multianno.txt --hap haplotype_dir/haplotype.block --db $db --o qtl_pick";
        # /home/user/BSATOS2/bsatos2 qtl_pick  --gtf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf --win 1000000 --q 10 --pr 2000 --gp polish_dir/P.polished.afd --gm polish_dir/M.polished.afd --gpm polish_dir/PM.polished.afd --snv prepar_dir/anno/snv.AT_multianno.txt --sv prepar_dir/anno/sv.AT_multianno.txt --hap haplotype_dir/haplotype.block --db Malus_domestica --o qtl_pick
        unless(defined($hlog{"stage6"})) {$hlog{"stage6"} = 0;}
        my $m6 = $hlog{"stage6"};
        &process_cmd($stage6, $lo, $m6);
    }else{
        my $stage6 = "$bsatos2 qtl_pick --gff $gff --win $w --q $q --pr $pr --gp polish_dir/P.polished.afd --gm polish_dir/M.polished.afd --gpm polish_dir/PM.polished.afd --snv prepar_dir/anno/snv.AT_multianno.txt --sv prepar_dir/anno/sv.AT_multianno.txt --hap haplotype_dir/haplotype.block --db $db --o qtl_pick";
        # /home/user/BSATOS2/bsatos2 qtl_pick  --gff /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff --win 1000000 --q 10 --pr 2000 --gp polish_dir/P.polished.afd --gm polish_dir/M.polished.afd --gpm polish_dir/PM.polished.afd --snv prepar_dir/anno/snv.AT_multianno.txt --sv prepar_dir/anno/sv.AT_multianno.txt --hap haplotype_dir/haplotype.block --db Malus_domestica --o qtl_pick
        unless(defined($hlog{"stage6"})) {$hlog{"stage6"} = 0;}
        my $m6 = $hlog{"stage6"};
        &process_cmd($stage6, $lo, $m6);
    }

##################################################################

    print STDOUT "\nThe last module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        igv      generate files for Integrative Genomics Viewer\n\n";

    print $lo "\nThe last module
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        igv      generate files for Integrative Genomics Viewer\n\n";

    print STDOUT "stage7:\t";
    print $lo "stage7:\t";

    if (defined($gtf)){
        my $stage7 = "$bsatos2 igv --r $genome --gtf $gtf --prepar $prepar_dir --hap $hap_dir --qtl $qtl_dir --po $po_dir --db $db --o igv";
        # /home/user/BSATOS2/bsatos2 igv --r genome.fasta --gtf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf --prepar /home/user/bsa/prepar_dir --hap /home/user/bsa/haplotype_dir --qtl /home/user/bsa/qtl_pick_dir --po /home/user/bsa/polish_dir --db Malus_domestica --o igv
        unless(defined($hlog{"stage7"})) {$hlog{"stage7"} = 0;}
        my $m7 = $hlog{"stage7"};
        &process_cmd($stage7, $lo, $m7);
    }else{
        my $stage7 = "$bsatos2 igv --r $genome --gff $gff --prepar $prepar_dir --hap $hap_dir --qtl $qtl_dir --po $po_dir --db $db --o igv";
        # /home/user/BSATOS2/bsatos2 igv --r genome.fasta --gff /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff3 --prepar /home/user/bsa/prepar_dir --hap /home/user/bsa/haplotype_dir --qtl /home/user/bsa/qtl_pick_dir --po /home/user/bsa/polish_dir --db Malus_domestica --o igv
        unless(defined($hlog{"stage7"})) {$hlog{"stage7"} = 0;}
        my $m7 = $hlog{"stage7"};
        &process_cmd($stage7, $lo, $m7);
    }

##################################################################

    print STDOUT "move all the data to the result dir\n";
    print $lo "move all the data to the result dir\nstage8\t";

    my $stage8 = "mv $work_dir/prepar_dir $work_dir/prep_dir $work_dir/haplotype_dir $work_dir/afd_dir $work_dir/polish_dir $work_dir/qtl_pick_dir $work_dir/igv_dir $work_dir/*log $dir";
    # mv /home/user/bsa/prepar_dir /home/user/bsa/prep_dir /home/user/bsa/haplotype_dir /home/user/bsa/afd_dir /home/user/bsa/polish_dir /home/user/bsa/qtl_pick_dir /home/user/bsa/igv_dir /home/user/bsa/*log /home/user/bsa/result_dir
    unless(defined($hlog{"stage8"})) {$hlog{"stage8"} = 0;}
    my $m8 = $hlog{"stage8"};
    &process_cmd($stage8, $lo, $m8);

    print STDOUT "\n------ $o BSATOS2 finished!!!------\n\n";
    print $lo "\n------ $o BSATOS2 finished!!!------\n\n";
    exit(0);
}

##################################################################

sub process_cmd {
    my ($cmd, $lk, $hg) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t................................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t................................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime = "[".localtime()."]";
        if($ret){
            print $lk "\#die\t$starttime ... $endtime\n";
            print STDOUT "\#die\t$starttime ... $endtime\n";
            die "Error, cmd: < $cmd > died with return value $ret";
        }else{
            print $lk  "\#done\t$starttime ... $endtime\n";
            print STDOUT "\#done\t$starttime ... $endtime\n";
        }
        return;
    }
}
