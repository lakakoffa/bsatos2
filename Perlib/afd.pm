#!/usr/bin/env perl
package afd;
BEGIN {my $VERSION = '2.0';}

use Cwd;
use strict;
use Getopt::Long;
use base 'Exporter';
use File::Basename;
use FindBin qw($Bin);
our @EXPORT = qw(runafd);

my $G_USAGE = "
Usage: bastos2 afd [options]

Options:
    --o     [STR]    name prefix of output directory [afd]
    --pc    [FILE]   read counts with different alleles from high & low pools in P type loci from prep module [required]
    --mc    [FILE]   read counts with different alleles from high & low pools in M type loci from prep module [required]
    --pmc   [FILE]   read counts with different alleles from high & low pools in PM type loci from prep module [required]
    --hap   [FILE]   merged, corrented and patched haplotype block file of two parents and two pools from haplotype module [required]
    --log   [FILE]   afd module log [afd.log]
    --db    [STR]    database directory of .fa & GTF & GFF files [Malus_domestica]

Statistics Options:
    --sd    [STR]    statistic method: ed/g/abs [g]
    --w     [INT]    sliding window size [1000000]
    --fn    [INT]    batches for smoothing; the smaller the faster, but more memory needed [20]

Outputs:
    P.AFD   [FILE]   G value (ed/abs) based P type loci and smoothed curve with different window across genome with haplotype information
    M.AFD   [FILE]   G value (ed/abs) based M type loci and smoothed curve with different window across genome with haplotype information
    PM.AFD  [FILE]   G value (ed/abs) based PM type loci and smoothed curve with different window across genome with haplotype information

Example:
    bsatos2 afd --o afd --sd g --w 1000000 --pc P.counts --mc M.counts --pmc PM.counts --hap haplotype.block
";


sub runafd {
    my $pc = undef;     my $mc = undef;      my $pmc = undef;    my $hap = undef;
    my $sd = undef;     my $fn = undef;      my $help = undef;   my $cal = undef;
    my $md = undef;     my $w = undef;       my $th = undef;     my $m = undef;
    my $log = undef;    my $db = undef;      my $db = undef;     my $o = undef;

    GetOptions (
        "sd=s" => \$sd,       "pc=s" => \$pc,        "mc=s" => \$mc,     "pmc=s" => \$pmc,    "th=s" => \$th,
        "hap=s" => \$hap,     "o=s" => \$o,          "w=i" => \$w,       "fn=i" => \$fn,      "m=s" => \$m,
        "db=s" => \$db,       "log=s" => \$log,      "db=s" => \$db,     "help" => \$help)
    or die("$G_USAGE");
    die "$G_USAGE" if ($help || !defined($o));

    my $para_afd_file = "$FindBin::Bin/Python/src/para_afd_file";
    if (-e $para_afd_file){
        open PARAAFDFILE, "$para_afd_file" or die $!;
        while(<PARAAFDFILE>){
            chomp;
            my @para = split /\t/, $_;
            if (@para[0] =~ /^o\b/ && @para[1] =~ /\w/) {$o = @para[1];}
            if (@para[0] =~ /^pc\b/ && @para[1] =~ /\w/) {$pc = @para[1];}
            if (@para[0] =~ /^mc\b/ && @para[1] =~ /\w/) {$mc = @para[1];}
            if (@para[0] =~ /^pmc\b/ && @para[1] =~ /\w/) {$pmc = @para[1];}
            if (@para[0] =~ /^hap\b/ && @para[1] =~ /\w/) {$hap = @para[1];}
            if (@para[0] =~ /^sd\b/ && @para[1] =~ /\w/) {$sd = @para[1];}
            if (@para[0] =~ /^w\b/ && @para[1] =~ /\w/) {$w = @para[1];}
            if (@para[0] =~ /^fn\b/ && @para[1] =~ /\w/) {$fn = @para[1];}
            # if (@para[0] =~ /^db\b/ && @para[1] =~ /\w/) {$db = @para[1];}
        }
        close PARAAFDFILE;
    }

    unless(defined($w)) {$w = 1000000;}
    unless(defined($m)) {$m = "N";}
    unless(defined($fn)) {$fn = 20;}
    unless(defined($o)) {$o = "afd";}

    if(!defined($sd) || $sd eq "g" ) {$cal = "$FindBin::Bin/R/g.R";}      # /home/user/BSATOS2/R/g.R
    if($sd eq "ed") {$cal = "$FindBin::Bin/R/ed.R";}                      # /home/user/BSATOS2/R/ed.R
    if($sd eq "abs") {$cal = "$FindBin::Bin/R/abs.R";}                    # /home/user/BSATOS2/R/abs.R
    if($m eq "Y") {$md = 1;} else {$md = 0;}

    my $work_dir = getcwd;                                                # /home/user/bsa
    my $dir = $work_dir."/".$o."_dir";                                    # /home/user/bsa/afd_dir
    my $P_counts = basename($pc);                                         # P.counts
    my $M_counts = basename($mc);                                         # M.counts
    my $PM_counts = basename($pmc);                                       # PM.counts
    my $p_counts_cal_out = $dir."/".$P_counts.".cal.out";                 # /home/user/bsa/afd_dir/P.counts.cal.out
    my $m_counts_cal_out = $dir."/".$M_counts.".cal.out";                 # /home/user/bsa/afd_dir/M.counts.cal.out
    my $pm_counts_cal_out = $dir."/".$PM_counts.".cal.out";               # /home/user/bsa/afd_dir/PM.counts.cal.out
    my $p_afd = $dir."/P.AFD";                                            # /home/user/bsa/afd_dir/P.AFD
    my $m_afd = $dir."/M.AFD";                                            # /home/user/bsa/afd_dir/M.AFD
    my $pm_afd = $dir."/PM.AFD";                                          # /home/user/bsa/afd_dir/PM.AFD
    my $add_afd_header = "$FindBin::Bin/scripts/add_afd_header.pl";       # /home/user/BSATOS2/scripts/add_afd_header.pl
    my $filter = "$FindBin::Bin/tools/bin/filter";                        # /home/user/BSATOS2/tools/bin/filter
    my $rg = "$FindBin::Bin/tools/bin/rg";                                # /home/user/BSATOS2/tools/bin/rg

    unless(defined($log)) {$log = $work_dir."/".$o.".log";}               # /home/user/bsa/afd_dir/afd.log
    # unless(defined($db)) {$db = "Malus_domestica";}

###############################################################

    my %hlog = ();
    if(-e $log){
        open LOD, "<$log";
        my $count = 0;
        while(<LOD>){
            chomp;
            my @sp = split /\t/, $_;
            if($sp[2] eq "\#done") {$hlog{$sp[0]} = 1;}
            if($sp[2] eq "\#die") {$hlog{$sp[0]} = 0;}
            if($_ =~ /finish/) {$count++;}
        }
        if($count != 0){
            print "
                Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
                Version: 2.0
                The third module: afd
                afd         calculate and filter allele frequency difference between two extreme pools\n\n
                This module has been finished!!!!!\n\n";
            exit(0);
        }
    }
    open (my $lo, ">>$log") or die $!;

    print STDOUT "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The fourth module: afd
        afd         calculate and filter allele frequency difference between two extreme pools\n\n
        ██████╗ ███████╗ █████╗ ████████╗ ██████╗ ███████╗██████╗
        ██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██╔═══██╗██╔════╝╚════██╗
        ██████╔╝███████╗███████║   ██║   ██║   ██║███████╗ █████╔╝
        ██╔══██╗╚════██║██╔══██║   ██║   ██║   ██║╚════██║██╔═══╝
        ██████╔╝███████║██║  ██║   ██║   ╚██████╔╝███████║███████╗
        ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝╚══════╝\n\n";

    print $lo "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The fourth module: afd
        afd         calculate and filter allele frequency difference between two extreme pools\n\n";

###############################################################

    unless(-e $dir){
        print STDOUT "create result dir\n";
        print $lo "create result dir\nCMD0:\t";
        my $cd1 = "mkdir $dir";            # mkdir /home/user/bsa/afd_dir
        unless(defined($hlog{"CMD0"})) {$hlog{"CMD0"} = 0;}
        my $mfd = $hlog{"CMD0"};
        &process_cmd($cd1, $lo, $mfd);
    }


    my @chrs = &get_chr($pc, $rg);
    my $idx = 0;
    foreach my $chr (sort @chrs){
        $idx++;
        my $cdn1 = "CMD".$idx.":";
        print STDOUT "calculate P allele frequency difference between two extreme pools\n";
        print $lo "calculate P allele frequency difference between two extreme pools\n$cdn1\n";
        unless(defined($hlog{$cdn1})) {$hlog{$cdn1} = 0;}
        my $mx1 = $hlog{$cdn1};
        my $cd1 = "Rscript $cal $pc $chr $P_counts $w $md $fn";
        # Rscript /home/user/BSATOS2/R/g.R prep_dir/P.counts Chr06 P.counts 1000000 1 20
        my $cd12 = "Rscript $cal $pc $chr $P_counts $w 0 $fn";
        # Rscript /home/user/BSATOS2/R/g.R prep_dir/P.counts Chr06 P.counts 1000000 0 20
        &process_cmd2($cd1, $lo, $mx1, $cd12);

        $idx++;
        my $cdn2 = "CMD".$idx.":";
        print STDOUT "calculate M allele frequency difference between two extreme pools\n";
        print $lo "calculate M allele frequency difference between two extreme pools\n$cdn2\n";
        unless(defined($hlog{$cdn2})) {$hlog{$cdn2} = 0;}
        my $mx2 = $hlog{$cdn2};
        my $cd2 = "Rscript $cal $mc $chr $M_counts $w $md $fn";
        # Rscript /home/user/BSATOS2/R/g.R prep_dir/M.counts Chr06 M.counts 1000000 1 20
        my $cd22 = "Rscript $cal $mc $chr $M_counts $w 0 $fn";
        # Rscript /home/user/BSATOS2/R/g.R prep_dir/M.counts Chr06 M.counts 1000000 0 20
        &process_cmd2($cd2, $lo, $mx2, $cd22);

        $idx++;
        my $cdn3 = "CMD".$idx.":";
        print STDOUT "calculate PM allele frequency difference between two extreme pools\n";
        print $lo "calculate PM allele frequency difference between two extreme pools\n$cdn3\n";
        unless(defined($hlog{$cdn3})) {$hlog{$cdn3} = 0;}
        my $mx3 = $hlog{$cdn3};
        my $cd3 = "Rscript $cal $pmc $chr $PM_counts $w $md $fn";
        # Rscript /home/user/BSATOS2/R/g.R prep_dir/PM.counts Chr06 PM.counts 1000000 1 20
        my $cd32 = "Rscript $cal $pmc $chr $PM_counts $w 0 $fn";
        # Rscript /home/user/BSATOS2/R/g.R prep_dir/PM.counts Chr06 PM.counts 1000000 0 20
        &process_cmd2($cd3, $lo, $mx3, $cd32);
    }

    $idx++;
    my $cdn4 = "CMD".$idx.":";
    print STDOUT "merge P AFD file of different chromosomes\n";
    print $lo "merge P AFD file of different chromosomes\n$cdn4\t";
    unless(defined($hlog{$cdn4})) {$hlog{$cdn4} = 0;}
    my $mx4 = $hlog{$cdn4};
    my $cd4 = "cat $work_dir/\*_${P_counts}\_afd >> $p_counts_cal_out";
    # cat /home/user/bsa/*_P.counts_afd >> /home/user/bsa/afd_dir/P.counts.cal.out
    &process_cmd($cd4, $lo, $mx4);

    $idx++;
    my $cdn5 = "CMD".$idx.":";
    print STDOUT "merge M AFD file of different chromosomes\n";
    print $lo "merge M AFD file of different chromosomes\n$cdn5\t";
    unless(defined($hlog{$cdn5})) {$hlog{$cdn5} = 0;}
    my $mx5 = $hlog{$cdn5};
    my $cd5 = "cat $work_dir/\*_${M_counts}\_afd >> $m_counts_cal_out";
    # cat /home/user/bsa/*_M.counts_afd >> /home/user/bsa/afd_dir/M.counts.cal.out
    &process_cmd($cd5, $lo, $mx5);

    $idx++;
    my $cdn6 = "CMD".$idx.":";
    print STDOUT "merge PM AFD file of different chromosomes\n";
    print $lo "merge PM AFD file of different chromosomes\n$cdn6\t";
    unless(defined($hlog{$cdn6})) {$hlog{$cdn6} = 0;}
    my $mx6 = $hlog{$cdn6};
    my $cd6 = "cat $work_dir/\*_${PM_counts}\_afd >> $pm_counts_cal_out";
    # cat /home/user/bsa/*_PM.counts_afd >> /home/user/bsa/afd_dir/PM.counts.cal.out
    &process_cmd($cd6, $lo, $mx6);

    $idx++;
    my $cdn7 = "CMD".$idx.":";
    print STDOUT "add haplotype block information to P AFD file\n";
    print $lo "add haplotype block information to P AFD file\n$cdn7\t";
    unless(defined($hlog{$cdn7})) {$hlog{$cdn7} = 0;}
    my $mx7 = $hlog{$cdn7};
    # my $cd7 = "$filter -k cn -A A -B E $p_counts_cal_out $hap | $add_afd_header -> $p_afd";
    # /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/afd_dir/P.counts.cal.out haplotype_dir/haplotype.block | /home/user/BSATOS2/scripts/add_afd_header.pl -> /home/user/bsa/afd_dir/P.AFD
    my $cd7 = "$add_afd_header > $p_afd && $filter -k cn -A A -B E $p_counts_cal_out $hap >> $p_afd";
    # /home/user/BSATOS2/scripts/add_afd_header.pl > /home/user/bsa/afd_dir/P.AFD && /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/afd_dir/P.counts.cal.out haplotype_dir/haplotype.block >> /home/user/bsa/afd_dir/P.AFD
    &process_cmd($cd7, $lo, $mx7);

    $idx++;
    my $cdn8 = "CMD".$idx.":";
    print STDOUT "add haplotype block information to M AFD file\n";
    print $lo "add haplotype block information to M AFD file\n$cdn8\t";
    unless(defined($hlog{$cdn8})) {$hlog{$cdn8} = 0;}
    my $mx8 = $hlog{$cdn8};
    # my $cd8 = "$filter -k cn -A A -B E $m_counts_cal_out $hap | $add_afd_header -> $m_afd";
    # /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/afd_dir/M.counts.cal.out haplotype_dir/haplotype.block | /home/user/BSATOS2/scripts/add_afd_header.pl -> /home/user/bsa/afd_dir/M.AFD
    my $cd8 = "$add_afd_header > $m_afd && $filter -k cn -A A -B E $m_counts_cal_out $hap >> $m_afd";
    # /home/user/BSATOS2/scripts/add_afd_header.pl > /home/user/bsa/afd_dir/M.AFD && /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/afd_dir/M.counts.cal.out haplotype_dir/haplotype.block >> /home/user/bsa/afd_dir/M.AFD
    &process_cmd($cd8, $lo, $mx8);

    $idx++;
    my $cdn9 = "CMD".$idx.":";
    print STDOUT "add haplotype block information to PM AFD file\n";
    print $lo "add haplotype block information to PM AFD file\n$cdn9\t";
    unless(defined($hlog{$cdn9})) {$hlog{$cdn9} = 0;}
    my $mx9 = $hlog{$cdn9};
    # my $cd9 = "$filter -k cn -A A -B E $pm_counts_cal_out $hap | $add_afd_header -> $pm_afd";
    # /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/afd_dir/PM.counts.cal.out haplotype_dir/haplotype.block | /home/user/BSATOS2/scripts/add_afd_header.pl -> /home/user/bsa/afd_dir/PM.AFD
    my $cd9 = "$add_afd_header > $pm_afd && $filter -k cn -A A -B E $pm_counts_cal_out $hap >> $pm_afd";
    # /home/user/BSATOS2/scripts/add_afd_header.pl > /home/user/bsa/afd_dir/PM.AFD && /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/afd_dir/PM.counts.cal.out haplotype_dir/haplotype.block >> /home/user/bsa/afd_dir/PM.AFD
    &process_cmd($cd9, $lo, $mx9);

    $idx++;
    my $cdn10 ="CMD".$idx.":";
    print STDOUT "move some files\n";
    print $lo "move spome files\n$cdn10\t";
    unless(defined($hlog{$cdn10})) {$hlog{$cdn10} = 0;}
    my $mx10 = $hlog{$cdn10};
    my $cd10 = "mv $work_dir/Chr*counts_afd $dir";
    # mv /home/user/bsa/Chr*counts_afd /home/user/bsa/afd_dir
    &process_cmd($cd10, $lo, $mx10);

    print STDOUT "\nafd module finished!!!\n";
    print $lo "\nafd module finished!!!\n";
    exit(0);
}

###############################################################

sub process_cmd {
    my ($cmd, $lk, $hg) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t...............................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t...............................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime = "[".localtime()."]";
        if($ret){
            print $lk "\#die\t$starttime ... $endtime\n";
            print STDOUT "\#die\t$starttime ... $endtime\n";
            die "Error, cmd: < $cmd > died with return value $ret";
        }else{
            print $lk "\#done\t$starttime ... $endtime\n";
            print STDOUT "\#done\t$starttime ... $endtime\n";
        }
        return;
    }
}


###############################################################

sub process_cmd2 {
    my ($cmd, $lk, $hg, $cmd2) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t...............................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t...............................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime1 = "[".localtime()."]";
        if($ret){
            my $ret2 = system($cmd2);
            my $endtime2 = "[".localtime()."]";
            print $lk "\#failed and try other method\t$starttime ... $endtime2\n";
            print STDOUT "\#failed and try other method\t$starttime ... $endtime2\n";
            print STDOUT "$cmd2\t...............................";
            print $lk "$cmd2\t";
            if($ret2){
                print $lk "\#die\t$starttime ... $endtime2\n";
                print STDOUT "\#die\t$starttime ... $endtime2\n";
                die "Error, cmd: < $cmd2 > died with return value $ret";
            }else{
                print $lk "\#done\t$starttime ... $endtime2\n";
                print STDOUT "\#done\t$starttime ... $endtime2\n";
            }
        }else{
            print $lk "\#done\t$starttime ... $endtime1\n";
            print STDOUT "\#done\t$starttime ... $endtime1\n";
        }
        return;
    }
}


###############################################################

sub get_chr {
    my @para = @_;
    # my (%hash, @line, @chrs);
    # open LOD, "<$para[0]";
    # while(<LOD>){
    #     if($_ =~ /#/) {next;}
    #     @line = split /\t/, $_;     # CHROM	POS	H_REF	H_ALT	L_REF	L_ALT
    #     $hash{$line[0]}++;     # 以染色体号为键, 增加计数
    # }
    # @chrs = keys %hash;
    # return(@chrs);

    my $chrs = `cut -f1 $para[0] | $para[1] -vN "#" | uniq`;
    chomp($chrs);
    my @chrs = split /\n/, $chrs;
    return(@chrs);
}
