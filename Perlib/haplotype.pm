#!/usr/bin/env perl
package haplotype;
BEGIN {my $VERSION = '2.0';}

use Cwd;
use strict;
use Getopt::Long;
use File::Basename;
use FindBin qw($Bin);
use base 'Exporter';
our @EXPORT = qw(runhaplotype);

my $G_USAGE = "
Usage: bastos2 haplotype [options]

Options:
    --o         [STR]     name prefix of output directory [haplotype]
    --r         [FILE]    reference genome (fasta format) [Malus_domestica.fa]
    --pb        [FILE]    pre-aligned bam file from pollen parent [required]
    --mb        [FILE]    pre-aligned bam file from maternal parent [required]
    --hb        [FILE]    pre-aligned bam file from high extreme pool [required]
    --lb        [FILE]    pre-aligned bam file from low extreme pool [required]
    --hapcut2   [STR]     use samtools algorithm or HAPCUT2 algorithm to assembly haplotype [default: NO for HAPCUT2; YES for samtools]
    --var       [STR]     parent SNV calling file
    --log       [FILE]    haplotype module log file [haplotype.log]
    --db        [STR]     database directory of .fa & GTF & GFF files [Malus_domestica]

SNP Genotyping Options:
    --t3        [INT]     number of threads [1]
    --dep       [INT]     skip SNPs with read depth smaller than INT [10]
    --aq3       [INT]     skip alignment with mapQ smaller than INT [10]
    --vq2       [INT]     skip SNPs with phred-scaled quality smaller than INT [10]

Outputs:
    P_block            [FILE]      haplotype blocks of pollen parent
    M.block            [FILE]      haplotype blocks of maternal parent
    haplotype.block    [FILE]      merged, corrected and patched haplotype blocks of pollen parent, maternal parent, High extreme pool and Low extreme pool
    P_haplotype.bed    [FILE]      BED format haplotype information of pollen parent
    M_haplotype.bed    [FILE]      BED format haplotype information of maternal parent
    overlapped.bed     [FILE]      BED format haplotype information classified from two parents and two pools
    sub_haplotype      [FILE]      haplotype sub-blocks information within haplotype block

Example:
    bsatos2 haplotype --o haplotype --pb P.bam --mb M.bam --hb H.bam --lb L.bam --r Malus_domestica.fa --var prepar_dir/P_M.snv
";

sub runhaplotype {
    my $pb = undef;         my $mb = undef;         my $hb = undef;         my $lb = undef;         my $aq3 = undef;
    my $genome = undef;     my $o = undef;          my $help = undef;       my $hapcut2 = undef;    my $vq2 = undef;
    my $var = undef;        my $myvar =undef;       my $s = undef;          my $dep = undef;
    my $t3 = undef;         my $db = undef;         my $log = undef;

    GetOptions (
        "pb=s" => \$pb,      "mb=s" => \$mb,        "hb=s" => \$hb,       "lb=s" => \$lb,
        "r=s" => \$genome,   "var=s" => \$var,      "o=s" => \$o,         "hapcut2=s" => \$hapcut2,
        "s=s" => \$s,        "dep=i" => \$dep,      "aq3=i" => \$aq3,     "vq2=i" => \$vq2,
        "t3=i" => \$t3,      "log=s" => \$log,      "db=s" => \$db,       "help" => \$help)
    or die("$G_USAGE");
    die "$G_USAGE" if ($help);
    die "$G_USAGE" if ((!defined ($o)) || (!defined($genome)));

    my $para_haplotype_file = "$FindBin::Bin/Python/src/para_haplotype_file";
    if (-e $para_haplotype_file){
        open PARAHAPLOTYPEFILE, "$para_haplotype_file" or die $!;
        while(<PARAHAPLOTYPEFILE>){
            chomp;
            my @para = split /\t/, $_;
            if (@para[0] =~ /^o\b/ && @para[1] =~ /\w/) {$o = @para[1];}
            if (@para[0] =~ /^r\b/ && @para[1] =~ /\w/) {$genome = @para[1];}
            if (@para[0] =~ /^hb\b/ && @para[1] =~ /\w/) {$hb = @para[1];}
            if (@para[0] =~ /^lb\b/ && @para[1] =~ /\w/) {$lb = @para[1];}
            if (@para[0] =~ /^pb\b/ && @para[1] =~ /\w/) {$pb = @para[1];}
            if (@para[0] =~ /^mb\b/ && @para[1] =~ /\w/) {$mb = @para[1];}
            if (@para[0] =~ /^hapcut2\b/ && @para[1] =~ /\w/) {$hapcut2 = @para[1];}
            if (@para[0] =~ /^t3\b/ && @para[1] =~ /\w/) {$t3 = @para[1];}
            if (@para[0] =~ /^aq3\b/ && @para[1] =~ /\w/) {$aq3 = @para[1];}
            if (@para[0] =~ /^vq2\b/ && @para[1] =~ /\w/) {$vq2 = @para[1];}
            if (@para[0] =~ /^dep\b/ && @para[1] =~ /\w/) {$dep = @para[1];}
            if (@para[0] =~ /^db\b/ && @para[1] =~ /\w/) {$db = @para[1];}
        }
        close PARAHAPLOTYPEFILE;
    }

    unless(defined($o)) {$o = "haplotype";}
    unless(defined($hapcut2)) {$hapcut2 = "NO";}
    unless(defined($aq3)) {$aq3 = 10;}
    unless(defined($vq2)) {$vq2 = 10;}
    unless(defined($dep)) {$dep = 10;}
    unless(defined($t3)) {$t3 = 1;}

    my $bedops = "bedops";
    my $HAPCUT2 = "HAPCUT2";
    my $bedtools = "bedtools";
    my $extractHAIRS = "extractHAIRS";
    my $filter = "$FindBin::Bin/tools/bin/filter";                          # /home/user/BSATOS2/tools/bin/filter
    my $samtools = "$FindBin::Bin/tools/bin/samtools";                      # /home/user/BSATOS2/tools/bin/samtools
    my $bcftools = "$FindBin::Bin/tools/bin/bcftools";                      # /home/user/BSATOS2/tools/bin/bcftools
    my $work_dir = getcwd;                                                  # /home/user/bsa
    my $dir = $work_dir."/".$o."_dir";                                      # /home/user/bsa/haplotype_dir
    my $detect_crossover = "$FindBin::Bin/scripts/detect_crossover.pl";     # /home/user/BSATOS2/scripts/detect_crossover.pl
    my $get_refer = "$FindBin::Bin/scripts/get_refer.pl";                   # /home/user/BSATOS2/scripts/get_refer.pl
    my $get_block = "$FindBin::Bin/scripts/get_block.pl";                   # /home/user/BSATOS2/scripts/get_block.pl
    my $get_hap = "$FindBin::Bin/scripts/get_hap.pl";                       # /home/user/BSATOS2/scripts/get_hap.pl
    my $merge_haplotype = "$FindBin::Bin/scripts/merge_haplotype.pl";       # /home/user/BSATOS2/scripts/merge_haplotype.pl
    my $judge_sort = "$FindBin::Bin/scripts/judge_and_sort_haplotype.pl";   # /home/user/BSATOS2/scripts/judge_and_sort_haplotype.pl
    my $fill_homo = "$FindBin::Bin/scripts/fill_homo.pl";                   # /home/user/BSATOS2/scripts/fill_homo.pl
    my $cmp1 = "$FindBin::Bin/scripts/cmp_infer_block_step1.pl";            # /home/user/BSATOS2/scripts/cmp_infer_block_step1.pl
    # my $cmp1v2 = "$FindBin::Bin/scripts/cmp_infer_block_step1_v2.pl";       # /home/user/BSATOS2/scripts/cmp_infer_block_step1_v2.pl
    my $cmp1v2 = "$FindBin::Bin/cpp/cmp_infer_block_step1_v2";              # /home/user/BSATOS2/cpp/cmp_infer_block_step1_v2
    my $cmp2 = "$FindBin::Bin/scripts/cmp_infer_block_step2.pl";            # /home/user/BSATOS2/scripts/cmp_infer_block_step2.pl
    # my $cmp2v2 = "$FindBin::Bin/scripts/cmp_infer_block_step2_v2.pl";       # /home/user/BSATOS2/scripts/cmp_infer_block_step2_v2.pl
    my $cmp2v2 = "$FindBin::Bin/cpp/cmp_infer_block_step2_v2";              # /home/user/BSATOS2/scripts/cmp_infer_block_step2_v2
    my $classify_haplotype = "$FindBin::Bin/scripts/classify_haplotype.pl"; # /home/user/BSATOS2/scripts/classify_haplotype.pl
    my $filter_vcf = "$FindBin::Bin/scripts/filter_vcf.pl";                 # /home/user/BSATOS2/scripts/filter_vcf.pl
    my $add_header = "$FindBin::Bin/scripts/add_header.pl";                 # /home/user/BSATOS2/scripts/add_header.pl
    my $ch2bed = "$FindBin::Bin/scripts/ch2bed.pl";                         # /home/user/BSATOS2/scripts/ch2bed.pl
    my $P_phase = $work_dir."/".basename($pb).".phase";                     # /home/user/bsa/P.bam.phase
    my $M_phase = $work_dir."/".basename($mb).".phase";                     # /home/user/bsa/M.bam.phase
    my $H_phase = $work_dir."/".basename($hb).".phase";                     # /home/user/bsa/H.bam.phase
    my $L_phase = $work_dir."/".basename($lb).".phase";                     # /home/user/bsa/L.bam.phase
    my $P_ref = $dir."/".basename($pb)."_ref";                              # /home/user/bsa/haplotype_dir/P.bam_ref
    my $M_ref = $dir."/".basename($mb)."_ref";                              # /home/user/bsa/haplotype_dir/M.bam_ref
    my $H_ref = $dir."/".basename($hb)."_ref";                              # /home/user/bsa/haplotype_dir/H.bam_ref
    my $L_ref = $dir."/".basename($lb)."_ref";                              # /home/user/bsa/haplotype_dir/L.bam_ref
    my $P_block = $dir."/P_block";                                          # /home/user/bsa/haplotype_dir/P_block
    my $M_block = $dir."/M_block";                                          # /home/user/bsa/haplotype_dir/M_block
    my $H_block = $dir."/H_block";                                          # /home/user/bsa/haplotype_dir/H_block
    my $L_block = $dir."/L_block";                                          # /home/user/bsa/haplotype_dir/L_block
    my $P_bam_block = $dir."/".basename($pb)."_block";                      # /home/user/bsa/haplotype_dir/P.bam_block
    my $M_bam_block = $dir."/".basename($mb)."_block";                      # /home/user/bsa/haplotype_dir/M.bam_block
    my $H_bam_block = $dir."/".basename($hb)."_block";                      # /home/user/bsa/haplotype_dir/H.bam_block
    my $L_bam_block = $dir."/".basename($lb)."_block";                      # /home/user/bsa/haplotype_dir/L.bam_block
    my $merge_list = $dir."/merge.list";                                    # /home/user/bsa/haplotype_dir/merge.list
    my $merged_block = $dir."/merged.block";                                # /home/user/bsa/haplotype_dir/merged.block
    my $merged_block_filter = $dir."/merged.block.filter";                  # /home/user/bsa/haplotype_dir/merged.block.filter
    my $haplotype_block = $dir."/haplotype.block";                          # /home/user/bsa/haplotype_dir/haplotype.block
    my $P_hap = $dir."/P.haplotype";                                        # /home/user/bsa/haplotype_dir/P.haplotype
    my $P_hap_bed = $dir."/P_haplotype.bed";                                # /home/user/bsa/haplotype_dir/P_haplotype.bed
    my $M_hap = $dir."/M.haplotype";                                        # /home/user/bsa/haplotype_dir/M.haplotype
    my $M_hap_bed = $dir."/M_haplotype.bed";                                # /home/user/bsa/haplotype_dir/M_haplotype.bed
    my $overlap = $dir."/overlapped.bed";                                   # /home/user/bsa/haplotype_dir/overlapped.bed
    my $phase_tmp = $dir."/phase_tmp";                                      # /home/user/bsa/haplotype_dir/phase_tmp
    my $sub_hap = $dir."/sub_haplotype";                                    # /home/user/bsa/haplotype_dir/sub_haplotype
    my $myvar = $dir."/myvar";                                              # /home/user/bsa/haplotype_dir/myvar
    my $mytmp1 = $dir."/mytmp1";                                            # /home/user/bsa/haplotype_dir/mytmp1
    my $mytmp2 = $dir."/mytmp2";                                            # /home/user/bsa/haplotype_dir/mytmp2
    my $mytmp3 = $dir."/mytmp3";                                            # /home/user/bsa/haplotype_dir/mytmp3
    my $mytmp4 = $dir."/mytmp4";                                            # /home/user/bsa/haplotype_dir/mytmp4
    my $mytmp5 = $dir."/mytmp5";                                            # /home/user/bsa/haplotype_dir/mytmp5
    my $mytmp6 = $dir."/mytmp6";                                            # /home/user/bsa/haplotype_dir/mytmp6

    unless(defined($log)) {$log = $work_dir."/".$o.".log";}                   # /home/user/bsa/haplotype_dir/haplotype.log
    unless(defined($db)) {$db = "Malus_domestica";}
    unless(defined($genome)) {
        if (-e "$FindBin::Bin/database/$db/$db.fa"){$genome = "$FindBin::Bin/database/$db/$db.fa";}         # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa
        if (-e "$FindBin::Bin/database/$db/$db.fasta"){$genome = "$FindBin::Bin/database/$db/$db.fasta";}   # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fasta
    }

###############################################################

    my %hlog = ();
    if(-e $log){
        open LOD, "<$log";
        my $count = 0;
        while(<LOD>){
            chomp;
            my @sp = split /\t/, $_;
            if($sp[2] eq "\#done") {$hlog{$sp[0]} = 1;}
            if($sp[2] eq "\#die") {$hlog{$sp[0]} = 0;}
            if($_ =~ /finish/) {$count++;}
        }
        if($count != 0){
            print "
                Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
                Version: 2.0
                The third module: haplotype
                haplotype   construct haplotype block\n\n
                This module has been finished!!!!!\n\n";
            exit(0);
        }
    }
    open (my $lo, ">>$log") or die $!;

    print STDOUT "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The third module: haplotype
        haplotype   construct haplotype block\n\n
        ██████╗ ███████╗ █████╗ ████████╗ ██████╗ ███████╗██████╗
        ██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██╔═══██╗██╔════╝╚════██╗
        ██████╔╝███████╗███████║   ██║   ██║   ██║███████╗ █████╔╝
        ██╔══██╗╚════██║██╔══██║   ██║   ██║   ██║╚════██║██╔═══╝
        ██████╔╝███████║██║  ██║   ██║   ╚██████╔╝███████║███████╗
        ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝╚══════╝\n\n";

    print $lo "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The third module: haplotype
        haplotype   construct haplotype block\n\n";


###############################################################

    unless(-e $dir){
        print STDOUT "create haplotype result dir\n";
        print $lo "create haplotype result dir\nCMD1:\t";
        unless(defined($hlog{"CMD1:"})) {$hlog{"CMD1:"} = 0;}
        my $mx1 = $hlog{"CMD1:"};
        my $cd1 = "mkdir $dir";        # mkdir /home/user/bsa/haplotype_dir
        &process_cmd($cd1, $lo, $mx1);
    }

    if($hapcut2 eq "NO"){

        unless(defined($var)){
            $var = $dir."/var";          # /home/user/bsa/haplotype_dir/var
            print STDOUT "SNV calling based on BAM files\n";
            print $lo "SNV calling based on BAM files\nCMD2:\t";
            unless(defined($hlog{"CMD2:"})) {$hlog{"CMD2:"} = 0;}
            my $mx2 = $hlog{"CMD2:"};
            my $cd2 = "$samtools mpileup -ugf $genome -q $aq3 $pb $mb $hb $lb | $bcftools call --threads $t3 -mv - | $filter_vcf -d $dep -q $vq2 -> $var";
            # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 P.bam M.bam H.bam L.bam | bcftools call --threads 1 -mv - | /home/user/BSATOS2/scripts/filter_vcf.pl -d 10 -q 40 -> /home/user/bsa/haplotype_dir/var
            &process_cmd($cd2, $lo, $mx2);
        }

        print STDOUT "filter SNVs based on reads depth and quality\n";
        print $lo "filter SNVs based on reads depth and quality\nCMD3:\t";
        unless(defined($hlog{"CMD3:"})) {$hlog{"CMD3:"} = 0;}
        my $mx3 = $hlog{"CMD3:"};
        my $cd3 = "$filter_vcf -d $dep -q $vq2 $var > $myvar";
        # /home/user/BSATOS2/scripts/filter_vcf.pl -d 10 -q 40 prepar_dir/P_M.snv > /home/user/bsa/haplotype_dir/myvar
        &process_cmd($cd3, $lo, $mx3);

        unless(-e $P_phase){
            print STDOUT "samtools phase pollen parent\n";
            print $lo "samtools phase pollen parent\nCMD4:\t";
            unless(defined($hlog{"CMD4:"})) {$hlog{"CMD4:"} = 0;}
            my $mx4 = $hlog{"CMD4:"};
            my $cd4 = "$samtools phase $pb > $P_phase";
            # samtools phase P.bam > /home/user/bsa/P.bam.phase
            &process_cmd($cd4, $lo, $mx4);
        }

        unless(-e $M_phase){
            print STDOUT "samtools phase maternal parent\n";
            print $lo "samtools phase maternal parent\nCMD5:\t";
            unless(defined($hlog{"CMD5:"})) {$hlog{"CMD5:"} = 0;}
            my $mx5 = $hlog{"CMD5:"};
            my $cd5 = "$samtools phase $mb > $M_phase";
            # samtools phase M.bam > /home/user/bsa/M.bam.phase
            &process_cmd($cd5, $lo, $mx5);
        }

        unless(-e $H_phase){
            print STDOUT "samtools phase high pool\n";
            print $lo "samtools phase high pool\nCMD6:\t";
            unless(defined($hlog{"CMD6:"})) {$hlog{"CMD6:"} = 0;}
            my $mx6 = $hlog{"CMD6:"};
            my $cd6 = "$samtools phase $hb > $H_phase";
            # samtools phase H.bam > /home/user/bsa/H.bam.phase
            &process_cmd($cd6, $lo, $mx6);
        }

        unless(-e $L_phase){
            print STDOUT "samtools phase low pool\n";
            print $lo "samtools phase low pool\nCMD7:\t";
            unless(defined($hlog{"CMD7:"})) {$hlog{"CMD7:"} = 0;}
            my $mx7 = $hlog{"CMD7:"};
            my $cd7 = "$samtools phase $lb > $L_phase";
            # samtools phase L.bam > /home/user/bsa/L.bam.phase
            &process_cmd($cd7, $lo, $mx7);
        }

        print STDOUT "get pollen parent reference genome loci\n";
        print $lo "get pollen parent reference genome loci\nCMD8:\t";
        unless(defined($hlog{"CMD8:"})) {$hlog{"CMD8:"} = 0;}
        my $mx8 = $hlog{"CMD8:"};
        my $cd8 = "perl $get_refer $P_phase $genome > $P_ref";
        # perl /home/user/BSATOS2/scripts/get_refer.pl /home/user/bsa/P.bam.phase /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa > /home/user/bsa/haplotype_dir/P.bam_ref
        &process_cmd($cd8, $lo, $mx8);

        print STDOUT "get maternal parent reference genome loci\n";
        print $lo "get maternal parent reference genome loci\nCMD9:\t";
        unless(defined($hlog{"CMD9:"})) {$hlog{"CMD9:"} = 0;}
        my $mx9 = $hlog{"CMD9:"};
        my $cd9 = "perl $get_refer $M_phase $genome > $M_ref";
        # perl /home/user/BSATOS2/scripts/get_refer.pl /home/user/bsa/M.bam.phase /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa > /home/user/bsa/haplotype_dir/M.bam_ref
        &process_cmd($cd9, $lo, $mx9);

        print STDOUT "get high pool reference genome loci\n";
        print $lo "get high pool reference genome loci\nCMD10:\t";
        unless(defined($hlog{"CMD10:"})) {$hlog{"CMD10:"} = 0;}
        my $mx10 = $hlog{"CMD10:"};
        my $cd10 = "perl $get_refer $H_phase $genome > $H_ref";
        # perl /home/user/BSATOS2/scripts/get_refer.pl /home/user/bsa/H.bam.phase /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa > /home/user/bsa/haplotype_dir/H.bam_ref
        &process_cmd($cd10, $lo, $mx10);

        print STDOUT "get low pool reference genome loci\n";
        print $lo "get low pool reference genome loci\nCMD11:\t";
        unless(defined($hlog{"CMD11:"})) {$hlog{"CMD11:"} = 0;}
        my $mx11 = $hlog{"CMD11:"};
        my $cd11 = "perl $get_refer $L_phase $genome > $L_ref";
        # perl /home/user/BSATOS2/scripts/get_refer.pl /home/user/bsa/L.bam.phase /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa > /home/user/bsa/haplotype_dir/L.bam_ref
        &process_cmd($cd11, $lo, $mx11);

        print STDOUT "construct pollen parent haplotype blocks\n";
        print $lo "construct pollen parent haplotype block\nCMD12:\t";
        unless(defined($hlog{"CMD12:"})) {$hlog{"CMD12:"} = 0;}
        my $mx12 = $hlog{"CMD12:"};
        my $cd12 = "perl $get_block $P_phase $P_ref | $judge_sort -> $P_block";
        # perl /home/user/BSATOS2/scripts/get_block.pl /home/user/bsa/P.bam.phase /home/user/bsa/haplotype_dir/P.bam_ref | /home/user/BSATOS2/scripts/judge_and_sort_haplotype.pl -> /home/user/bsa/haplotype_dir/P_block
        &process_cmd($cd12, $lo, $mx12);

        print STDOUT "construct maternal parent haplotype blocks\n";
        print $lo "construct maternal parent haplotype blocks\nCMD13:\t";
        unless(defined($hlog{"CMD13:"})) {$hlog{"CMD13:"} = 0;}
        my $mx13 = $hlog{"CMD13:"};
        my $cd13 = "perl $get_block $M_phase $M_ref | $judge_sort -> $M_block";
        # perl /home/user/BSATOS2/scripts/get_block.pl /home/user/bsa/M.bam.phase /home/user/bsa/haplotype_dir/M.bam_ref | /home/user/BSATOS2/scripts/judge_and_sort_haplotype.pl -> /home/user/bsa/haplotype_dir/M_block
        &process_cmd($cd13, $lo, $mx13);

        print STDOUT "construct high pool haplotype blocks\n";
        print $lo "construct high pool haplotype blocks\nCMD14:\t";
        unless(defined($hlog{"CMD14:"})) {$hlog{"CMD14:"} = 0;}
        my $mx14 = $hlog{"CMD14:"};
        my $cd14 = "perl $get_block $H_phase $H_ref | $judge_sort -> $H_block";
        # perl /home/user/BSATOS2/scripts/get_block.pl /home/user/bsa/H.bam.phase /home/user/bsa/haplotype_dir/H.bam_ref | /home/user/BSATOS2/scripts/judge_and_sort_haplotype.pl -> /home/user/bsa/haplotype_dir/H_block
        &process_cmd($cd14, $lo, $mx14);

        print STDOUT "construct low pool haplotype blocks\n";
        print $lo "contruct low pool haplotype blocks\nCMD15:\t";
        unless(defined($hlog{"CMD15:"})) {$hlog{"CMD15:"} = 0;}
        my $mx15 = $hlog{"CMD15:"};
        my $cd15 = "perl $get_block $L_phase $L_ref | $judge_sort -> $L_block";
        # perl /home/user/BSATOS2/scripts/get_block.pl /home/user/bsa/L.bam.phase /home/user/bsa/haplotype_dir/L.bam_ref | /home/user/BSATOS2/scripts/judge_and_sort_haplotype.pl -> /home/user/bsa/haplotype_dir/L_block
        &process_cmd($cd15, $lo, $mx15);

    } else {

        unless(defined($var)){
            $var = $dir."/var";
            print STDOUT "get SNVs loci and filter\n";
            print $lo "get SNVs loci and filter\nCMD16:\t";
            unless(defined($hlog{"CMD16:"})) {$hlog{"CMD16:"} = 0;}
            my $mx16 = $hlog{"CMD16:"};
            my $cd16 = "$samtools mpileup -ugf $genome -q $aq3 $pb $mb $hb $lb | $bcftools call --threads $t3 -mv - | $filter_vcf -d $dep -q $vq2 -> $var";
            # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 P.bam M.bam H.bam L.bam | bcftools call --threads 1 -mv - | /home/user/BSATOS2/scripts/filter_vcf.pl -d 10 -q 40 -> /home/user/bsa/haplotype_dir/var
            &process_cmd($cd16, $lo, $mx16);
        }

        print STDOUT "filter SNVs loci\n";
        print $lo "filter SNVs loci\nCMD17:\t";
        unless(defined($hlog{"CMD17:"})) {$hlog{"CMD17:"} = 0;}
        my $mx17 = $hlog{"CMD17:"};
        my $cd17 = "perl $filter_vcf -d $dep -q $vq2 $var > $myvar";
        # perl /home/user/BSATOS2/scripts/filter_vcf.pl -d 10 -q 40 /home/user/bsa/haplotype_dir/P_M.snv > /home/user/bsa/haplotype_dir/myvar
        &process_cmd($cd17, $lo, $mx17);

        print STDOUT "extract pollen parent fragments\n";
        print $lo "extract pollen parent fragments\nCMD18:\t";
        unless(defined($hlog{"CMD18:"})) {$hlog{"CMD18:"} = 0;}
        my $mx18 = $hlog{"CMD18:"};
        my $cd18= "$extractHAIRS --bam $pb --VCF $myvar --out $P_phase";
        # extractHAIRS --bam P.bam --VCF /home/user/bsa/haplotype_dir/myvar --out /home/user/bsa/P.bam.phase
        &process_cmd($cd18, $lo, $mx18);

        print STDOUT "extract maternal parent fragments\n";
        print $lo "extract maternal parent fragments\nCMD19:\t";
        unless(defined($hlog{"CMD19:"})) {$hlog{"CMD19:"} = 0;}
        my $mx19 = $hlog{"CMD19:"};
        my $cd19 = "$extractHAIRS --bam $mb --VCF $myvar --out $M_phase";
        # extractHAIRS --bam M.bam --VCF /home/user/bsa/haplotype_dir/myvar --out /home/user/bsa/M.bam.phase
        &process_cmd($cd19, $lo, $mx19);

        print STDOUT "extract high pool fragments\n";
        print $lo "extract high pool fragmets\nCMD20:\t";
        unless(defined($hlog{"CMD20:"})) {$hlog{"CMD20:"} = 0;}
        my $mx20 = $hlog{"CMD20:"};
        my $cd20 = "$extractHAIRS --bam $hb --VCF $myvar --out $H_phase";
        # extractHAIRS --bam H.bam --VCF /home/user/bsa/haplotype_dir/myvar --out /home/user/bsa/H.bam.phase
        &process_cmd($cd20, $lo, $mx20);

        print STDOUT "extract low pool fragments\n";
        print $lo "extract low pool fragments\nCMD21:\t";
        unless(defined($hlog{"CMD21:"})) {$hlog{"CMD21:"} = 0;}
        my $mx21 = $hlog{"CMD21:"};
        my $cd21 = "$extractHAIRS --bam $lb --VCF $myvar --out $L_phase";
        # extractHAIRS --bam L.bam --VCF /home/user/bsa/haplotype_dir/myvar --out /home/user/bsa/L.bam.phase
        &process_cmd($cd21, $lo, $mx21);

        print STDOUT "construct pollen parent haplotype blocks\n";
        print $lo "construct pollen parent haplotype blocks\nCMD22:\n";
        unless(defined($hlog{"CMD22:"})) {$hlog{"CMD22:"} = 0;}
        my $mx22 = $hlog{"CMD22:"};
        my $cd22 = "$HAPCUT2 --fragments $P_phase --VCF $myvar --output $P_bam_block";
        # HAPCUT2 --fragments /home/user/bsa/P.bam.phase --VCF /home/user/bsa/haplotype_dir/myvar --out /home/user/bsa/haplotype_dir/P.bam_block
        &process_cmd($cd22, $lo, $mx22);

        print STDOUT "construct maternal parent haplotype blocks\n";
        print $lo "construct maternal parent haplotype blocks\nCMD23:\n";
        unless(defined($hlog{"CMD23:"})) {$hlog{"CMD23:"} = 0;}
        my $mx23 = $hlog{"CMD23:"};
        my $cd23 = "$HAPCUT2 --fragments $M_phase --VCF $myvar --output $M_bam_block";
        # HAPCUT2 --fragments /home/user/bsa/M.bam.phase --VCF /home/user/bsa/haplotype_dir/myvar --out /home/user/bsa/haplotype_dir/M.bam_block
        &process_cmd($cd23, $lo, $mx23);

        print STDOUT "construct high pool haplotype blocks\n";
        print $lo "construct high pool haplotype blocks\nCMD24:\t";
        unless(defined($hlog{"CMD24:"})) {$hlog{"CMD24:"} = 0;}
        my $mx24 = $hlog{"CMD24:"};
        my $cd24 = "$HAPCUT2 --fragments $H_phase --VCF $myvar --output $H_bam_block";
        # HAPCUT2 --fragments /home/user/bsa/H.bam.phase --VCF /home/user/bsa/haplotype_dir/myvar --out /home/user/bsa/haplotype_dir/H.bam_block
        &process_cmd($cd24, $lo, $mx24);

        print STDOUT "construct low pool haplotype blocks\n";
        print $lo "construct low pool haplotype blocks\nCMD25:\t";
        unless(defined($hlog{"CMD25:"})) {$hlog{"CMD25:"} = 0;}
        my $mx25 = $hlog{"CMD25:"};
        my $cd25 ="$HAPCUT2 --fragments $L_phase --VCF $myvar --output $L_bam_block";
        # HAPCUT2 --fragments /home/user/bsa/L.bam.phase --VCF /home/user/bsa/haplotype_dir/myvar --out /home/user/bsa/haplotype_dir/L.bam_block
        &process_cmd($cd25, $lo, $mx25);

        print STDOUT "sort pollen parent haplotype blocks\n";
        print $lo "sorting pollen parent haplotype blocks\nCMD26:\t";
        unless(defined($hlog{"CMD26:"})) {$hlog{"CMD26:"} = 0;}
        my $mx26 = $hlog{"CMD26:"};
        my $cd26 = "perl $get_hap $P_bam_block | $judge_sort -> $P_block";
        # perl /home/user/BSATOS2/scripts/get_hap.pl /home/user/bsa/haplotype_dir/P.bam_block | /home/user/BSATOS2/scripts/judge_and_sort_haplotype.pl -> /home/user/bsa/haplotype_dir/P_block
        &process_cmd($cd26, $lo, $mx26);

        print STDOUT "sort maternal parent haplotype blocks\n";
        print $lo "sorting maternal parent haplotype blocks\nCMD27:\t";
        unless(defined($hlog{"CMD27:"})) {$hlog{"CMD27:"} = 0;}
        my $mx27 = $hlog{"CMD27:"};
        my $cd27 = "perl $get_hap $M_bam_block | $judge_sort -> $M_block";
        # perl /home/user/BSATOS2/scripts/get_hap.pl /home/user/bsa/haplotype_dir/M.bam_block | /home/user/BSATOS2/scripts/judge_and_sort_haplotype.pl -> /home/user/bsa/haplotype_dir/M_block
        &process_cmd($cd27, $lo, $mx27);

        print STDOUT "sort high pool haplotype blocks\n";
        print $lo "sorting high pool haplotype blocks\nCMD28:\t";
        unless(defined($hlog{"CMD28:"})) {$hlog{"CMD28:"} = 0;}
        my $mx28 = $hlog{"CMD28:"};
        my $cd28 = "perl $get_hap $H_bam_block | $judge_sort -> $H_block";
        # perl /home/user/BSATOS2/scripts/get_hap.pl /home/user/bsa/haplotype_dir/H.bam_block | /home/user/BSATOS2/scripts/judge_and_sort_haplotype.pl -> /home/user/bsa/haplotype_dir/H_block
        &process_cmd($cd28, $lo, $mx28);

        print STDOUT "sort low pool haplotype blocks\n";
        print $lo "sorting low pool haplotype blocks\nCMD29:\t";
        unless(defined($hlog{"CMD29:"})) {$hlog{"CMD29:"} = 0;}
        my $mx29 = $hlog{"CMD29:"};
        my $cd29 = "perl $get_hap $L_bam_block | $judge_sort -> $L_block";
        # perl /home/user/BSATOS2/scripts/get_hap.pl /home/user/bsa/haplotype_dir/L.bam_block | /home/user/BSATOS2/scripts/judge_and_sort_haplotype.pl -> /home/user/bsa/haplotype_dir/L_block
        &process_cmd($cd29, $lo, $mx29);
    }

    print STDOUT "get all the haplotype loci\n";
    print $lo "get all the haplotype loci\nCMD30:\t";
    unless(defined($hlog{"CMD30:"})) {$hlog{"CMD30:"} = 0;}
    my $mx30 = $hlog{"CMD30:"};
    my $cd30 = "perl $merge_haplotype --Pb $P_block --Mb $M_block --Hb $H_block --Lb $L_block > $merge_list";
    # perl /home/user/BSATOS2/scripts/merge_haplotype.pl --Pb /home/user/bsa/haplotype_dir/P_block --Mb /home/user/bsa/haplotype_dir/M_block --Hb /home/user/bsa/haplotype_dir/H_block --Lb /home/user/bsa/haplotype_dir/L_block > /home/user/bsa/haplotype_dir/merge.list
    &process_cmd($cd30, $lo, $mx30);

    print STDOUT "merge all the haplptype blocks files\n";
    print $lo "merge all the haplotype blocks files\nCMD31:\t";
    unless(defined($hlog{"CMD31:"})) {$hlog{"CMD31:"} = 0;}
    my $mx31 = $hlog{"CMD31:"};
    my $cd31 = "$filter -k cn -A A -B E -C E -D E -E E $merge_list $P_block $M_block $H_block $L_block > $merged_block_filter && cut -f 1-2,5-9,14-16,21-23,28-30 $merged_block_filter > $merged_block";
    # /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E -C E -D E -E E /home/user/bsa/haplotype_dir/merge.list /home/user/bsa/haplotype_dir/P_block /home/user/bsa/haplotype_dir/M_block /home/user/bsa/haplotype_dir/H_block /home/user/bsa/haplotype_dir/L_block > /home/user/bsa/haplotype_dir/merged.block.filter
    # cut -f 1-2,5-9,14-16,21-23,28-30 /home/user/bsa/haplotype_dir/merged.block.filter > /home/user/bsa/haplotype_dir/merged.block
    &process_cmd($cd31, $lo, $mx31);

    print STDOUT "stitch and compare the haplotype blocks\n";
    print $lo "stitch and compare the haplotype blocks\nCMD32:\t";
    unless(defined($hlog{"CMD32:"})) {$hlog{"CMD32:"} = 0;}
    my $mx32 = $hlog{"CMD32:"};
    ## my $cd32 = "perl $fill_homo $merged_block $myvar | perl $cmp1 - | perl $cmp2 - | perl $cmp1 - | perl $cmp2 - | perl $cmp1 - | perl $add_header -> $haplotype_block";
    ## perl /home/user/BSATOS2/scripts/fill_homo.pl /home/user/bsa/haplotype_dir/merged.block /home/user/bsa/haplotype_dir/myvar | perl /home/user/BSATOS2/scripts/cmp_infer_block_step1.pl - | perl /home/user/BSATOS2/scripts/cmp_infer_block_step2.pl - | perl /home/user/BSATOS2/scripts/cmp_infer_block_step1.pl - | perl /home/user/BSATOS2/scripts/cmp_infer_block_step2.pl - | perl /home/user/BSATOS2/scripts/cmp_infer_block_step1.pl - | perl /home/user/BSATOS2/scripts/add_header.pl -> /home/user/bsa/haplotype_dir/haplotype.block
    # my $cd32 = "perl $fill_homo $merged_block $myvar > $mytmp1; perl $cmp1v2 $mytmp1 > $mytmp2; perl $cmp2v2 $mytmp2 > $mytmp3; perl $cmp1v2 $mytmp3 > $mytmp4; perl $cmp2v2 $mytmp4 > $mytmp5; perl $cmp1v2 $mytmp5 > $mytmp6; perl $add_header $mytmp6 > $haplotype_block";
    my $cd32 = "$fill_homo $merged_block $myvar > $mytmp1; $cmp1v2 $mytmp1 > $mytmp2; $cmp2v2 $mytmp2 > $mytmp3; $cmp1v2 $mytmp3 > $mytmp4; $cmp2v2 $mytmp4 > $mytmp5; $cmp1v2 $mytmp5 > $mytmp6; $add_header > $haplotype_block && cat $mytmp6 >> $haplotype_block";
    # /home/user/BSATOS2/scripts/fill_homo.pl /home/user/bsa/haplotype_dir/merged.block /home/user/bsa/haplotype_dir/myvar > /home/user/bsa/haplotype_dir/mytmp1;
    # /home/user/BSATOS2/cpp/cmp_infer_block_step1_v2 /home/user/bsa/haplotype_dir/mytmp1 > /home/user/bsa/haplotype_dir/mytmp2;
    # /home/user/BSATOS2/cpp/cmp_infer_block_step2_v2 /home/user/bsa/haplotype_dir/mytmp2 > /home/user/bsa/haplotype_dir/mytmp3;
    # /home/user/BSATOS2/cpp/cmp_infer_block_step1_v2 /home/user/bsa/haplotype_dir/mytmp3 > /home/user/bsa/haplotype_dir/mytmp4;
    # /home/user/BSATOS2/cpp/cmp_infer_block_step2_v2 /home/user/bsa/haplotype_dir/mytmp4 > /home/user/bsa/haplotype_dir/mytmp5;
    # /home/user/BSATOS2/cpp/cmp_infer_block_step1_v2 /home/user/bsa/haplotype_dir/mytmp5 > /home/user/bsa/haplotype_dir/mytmp6;
    # /home/user/BSATOS2/scripts/add_header.pl > /home/user/bsa/haplotype_dir/haplotype.block
    # cat /home/user/bsa/haplotype_dir/mytmp6 >> /home/user/bsa/haplotype_dir/haplotype.block
    &process_cmd($cd32, $lo, $mx32);

    print STDOUT "separate the haplotype blocks from pollen and maternal parents\n";
    print $lo "separate the haplotype blocks from pollen and maternal parents\nCMD33:\t";
    unless(defined($hlog{"CMD33:"})) {$hlog{"CMD33:"} = 0;}
    my $mx33 = $hlog{"CMD33:"};
    my $cd33 = "perl $classify_haplotype $haplotype_block $P_hap $M_hap";
    # perl /home/user/BSATOS2/scripts/classify_haplotype.pl /home/user/bsa/haplotype_dir/haplotype.block /home/user/bsa/haplotype_dir/P.haplotype /home/user/bsa/haplotype_dir/M.haplotype
    &process_cmd($cd33, $lo, $mx33);

    print STDOUT "sort and compare the haplptype blocks from pollen and maternal parents\n";
    print $lo "sort and compare the haplotype blocks from pollen and maternal parents\nCMD34:\t";
    unless(defined($hlog{"CMD34:"})) {$hlog{"CMD34:"} = 0;}
    my $mx34 = $hlog{"CMD34:"};
    my $cd34 = "sort -k 1,1 -k 2,2n $P_hap > $P_hap_bed; sort -k 1,1 -k 2,2n $M_hap > $M_hap_bed; $bedops -i $P_hap_bed $M_hap_bed > $overlap";
    # sort -k 1,1 -k 2,2n /home/user/bsa/haplotype_dir/P.haplotype > /home/user/bsa/haplotype_dir/P_haplotype.bed
    # sort -k 1,1 -k 2,2n /home/user/bsa/haplotype_dir/M.haplotype > /home/user/bsa/haplotype_dir/M_haplotype.bed
    # bedops -i /home/user/bsa/haplotype_dir/P_haplotype.bed /home/user/bsa/haplotype_dir/M_haplotype.bed > /home/user/bsa/haplotype_dir/overlapped.bed
    &process_cmd($cd34, $lo, $mx34);

    print STDOUT "detect crossovers\n";
    print $lo "detect crossovers\nCMD39:\t";
    unless(defined($hlog{"CMD39:"})) {$hlog{"CMD39:"} = 0;}
    my $mx35 = $hlog{"CMD39:"};
    my $cd35 = "perl $ch2bed $haplotype_block > $phase_tmp && $bedtools intersect -wao -a $overlap -b $phase_tmp | perl $detect_crossover -> $sub_hap";
    # perl /home/user/BSATOS2/scripts/ch2bed.pl /home/user/bsa/haplotype_dir/haplotype.block > /home/user/bsa/haplotype_dir/phase_tmp
    # bedtools intersect -wao -a /home/user/bsa/haplotype_dir/overlapped.bed -b /home/user/bsa/haplotype_dir/phase_tmp | perl /home/user/BSATOS2/scripts/detect_crossover.pl -> /home/user/bsa/haplotype_dir/sub_haplotype
    &process_cmd($cd35, $lo, $mx35);

    if(-e $myvar){
        print STDOUT "remove some files\n";
        print $lo "remove some files\nCMD35:\t";
        unless(defined($hlog{"CMD35:"})) {$hlog{"CMD35:"} = 0;}
        my $mx36 = $hlog{"CMD35:"};
        my $cd36 = "rm $myvar $phase_tmp $H_ref $L_ref $P_ref $M_ref $mytmp1 $mytmp2 $mytmp3 $mytmp4 $mytmp5 $mytmp6";
        # rm /home/user/bsa/haplotype_dir/myvar /home/user/bsa/haplotype_dir/phase_tmp
        # /home/user/bsa/haplotype_dir/H.bam_ref /home/user/bsa/haplotype_dir/L.bam_ref /home/user/bsa/haplotype_dir/P.bam_ref /home/user/bsa/haplotype_dir/M.bam_ref
        # /home/user/bsa/haplotype_dir/mytmp1 /home/user/bsa/haplotype_dir/mytmp2 /home/user/bsa/haplotype_dir/mytmp3
        # /home/user/bsa/haplotype_dir/mytmp4 /home/user/bsa/haplotype_dir/mytmp5 /home/user/bsa/haplotype_dir/mytmp6
        &process_cmd($cd36, $lo, $mx36);
    }

    print STDOUT "\nhaplotype module finished!!!\n";
    print $lo "\nhaplotype module finished!!!\n";
    exit(0);
}

###############################################

sub process_cmd {
    my ($cmd, $lk, $hg) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t...............................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t...............................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime = "[".localtime()."]";
        if($ret){
            print $lk "\#die\t$starttime ... $endtime\n";
            print STDOUT "\#die\t$starttime ... $endtime\n";
            die "Error, cmd: < $cmd > died with return value $ret";
        }else{
            print $lk "\#done\t$starttime ... $endtime\n";
            print STDOUT "\#done\t$starttime ... $endtime\n";
        }
        return;
    }
}
