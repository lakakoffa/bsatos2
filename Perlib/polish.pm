#!/usr/bin/env perl
package polish;
BEGIN {my $VERSION = '2.0';}

use Cwd;
use strict;
use Getopt::Long;
use base 'Exporter';
use File::Basename;
use FindBin qw($Bin);
our @EXPORT = qw(runpolish);

my $G_USAGE = "
Usage: bastos2 polish [options]

Options:
    --o       [STR]      name prefix of output directory [polish]
    --p       [FILE]     smoothed curve base on P type loci across genome with haplotype information from afd module [required]
    --m       [FILE]     smoothed curve base on M type loci across genome with haplotype information from afd module [required]
    --pm      [FILE]     smoothed curve base on PM type loci across genome with haplotype information from afd module [required]
    --hap     [FILE]     merged haplotype block file from haplotype module [required]
    --fdr     [FLOAT]    FDR threshold for polishing [0.01]
    --log     [FILE]     polish module log file [polish.log]
    --db      [STR]      database directory of .fa & GTF & GFF files [Malus_domestica]

Statistics Options:
    --sd2     [STR]      statistic caculation method: ed/g/abs [g]
    --w2      [INT]      sliding window size [1000000]
    --fn2     [INT]      batches for smoothing; the smaller the faster, but more memory needed [20]

Outputs:
    P.polished.afd   [FILE]     smoothed curve with haplotype information based on polished read counts of P type loci
    M.polished.afd   [FILE]     smoothed curve with haplotype information based on polished read counts of M type loci
    PM.polished.afd  [FILE]     smoothed curve with haplotype information based on polished read counts of PM type loci
    p.igv            [FILE]     P IGV format file for Integrative Genomics Viewer
    m.igv            [FILE]     M IGV format file for Integrative Genomics Viewer
    pm.igv           [FILE]     PM IGV format file for Integrative Genomics Viewer

Example:
    bsatos2 polish --o polish --sd2 g --p P.AFD --m M.AFD --pm PM.AFD --hap haplotype.block
";


sub runpolish {
    my $p = undef;       my $m = undef;       my $pm = undef;       my $hap = undef;    my $g_R = undef;
    my $sd2 = undef;     my $o = undef;       my $help = undef;     my $s = undef;      my $fdr = undef;
    my $th2 = undef;     my $w2 = undef;      my $m2 = undef;       my $md = undef;     my $db = undef;
    my $fn2 = undef;     my $log = undef;

    GetOptions (
        "sd2=s" => \$sd2,   "p=s" => \$p,       "m=s" => \$m,       "pm=s" => \$pm,
        "hap=s" => \$hap,   "o=s" => \$o,       "s=s" => \$s,       "w2=i" => \$w2,
        "m2=s" => \$m2,     "th2=s" => \$th2,   "fdr=f" => \$fdr,   "fn2=i" => \$fn2,
        "log=s" => \$log,   "db=s" => \$db,     "help" => \$help)
    or die("$G_USAGE");
    die "$G_USAGE" if ($help);
    die "$G_USAGE" if (!defined $o);

    my $para_polish_file = "$FindBin::Bin/Python/src/para_polish_file";
    if (-e $para_polish_file){
        open PARAPOLISHFILE, "$para_polish_file" or die $!;
        while(<PARAPOLISHFILE>){
            chomp;
            my @para = split /\t/, $_;
            if (@para[0] =~ /^o\b/ && @para[1] =~ /\w/) {$o = @para[1];}
            if (@para[0] =~ /^p\b/ && @para[1] =~ /\w/) {$p = @para[1];}
            if (@para[0] =~ /^m\b/ && @para[1] =~ /\w/) {$m = @para[1];}
            if (@para[0] =~ /^pm\b/ && @para[1] =~ /\w/) {$pm = @para[1];}
            if (@para[0] =~ /^fdr\b/ && @para[1] =~ /\w/) {$fdr = @para[1];}
            if (@para[0] =~ /^hap\b/ && @para[1] =~ /\w/) {$hap = @para[1];}
            if (@para[0] =~ /^sd2\b/ && @para[1] =~ /\w/) {$sd2 = @para[1];}
            if (@para[0] =~ /^w2\b/ && @para[1] =~ /\w/) {$w2 = @para[1];}
            if (@para[0] =~ /^fn2\b/ && @para[1] =~ /\w/) {$fn2 = @para[1];}
            # if (@para[0] =~ /^db\b/ && @para[1] =~ /\w/) {$db = @para[1];}
        }
        close PARAPOLISHFILE;
    }

    unless(defined($o)) {$o = "polish";}
    unless(defined($fdr)) {$fdr = "0.01";}
    unless(defined($w2)) {$w2 = 1000000;}
    unless(defined($m2)) {$m2 = "N";}
    unless(defined($fn2)) {$fn2 = 20;}
    if($m2 eq "Y") {$md = 1;} else {$md = 0;}
    if(!defined($sd2) || $sd2 eq "g" ) {$g_R = "$FindBin::Bin/R/g.R";}      # /home/user/BSATOS2/R/g.R
    if($sd2 eq "ed") {$g_R = "$FindBin::Bin/R/ed.R";}                       # /home/user/BSATOS2/R/ed.R
    if($sd2 eq "abs") {$g_R = "$FindBin::Bin/R/abs.R";}                     # /home/user/BSATOS2/R/abs.R

    my $p_afd = basename($p);                                               # P.AFD
    my $m_afd = basename($m);                                               # M.AFD
    my $pm_afd = basename($pm);                                             # PM.AFD
    my $work_dir = getcwd;                                                  # /home/user/bsa
    my $dir = $work_dir."/".$o."_dir";                                      # /home/user/bsa/polish_dir
    my $polish = "$FindBin::Bin/scripts/polish_qtl_region.pl";              # /home/user/BSATOS2/scripts/polish_qtl_region.pl
    my $add_afd_header = "$FindBin::Bin/scripts/add_afd_header.pl";         # /home/user/BSATOS2/scripts/add_afd_header.pl
    my $bsa2igv = "$FindBin::Bin/scripts/bsa2igv.pl";                       # /home/user/BSATOS2/scripts/bsa2igv.pl
    my $filter = "$FindBin::Bin/tools/bin/filter";                          # /home/user/BSATOS2/tools/bin/filter
    my $rg = "$FindBin::Bin/tools/bin/rg";                                  # /home/user/BSATOS2/tools/bin/rg
    my $thres = "$FindBin::Bin/R/thres.R";                                  # /home/user/BSATOS2/R/thres.R
    my $p_th = $work_dir."/".$o."_dir/p.thres";                             # /home/user/bsa/polish_dir/p.thres
    my $m_th = $work_dir."/".$o."_dir/m.thres";                             # /home/user/bsa/polish_dir/m.thres
    my $pm_th = $work_dir."/".$o."_dir/pm.thres";                           # /home/user/bsa/polish_dir/pm.thres
    my $p_afd_cal_out = $dir."/".$p_afd.".cal.out";                         # /home/user/bsa/polish_dir/P.AFD.cal.out
    my $m_afd_cal_out = $dir."/".$m_afd.".cal.out";                         # /home/user/bsa/polish_dir/M.AFD.cal.out
    my $pm_afd_cal_out = $dir."/".$pm_afd.".cal.out";                       # /home/user/bsa/polish_dir/PM.AFD.cal.out
    my $p_polished_afd = $dir."/P.polished.afd";                            # /home/user/bsa/polish_dir/P.polished.afd
    my $m_polished_afd = $dir."/M.polished.afd";                            # /home/user/bsa/polish_dir/M.polished.afd
    my $pm_polished_afd = $dir."/PM.polished.afd";                          # /home/user/bsa/polish_dir/PM.polished.afd
    my $p_afd_polish = $dir."/".$p_afd.".polish";                           # /home/user/bsa/polish_dir/P.AFD.polish
    my $m_afd_polish = $dir."/".$m_afd.".polish";                           # /home/user/bsa/polish_dir/M.AFD.polish
    my $pm_afd_polish = $dir."/".$pm_afd.".polish";                         # /home/user/bsa/polish_dir/PM.AFD.polish
    my $p_igv = $dir."/p.igv";                                              # /home/user/bsa/polish_dir/p.igv
    my $m_igv = $dir."/m.igv";                                              # /home/user/bsa/polish_dir/m.igv
    my $pm_igv = $dir."/pm.igv";                                            # /home/user/bsa/polish_dir/pm.igv

    unless(defined($log)) {$log = $work_dir."/".$o.".log";}                   # /home/user/bsa/polish_dir/polish.log
    # unless(defined($db)) {$db = "Malus_domestica";}

###############################################################

    my %hlog = ();
    if(-e $log){
        open LOD, "<$log";
        my $count = 0;
        while(<LOD>){
            chomp;
            my @sp = split /\t/, $_;
            if($sp[2] eq "\#done") {$hlog{$sp[0]} = 1;}
            if($sp[2] eq "\#die") {$hlog{$sp[0]} = 0;}
            if($_ =~ /finish/) {$count++;}
        }
        if($count != 0){
            print "
                Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
                Version: 2.0
                The fourth module: polish
                polish      polish candidate QTLs region and remove noisy makers based on haplotype information\n\n
                This module has been finished!!!!!\n\n";
            exit(0);
        }
    }
    open (my $lo, ">$log") or die $!;

    print STDOUT "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The fifth module: polish
        polish      polish candidate QTLs region and remove nosiy makers based on haplotype information\n\n
        ██████╗ ███████╗ █████╗ ████████╗ ██████╗ ███████╗██████╗
        ██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██╔═══██╗██╔════╝╚════██╗
        ██████╔╝███████╗███████║   ██║   ██║   ██║███████╗ █████╔╝
        ██╔══██╗╚════██║██╔══██║   ██║   ██║   ██║╚════██║██╔═══╝
        ██████╔╝███████║██║  ██║   ██║   ╚██████╔╝███████║███████╗
        ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝╚══════╝\n\n";

    print $lo "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The fifth module: polish
        polish      polish candidate QTLs region and remove noisy makers based on haplotype information\n\n";


#############################################################

    unless(-e $dir){
        print STDOUT "create result dir\n";
        print $lo "create result dir\nCMD0:\t";
        unless(defined($hlog{"CMD0:"})) {$hlog{"CMD0:"} = 0;}
        my $mx0 = $hlog{"CMD0:"};
        my $cd1 = "mkdir $dir";   # mkdir /home/user/bsa/polish_dir
        &process_cmd($cd1, $lo, $mx0);
    }

    print STDOUT "calculate threshold of P\n";
    print $lo "calculate threshold of P\nCMD B1:\t";
    my $cdb1 = "Rscript $thres $p $fdr $p_th";
    # Rscript /home/user/BSATOS2/R/thres.R afd_dir/P.AFD 0.01 /home/user/bsa/polish_dir/p.thres
    unless(defined($hlog{"CMD B1:"})) {$hlog{"CMD B1:"} = 0;}
    my $mxb1 = $hlog{"CMD B1:"};
    &process_cmd($cdb1, $lo, $mxb1);

    print STDOUT "calculate threshold of M\n";
    print $lo "calculate threshold of M\nCMD B2:\t";
    my $cdb2 = "Rscript $thres $m $fdr $m_th";
    # Rscript /home/user/BSATOS2/R/thres.R afd_dir/M.AFD 0.01 /home/user/bsa/polish_dir/m.thres
    unless(defined($hlog{"CMD B2:"})) {$hlog{"CMD B2:"} = 0;}
    my $mxb2 = $hlog{"CMD B2:"};
    &process_cmd($cdb2, $lo, $mxb2);

    print STDOUT "calculate threshold of PM\n";
    print $lo "calculate threshold of PM\nCMD B3:\t";
    my $cdb3 = "Rscript $thres $pm $fdr $pm_th";
    # Rscript /home/user/BSATOS2/R/thres.R afd_dir/PM.AFD 0.01 /home/user/bsa/polish_dir/pm.thres
    unless(defined($hlog{"CMD B3:"})) {$hlog{"CMD B3:"} = 0;}
    my $mxb3 = $hlog{"CMD B3:"};
    &process_cmd($cdb3, $lo, $mxb3);

    my @t1 = split /\n/, `cat $p_th`;
    my @t2 = split /\n/, `cat $m_th`;
    my @t3 = split /\n/, `cat $pm_th`;

    # open(PTH, $p_th) or die "$!";
    # my @t1 = <PTH>;
    # open(MTH, $m_th) or die "$!";
    # my @t2 = <MTH>;
    # open(PMTH, $pm_th) or die "$!";
    # my @t3 = <PMTH>;

    print STDOUT "polish P loci data\n";
    print $lo "polish P loci data\nCMD1:\t";
    my $cd2 = "perl $polish $p $t1[0] $t1[1] $t1[2] $t1[3] > $p_afd_polish";
    # perl /home/user/BSATOS2/scripts/polish_qtl_region.pl afd_dir/P.AFD 3.161538 5.179168 2.553588 2.864549 > /home/user/bsa/polish_dir/P.AFD.polish
    unless(defined($hlog{"CMD1:"})) {$hlog{"CMD1:"} = 0;}
    my $mx1 = $hlog{"CMD1:"};
    &process_cmd($cd2, $lo, $mx1);

    print STDOUT "polish M loci data\n";
    print $lo "polish M loci data\nCMD2:\t";
    my $cd3 = "perl $polish $m $t2[0] $t2[1] $t2[2] $t2[3] > $m_afd_polish";
    # perl /home/user/BSATOS2/scripts/polish_qtl_region.pl afd_dir/M.AFD 4.632887 4.658972 1.823091 2.878615 > /home/user/bsa/polish_dir/M.AFD.polish
    unless(defined($hlog{"CMD2:"})) {$hlog{"CMD2:"} = 0;}
    my $mx2 = $hlog{"CMD2:"};
    &process_cmd($cd3, $lo, $mx2);

    print STDOUT "polish PM loci data\n";
    print $lo "polish PM loci data\nCMD3:\t";
    my $cd4= "perl $polish $pm $t3[0] $t3[1] $t3[2] $t3[3] > $pm_afd_polish";
    # perl /home/user/BSATOS2/scripts/polish_qtl_region.pl afd_dir/PM.AFD 4.550595 4.650005 4.703816 4.785926 > /home/user/bsa/polish_dir/PM.AFD.polish
    unless(defined($hlog{"CMD3:"})) {$hlog{"CMD3:"} = 0;}
    my $mx3 = $hlog{"CMD3:"};
    &process_cmd($cd4, $lo, $mx3);

    my $idx = 3;
    my @chrs = &get_chr($p, $rg);
    foreach my $chr (sort @chrs){
        $idx++;
        my $cdn1 = "CMD".$idx.":";
        print STDOUT "smooth P loci in $chr\n";
        print $lo "smooth the P loci in $chr\n$cdn1\t";
        unless(defined($hlog{$cdn1})) {$hlog{$cdn1} = 0;}
        my $mxc1 = $hlog{$cdn1};
        my $pc = "Rscript $g_R $p_afd_polish $chr $p_afd $w2 $md $fn2";
        # Rscript /home/user/BSATOS2/R/g.R /home/user/bsa/polish_dir/P.AFD.polish chr06 P.AFD 1000000 1 20
        my $pc2 = "Rscript $g_R $p_afd_polish $chr $p_afd $w2 0 $fn2";
        &process_cmd2($pc, $lo, $mxc1, $pc2);

        $idx++;
        my $cdn2= "CMD".$idx.":";
        print STDOUT "smooth M loci in $chr\n";
        print $lo "smooth M loci in $chr\n$cdn2\t";
        unless(defined($hlog{$cdn2})) {$hlog{$cdn2} = 0;}
        my $mxc2 = $hlog{$cdn2};
        my $mc = "Rscript $g_R $m_afd_polish $chr $m_afd $w2 $md $fn2";
        # Rscript /home/user/BSATOS2/R/g.R /home/user/bsa/polish_dir/M.AFD.polish chr06 M.AFD 1000000 1 20
        my $mc2 = "Rscript $g_R $m_afd_polish $chr $m_afd $w2 0 $fn2";
        &process_cmd2($mc, $lo, $mxc2, $mc2);

        $idx++;
        my $cdn3= "CMD".$idx.":";
        print STDOUT "smooth PM loci in $chr\n";
        print $lo "smooth PM loci in $chr\n$cdn3\t";
        unless(defined($hlog{$cdn3})) {$hlog{$cdn3} = 0;}
        my $mxc3 = $hlog{$cdn3};
        my $mc = "Rscript $g_R $pm_afd_polish $chr $pm_afd $w2 $md $fn2";
        # Rscript /home/user/BSATOS2/R/g.R /home/user/bsa/polish_dir/PM.AFD.polish chr06 PM.AFD 1000000 1 20
        my $mc2 = "Rscript $g_R $pm_afd_polish $chr $pm_afd $w2 0 $fn2";
        &process_cmd2($mc, $lo, $mxc3, $mc2);
    }

    $idx++;
    my $cdn4= "CMD".$idx.":";
    print STDOUT "merge smoothed P loci\n";
    print $lo "merge smoothed P loci\n$cdn4\t";
    unless(defined($hlog{$cdn4})) {$hlog{$cdn4} = 0;}
    my $mxc4 = $hlog{$cdn4};
    my $cmd4 = "cat $work_dir/\*_$p_afd\_afd >> $p_afd_cal_out";
    # cat /home/user/bsa/*_P.AFD_afd >> /home/user/bsa/polish_dir/P.AFD.cal.out
    &process_cmd($cmd4, $lo, $mxc4);

    $idx++;
    my $cdn5= "CMD".$idx.":";
    print STDOUT "merge smoothed M loci\n";
    print $lo "merge smoothed M loci\n$cdn5\t";
    unless(defined($hlog{$cdn5})) {$hlog{$cdn5} = 0;}
    my $mxc5 = $hlog{$cdn5};
    my $cmd5 = "cat $work_dir/\*_$m_afd\_afd >> $m_afd_cal_out";
    # cat /home/user/bsa/*_M.AFD_afd >> /home/user/bsa/polish_dir/M.AFD.cal.out
    &process_cmd($cmd5, $lo, $mxc5);

    $idx++;
    my $cdn6= "CMD".$idx.":";
    print STDOUT "merge smoothed PM loci\n";
    print $lo "merge smoothed PM loci\n$cdn6\t";
    unless(defined($hlog{$cdn6})) {$hlog{$cdn6} = 0;}
    my $mxc6 = $hlog{$cdn6};
    my $cmd6 = "cat $work_dir/\*_$pm_afd\_afd >> $pm_afd_cal_out";
    # cat /home/user/bsa/*_PM.AFD_afd >> /home/user/bsa/polish_dir/PM.AFD.cal.out
    &process_cmd($cmd6, $lo, $mxc6);

    $idx++;
    my $cdn7= "CMD".$idx.":";
    print STDOUT "adding haplotype block information to smoothed P loci\n";
    print $lo "adding haplotype block information to smoothed P loci\n$cdn7\t";
    unless(defined($hlog{$cdn7})) {$hlog{$cdn7} = 0;}
    my $mxc7 = $hlog{$cdn7};
    # my $cmd7 = "$filter -k cn -A A -B E $p_afd_cal_out $hap | $add_afd_header -> $p_polished_afd";
    # /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/polish_dir/P.AFD.cal.out haplotype_dir/haplotype.block | /home/user/BSATOS2/scripts/add_afd_header.pl -> /home/user/bsa/polish_dir/P.polished.afd
    my $cmd7 = "$add_afd_header > $p_polished_afd && $filter -k cn -A A -B E $p_afd_cal_out $hap >> $p_polished_afd";
    # /home/user/BSATOS2/scripts/add_afd_header.pl > /home/user/bsa/polish_dir/P.polished.afd && /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/polish_dir/P.AFD.cal.out haplotype_dir/haplotype.block >> /home/user/bsa/polish_dir/P.polished.afd
    &process_cmd($cmd7, $lo, $mxc7);

    $idx++;
    my $cdn8= "CMD".$idx.":";
    print STDOUT "adding haplotype information to smoothed M loci\n";
    print $lo "adding haplotype information to smoothed M loci\n$cdn8\t";
    my $mxc8 = $hlog{$cdn8};
    # my $cmd8 = "$filter -k cn -A A -B E $m_afd_cal_out $hap | $add_afd_header -> $m_polished_afd";
    # /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/polish_dir/M.AFD.cal.out haplotype_dir/haplotype.block | /home/user/BSATOS2/scripts/add_afd_header.pl -> /home/user/bsa/polish_dir/M.polished.afd
    my $cmd8 = "$add_afd_header > $m_polished_afd && $filter -k cn -A A -B E $m_afd_cal_out $hap >> $m_polished_afd";
    # /home/user/BSATOS2/scripts/add_afd_header.pl > /home/user/bsa/polish_dir/M.polished.afd && /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/polish_dir/M.AFD.cal.out haplotype_dir/haplotype.block >> /home/user/bsa/polish_dir/M.polished.afd
    unless(defined($hlog{$cdn8})) {$hlog{$cdn8} = 0;}
    &process_cmd($cmd8, $lo, $mxc8);

    $idx++;
    my $cdn9= "CMD".$idx.":";
    print STDOUT "adding haplotype information to smoothed PM loci\n";
    print $lo "adding haplotype information to smooothed PM loci\n$cdn9\t";
    unless(defined($hlog{$cdn9})) {$hlog{$cdn9} = 0;}
    my $mxc9 = $hlog{$cdn9};
    # my $cmd9 = "$filter -k cn -A A -B E $pm_afd_cal_out $hap | $add_afd_header -> $pm_polished_afd";
    # /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/polish_dir/PM.AFD.cal.out haplotype_dir/haplotype.block | /home/user/BSATOS2/scripts/add_afd_header.pl -> /home/user/bsa/polish_dir/PM.polished.afd
    my $cmd9 = "$add_afd_header > $pm_polished_afd && $filter -k cn -A A -B E $pm_afd_cal_out $hap >> $pm_polished_afd";
    # /home/user/BSATOS2/scripts/add_afd_header.pl > /home/user/bsa/polish_dir/PM.polished.afd && /home/user/BSATOS2/tools/bin/filter -k cn -A A -B E /home/user/bsa/polish_dir/PM.AFD.cal.out haplotype_dir/haplotype.block >> /home/user/bsa/polish_dir/PM.polished.afd
    &process_cmd($cmd9, $lo);

    $idx++;
    my $cdn10= "CMD".$idx.":";
    print STDOUT "convert P afd file to IGV format file\n";
    print $lo "convert P afd file to IGV format file\n$cdn10\t";
    unless(defined($hlog{$cdn10})) {$hlog{$cdn10} = 0;}
    my $mxc10 = $hlog{$cdn10};
    my $cmd10 = "$bsa2igv -i P $p_polished_afd > $p_igv";
    # /home/user/BSATOS2/scripts/bsa2igv.pl -i P /home/user/bsa/polish_dir/P.polished.afd > /home/user/bsa/polish_dir/p.igv
    &process_cmd($cmd10, $lo, $mxc10);

    $idx++;
    my $cdn11= "CMD".$idx.":";
    print STDOUT "convert M afd file to IGV format file\n";
    print $lo "convert M afd file to IGV format file\nCMD$cdn11\t";
    unless(defined($hlog{$cdn11})) {$hlog{$cdn11} = 0;}
    my $mxc11 = $hlog{$cdn11};
    my $cmd11 = "$bsa2igv -i M $m_polished_afd > $m_igv";
    # /home/user/BSATOS2/scripts/bsa2igv.pl -i M /home/user/bsa/polish_dir/M.polished.afd > /home/user/bsa/polish_dir/m.igv
    &process_cmd($cmd11, $lo, $mxc11);

    $idx++;
    my $cdn12= "CMD".$idx.":";
    print STDOUT "convert PM afd file to IGV format file\n";
    print $lo "convert PM afd file to IGV format file\n$cdn12\t";
    unless(defined($hlog{$cdn12})) {$hlog{$cdn12} = 0;}
    my $mxc12 = $hlog{$cdn12};
    my $cmd12 = "$bsa2igv -i PM $pm_polished_afd > $pm_igv";
    # /home/user/BSATOS2/scripts/bsa2igv.pl -i PM /home/user/bsa/polish_dir/PM.polished.afd > /home/user/bsa/polish_dir/pm.igv
    &process_cmd($cmd12, $lo, $mxc12);

    $idx++;
    my $cdn13= "CMD".$idx.":";
    print STDOUT "move all the files to result dir\n";
    print $lo "move all the files to result dir\nCMD$cdn13\t";
    unless(defined($hlog{$cdn13})) {$hlog{$cdn13} = 0;}
    my $mxc13 = $hlog{$cdn13};
    my $cmd13 = "rm $work_dir/\*AFD_afd";
    # rm /home/user/bsa/*AFD_afd
    &process_cmd($cmd13, $lo, $mxc13);

    print STDOUT "\npolish module finished!!!\n";
    print $lo "\npolish module finished!!!\n";
    exit(0);
}


####################################################################

sub get_chr {
    my @para = @_;
    # my (%hash, @line, @chrs);
    # open LOD, "<$para[0]";
    # while(<LOD>){
    #     chomp;
    #     if($_ =~ /#/) {next;}
    #     @line = split /\t/, $_;     # CHROM	POS	H_REF	H_ALT	L_REF	L_ALT
    #     $hash{$line[0]}++;     # 以染色体号为键, 增加计数
    # }
    # @chrs = keys %hash;
    # return(@chrs);

    my $chrs = `cut -f1 $para[0] | $para[1] -vN "#" | uniq`;
    chomp($chrs);
    my @chrs = split /\n/, $chrs;
    return(@chrs);
}

####################################################################

sub process_cmd {
    my ($cmd, $lk, $hg) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t...............................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t...............................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime = "[".localtime()."]";
        if ($ret) {
            print $lk "\#die\t$starttime ... $endtime\n";
            print STDOUT "\#die\t$starttime ... $endtime\n";
            die "Error, cmd: < $cmd > died with return value $ret";
        }else{
            print $lk "\#done\t$starttime ... $endtime\n";
            print STDOUT "\#done\t$starttime ... $endtime\n";
        }
        return;
    }
}

####################################################################

sub process_cmd2 {
    my ($cmd, $lk, $hg, $cmd2) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t...............................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t...............................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime1 = "[".localtime()."]";
        if ($ret) {
            my $ret2 = system($cmd2);
            my $endtime2 = "[".localtime()."]";
            print $lk "\#failed and try other method\t$starttime ... $endtime2\n";
            print STDOUT "\#failed and try other method\t$starttime ... $endtime2\n";
            print STDOUT "$cmd2\t...............................";
            print $lk "$cmd2\t";
            if ($ret2) {
                print $lk "\#die\t$starttime ... $endtime2\n";
                print STDOUT "\#die\t$starttime ... $endtime2\n";
                die "Error, cmd: < $cmd2 > died with return value $ret";
            }else{
                print $lk "\#done\t$starttime ... $endtime2\n";
                print STDOUT "\#done\t$starttime ... $endtime2\n";
            }
        }else{
            print $lk "\#done\t$starttime ... $endtime1\n";
            print STDOUT "\#done\t$starttime ... $endtime1\n";
        }
        return;
    }
}
