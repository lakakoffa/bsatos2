#!/usr/bin/env perl
package prep;
BEGIN {my $VERSION = '2.0';}

use Cwd;
use strict;
use Getopt::Long;
use base 'Exporter';
use FindBin qw($Bin);
our @EXPORT = qw(runprep);

my $G_USAGE = "
Usage: bastos2 prep [options]

Options:
    --o     [STR]     output directory name prefix [prep]
    --r     [FILE]    reference genome (fasta format) [Malus_domestica.fa]
    --hf1   [FILE]    paired-end1 fastq file from high extreme pool  (FASTQ format; Not required if pre-aligned BAM files is provided)
    --hf2   [FILE]    paired-end2 fastq file from high extreme pool  (FASTQ format; Not required if pre-aligned BAM files is provided)
    --lf1   [FILE]    paired-end1 fastq file from low extreme pool   (FASTQ format; Not required if pre-aligned BAM files is provided)
    --lf2   [FILE]    paired-end2 fastq file from low extreme pool   (FASTQ format; Not required if pre-aligned BAM files is provided)
    --hb    [FILE]    pre-aligned bam file from high extreme pool    (Not required if --hf1 & --hf2 is provided)
    --lb    [FILE]    pre-aligned bam file from low extreme pool     (Not required if --lf1 & --lf2 is provided)
    --gp    [FILE]    P file from prepar step
    --gm    [FILE]    M file from prepar step
    --gpm   [FILE]    PM file from prepar step
    --log   [FILE]    prep module log file [prep.log]
    --db    [STR]     database directory of .fa & GTF & GFF files [Malus_domestica]

Aligment Options:
    --t2    [INT]     number of threads [1]

SNV Calling Options:
    --aq2   [INT]     skip alignments with mapQ smaller than [10]

SNV filtering Options:
    --d2    [INT]      average sequencing coverage [10]
    --sq2   [INT]     skip SNVs with phred-scaled quality smaller than [10]
    --pn    [INT]     reads counts of minor allele should greater than [3]

Outputs:
    prefix_dir [DIR]
        P.counts   [FILE]   read counts with different alleles from high & low pools in P  type loci
        M.counts   [FILE]   read counts with different alleles from high & low pools in M  type loci
        PM.counts  [FILE]   read counts with different alleles from high & low pools in PM type loci
        summary    [FILE]   summary of P.counts, M.counts and PM.counts file

    File format:
        Chromosome      Position        H_REF       H_ALT       L_REF       L_ALT
        Chr01           11000           10          1           1           10
        .               .               .           .           .           .

        Chromosome: chromosome of markers
        Position  : positon in chromosome of markers
        H_REF     : read counts with REF alleles in high pool
        H_ALT     : read counts with ALT alleles in high pool
        L_REF     : read counts with REF alleles in low pool
        L_ALT     : read counts with ALT alleles in low pool

Example:
    1) Use paired-end fastq files to run BSATOS2
        bsatos2 prep --o prep --r Malus_domestica.fa --hf1 H_1.fastq --hf2 H_2.fastq --lf1 L_1.fastq --lf2 L_2.fastq --gp P_M.p --gm P_M.m --gpm P_M.pm
    2) Use pre-aligned BAM files to run BSATOS2
        bastos2 prep --o prep --r Malus_domestica.fa --hb H.bam --lb L.bam --gp P_M.p --gm P_M.m --gpm P_M.pm
";


sub runprep {
    my $hf1 = undef;    my $hf2 = undef;        my $lf1 = undef;    my $lf2 = undef;      my $hb = undef;     my $lb = undef;
    my $P  = undef;     my $M  = undef;         my $PM = undef;     my $genome = undef;   my $d2 = undef;     my $sq2 = undef;
    my $help = undef;   my $s = undef;          my $t2 = undef;     my $aq2 = undef;      my $pn = undef;     my $log = undef;
    my $db = undef;     my $prefix = undef;

    GetOptions (
        "hf1=s" => \$hf1,    "hf2=s" => \$hf2,     "lf1=s" => \$lf1,        "lf2=s" => \$lf2,
        "hb=s" => \$hb,      "lb=s" => \$lb,       "r=s" => \$genome,       "gp=s" => \$P,
        "gm=s" => \$M,       "gpm=s" => \$PM,      "o=s" => \$prefix,       "s=s" => \$s,
        "t2=i" => \$t2,      "aq2=i" => \$aq2,     "d2=i" => \$d2,          "sq2=i" => \$sq2,
        "pn=i" => \$pn,      "log=s" => \$log,     "db=s" => \$db,          "help" => \$help)
    or die("$G_USAGE");
    die "$G_USAGE" if ($help);
    die "$G_USAGE" if (!defined $prefix);

    my $para_prep_file = "$FindBin::Bin/Python/src/para_prep_file";
    if (-e $para_prep_file){
        open PARAPREPFILE, "$para_prep_file" or die $!;
        while(<PARAPREPFILE>){
            chomp;
            my @para = split /\t/, $_;
            if (@para[0] =~ /^o\b/ && @para[1] =~ /\w/) {$prefix = @para[1];}
            if (@para[0] =~ /^r\b/ && @para[1] =~ /\w/) {$genome = @para[1];}
            if (@para[0] =~ /^hf1\b/ && @para[1] =~ /\w/) {$hf1 = @para[1];}
            if (@para[0] =~ /^hf2\b/ && @para[1] =~ /\w/) {$hf2 = @para[1];}
            if (@para[0] =~ /^lf1\b/ && @para[1] =~ /\w/) {$lf1 = @para[1];}
            if (@para[0] =~ /^lf2\b/ && @para[1] =~ /\w/) {$lf2 = @para[1];}
            if (@para[0] =~ /^hb\b/ && @para[1] =~ /\w/) {$hb = @para[1];}
            if (@para[0] =~ /^lb\b/ && @para[1] =~ /\w/) {$lb = @para[1];}
            if (@para[0] =~ /^gp\b/ && @para[1] =~ /\w/) {$P = @para[1];}
            if (@para[0] =~ /^gm\b/ && @para[1] =~ /\w/) {$M = @para[1];}
            if (@para[0] =~ /^gpm\b/ && @para[1] =~ /\w/) {$PM = @para[1];}
            if (@para[0] =~ /^t2\b/ && @para[1] =~ /\w/) {$t2 = @para[1];}
            if (@para[0] =~ /^aq2\b/ && @para[1] =~ /\w/) {$aq2 = @para[1];}
            if (@para[0] =~ /^sq2\b/ && @para[1] =~ /\w/) {$sq2 = @para[1];}
            if (@para[0] =~ /^d2\b/ && @para[1] =~ /\w/) {$d2 = @para[1];}
            if (@para[0] =~ /^pn\b/ && @para[1] =~ /\w/) {$pn = @para[1];}
        }
        close PARAPREPFILE;
    }

    unless(defined($prefix)) {$prefix = "prep";}
    unless(defined($t2)) {$t2 = 1;}
    unless(defined($aq2)) {$aq2 = 10;}
    unless(defined($sq2)) {$sq2 = 10;}
    unless(defined($d2)) {$d2 = 10;}
    unless(defined($pn)) {$pn = 3;}

    my $minimap2 = "minimap2";
    my $samtools = "$FindBin::Bin/tools/bin/samtools";      # /home/user/BSATOS2/tools/bin/samtools
    my $bcftools = "$FindBin::Bin/tools/bin/bcftools";      # /home/user/BSATOS2/tools/bin/bcftools
    my $filter = "$FindBin::Bin/tools/bin/filter";          # /home/user/BSATOS2/tools/bin/filter
    my $get_counts = "$FindBin::Bin/scripts/get_counts.pl"; # /home/user/BSATOS2/scripts/get_counts.pl
    my $filter_snp = "$FindBin::Bin/scripts/filter.pl";     # /home/user/BSATOS2/scripts/filter.pl
    my $work_dir = getcwd;                                  # /home/user/bsa
    my $hb_bai = $hb.".bai";                                # /home/user/bsa/H.bam.bai
    my $lb_bai = $lb.".bai";                                # /home/user/bsa/L.bam.bai
    my $dir = $work_dir."/".$prefix."_dir";                 # /home/user/bsa/prep_dir
    my $H_sam = $dir."/".$prefix."_H.sam";                  # /home/user/bsa/prep_dir/prep_H.sam
    my $H_bam = $dir."/".$prefix."_H.bam";                  # /home/user/bsa/prep_dir/prep_H.bam
    my $H_srt_bam = $dir."/".$prefix."_H_srt.bam";          # /home/user/bsa/prep_dir/prep_H_srt.bam
    my $H_rm_bam = $dir."/".$prefix."_H_rm.bam";            # /home/user/bsa/prep_dir/prep_H_rm.bam
    my $H_P_snp = $dir."/H_P_snp";                          # /home/user/bsa/prep_dir/H_P_snp
    my $H_M_snp = $dir."/H_M_snp";                          # /home/user/bsa/prep_dir/H_M_snp
    my $H_PM_snp = $dir."/H_PM_snp";                        # /home/user/bsa/prep_dir/H_PM_snp
    my $H_P_counts = $dir."/H_P_counts";                    # /home/user/bsa/prep_dir/H_P_counts
    my $H_M_counts = $dir."/H_M_counts";                    # /home/user/bsa/prep_dir/H_M_counts
    my $H_PM_counts = $dir."/H_PM_counts";                  # /home/user/bsa/prep_dir/H_PM_counts
    my $H_P_counts_sort = $dir."/H_P_counts_sort";          # /home/user/bsa/prep_dir/H_P_counts_sort
    my $H_M_counts_sort = $dir."/H_M_counts_sort";          # /home/user/bsa/prep_dir/H_M_counts_sort
    my $H_PM_counts_sort = $dir."/H_PM_counts_sort";        # /home/user/bsa/prep_dir/H_PM_counts_sort
    my $L_sam = $dir."/". $prefix."_L.sam";                 # /home/user/bsa/prep_dir/prep_L.sam
    my $L_bam = $dir."/". $prefix."_L.bam";                 # /home/user/bsa/prep_dir/prep_L.bam
    my $L_srt_bam = $dir."/".$prefix."_L_srt.bam";          # /home/user/bsa/prep_dir/prep_L_srt.bam
    my $L_rm_bam = $dir."/".$prefix."_L_rm.bam";            # /home/user/bsa/prep_dir/prep_L_rm.bam
    my $L_P_snp = $dir."/L_P_snp";                          # /home/user/bsa/prep_dir/L_P_snp
    my $L_M_snp = $dir."/L_M_snp";                          # /home/user/bsa/prep_dir/L_M_snp
    my $L_PM_snp = $dir."/L_PM_snp";                        # /home/user/bsa/prep_dir/L_PM_snp
    my $L_P_counts = $dir."/L_P_counts";                    # /home/user/bsa/prep_dir/L_P_counts
    my $L_M_counts = $dir."/L_M_counts";                    # /home/user/bsa/prep_dir/L_M_counts
    my $L_PM_counts = $dir."/L_PM_counts";                  # /home/user/bsa/prep_dir/L_PM_counts
    my $L_P_counts_sort = $dir."/L_P_counts_sort";          # /home/user/bsa/prep_dir/L_P_counts_sort
    my $L_M_counts_sort = $dir."/L_M_counts_sort";          # /home/user/bsa/prep_dir/L_M_counts_sort
    my $L_PM_counts_sort = $dir."/L_PM_counts_sort";        # /home/user/bsa/prep_dir/L_PM_counts_sort
    my $P_counts = $dir."/P.counts";                        # /home/user/bsa/prep_dir/P.counts
    my $M_counts = $dir."/M.counts";                        # /home/user/bsa/prep_dir/M.counts
    my $PM_counts = $dir."/PM.counts";                      # /home/user/bsa/prep_dir/PM.counts
    my $sum =  $dir."/summary";                             # /home/user/bsa/prep_dir/summary

    unless(defined($log)) {$log = $work_dir."/".$prefix.".log";}  # /home/user/bsa/prep_dir/prep.log
    unless(defined($db)) {$db = "Malus_domestica";}
    unless(defined($genome)) {
        if (-e "$FindBin::Bin/database/$db/$db.fa"){$genome = "$FindBin::Bin/database/$db/$db.fa";}         # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa
        if (-e "$FindBin::Bin/database/$db/$db.fasta"){$genome = "$FindBin::Bin/database/$db/$db.fasta";}   # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fasta
    }

############################################################################

    open SS, ">>$sum";
    my %hlog = ();
    if(-e $log){
        open LOD, "<$log";
        my $count = 0;
        while(<LOD>){
            chomp;
            my @sp = split /\t/, $_;
            if($sp[2] eq "\#done") {$hlog{$sp[0]} = 1;}
            if($sp[2] eq "\#die") {$hlog{$sp[0]} = 0;}
            if($_ =~ /finish/) {$count++;}
        }
        if($count != 0 ){
            print "
                Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
                Version: 2.0
                The first module: prep
                prep      prepare the pool data\n\n
                This module has been finished!!!!!\n\n";
            exit(0);
        }
    }
    open (my $lo, ">>$log") or die $!;

    print STDOUT "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The second module: prep
        prep        prepare the pool data\n\n
        ██████╗ ███████╗ █████╗ ████████╗ ██████╗ ███████╗██████╗
        ██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██╔═══██╗██╔════╝╚════██╗
        ██████╔╝███████╗███████║   ██║   ██║   ██║███████╗ █████╔╝
        ██╔══██╗╚════██║██╔══██║   ██║   ██║   ██║╚════██║██╔═══╝
        ██████╔╝███████║██║  ██║   ██║   ╚██████╔╝███████║███████╗
        ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝╚══════╝\n\n";

    print $lo "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The second module: prep
        prep        prepare the pool data\n\n";



#############################################################################

    my $mmi = $genome.".mmi";     # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.mmi
    my $fai = $genome.".fai";     # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.fai

    unless(-e $dir){
        print STDOUT "create prep result dir\n";
        print $lo "create prep result dir\nCMD1:\t";
        unless(defined($hlog{"CMD1:"})) {$hlog{"CMD1:"} = 0;}
        my $mx1 = $hlog{"CMD1:"};
        my $cd1 = "mkdir $dir";     # mkdir /home/user/bsa/prep_dir
        &process_cmd($cd1, $lo, $mx1);
    }

    unless(-e $fai){
        print STDOUT "samtools index reference genome\n";
        print $lo "samtools index reference genome\nCMD2:\t";
        unless(defined($hlog{"CMD2:"})) {$hlog{"CMD2:"} = 0;}
        my $mx2 = $hlog{"CMD2:"};
        my $cd2 = "$samtools faidx $genome";
        # samtools faidx /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa
        &process_cmd($cd2, $lo, $mx2);
    }

    unless(defined($hb) || defined($lb)){
        unless(-e $mmi){
            print STDOUT "minimap index reference genome\n";
            print $lo "minimap index reference genome\nCMD2:\t";
            unless(defined($hlog{"CMD2b:"})) {$hlog{"CMD2b:"} = 0;}
            my $mx2b = $hlog{"CMD2b:"};
            my $cd2b = "$minimap2 -d $mmi $genome";
            # minimap2 -d /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.mmi /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa
            &process_cmd($cd2b, $lo, $mx2b);
        }

        print STDOUT "align High pool reads to reference genome\n";
        print $lo "align High pool reads to reference genome\nCMD3:\t";
        unless(defined($hlog{"CMD3:"})) {$hlog{"CMD3:"} = 0;}
        my $mx3 = $hlog{"CMD3:"};
        my $cd3 = "$minimap2 -ax sr -t $t2 -R \"\@RG\\tID:H\\tSM:H\" $mmi $hf1 $hf2 | $samtools view -@ $t2 -bS -> $H_bam";
        ## minimap2 -ax sr -t 1 -R "@RG\tID:H\tSM:H" /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.mmi H_1.fastq H_2.fastq | samtools view -@ 1 -bS -> /home/user/bsa/prep_dir/prep_H.bam
        # my $cd3 = "$minimap2 -ax sr -t $t2 -R \"\@RG\\tID:H\\tSM:H\" $mmi $hf1 $hf2 -o $H_sam; $samtools view -@ $t2 -bS -o $H_bam $H_sam";
        # minimap2 -ax sr -t 1 -R "@RG\tID:H\tSM:H" /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.mmi H_1.fastq H_2.fastq -o /home/user/bsa/prep_dir/prep_H.sam
        # samtools view -@ 1 -bS -o /home/user/bsa/prep_dir/prep_H.bam /home/user/bsa/prep_dir/prep_H.sam
        &process_cmd($cd3, $lo, $mx3);

        print STDOUT "align Low pool reads to reference genome\n";
        print $lo "align Low pools reads to reference genome\nCMD4:\t";
        unless(defined($hlog{"CMD4:"})) {$hlog{"CMD4:"} = 0;}
        my $mx4 = $hlog{"CMD4:"};
        my $cd4 = "$minimap2 -ax sr -t $t2 -R \"\@RG\\tID:L\\tSM:L\" $mmi $lf1 $lf2 | $samtools view -@ $t2 -bS -> $L_bam";
        ## minimap2 -ax sr -t 1 -R "@RG\tID:L\tSM:L" /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.mmi L_1.fastq L_2.fastq | samtools view -@ 1 -bS -> /home/user/bsa/prep_dir/prep_L.bam
        # my $cd4 = "$minimap2 -ax sr -t $t2 -R \"\@RG\\tID:L\\tSM:L\" $mmi $lf1 $lf2 -o $L_sam; $samtools view -@ $t2 -bS -o $L_bam $L_sam";
        # minimap2 -ax sr -t 1 -R "@RG\tID:L\tSM:L" /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.mmi L_1.fastq L_2.fastq -o /home/user/bsa/prep_dir/prep_L.sam
        # samtools view -@ 1 -bS -o /home/user/bsa/prep_dir/prep_L.bam /home/user/bsa/prep_dir/prep_L.sam
        &process_cmd($cd4, $lo, $mx4);

        print STDOUT "sort high pool BAM FILE\n";
        print $lo "sort high pool BAM FILE\nCMD5:\t";
        unless(defined($hlog{"CMD5:"})) {$hlog{"CMD5:"} = 0;}
        my $mx5 = $hlog{"CMD5:"};
        my $cd5 = "$samtools sort -@ $t2 -o $H_srt_bam $H_bam";
        # samtools sort -@ 1 -o /home/user/bsa/prep_dir/prep_H_srt.bam /home/user/bsa/prep_dir/prep_H.bam
        &process_cmd($cd5, $lo, $mx5);

        print STDOUT "sort low pool BAM FILE\n";
        print $lo "sort low pool BAM FILE\nCMD6:\t";
        my $cd6= "$samtools sort -@ $t2 -o $L_srt_bam $L_bam";
        # samtools sort -@ 1 -o /home/user/bsa/prep_dir/prep_L_srt.bam /home/user/bsa/prep_dir/prep_L.bam
        unless(defined($hlog{"CMD6:"})) {$hlog{"CMD6:"} = 0;}
        my $mx6 = $hlog{"CMD6:"};
        &process_cmd($cd6, $lo, $mx6);

        print STDOUT "remove duplicates reads of H BAM FILE\n";
        print $lo "remove duplicates reads of H BAM FILE\nCMD7:\t";
        unless(defined($hlog{"CMD7:"})) {$hlog{"CMD7:"} = 0;}
        my $mx7 = $hlog{"CMD7:"};
        my $cd7 = "$samtools rmdup $H_srt_bam $H_rm_bam";
        # samtools rmdup /home/user/bsa/prep_dir/prep_H_srt.bam /home/user/bsa/prep_dir/prep_H_rm.bam
        &process_cmd($cd7, $lo, $mx7);

        print STDOUT "remove duplicates reads of L BAM FILE\n";
        print $lo "remove duplicates reads of L BAM FILE\nCMD8:\t";
        my $cd8 = "$samtools rmdup $L_srt_bam $L_rm_bam";
        # samtools rmdup /home/user/bsa/prep_dir/prep_L_srt.bam /home/user/bsa/prep_dir/prep_L_rm.bam
        unless(defined($hlog{"CMD8:"})) {$hlog{"CMD8:"} = 0;}
        my $mx8 = $hlog{"CMD8:"};
        &process_cmd($cd8, $lo, $mx8);

        unless(-e $hb_bai){
            print STDOUT "samtools index L BAM FILE\n";
            print $lo "samtools index L BAM FILE\nCMD9:\t";
            unless(defined($hlog{"CMD9:"})) {$hlog{"CMD9:"} = 0;}
            my $mx9 = $hlog{"CMD9:"};
            my $cd9 = "$samtools index -@ $t2 $L_rm_bam";
            # samtools index -@ 1 /home/user/bsa/prep_dir/prep_L_rm.bam
            &process_cmd($cd9, $lo, $mx9);
        }

        print STDOUT "samtools flagstat L BAM FILE\n";
        print $lo "samtools flagstat L BAM FILE\nCMD9b:\t";
        print SS "summary the alignment of low pool alignment\n";
        unless(defined($hlog{"CMD9b:"})) {$hlog{"CMD9b:"} = 0;}
        my $mx9b = $hlog{"CMD9b:"};
        my $cd9b = "$samtools flagstat -@ $t2 $L_rm_bam >> $sum";
        # samtools flagstat -@ 1 /home/user/bsa/prep_dir/prep_L_rm.bam >> /home/user/bsa/prep_dir/summary
        # &process_cmd($cd9b, $lo, $mx9b);

        unless(-e $lb_bai){
            print STDOUT "samtools index H BAM FILE\n";
            print $lo "samtools index H BAM FILE\nCMD10:\t";
            unless(defined($hlog{"CMD10:"})) {$hlog{"CMD10:"} = 0;}
            my $mx10 = $hlog{"CMD10:"};
            my $cd10 = "$samtools index -@ $t2 $H_rm_bam";
            # samtools index -@ 1 /home/user/bsa/prep_dir/prep_H_rm.bam
            &process_cmd($cd10, $lo, $mx10);
        }

        print STDOUT "samtools flagstat H BAM FILE\n";
        print $lo "samtools flagstat H BAM FILE\nCMD9b:\t";
        print SS "\nsummary the alignment of high pool alignment\n";
        unless(defined($hlog{"CMD10b:"})) {$hlog{"CMD10b:"} = 0;}
        my $mx10b = $hlog{"CMD10b:"};
        my $cd10b = "$samtools flagstat -@ $t2 $H_rm_bam >> $sum";
        # samtools flagstat -@ 1 /home/user/bsa/prep_dir/prep_H_rm.bam >> /home/user/bsa/prep_dir/summary
        # &process_cmd($cd10b, $lo, $mx10b);

        if(-e $H_srt_bam){
            print STDOUT "remove some files\n";
            print $lo "remove some files\nCMD11:\t";
            unless(defined($hlog{"CMD11:"})) {$hlog{"CMD11:"} = 0;}
            my $mx11 = $hlog{"CMD11:"};
            my $cd11 = "rm $H_bam $L_bam $H_srt_bam $L_srt_bam";
            # rm /home/user/bsa/prep_dir/prep_H.bam /home/user/bsa/prep_dir/prep_L.bam /home/user/bsa/prep_dir/prep_H_srt.bam /home/user/bsa/prep_dir/prep_L_srt.bam
            &process_cmd($cd11, $lo, $mx11);
        }

        print STDOUT "P loci genotyping in High pool\n";
        print $lo "P loci genotyping in High pool\nCMD12:\t";
        unless(defined($hlog{"CMD12:"})) {$hlog{"CMD12:"} = 0;}
        my $mx12 = $hlog{"CMD12:"};
        my $cd12 = "$samtools mpileup -ugf $genome -q $aq2 -l $P $H_rm_bam | $bcftools call --threads $t2 -m -> $H_P_snp";
        # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.p /home/user/bsa/prep_dir/prep_H_rm.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/H_P_snp
        &process_cmd($cd12, $lo, $mx12);

        print STDOUT "M loci genotyping in High pool\n";
        print $lo "M loci genotyping in High pool\nCMD13:\t";
        unless(defined($hlog{"CMD13:"})) {$hlog{"CMD13:"} = 0;}
        my $mx13 = $hlog{"CMD13:"};
        my $cd13 = "$samtools mpileup -ugf $genome -q $aq2 -l $M $H_rm_bam | $bcftools call --threads $t2 -m -> $H_M_snp";
        # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.m /home/user/bsa/prep_dir/prep_H_rm.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/H_M_snp
        &process_cmd($cd13, $lo, $mx13);

        print STDOUT "PM loci genotyping in High pool\n";
        print $lo "PM loci genotyping in High pool\nCMD14:\t";
        unless(defined($hlog{"CMD14:"})) {$hlog{"CMD14:"} = 0;}
        my $mx14 = $hlog{"CMD14:"};
        my $cd14 = "$samtools mpileup -ugf $genome -q $aq2 -l $PM $H_rm_bam | $bcftools call --threads $t2 -m -> $H_PM_snp";
        # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.pm /home/user/bsa/prep_dir/prep_H_rm.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/H_PM_snp
        &process_cmd($cd14, $lo, $mx14);

        print STDOUT "P loci genotyping in Low pool\n";
        print $lo "P loci genotyping in Low pool\nCMD15:\t";
        unless(defined($hlog{"CMD15:"})) {$hlog{"CMD15:"} = 0;}
        my $mx15 = $hlog{"CMD15:"};
        my $cd15 = "$samtools mpileup -ugf $genome -q $aq2 -l $P $L_rm_bam | $bcftools call --threads $t2 -m -> $L_P_snp";
        # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.p /home/user/bsa/prep_dir/prep_L_rm.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/L_P_snp
        &process_cmd($cd15, $lo, $mx15);

        print STDOUT "M loci genotyping in Low pool\n";
        print $lo "M loci genotyping in Low pool\nCMD16:\t";
        unless(defined($hlog{"CMD16:"})) {$hlog{"CMD16:"} = 0;}
        my $mx16 = $hlog{"CMD16:"};
        my $cd16 = "$samtools mpileup -ugf $genome -q $aq2 -l $M $L_rm_bam | $bcftools call --threads $t2 -m -> $L_M_snp";
        # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.m /home/user/bsa/prep_dir/prep_L_rm.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/L_M_snp
        &process_cmd($cd16, $lo, $mx16);

        print STDOUT "PM loci genotyping in Low pool\n";
        print $lo "PM loci genotyping in Low pool\nCMD17\t";
        unless(defined($hlog{"CMD17:"})) {$hlog{"CMD17:"} = 0;}
        my $mx17 = $hlog{"CMD17:"};
        my $cd17 = "$samtools mpileup -ugf $genome -q $aq2 -l $PM $L_rm_bam | $bcftools call --threads $t2 -m -> $L_PM_snp";
        # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.pm /home/user/bsa/prep_dir/prep_L_rm.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/L_PM_snp
        &process_cmd($cd16, $lo, $mx17);

    }else{

        unless(-e $H_P_snp){
            print STDOUT "P loci genotyping in High pool\n";
            print $lo "P loci genotyping in High pool\nCMD18:\t";
            unless(defined($hlog{"CMD18:"})) {$hlog{"CMD18:"} = 0;}
            my $mx18 = $hlog{"CMD18:"};
            my $cd18 ="$samtools mpileup -ugf $genome -q $aq2 -l $P $hb | $bcftools call --threads $t2 -m -> $H_P_snp";
            # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.p H.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/H_P_snp
            &process_cmd($cd18, $lo, $mx18);
        }

        unless(-e $H_M_snp){
            print STDOUT "M loci genotyping in High pool\n";
            print $lo "M loci genotyping in High pool\nCMD19:\t";
            unless(defined($hlog{"CMD19:"})) {$hlog{"CMD19:"} = 0;}
            my $mx19 = $hlog{"CMD19:"};
            my $cd19= "$samtools mpileup -ugf $genome -q $aq2 -l $M $hb | $bcftools call --threads $t2 -m -> $H_M_snp";
            # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.m H.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/H_M_snp
            &process_cmd($cd19, $lo, $mx19);
        }

        unless(-e $H_PM_snp){
            print STDOUT "PM loci genotyping in High pool\n";
            print $lo "PM loci genotyping in High pool\nCMD20:\t";
            unless(defined($hlog{"CMD20:"})) {$hlog{"CMD20:"} = 0;}
            my $mx20 = $hlog{"CMD20:"};
            my $cd20= "$samtools mpileup -ugf $genome -q $aq2 -l $PM $hb | $bcftools call --threads $t2 -m -> $H_PM_snp";
            # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.pm H.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/H_PM_snp
            &process_cmd($cd20, $lo, $mx20);
        }

        unless(-e $L_P_snp){
            print STDOUT "P loci genotyping in Low pool\n";
            print $lo "P loci genotyping in Low pool\nCMD21:\t";
            unless(defined($hlog{"CMD21:"})) {$hlog{"CMD21:"} = 0;}
            my $mx21 = $hlog{"CMD21:"};
            my $cd21 = "$samtools mpileup -ugf $genome -q $aq2 -l $P $lb | $bcftools call --threads $t2 -m -> $L_P_snp";
            # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.p L.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/L_P_snp
            &process_cmd($cd21, $lo, $mx21);
        }

        unless(-e $L_M_snp){
            print STDOUT "M loci genotyping in Low pool\n";
            print $lo "M loci genotyping in Low pool\nCMD22:\t";
            unless(defined($hlog{"CMD22:"})) {$hlog{"CMD22:"} = 0;}
            my $mx22 = $hlog{"CMD22:"};
            my $cd22 = "$samtools mpileup -ugf $genome -q $aq2 -l $M $lb | $bcftools call --threads $t2 -m -> $L_M_snp";
            # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.m L.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/L_M_snp
            &process_cmd($cd22, $lo, $mx22);
        }

        unless(-e $L_PM_snp){
            print STDOUT "PM loci genotyping in Low pool\n";
            print $lo "PM loci genotyping in Low pool\nCMD23:\t";
            unless(defined($hlog{"CMD23:"})) {$hlog{"CMD23:"} = 0;}
            my $mx23 = $hlog{"CMD23:"};
            my $cd23 = "$samtools mpileup -ugf $genome -q $aq2 -l $PM $lb | $bcftools call --threads $t2 -m -> $L_PM_snp";
            # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 -l prepar_dir/P_M.pm L.bam | bcftools call --threads 1 -m -> /home/user/bsa/prep_dir/L_PM_snp
            &process_cmd($cd23, $lo, $mx23);
        }
    }

        unless(-e $H_P_counts){
            print STDOUT "filter and get P loci reads count data in high pool\n";
            print $lo "filter and get P loci reads count data in high pool\nCMD24:\t";
            unless(defined($hlog{"CMD24:"})) {$hlog{"CMD24:"} = 0;}
            my $mx24 = $hlog{"CMD24:"};
            my $cd24 = "$get_counts -q $sq2 -d $d2 $H_P_snp > $H_P_counts";
            # /home/user/BSATOS2/scripts/get_counts.pl -q 10 -d 10 /home/user/bsa/prep_dir/H_P_snp > /home/user/bsa/prep_dir/H_P_counts
            &process_cmd($cd24, $lo, $mx24);
        }

        unless(-e $H_M_counts){
            print STDOUT "filter and get M loci reads count data in high pool\n";
            print $lo "filter and get M loci reads count data in high pool\nCMD25:\t";
            unless(defined($hlog{"CMD25:"})) {$hlog{"CMD25:"} = 0;}
            my $mx25 = $hlog{"CMD25:"};
            my $cd25 = "$get_counts -q $sq2 -d $d2 $H_M_snp > $H_M_counts";
            # /home/user/BSATOS2/scripts/get_counts.pl -q 10 -d 10 /home/user/bsa/prep_dir/H_M_snp > /home/user/bsa/prep_dir/H_M_counts
            &process_cmd($cd25, $lo, $mx25);
        }


        unless(-e $H_PM_counts){
            print STDOUT "filter and get PM loci reads count data in high pool\n";
            print $lo "filter and get PM loci reads count data in high pool\nCMD26:\t";
            unless(defined($hlog{"CMD26:"})) {$hlog{"CMD26:"} = 0;}
            my $mx26 = $hlog{"CMD26:"};
            my $cd26 = "$get_counts -q $sq2 -d $d2 $H_PM_snp > $H_PM_counts";
            # /home/user/BSATOS2/scripts/get_counts.pl -q 10 -d 10 /home/user/bsa/prep_dir/H_PM_snp > /home/user/bsa/prep_dir/H_PM_counts
            &process_cmd($cd26, $lo, $mx26);
        }

        unless(-e $L_P_counts){
            print STDOUT "filter and get P loci reads count data in low pool\n";
            print $lo "filter and get P loci reads count data in low pool\nCMD27:\t";
            unless(defined($hlog{"CMD27:"})) {$hlog{"CMD27:"} = 0;}
            my $mx27 = $hlog{"CMD27:"};
            my $cd27 = "$get_counts -q $sq2 -d $d2 $L_P_snp > $L_P_counts";
            # /home/user/BSATOS2/scripts/get_counts.pl -q 10 -d 10 /home/user/bsa/prep_dir/L_P_snp > /home/user/bsa/prep_dir/L_P_counts
            &process_cmd($cd27, $lo, $mx27);
        }

        unless(-e $L_M_counts){
            print STDOUT "filter and get M loci reads count data in low pool\n";
            print $lo "filter and get M loci reads count data in low pool\nCMD28:\t";
            my $cd28= "$get_counts -q $sq2 -d $d2 $L_M_snp > $L_M_counts";
            # /home/user/BSATOS2/scripts/get_counts.pl -q 10 -d 10 /home/user/bsa/prep_dir/L_M_snp > /home/user/bsa/prep_dir/L_M_counts
            unless(defined($hlog{"CMD28:"})) {$hlog{"CMD28:"} = 0;}
            my $mx28 = $hlog{"CMD28:"};
            &process_cmd($cd28, $lo, $mx28);
        }

        unless(-e $L_PM_counts){
            print STDOUT "filter and get PM loci reads count data in low pool\n";
            print $lo "filter and get PM loci reads count data in low pool\nCMD29:\t";
            my $cd29= "$get_counts -q $sq2 -d $d2 $L_PM_snp > $L_PM_counts";
            # /home/user/BSATOS2/scripts/get_counts.pl -q 10 -d 10 /home/user/bsa/prep_dir/L_PM_snp > /home/user/bsa/prep_dir/L_PM_counts
            unless(defined($hlog{"CMD29:"})) {$hlog{"CMD29:"} = 0;}
            my $mx29 = $hlog{"CMD29:"};
            &process_cmd($cd29, $lo, $mx29);
        }

        print STDOUT "obtain P loci reads count data\n";
        print $lo "obtain P loci reads count data\nCMD30:\t";
        unless(defined($hlog{"CMD30:"})) {$hlog{"CMD30:"} = 0;}
        my $mx30 = $hlog{"CMD30:"};
        my $cd30 = "sort -k 1,1 -k 2,2n $H_P_counts > $H_P_counts_sort; sort -k 1,1 -k 2,2n $L_P_counts > $L_P_counts_sort; $filter -k cn $H_P_counts_sort $L_P_counts_sort | cut -f 1-4,7-8 - | $filter_snp -d $pn -> $P_counts";
        # sort -k 1,1 -k 2,2n /home/user/bsa/prep_dir/H_P_counts > /home/user/bsa/prep_dir/H_P_counts_sort
        # sort -k 1,1 -k 2,2n /home/user/bsa/prep_dir/L_P_counts > /home/user/bsa/prep_dir/L_P_counts_sort
        # /home/user/BSATOS2/tools/bin/filter -k cn /home/user/bsa/prep_dir/H_P_counts_sort /home/user/bsa/prep_dir/L_P_counts_sort | cut -f 1-4,7-8 - | /home/user/BSATOS2/scripts/filter.pl -d 3 -> /home/user/bsa/prep_dir/P.counts
        &process_cmd($cd30, $lo, $mx30);

        print STDOUT "summary P type loci data\n";
        print $lo "summary P loci reads count data\nCMD30b:\t";
        print SS "summary P type loci data\n";
        unless(defined($hlog{"CMD30b:"})) {$hlog{"CMD30b:"} = 0;}
        my $mx30b = $hlog{"CMD30b:"};
        my $cd30b = "wc -l $P_counts >> $sum";
        # wc -l /home/user/bsa/prep_dir/P.counts >> /home/user/bsa/prep_dir/summary
        # &process_cmd($cd30b, $lo, $mx30b);

        print STDOUT "obtain M loci reads count data\n";
        print $lo "obtain M loci reads count data\nCMD31\t";
        unless(defined($hlog{"CMD31:"})) {$hlog{"CMD31:"} = 0;}
        my $mx31 = $hlog{"CMD31:"};
        my $cd31 = "sort -k 1,1 -k 2,2n $H_M_counts > $H_M_counts_sort; sort -k 1,1 -k 2,2n $L_M_counts > $L_M_counts_sort; $filter -k cn $H_M_counts_sort $L_M_counts_sort | cut -f 1-4,7-8 - | $filter_snp -d $pn -> $M_counts";
        # sort -k 1,1 -k 2,2n /home/user/bsa/prep_dir/H_M_counts > /home/user/bsa/prep_dir/H_M_counts_sort
        # sort -k 1,1 -k 2,2n /home/user/bsa/prep_dir/L_M_counts >/home/user/bsa/prep_dir/L_M_counts_sort
        # /home/user/BSATOS2/tools/bin/filter -k cn /home/user/bsa/prep_dir/H_M_counts_sort /home/user/bsa/prep_dir/L_M_counts_sort | cut -f 1-4,7-8 - | /home/user/BSATOS2/scripts/filter.pl -d 3 -> /home/user/bsa/prep_dir/M.counts
        &process_cmd($cd31, $lo, $mx31);

        print STDOUT "summary M type loci data\n";
        print $lo "summary M loci reads count data\nCMD31b:\t";
        print SS "summary M type loci data\n";
        unless(defined($hlog{"CMD31b:"})) {$hlog{"CMD31b:"} = 0;}
        my $mx31b = $hlog{"CMD31b:"};
        my $cd31b = "wc -l $M_counts >> $sum";
        # wc -l /home/user/bsa/prep_dir/M.counts >> /home/user/bsa/prep_dir/summary
        # &process_cmd($cd31b, $lo, $mx31b);

        print STDOUT "obtain PM loci reads count data\n";
        print $lo "obtain PM loci reads count data\nCMD32\t";
        unless(defined($hlog{"CMD32:"})) {$hlog{"CMD32:"} = 0;}
        my $mx32 = $hlog{"CMD32:"};
        my $cd32 = "sort -k 1,1 -k 2,2n $H_PM_counts > $H_PM_counts_sort; sort -k 1,1 -k 2,2n $L_PM_counts > $L_PM_counts_sort; $filter -k cn $H_PM_counts_sort $L_PM_counts_sort | cut -f 1-4,7-8 - | $filter_snp -d $pn -> $PM_counts";
        # sort -k 1,1 -k 2,2n /home/user/bsa/prep_dir/H_PM_counts > /home/user/bsa/prep_dir/H_PM_counts_sort
        # sort -k 1,1 -k 2,2n /home/user/bsa/prep_dir/L_PM_counts > /home/user/bsa/prep_dir/L_PM_counts_sort
        # /home/user/BSATOS2/tools/bin/filter -k cn /home/user/bsa/prep_dir/H_PM_counts_sort /home/user/bsa/prep_dir/L_PM_counts_sort | cut -f 1-4,7-8 - | /home/user/BSATOS2/scripts/filter.pl -d 3 -> /home/user/bsa/prep_dir/PM.counts
        &process_cmd($cd32, $lo, $mx32);

        print STDOUT "summary PM type loci data\n";
        print $lo "summary PM loci reads count data\nCMD32b:\t";
        print SS "summary PM type loci data\n";
        unless(defined($hlog{"CMD32b:"})) {$hlog{"CMD32b:"} = 0;}
        my $mx32b = $hlog{"CMD32b:"};
        my $cd32b = "wc -l $PM_counts >> $sum";
        # wc -l /home/user/bsa/prep_dir/PM.counts >> /home/user/bsa/prep_dir/summary
        # &process_cmd($cd32b, $lo, $mx32b);

        if(-e $H_P_snp){
            print STDOUT "remove some files\n";
            print $lo "remove some files\nCMD33\t";
            unless(defined($hlog{"CMD33:"})) {$hlog{"CMD33:"} = 0;}
            my $mx33 = $hlog{"CMD33:"};
            my $cd33 = "rm $H_P_snp $H_M_snp $H_PM_snp $L_P_snp $L_M_snp $L_PM_snp $H_P_counts_sort $H_M_counts_sort $H_PM_counts_sort $L_P_counts_sort $L_M_counts_sort $L_PM_counts_sort $H_P_counts $H_M_counts $H_PM_counts $L_P_counts $L_M_counts $L_PM_counts";
            # rm /home/user/bsa/prep_dir/H_P_snp /home/user/bsa/prep_dir/H_M_snp /home/user/bsa/prep_dir/H_PM_snp /home/user/bsa/prep_dir/L_P_snp /home/user/bsa/prep_dir/L_M_snp /home/user/bsa/prep_dir/L_PM_snp
            # /home/user/bsa/prep_dir/H_P_counts_sort /home/user/bsa/prep_dir/H_M_counts_sort /home/user/bsa/prep_dir/H_PM_counts_sort
            # /home/user/bsa/prep_dir/L_P_counts_sort /home/user/bsa/prep_dir/L_M_counts_sort /home/user/bsa/prep_dir/L_PM_counts_sort
            # /home/user/bsa/prep_dir/H_P_counts /home/user/bsa/prep_dir/H_M_counts /home/user/bsa/prep_dir/H_PM_counts
            # /home/user/bsa/prep_dir/L_P_counts /home/user/bsa/prep_dir/L_M_counts /home/user/bsa/prep_dir/L_PM_counts
            &process_cmd($cd33, $lo, $mx33);
        }

        print STDOUT "\nprep module finished!!!\n";
        print $lo "\nprep module finished!!!\n";
        exit(0);
}

############################################################################

sub process_cmd {
    my ($cmd, $lk, $hg) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t...............................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t...............................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime = "[".localtime()."]";
        if($ret){
            print $lk "\#die\t$starttime ... $endtime\n";
            print STDOUT "\#die\t$starttime ... $endtime\n";
            die "Error, cmd: < $cmd > died with return value $ret";
        }else{
            print $lk "\#done\t$starttime ... $endtime\n";
            print STDOUT "\#done\t$starttime ... $endtime\n";
        }
        return;
    }
}
