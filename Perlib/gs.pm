#!/usr/bin/env perl
package gs;
BEGIN {my $VERSION = '2.0';}

use Cwd;
use strict;
use Getopt::Long;
use base 'Exporter';
use FindBin qw($Bin);
our @EXPORT = qw(rungs);

my $G_USAGE = "
Usage: bastos2 gs [options]

Options:
    --gen  [FILE]     Genotype file
    --phe  [FILE]     Phenotype file
    --rou  [INT]      Rounds of calculate [1000]
    --pre  [FLOAT]    Trainning set in the all populations [0.6]
    --o    [STR]      name prefix of output directory [gs]

Outputs:
    gs_dir [DIR]
";

sub rungs {
    my $gen = undef;      # 基因型
    my $phe = undef;      # 表型
    my $rou = undef;      # 计算次数
    my $pre = undef;      # 种群训练集
    my $prefix = undef;   # 输出前缀
    my $help = undef;
    my $log = undef;

    GetOptions (
        "gen=s" => \$gen,
        "phe=s" => \$phe,
        "pre=f" => \$pre,
        "rou=i" => \$rou,
        "o=s"   => \$prefix,
        "help!" => \$help)
    or die("$G_USAGE");
    die "$G_USAGE" if ($help);
    die "$G_USAGE" if (!defined($prefix));

    my $gs2valid = "$FindBin::Bin/scripts/gs2valid.pl";       # /home/user/BSATOS2/scripts/gs2valid.pl
    my $pr = "$FindBin::Bin/scripts/pearson.R";               # /home/user/BSATOS2/R/pearson.R
    my $work_dir = getcwd;                                  # /home/user/bsa
    my $dir = $work_dir."/".$prefix."_dir";                 # /home/user/bsa/gs_dir
    my $acc = "$dir/accurancy";                             # /home/user/bsa/gs_dir/accurancy

    unless(defined($prefix)){$prefix = "gs";}                 # 默认输出目录前缀为"gs"
    unless(defined($rou)){$rou = 1000;}                       # 默认计算次数为1000
    unless(defined($pre)){$pre = 0.6;}                        # 默认种群训练集为60%
    unless(-e $dir) {system("mkdir $dir");}
    system("cp $pr $work_dir");                             # cp /home/user/BSATOS2/R/pearson.R /home/user/bsa

    foreach my $index (1 .. $rou){
        system("perl $gs2valid -p $pre $gen $phe > $acc");
        # perl /home/user/BSATOS2/scripts/gs2valid.pl -p 0.6 $gen $phe > /home/user/bsa/gs_dir/accurancy
    }
    system("mv EPV_file marker_effect.txt pearson.R $dir");
    # mv EPV_file marker_effect.txt pearson.R /home/user/bsa/gs_dir
}
