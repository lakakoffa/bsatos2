#!/usr/bin/env perl
package qtl_pick;
BEGIN {my $VERSION = '2.0';}

use Cwd;
use strict;
use Getopt::Long;
use base 'Exporter';
use File::Basename;
use FindBin qw($Bin);
our @EXPORT = qw(runqtl_pick);

my $G_USAGE = "
Usage: bastos2 qtl_pick [options]

Options:
    --o       [STR]       name prefix of output directory [qtl_pick]
    --gp      [FILE]      smoothed curve base on P type loci across genome with haplotype information from polish module
    --gm      [FILE]      smoothed curve base on M type loci across genome with haplotype information from polish module
    --gpm     [FILE]      smoothed curve base on PM type loci across genome with haplotype information from polish module
    --gtf     [FILE]      GTF file of genes (GTF2 format; Not required if GFF file is provided) [Malus_domestica.gtf]
    --gff     [FILE]      GFF file of genes (GFF3 format; Not required if GTF file is provided) [Malus_domestica.gff3]
    --snv     [FILE]      annotated SNVs file from prepar step
    --sv      [FILE]      annotated SVs file from prepar step
    --hap     [FILE]      haplotye file from haplotype step
    --q       [INT]       mininum phred-scaled quality score [10]
    --pr      [INT]       promoter region [2000]
    --per     [FLOAT]     minimum percentage of blocks in QTLs region [0.7]
    --bl      [INT]       minimum enriched block length [2]
    --fdr     [FLOAT]     FDR threshold for the QTLs [0.01]
    --win     [INT]       sliding window size [1000000]
    --log     [FILE]      qtl_pick module log file [qtl_pick.log]
    --db      [STR]       database directory of .fa & GTF & GFF files [Malus_domestica]

Outputs:
    qtl     [FILE]        detected QTLs list file
    *.pdf   [FILE]        G value/ED/SI profiles across each chromosome (*:chromosome)
    *.pdf   [FILE]        multiple G' values profiles across QTL region (*:QTL accession)
    p_hap   [FILE]        haplotype information in P type loci
    m_hap   [FILE]        haplotype information in M type loci
    pm_hap  [FILE]        haplotype information in PM type loci
    *.gene  [FILE]        gene list located in the QTL regions (*: QTL accession)
    *.hap   [FILE]        haplotype information located in the QTL regions (*:QTL accession)
    *.snv   [FILE]        screened SNVs based on genetic rules located in the QTL regions (*: QTL accession)
    *.sv    [FILE]        screened SNVs based on genetic rules located in the QTL regions (*: QTL accession)

Example:
    bsatos2 qtl_pick  --gtf Malus_domestica.gtf --o qtl_pick --gp P.polished.afd --gm M.polished.afd --gpm PM.polished.afd --snv snv.AT_multianno.txt --sv sv.AT_multianno.txt --hap haplotyle.block --win 1000000
";


sub runqtl_pick {
    my $gp = undef;      my $gm = undef;       my $gpm = undef;      my $snv = undef;      my $per = undef;
    my $sv = undef;      my $gtf = undef;      my $win = undef;      my $help = undef;     my $bl = undef;
    my $s = undef;       my $hap = undef;      my $pr = undef;       my $q = undef;        my $log = undef;
    my $fdr = undef;     my $gff = undef;      my $db = undef;       my $o = undef;

    GetOptions (
        "hap=s" => \$hap,   "gp=s" => \$gp,      "gm=s" => \$gm,     "gpm=s" => \$gpm,     "win=i" => \$win,
        "gtf=s" => \$gtf,   "snv=s" => \$snv,    "s=s" => \$s,       "pr=i" => \$pr,       "log=s" => \$log,
        "q=i" => \$q,       "sv=s" => \$sv,      "fdr=f" => \$fdr,   "bl=i" => \$bl,       "per=f" => \$per,
        "gff=s" => \$gff,   "help" => \$help,    "db=s" => \$db,     "o=s" => \$o)
    or die("$G_USAGE");
    die "$G_USAGE" if ($help);
    die "$G_USAGE" if (!defined($o));

    my $para_qtlpick_file = "$FindBin::Bin/Python/src/para_qtlpick_file";
    if (-e $para_qtlpick_file){
        open PARAQTLPICKFILE, "$para_qtlpick_file" or die $!;
        while(<PARAQTLPICKFILE>){
            chomp;
            my @para = split /\t/, $_;
            if (@para[0] =~ /^o\b/ && @para[1] =~ /\w/) {$o = @para[1];}
            if (@para[0] =~ /^gtf\b/ && @para[1] =~ /\w/) {$gtf = @para[1];}
            if (@para[0] =~ /^gff\b/ && @para[1] =~ /\w/) {$gff = @para[1];}
            if (@para[0] =~ /^gp\b/ && @para[1] =~ /\w/) {$gp = @para[1];}
            if (@para[0] =~ /^gm\b/ && @para[1] =~ /\w/) {$gm = @para[1];}
            if (@para[0] =~ /^gpm\b/ && @para[1] =~ /\w/) {$gpm = @para[1];}
            if (@para[0] =~ /^snv\b/ && @para[1] =~ /\w/) {$snv = @para[1];}
            if (@para[0] =~ /^pr\b/ && @para[1] =~ /\w/) {$pr = @para[1];}
            if (@para[0] =~ /^q\b/ && @para[1] =~ /\w/) {$q = @para[1];}
            if (@para[0] =~ /^sv\b/ && @para[1] =~ /\w/) {$sv = @para[1];}
            if (@para[0] =~ /^per\b/ && @para[1] =~ /\w/) {$per = @para[1];}
            if (@para[0] =~ /^bl\b/ && @para[1] =~ /\w/) {$bl = @para[1];}
            if (@para[0] =~ /^fdr\b/ && @para[1] =~ /\w/) {$fdr = @para[1];}
            if (@para[0] =~ /^hap\b/ && @para[1] =~ /\w/) {$hap = @para[1];}
            if (@para[0] =~ /^win\b/ && @para[1] =~ /\w/) {$win = @para[1];}
            if (@para[0] =~ /^db\b/ && @para[1] =~ /\w/) {$db = @para[1];}
        }
        close PARAQTLPICKFILE;
    }

    unless(defined($o)) {$o = "qtl_pick";}
    unless(defined($fdr)) {$fdr = 0.01;}
    unless(defined($pr)) {$pr = 2000;}
    unless(defined($q)) {$q = 10;}
    unless(defined($per)) {$per = 0.7;}
    unless(defined($bl)) {$bl = 2;}
    unless(defined($win)) {$win = 1000000;}

    my $gffread = "gffread";
    my $work_dir = getcwd;                                                      # /home/user/bsa
    my $dir = $work_dir."/".$o."_dir";                                          # /home/user/bsa/qtl_pick_dir
    my $cwd_basename = basename($work_dir);                                     # bsa
    my $p_polished_afd = basename($gp);                                         # P.polished.afd
    my $m_polished_afd = basename($gm);                                         # M.polished.afd
    my $pm_polished_afd = basename($gpm);                                       # PM.polished.afd
    my $gp_th = "$dir/p.thres";                                                 # /home/user/bsa/qtl_pick_dir/p.thres
    my $gm_th = "$dir/m.thres";                                                 # /home/user/bsa/qtl_pick_dir/m.thres
    my $gpm_th = "$dir/pm.thres";                                               # /home/user/bsa/qtl_pick_dir/pm.thres
    my $qtl = "$dir/QTL.txt";                                                   # /home/user/bsa/qtl_pick_dir/QTL.txt
    my $qtl_snv_anno = "$dir/QTL_SNV_anno.txt";                                 # /home/user/bsa/qtl_pick_dir/QTL_SNV_anno.txt
    my $qtl_sv_anno = "$dir/QTL_SV_anno.txt";                                   # /home/user/bsa/qtl_pick_dir/QTL_SV_anno.txt
    my $qtl_snv = "$dir/QTL_SNV_genes.txt";                                     # /home/user/bsa/qtl_pick_dir/QTL_SNV_genes.txt
    my $qtl_sv = "$dir/QTL_SV_genes.txt";                                       # /home/user/bsa/qtl_pick_dir/QTL_SV_genes.txt
    my $qtl_anno_excel = "$dir/QTL_anno.xlsx";                                  # /home/user/bsa/qtl_pick_dir/QTL_anno.xlsx
    my $merge = "$FindBin::Bin/scripts/merge.pl";                               # /home/user/BSATOS2/scripts/merge.pl
    my $qtl_info_pl = "$FindBin::Bin/scripts/qtl_info.pl";                      # /home/user/BSATOS2/scripts/qtl_info.pl
    my $gtf2gene_bed = "$FindBin::Bin/scripts/gtf2gene_bed.pl";                 # /home/user/BSATOS2/scripts/gtf2gene_bed.pl
    # my $get_bed = "$FindBin::Bin/scripts/get_bed.pl";                           # /home/user/BSATOS2/scripts/get_bed.pl
    # my $get_bed_v2 = "$FindBin::Bin/scripts/get_bed_v2.pl";                     # /home/user/BSATOS2/scripts/get_bed_v2.pl
    # my $filter_snv = "$FindBin::Bin/scripts/filter_snv_based_on_qtl.pl";        # /home/user/BSATOS2/scripts/filter_snv_based_on_qtl.pl
    # my $filter_sv = "$FindBin::Bin/scripts/filter_sv_based_on_qtl.pl";          # /home/user/BSATOS2/scripts/filter_sv_based_on_qtl.pl
    my $vcf_maf = "$FindBin::Bin/scripts/generate_vcf_and_maf.pl";              # /home/user/BSATOS2/scripts/generate_vcf_and_maf.pl
    my $main_peak_v2_pl = "$FindBin::Bin/scripts/main_peak_v2.pl";              # /home/user/BSATOS2/scripts/main_peak_v2.pl
    my $assign = "$FindBin::Bin/scripts/assign_enriched_haplotype.pl";          # /home/user/BSATOS2/scripts/assign_enriched_haplotype.pl
    my $get_rds = "$FindBin::Bin/scripts/get_rds.pl";                           # /home/user/BSATOS2/scripts/get_rds.pl
    my $summary = "$FindBin::Bin/scripts/summary.pl";                           # /home/user/BSATOS2/scripts/summary.pl
    my $summary_plot = "$FindBin::Bin/R/summary_plot.R";                        # /home/user/BSATOS2/R/summary_plot.R
    my $sub_plot = "$FindBin::Bin/R/sub_plot.R";                                # /home/user/BSATOS2/R/sub_plot.R
    my $plot = "$FindBin::Bin/R/plot.R";                                        # /home/user/BSATOS2/R/plot.R
    my $thres = "$FindBin::Bin/R/thres.R";                                      # /home/user/BSATOS2/R/thres.R
    my $txt2excel = "$FindBin::Bin/Python/txt2excel.py";                        # /home/user/BSATOS2/Python/txt2excel.py
    my $get_bed_v2 = "$FindBin::Bin/Python/get_bed_v2.py";                      # /home/user/BSATOS2/Python/get_bed_v2.py
    my $main_peak_v2_py = "$FindBin::Bin/Python/main_peak_v2.py";               # /home/user/BSATOS2/Python/main_peak_v2.py
    my $main_peak_int_py = "$FindBin::Bin/Python/main_peak_int.py";             # /home/user/BSATOS2/Python/main_peak_int.py
    my $mdo_gdr_expression = "$FindBin::Bin/Python/mdo_gdr_expression.py";      # /home/user/BSATOS2/Python/mdo_gdr_expression.py
    my $filter_snv = "$FindBin::Bin/Python/filter_snv_based_on_qtl.py";         # /home/user/BSATOS2/Python/filter_snv_based_on_qtl.py
    my $filter_sv = "$FindBin::Bin/Python/filter_sv_based_on_qtl.py";           # /home/user/BSATOS2/Python/filter_sv_based_on_qtl.py
    my $qtl_info_py = "$FindBin::Bin/Python/qtl_info.py";                       # /home/user/BSATOS2/Python/qtl_info.py
    my $exp_anno_dir = "$FindBin::Bin/ExpAnno";                                 # /home/user/BSATOS2/ExpAnno
    my $rg = "$FindBin::Bin/tools/bin/rg";                                      # /home/user/BSATOS2/tools/bin/rg

    unless(defined($log)) {$log = $work_dir."/".$o.".log";}                       # /home/user/bsa/qtl_pick_dir/qtl_pick.log
    unless(defined($db)) {$db = "Malus_domestica";}
    unless(defined($gtf)) {$gtf = "$FindBin::Bin/database/$db/$db.gtf";}        # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf
    unless(defined($gff)) {
        if (-e "$FindBin::Bin/database/$db/$db.gff"){$gff = "$FindBin::Bin/database/$db/$db.gff";}       # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff
        if (-e "$FindBin::Bin/database/$db/$db.gff3"){$gff = "$FindBin::Bin/database/$db/$db.gff3";}     # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff3
    }

############################################################################

    my %hlog = ();
    if(-e $log){
        open LOD, "<$log";
        my $count = 0;
        while(<LOD>){
            chomp;
            my @sp = split /\t/, $_;
            if($sp[2] eq "\#done") {$hlog{$sp[0]} = 1;}
            if($sp[2] eq "\#die") {$hlog{$sp[0]} = 0;}
            if($_ =~ /finish/) {$count++;}
        }
        if($count != 0){
            print "
                Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
                Version: 2.0
                The sixth module: qtl_pick
                qtl_pick    judge and pick up QTLs from three types of peaks\n\n
                This module has been processed last time\n\n";
            exit(0);
        }
    }
    open (my $lo, ">>$log") or die $!;

    print STDOUT "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The sixth module: qtl_pick
        qtl_pick    judge and pick up QTLs from three types of peaks\n\n
        ██████╗ ███████╗ █████╗ ████████╗ ██████╗ ███████╗██████╗
        ██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██╔═══██╗██╔════╝╚════██╗
        ██████╔╝███████╗███████║   ██║   ██║   ██║███████╗ █████╔╝
        ██╔══██╗╚════██║██╔══██║   ██║   ██║   ██║╚════██║██╔═══╝
        ██████╔╝███████║██║  ██║   ██║   ╚██████╔╝███████║███████╗
        ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝╚══════╝\n\n";

    print $lo "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The sixth module: qtl_pick
        qtl_pick    judge and pick up QTLs from three types of peaks\n\n";

###########################################################################

    if(!defined($gtf) && defined($gff)){
        $gtf = (split /\./, basename($gff))[0] . ".gtf";
        print STDOUT "convert GFF to GTF file:\n";
        print $lo "convert GFF to GTF file:\nCMD00:\t";
        unless(defined($hlog{"CMD00:"})) {$hlog{"CMD00:"} = 0;}
        my $mx00 = $hlog{"CMD00:"};
        my $cd00 = "$gffread $gff -T -o $gtf";
        # gffread /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff -T -o /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf
        &process_cmd($cd00, $lo, $mx00);
    }

    unless(-e $dir){
        print STDOUT "create result dir\n";
        print $lo "create result dir\nCMD0:\t";
        unless(defined($hlog{"CMD0:"})) {$hlog{"CMD0:"} = 0;}
        my $mx0 = $hlog{"CMD0:"};
        my $cd0 = "mkdir $dir";    # mkdir /home/user/bsa/qtl_pick_dir
        &process_cmd($cd0, $lo, $mx0);
    }

    print STDOUT "calculat threshold of P\n";
    print $lo "calculate threshold of P\nCMD B1:\t";
    my $cdb1 = "Rscript $thres $gp $fdr $gp_th";
    # Rscript /home/user/BSATOS2/R/thres.R polish_dir/P.polished.afd 0.01 /home/user/bsa/qtl_pick_dir/p.thres
    unless(defined($hlog{"CMD B1:"})) {$hlog{"CMD B1:"} = 0;}
    my $mxb1 = $hlog{"CMD B1:"};
    &process_cmd($cdb1, $lo, $mxb1);

    print STDOUT "calculate threshold of M\n";
    print $lo "calculate threshold of M\nCMD B2:\t";
    my $cdb2 = "Rscript $thres $gm $fdr $gm_th";
    # Rscript /home/user/BSATOS2/R/thres.R polish_dir/M.polished.afd 0.01 /home/user/bsa/qtl_pick_dir/m.thres
    unless(defined($hlog{"CMD B2:"})) {$hlog{"CMD B2:"} = 0;}
    my $mxb2 = $hlog{"CMD B2:"};
    &process_cmd($cdb2, $lo, $mxb2);

    print STDOUT "calculate threshold of PM\n";
    print $lo "calculate threshold of PM\nCMD B3:\t";
    my $cdb3 = "Rscript $thres $gpm $fdr $gpm_th";
    # Rscript /home/user/BSATOS2/R/thres.R polish_dir/PM.polished.afd 0.01 /home/user/bsa/qtl_pick_dir/pm.thres
    unless(defined($hlog{"CMD B3:"})) {$hlog{"CMD B3:"} = 0;}
    my $mxb3 = $hlog{"CMD B3:"};
    &process_cmd($cdb3, $lo, $mxb3);

    my @p_thres = split /\n/, `cat $gp_th`;     # /home/user/bsa/qtl_pick_dir/p.thres
    my @m_thres = split /\n/, `cat $gm_th`;     # /home/user/bsa/qtl_pick_dir/m.thres
    my @pm_thres = split /\n/, `cat $gpm_th`;   # /home/user/bsa/qtl_pick_dir/pm.thres

    # open(GPTH, $gp_th) or die "$!";     # /home/user/bsa/qtl_pick_dir/p.thres
    # my @p_thres = <GPTH>;
    # open(GMTH, $gm_th) or die "$!";     # /home/user/bsa/qtl_pick_dir/m.thres
    # my @m_thres = <GMTH>;
    # open(GPMTH, $gpm_th) or die "$!";   # /home/user/bsa/qtl_pick_dir/pm.thres
    # my @pm_thres = <GPMTH>;

    print STDOUT "assign enriched haplotype block in P loci\n";
    print $lo "assign enriched haplotype block in P loci\nCMD1:\t";
    my $cd1 = "perl $assign --per $per --block $bl $gp $p_thres[0] $p_thres[1] $p_thres[2] $p_thres[3] $hap > $dir/p_hap";
    # perl /home/user/BSATOS2/scripts/assign_enriched_haplotype.pl --per 0.7 --block 2 polish_dir/P.polished.afd 2.771678 2.870966 3.102225 1.796389 haplotype_dir/haplotype.block > /home/user/bsa/qtl_pick_dir/p_hap
    unless(defined($hlog{"CMD1:"})) {$hlog{"CMD1:"} = 0;}
    my $mx1 = $hlog{"CMD1:"};
    &process_cmd($cd1, $lo, $mx1);

    print STDOUT "assign enriched haplotype block in M loci\n";
    print $lo "assign enriched haplotype block in M loci\nCMD2:\t";
    my $cd2 = "perl $assign --per $per --block $bl $gm $m_thres[0] $m_thres[1] $m_thres[2] $m_thres[3] $hap > $dir/m_hap";
    # perl /home/user/BSATOS2/scripts/assign_enriched_haplotype.pl --per 0.7 --block 2 polish_dir/M.polished.afd 7.303028 7.341369 7.371755 7.43597 haplotype_dir/haplotype.block > /home/user/bsa/qtl_pick_dir/m_hap
    unless(defined($hlog{"CMD2:"})) {$hlog{"CMD2:"} = 0;}
    my $mx2 = $hlog{"CMD2:"};
    &process_cmd($cd2, $lo, $mx2);

    print STDOUT "assign enriched haplotype block in PM loci\n";
    print $lo "assign enriched haplotype block in PM loci\nCMD3:\t";
    my $cd3 = "perl $assign --per $per --block $bl $gpm $pm_thres[0] $pm_thres[1] $pm_thres[2] $pm_thres[3] $hap > $dir/pm_hap";
    # perl /home/user/BSATOS2/scripts/assign_enriched_haplotype.pl --per 0.7 --block 2 polish_dir/PM.polished.afd 3.390983 3.514713 3.498895 4.255783 haplotype_dir/haplotype.block > /home/user/bsa/qtl_pick_dir/pm_hap
    unless(defined($hlog{"CMD3:"})) {$hlog{"CMD3:"} = 0;}
    my $mx3 = $hlog{"CMD3:"};
    &process_cmd($cd3, $lo, $mx3);

    print STDOUT "convert gene GTF FILE to BED FILE\n";
    print $lo "convert gene GTF FILE to BED FILE\nCMD4:\t";
    my $cd4 = "perl $gtf2gene_bed $gtf | sort -k 1,1 -k 2,2n -> $dir/gene.bed";
    # perl /home/user/BSATOS2/scripts/gtf2gene_bed.pl Malus_domestica.gtf | sort -k 1,1 -k 2,2n -> /home/user/bsa/qtl_pick_dir/gene.bed
    unless(defined($hlog{"CMD4:"})) {$hlog{"CMD4:"} = 0;}
    my $mx4 = $hlog{"CMD4:"};
    &process_cmd($cd4, $lo, $mx4);

############################################################################

    my @chrs = &get_chr($gp, $rg);
    my $idx = 4;
    foreach my $chr (sort @chrs){
        $idx++;
        my $i1 = "CMD".$idx.":";
        print STDOUT "plot AFD profiles in $chr\n";
        print $lo "plot AFD profiles in $chr\n$i1\t";
        unless(defined($hlog{$i1})) {$hlog{$i1} = 0;}
        my $mx5 = $hlog{$i1};
        my $cd5 = "Rscript $plot $gp $gm $gpm $chr $p_thres[0] $m_thres[0] $pm_thres[0]";
        # Rscript /home/user/BSATOS2/R/plot.R polish_dir/P.polished.afd polish_dir/M.polished.afd polish_dir/PM.polished.afd Chr06 2.771678 7.303028 3.390983
        &process_cmd($cd5, $lo, $mx5);
    }

    $idx++;
    my $i2 = "CMD".$idx.":";
    print STDOUT "obtain QTL peak region\n";
    print $lo "obtain QTL peak region\n$i2\t";
    unless(defined($hlog{$i2})) {$hlog{$i2} = 0;}
    my $mx6 = $hlog{$i2};
    my $cd6 = "perl $main_peak_v2_pl --th1 $p_thres[0] --th2 $m_thres[0] --th3 $pm_thres[0] --p $gp --m $gm --pm $gpm > $qtl";
    # perl /home/user/BSATOS2/scripts/main_peak.pl --th1 2.771678 --th2 7.303028 --th3 3.390983 --p polish_dir/P.polished.afd --g2 polish_dir/M.polished.afd --pm polish_dir/PM.polished.afd > /home/user/bsa/qtl_pick_dir/QTL.txt
    # my $cd6 = "python $main_peak_v2_py -th1 $p_thres[0] -th2 $m_thres[0] -th3 $pm_thres[0] -p $gp -m $gm -pm $gpm > $qtl";
    # python /home/user/BSATOS2/scripts/main_peak_v2.py -th1 2.771678 -th2 7.303028 -th3 3.390983 -p polish_dir/P.polished.afd -m polish_dir/M.polished.afd -pm polish_dir/PM.polished.afd > /home/user/bsa/qtl_pick_dir/QTL.txt
    &process_cmd($cd6, $lo, $mx6);

    open QTL, "<$qtl";
    # my @qtlfile = <QTL>;
    # my $len = $#qtlfile - 2;
    foreach(<QTL>){
        chomp;
        if($_ =~ /#/) {next;}
        my @qtllines = split /\t/, $_;   # 0-Orgin  1-Chrom  2-QTL_Start  3-QTL_End  4-Peak_Pos  5-Peak  6-Accession
        my $origin = $qtllines[0];
        my $chrom = $qtllines[1];
        my $qtl_start = $qtllines[2];
        my $qtl_end = $qtllines[3];
        my $peak_pos = $qtllines[4];
        my $accession = $qtllines[6];
        my $out_gene = $dir."/".$accession.".gene";
        my $out_snv = $dir."/".$accession.".snv";
        my $out_sv = $dir."/".$accession.".sv";
        my $out_hap = $dir."/".$accession.".hap";

        $idx++;
        my $i3 = "CMD".$idx.":";
        print STDOUT "get QTL $accession region genes\n";
        print $lo "get QTL $accession region genes\n$i3\t";
        unless(defined($hlog{$i3})) {$hlog{$i3} = 0;}
        my $mx7 = $hlog{$i3};
        my $cd7 = "perl $get_rds $dir/gene.bed $chrom $qtl_start $qtl_end $peak_pos $win > $out_gene";
        # perl /home/user/BSATOS2/scripts/get_rds.pl /home/user/bsa/qtl_pick_dir/gene.bed Chr06 10027489 10527489 10527489 1000000 > /home/user/bsa/qtl_pick_dir/P1.gene
        # &process_cmd($cd7, $lo, $mx7);

        if($origin eq "P"){
            $idx++;
            my $i4 = "CMD".$idx.":";
            print STDOUT "obtain and filter SNVs in QTL $accession region based on genetic laws\n";
            print $lo "obtain and filter SNVs in QTL $accession region based on genetic laws\n$i4\t";
            # my $cd8 = "perl $get_bed $snv $chrom $qtl_start $qtl_end | perl $filter_snv --d $pr --q $q --i $origin --hap $dir/p_hap -> $out_snv";
            # perl /home/user/BSATOS2/scripts/get_bed.pl prepar_dir/anno/snv.AT_multianno.txt Chr06 10027489 10527489 | perl /home/user/BSATOS2/scripts/filter_snv_based_on_qtl.pl --d 2000 --q 10 --i P --hap /home/user/bsa/qtl_pick_dir/p_hap -> /home/user/bsa/qtl_pick_dir/P1.snv
            my $cd8 = "$rg $chrom $snv | perl $get_bed_v2 $qtl_start $qtl_end | perl $filter_snv -d $pr -q $q -i $origin --hap $dir/p_hap -> $out_snv";
            # /home/user/BSATOS2/tools/bin/rg Chr06 prepar_dir/anno/snv.AT_multianno.txt | perl /home/user/BSATOS2/scripts/get_bed_v2.pl 10027489 10527489 | perl /home/user/BSATOS2/scripts/filter_snv_based_on_qtl.pl -d 2000 -q 10 -i P --hap /home/user/bsa/qtl_pick_dir/p_hap -> /home/user/bsa/qtl_pick_dir/P1.snv

            unless(defined($hlog{$i4})){$hlog{$i4} = 0;}
            my $mx8 = $hlog{$i4};
            &process_cmd($cd8, $lo, $mx8);

            $idx++;
            my $i5 = "CMD".$idx.":";
            print STDOUT "convert filtered SNVs file of QTL $accession to IGV format\n";
            print $lo "convert filtered SNVs file of QTL $accession to IGV format\n$i5\t";
            unless(defined($hlog{$i5})){$hlog{$i5} = 0;}
            my $mx9 = $hlog{$i5};
            my $cd9 = "perl $vcf_maf --var snv --in $out_snv";
            # perl /home/user/BSATOS2/scripts/generate_vcf_and_maf.pl --var snv --in /home/user/bsa/qtl_pick_dir/P1.snv
            &process_cmd($cd9, $lo, $mx9);

            $idx++;
            my $i6 = "CMD".$idx.":";
            print STDOUT "obtain enriched haplotype information in QTL $accession region\n";
            print $lo "obtain enriched haplotype information in QTL $accession region\n$i6\t";
            unless(defined($hlog{$i6})){$hlog{$i6} = 0;}
            my $mx10 = $hlog{$i6};
            # my $cd10 = "perl $get_bed $dir/p_hap --header T $chrom $qtl_start $qtl_end > $out_hap";
            # perl /home/user/BSATOS2/scripts/get_bed.pl /home/user/bsa/qtl_pick_dir/p_hap --header T Chr06 10027489 10527489 > /home/user/bsa/qtl_pick_dir/P1.hap
            # my $cd10 = "$rg $chrom $dir/p_hap | perl $get_bed_v2 --header T $qtl_start $qtl_end > $out_hap";
            # /home/user/BSATOS2/tools/bin/rg Chr06 /home/user/bsa/qtl_pick_dir/p_hap | perl /home/user/BSATOS2/scripts/get_bed_v2.pl --header T 10027489 10527489 > /home/user/bsa/qtl_pick_dir/P1.hap
            # &process_cmd($cd10, $lo, $mx10);

            $idx++;
            my $i7 = "CMD".$idx.":";
            print STDOUT "obtain and filter SVs in QTL $accession region\n";
            print $lo "obtain and filter SVs in QTL $accession region\n$i7\t";
            unless(defined($hlog{$i7})){$hlog{$i7} = 0;}
            my $mx11 = $hlog{$i7};
            # my $cd11 = "perl $get_bed $sv $chrom $qtl_start $qtl_end | perl $filter_sv --d $pr --i $origin -> $out_sv";
            # perl /home/user/BSATOS2/scripts/get_bed.pl prepar_dir/anno/sv.AT_multianno.txt Chr06 10027489 10527489 | perl /home/user/BSATOS2/scripts/filter_sv_based_on_qtl.pl --d 2000 --i P -> /home/user/bsa/qtl_pick_dir/P1.sv
            my $cd11 = "$rg $chrom $sv | perl $get_bed_v2 $qtl_start $qtl_end | perl $filter_sv --d $pr --i $origin -> $out_sv";
            # /home/user/BSATOS2/tools/bin/rg Chr06 prepar_dir/anno/sv.AT_multianno.txt | perl /home/user/BSATOS2/scripts/get_bed_v2.pl 10027489 10527489 | perl /home/user/BSATOS2/scripts/filter_sv_based_on_qtl.pl --d 2000 --i P -> /home/user/bsa/qtl_pick_dir/P1.sv
            &process_cmd($cd11, $lo, $mx11);

            $idx++;
            my $i8 = "CMD".$idx.":";
            print STDOUT "convert filtered SVs file of QTL $accession to IGV format\n";
            print $lo "convert filtered SVs file of QTL $accession to IGV format\n$i8\t";
            unless(defined($hlog{$i8})){$hlog{$i8} = 0;}
            my $mx12 = $hlog{$i8};
            my $cd12 = "perl $vcf_maf --var sv --in $out_sv";
            # perl /home/user/BSATOS2/scripts/generate_vcf_and_maf.pl --var sv --in /home/user/bsa/qtl_pick_dir/P1.sv
            &process_cmd($cd12, $lo, $mx12);

            $idx++;
            my $i9 = "CMD".$idx.":";
            print STDOUT "plot profiles of QTL $accession\n";
            print $lo "plot profiles of QTL $accession\n$i9\t";
            unless(defined($hlog{$i9})) {$hlog{$i9} = 0;}
            my $mx13 = $hlog{$i9};
            my $cd13 = "Rscript $sub_plot $gp $chrom $qtl_start $qtl_end $accession";
            # Rscript /home/user/BSATOS2/R/sub_plot.R polish_dir/P.polished.afd Chr06 10027489 10527489 P1
            &process_cmd($cd13, $lo, $mx13);
        }

############################################################################

        if($origin eq "M"){

            $idx++;
            my $i10 = "CMD".$idx.":";
            print STDOUT "obtain and filter SNVs in QTL $accession region based on genetic laws\n";
            print $lo "obtain and filter SNVs in QTL $accession region based on genetic laws\n$i10\t";
            # my $cd14 = "perl $get_bed $snv $chrom $qtl_start $qtl_end | perl $filter_snv --d $pr --q $q --i $origin --hap $dir/m_hap -> $out_snv";
            # perl /home/user/BSATOS2/scripts/get_bed.pl prepar_dir/anno/snv.AT_multianno.txt Chr06 1812818 2563869 | perl /home/user/BSATOS2/scripts/filter_snv_based_on_qtl.pl --d 2000 --q 10 --i M --hap /home/user/bsa/qtl_pick_dir/m_hap -> /home/user/bsa/qtl_pick_dir/M1.snv
            my $cd14 = "$rg $chrom $snv | perl $get_bed_v2 $qtl_start $qtl_end | perl $filter_snv -d $pr -q $q -i $origin --hap $dir/m_hap -> $out_snv";
            # /home/user/BSATOS2/tools/bin/rg Chr06 prepar_dir/anno/snv.AT_multianno.txt | perl /home/user/BSATOS2/scripts/get_bed_v2.pl 1812818 2563869 | perl /home/user/BSATOS2/scripts/filter_snv_based_on_qtl.pl -d 2000 -q 10 -i M --hap /home/user/bsa/qtl_pick_dir/m_hap -> /home/user/bsa/qtl_pick_dir/M1.snv
            unless(defined($hlog{$i10})) {$hlog{$i10} = 0;}
            my $mx14 = $hlog{$i10};
            &process_cmd($cd14, $lo, $mx14);

            $idx++;
            my $i11 = "CMD".$idx.":";
            print STDOUT "convert filtered SNVs file of QTL $accession to IGV format\n";
            print $lo "convert filtered SNVs file of QTL $accession to IGV format\n$i11\t";
            unless(defined($hlog{$i11})) {$hlog{$i11} = 0;}
            my $mx15 = $hlog{$i11};
            my $cd15 = "perl $vcf_maf --var snv --in $out_snv";
            # perl /home/user/BSATOS2/scripts/generate_vcf_and_maf.pl --var snv --in /home/user/bsa/qtl_pick_dir/M1.snv
            &process_cmd($cd15, $lo, $mx15);

            $idx++;
            my $i12 = "CMD".$idx.":";
            print STDOUT "obtain enriched haplotype information in QTL $accession region\n";
            print $lo "obtain enriched haplotype information in QTL $accession region\n$i12\t";
            unless(defined($hlog{$i12})) {$hlog{$i12} = 0;}
            my $mx16 = $hlog{$i12};
            # my $cd16 = "perl $get_bed $dir/m_hap --header T $chrom $qtl_start $qtl_end > $out_hap";
            # perl /home/user/BSATOS2/scripts/get_bed.pl /home/user/bsa/qtl_pick_dir/m_hap --header T Chr06 1812818 2563869 > /home/user/bsa/qtl_pick_dir/M1.hap
            my $cd16 = "$rg $chrom $dir/m_hap | perl $get_bed_v2 --header T $qtl_start $qtl_end > $out_hap";
            # /home/user/BSATOS2/tools/bin/rg Chr06 /home/user/bsa/qtl_pick_dir/m_hap perl | /home/user/BSATOS2/scripts/get_bed_v2.pl --header T 1812818 2563869 > /home/user/bsa/qtl_pick_dir/M1.hap
            # &process_cmd($cd16, $lo, $mx16);

            $idx++;
            my $i13 = "CMD".$idx.":";
            print STDOUT "obtain and filter SVs in QTL $accession region\n";
            print $lo "obtain and filter SVs in QTL $accession region\n$i13\t";
            unless(defined($hlog{$i13})) {$hlog{$i13} = 0;}
            my $mx17 = $hlog{$i13};
            # my $cd17 = "perl $get_bed $sv $chrom $qtl_start $qtl_end | perl $filter_sv --d $pr --i $origin -> $out_sv";
            # perl /home/user/BSATOS2/scripts/get_bed.pl prepar_dir/anno/sv.AT_multianno.txt Chr06 1812818 2563869 | perl /home/user/BSATOS2/scripts/filter_sv_based_on_qtl.pl --d 2000 --i M -> /home/user/bsa/qtl_pick_dir/M1.sv
            my $cd17 = "$rg $chrom $sv | perl $get_bed_v2 $qtl_start $qtl_end | perl $filter_sv --d $pr --i $origin -> $out_sv";
            # /home/user/BSATOS2/tools/bin/rg Chr06 prepar_dir/anno/sv.AT_multianno.txt | perl /home/user/BSATOS2/scripts/get_bed_v2.pl 1812818 2563869 | perl /home/user/BSATOS2/scripts/filter_sv_based_on_qtl.pl --d 2000 --i M -> /home/user/bsa/qtl_pick_dir/M1.sv
            &process_cmd($cd17, $lo, $mx17);

            $idx++;
            my $i14 = "CMD".$idx.":";
            print STDOUT "convert filtered SVs file of QTL $accession to IGV format\n";
            print $lo "convert filtered SVs file of QTL $accession to IGV format\n$14\t";
            unless(defined($hlog{$i14})) {$hlog{$i14} = 0;}
            my $mx18 = $hlog{$i14};
            my $cd18 = "perl $vcf_maf --var sv --in $out_sv";
            # perl /home/user/BSATOS2/scripts/generate_vcf_and_maf.pl --var sv --in /home/user/bsa/qtl_pick_dir/M1.sv
            &process_cmd($cd18, $lo, $mx18);

            $idx++;
            my $i15 = "CMD".$idx.":";
            print STDOUT "plot profiles of QTL $accession\n";
            print $lo "plot profiles of QTL $accession\n$i15\t";
            unless(defined($hlog{$i15})) {$hlog{$i15} = 0;}
            my $mx19 = $hlog{$i15};
            my $cd19 = "Rscript $sub_plot $gp $chrom $qtl_start $qtl_end $accession";
            # Rscript /home/user/BSATOS2/R/sub_plot.R polish_dir/P.polished.afd Chr06 1812818 2563869 M1
            &process_cmd($cd19, $lo, $mx19);
        }

############################################################################

        if($origin eq "H"){

            $idx++;
            my $i16 = "CMD".$idx.":";
            print STDOUT "obtain and filter SNVs in QTL $accession region based on genetic laws\n";
            print $lo "obtain and filter SNVs in QTL $accession region based on genetic laws\n$i16\t";
            unless(defined($hlog{$i16})) {$hlog{$i16} = 0;}
            my $mx20 = $hlog{$i16};
            # my $cd20 = "perl $get_bed $snv $chrom $qtl_start $qtl_end | perl $filter_snv --d $pr --q $q --i $origin --hap $dir/pm_hap -> $out_snv";
            # perl /home/user/BSATOS2/scripts/get_bed.pl prepar_dir/anno/snv.AT_multianno.txt Chr06 3058865 3558865 | perl /home/user/BSATOS2/scripts/filter_snv_based_on_qtl.pl --d 2000 --q 10 --i H --hap /home/user/bsa/qtl_pick_dir/pm_hap -> /home/user/bsa/qtl_pick_dir/H1.snv
            my $cd20 = "$rg $chrom $snv | perl $get_bed_v2 $qtl_start $qtl_end | perl $filter_snv -d $pr -q $q -i $origin --hap $dir/pm_hap -> $out_snv";
            # /home/user/BSATOS2/tools/bin/rg Chr06 prepar_dir/anno/snv.AT_multianno.txt | perl /home/user/BSATOS2/scripts/get_bed_v2.pl 3058865 3558865 | perl /home/user/BSATOS2/scripts/filter_snv_based_on_qtl.pl -d 2000 -q 10 -i H --hap /home/user/bsa/qtl_pick_dir/pm_hap -> /home/user/bsa/qtl_pick_dir/H1.snv
            &process_cmd($cd20, $lo, $mx20);

            $idx++;
            my $i17 = "CMD".$idx.":";
            print STDOUT "convert filtered SNVs file of QTL $accession to IGV format\n";
            print $lo "convert filtered SNVs file of QTL $accession to IGV format\n$i17\t";
            unless(defined($hlog{$i17})) {$hlog{$i17} = 0;}
            my $mx21 = $hlog{$i17};
            my $cd21 = "perl $vcf_maf --var snv --in $out_snv";
            # perl /home/user/BSATOS2/scripts/generate_vcf_and_maf.pl --var snv --in /home/user/bsa/qtl_pick_dir/H1.snv
            &process_cmd($cd21, $lo, $mx21);

            $idx++;
            my $i18 = "CMD".$idx.":";
            print STDOUT "obtain enriched haplotype information in QTL $accession region\n";
            print $lo "obtain enriched haplotype information in QTL $accession region\n$i18\t";
            # my $cd22 = "perl $get_bed $dir/pm_hap --header T $chrom $qtl_start $qtl_end > $out_hap";
            # perl /home/user/BSATOS2/scripts/get_bed.pl /home/user/bsa/qtl_pick_dir/pm_hap --header T Chr06 3058865 3558865 > /home/user/bsa/qtl_pick_dir/H1.hap
            my $cd22 = "$rg $chrom $dir/pm_hap | perl $get_bed_v2 --header T $qtl_start $qtl_end > $out_hap";
            # /home/user/BSATOS2/tools/bin/rg Chr06 /home/user/bsa/qtl_pick_dir/pm_hap | perl /home/user/BSATOS2/scripts/get_bed_v2.pl --header T 3058865 3558865 > /home/user/bsa/qtl_pick_dir/H1.hap
            unless(defined($hlog{$i18})){$hlog{$i18} = 0;}
            my $mx22 = $hlog{$i18};
            # &process_cmd($cd22, $lo, $mx22);

            $idx++;
            my $i19 = "CMD".$idx.":";
            print STDOUT "obtain and filter SVs in QTL $accession region\n";
            print $lo "obtain and filter SVs in QTL $accession region\n$i19\t";
            # my $cd23 = "perl $get_bed $sv $chrom $qtl_start $qtl_end | perl $filter_sv --d $pr --i $origin -> $out_sv";
            # perl /home/user/BSATOS2/scripts/get_bed.pl prepar_dir/anno/sv.AT_multianno.txt Chr06 3058865 3558865 | perl /home/user/BSATOS2/scripts/filter_sv_based_on_qtl.pl --d 2000 --i H -> /home/user/bsa/qtl_pick_dir/H1.sv
            my $cd23 = "$rg $chrom $sv | perl $get_bed_v2 $qtl_start $qtl_end | perl $filter_sv --d $pr --i $origin -> $out_sv";
            # /home/user/BSATOS2/tools/bin/rg Chr06 prepar_dir/anno/sv.AT_multianno.txt | perl /home/user/BSATOS2/scripts/get_bed_v2.pl 3058865 3558865 | perl /home/user/BSATOS2/scripts/filter_sv_based_on_qtl.pl --d 2000 --i H -> /home/user/bsa/qtl_pick_dir/H1.sv
            unless(defined($hlog{$i19})) {$hlog{$i19} = 0;}
            my $mx23 = $hlog{$i19};
            &process_cmd($cd23, $lo, $mx23);

            $idx++;
            my $i20 = "CMD".$idx.":";
            print STDOUT "convert filtered SVs file of QTL $accession to IGV format\n";
            print $lo "convert filtered SVs file of QTL $accession to IGV format\n$i20\t";
            my $cd24 = "perl $vcf_maf --var sv --in $out_sv";
            # perl /home/user/BSATOS2/scripts/generate_vcf_and_maf.pl --var sv --in /home/user/bsa/qtl_pick_dir/H1.sv
            unless(defined($hlog{$i20})) {$hlog{$i20} = 0;}
            my $mx24 = $hlog{$i20};
            &process_cmd($cd24, $lo, $mx24);

            $idx++;
            my $i21 = "CMD".$idx.":";
            print STDOUT "plot profiles of QTL $accession\n";
            print $lo "plot profiles of QTL $accession\n$i21\t";
            my $cd25 = "Rscript $sub_plot $gp $chrom $qtl_start $qtl_end $accession";
            # Rscript /home/user/BSATOS2/R/sub_plot.R polish_dir/P.polished.afd Chr06 3058865 3558865 H1
            unless(defined($hlog{$i21})) {$hlog{$i21} = 0;}
            my $mx25 = $hlog{$i21};
            &process_cmd($cd25, $lo, $mx25);
        }
    }

    $idx++;
    my $i22 = "CMD".$idx.":";
    print STDOUT "move & remove some files\n";
    print $lo "move PDF files to qtl_pick_dir\n$i22\t";
    unless(defined($hlog{$i22})) {$hlog{$i22} = 0;}
    my $mx26 = $hlog{$i22};
    my $cmd26 = "mkdir -p $dir/PDF/subplot && mkdir -p $dir/PNG/subplot && mv Chr*_*.pdf $dir/PDF/subplot && mv Chr*.pdf $dir/PDF && mv Chr*_*.png $dir/PNG/subplot && mv Chr*.png $dir/PNG";
    # mkdir -p /home/user/bsa/qtl_pick_dir/PDF/subplot && mkdir -p /home/user/bsa/qtl_pick_dir/PNG/subplot
    # mv Chr*_*.pdf /home/user/bsa/qtl_pick_dir/PDF/subplot && mv Chr*.pdf /home/user/bsa/qtl_pick_dir/PDF
    # mv Chr*_*.png /home/user/bsa/qtl_pick_dir/PNG/subplot && mv Chr*.png /home/user/bsa/qtl_pick_dir/PNG
    &process_cmd($cmd26, $lo, $mx26);

    $idx++;
    my $i23 = "CMD".$idx.":";
    print STDOUT "integrate variant infomation with QTLs\n";
    print $lo "integrate variant infomation with QTLs\n$i23\t";
    unless(defined($hlog{$i23})) {$hlog{$i23} = 0;}
    my $mx27 = $hlog{$i23};
    # my $cmd27 = "perl $qtl_info_pl --qtl $qtl --snv $qtl_snv --sv $qtl_sv --dir $dir/SNV_SV";
    # perl /home/user/BSATOS2/scripts/qtl_info.pl --qtl /home/user/bsa/qtl_pick_dir/QTL.txt --snv /home/user/bsa/qtl_pick_dir/QTL_SNV_genes.txt --sv /home/user/bsa/qtl_pick_dir/QTL_SV_genes.txt --dir /home/user/bsa/qtl_pick_dir/SNV_SV
    my $cmd27 = "python $qtl_info_py -qtl $qtl -snv $qtl_snv -sv $qtl_sv -dir $dir/SNV_SV";
    # python /home/user/BSATOS2/Python/qtl_info.py -qtl /home/user/bsa/qtl_pick_dir/QTL.txt -snv /home/user/bsa/qtl_pick_dir/QTL_SNV_genes.txt -sv /home/user/bsa/qtl_pick_dir/QTL_SV_genes.txt -dir /home/user/bsa/qtl_pick_dir/SNV_SV
    &process_cmd($cmd27, $lo, $mx27);

    if ($db eq "Malus_domestica") {
        $idx++;
        my $i24 = "CMD".$idx.":";
        print STDOUT "add function annotation and expression pattern information for genes in QTL\n";
        print $lo "add function annotation and expression pattern information for genes in QTL\n$i24\t";
        unless(defined($hlog{$i24})) {$hlog{$i24} = 0;}
        my $mx28 = $hlog{$i24};
        my $cmd28 = "python $mdo_gdr_expression -expanno $exp_anno_dir -gene $qtl_snv -polish polish_dir -out $qtl_snv_anno -var snv && python $mdo_gdr_expression -expanno $exp_anno_dir -gene $qtl_sv -polish polish_dir -out $qtl_sv_anno -var sv";
        # python /home/user/BSATOS2/Python/mdo_gdr_expression.py -expanno /home/user/BSATOS2/ExpAnno -gene /home/user/bsa/qtl_pick_dir/QTL_SNV_genes.txt -polish polish_dir -out /home/user/bsa/qtl_pick_dir/QTL_SNV_anno.txt -var snv
        # python /home/user/BSATOS2/Python/mdo_gdr_expression.py -expanno /home/user/BSATOS2/ExpAnno -gene /home/user/bsa/qtl_pick_dir/QTL_SV_genes.txt -polish polish_dir -out /home/user/bsa/qtl_pick_dir/QTL_SV_anno.txt -var sv
        &process_cmd($cmd28, $lo, $mx28);

        $idx++;
        my $i25 = "CMD".$idx.":";
        print STDOUT "covert QTL, Func_Anno and variation files to Excel\n";
        print $lo "covert QTL, Func_Anno and variation files to Excel\n$i25\t";
        unless(defined($hlog{$i25})) {$hlog{$i25} = 0;}
        my $mx29 = $hlog{$i25};
        my $cmd29 = "python $txt2excel -snv $qtl_snv_anno -sv $qtl_sv_anno -xlsx $qtl_anno_excel";
        # python /home/user/BSATOS2/Python/txt2excel.py -snv /home/user/bsa/qtl_pick_dir/QTL_SNV_anno.txt -sv /home/user/bsa/qtl_pick_dir/QTL_SV_anno.txt /home/user/bsa/qtl_pick_dir/QTL_anno.xlsx
        &process_cmd($cmd29, $lo, $mx29);
    }

    $idx++;
    my $i26 = "CMD".$idx.":";
    print STDOUT "obtain QTL peak region based on specified interval\n";
    print $lo "obtain QTL peak region based on specified interval\n$i26\t";
    unless(defined($hlog{$i26})) {$hlog{$i26} = 0;}
    my $mx30 = $hlog{$i26};
    my $cmd30 = "python $main_peak_int_py -int 100 -qtl $qtl -out $dir/QTL_100K.txt && python $main_peak_int_py -int 250 -qtl $qtl -out $dir/QTL_250K.txt";
    # python /home/user/BSATOS2/scripts/main_peak_int.py -int 100 -qtl /home/user/bsa/qtl_pick_dir/QTL.txt -out /home/user/bsa/qtl_pick_dir/QTL_100K.txt
    # python /home/user/BSATOS2/scripts/main_peak_int.py -int 250 -qtl /home/user/bsa/qtl_pick_dir/QTL.txt -out /home/user/bsa/qtl_pick_dir/QTL_250K.txt
    &process_cmd($cmd30, $lo, $mx30);

    my $snv_sv_dir = $dir . "/SNV_SV";     # /home/user/bsa/qtl_pick_dir/SNV_SV
    unless(-e $snv_sv_dir) {system("mkdir -p $snv_sv_dir");}
    my @snv_sv_files = glob("$dir/P*.snv $dir/M*.snv $dir/H*.snv $dir/P*.sv $dir/M*.sv $dir/H*.sv");
    unless($#snv_sv_files == -1){
        $idx++;
        my $i27 = "CMD".$idx.":";
        print STDOUT "move SNV & SV files to SNV_SV\n";
        print $lo "move SNV & SV files to SNV_SV\n$i27\t";
        unless(defined($hlog{$i27})) {$hlog{$i27} = 0;}
        my $mx31 = $hlog{$i27};
        my $cmd31 = "mv $dir/*.snv $dir/*.sv $snv_sv_dir";
        # mv /home/user/bsa/qtl_pick_dir/*.snv /home/user/bsa/qtl_pick_dir/*.sv /home/user/bsa/qtl_pick_dir/SNV_SV
        &process_cmd($cmd31, $lo, $mx31);
    }

    $idx++;
    my $i28 = "CMD".$idx.":";
    print STDOUT "get summary information from the QTL SNV file and QTL SV file\n";
    print $lo "get summary information from the QTL SNV file and QTL SV file\n$i28\t";
    unless(defined($hlog{$i28})) {$hlog{$i28} = 0;}
    my $mx32 = $hlog{$i28};
    my $cmd32 = "perl $summary --dir $dir --qtl $qtl --snv $qtl_snv --sv $qtl_sv --vcfdir $dir/SNV_SV --pmsnv prepar_dir/P_M.snv --pmsv prepar_dir/P_M.ins.vcf --out $dir/README.txt";
    # perl /home/user/BSATOS2/scripts/summary.pl --dir /home/user/bsa/qtl_pick_dir --qtl /home/user/bsa/qtl_pick_dir/QTL.txt --snv /home/user/bsa/qtl_pick_dir/QTL_SNV_genes.txt --sv /home/user/bsa/qtl_pick_dir/QTL_SV_genes.txt --vcfdir /home/user/bsa/qtl_pick_dir/SNV_SV --pmsnv prepar_dir/P_M.snv --pmsv prepar_dir/P_M.ins.vcf --out /home/user/bsa/qtl_pick_dir/README.txt
    &process_cmd($cmd32, $lo, $mx32);

    $idx++;
    my $i29 = "CMD".$idx.":";
    print STDOUT "get summary information from the QTL SNV file and QTL SV file\n";
    print $lo "get summary information from the QTL SNV file and QTL SV file\n$i29\t";
    unless(defined($hlog{$i29})) {$hlog{$i29} = 0;}
    my $mx33 = $hlog{$i29};
    my $cmd33 = "Rscript $summary_plot polish_dir $p_thres[0] $m_thres[0] $pm_thres[0] summary_plot";
    # Rscript /home/user/BSATOS2/R/summary_plot.R polish_dir 2.771678 7.303028 3.390983 summary_plot
    &process_cmd($cmd32, $lo, $mx33);

    print STDOUT "\nqtl_pick module finished!!!\n";
    print $lo "\nqtl_pick_module finished!!!\n";
    exit(0);
}


############################################################################

sub process_cmd {
    my ($cmd, $lk, $hg) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t................................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t................................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime = "[".localtime()."]";
        if($ret){
            print $lk "\#die\t$starttime ... $endtime\n";
            print STDOUT "\#die\t$starttime ... $endtime\n";
            die "Error, cmd: < $cmd > died with return value $ret";
        }else{
            print $lk "\#done\t$starttime ... $endtime\n";
            print STDOUT "\#done\t$starttime ... $endtime\n";
        }
    return;
    }
}

############################################################################

sub get_chr {
    my @para = @_;
    # my (%hash, @line, @chrs);
    # open LOD, "<$para[0]";
    # while(<LOD>){
    #     chomp;
    #     if($_ =~ /#/) {next;}
    #     @line = split /\t/, $_;     # CHROM	POS	H_REF	H_ALT	L_REF	L_ALT
    #     $hash{$line[0]}++;     # 以染色体号为键, 增加计数
    # }
    # @chrs = keys %hash;
    # return(@chrs);

    my $chrs = `cut -f1 $para[0] | $para[1] -vN "#" | uniq`;
    chomp($chrs);
    my @chrs = split /\n/, $chrs;
    return(@chrs);
}
