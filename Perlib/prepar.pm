#!/usr/bin/env perl
package prepar;
BEGIN {my $VERSION = '2.0';}

use Cwd;
use strict;
use Getopt::Long;
use base 'Exporter';
use FindBin qw($Bin);
use File::Basename;
our @EXPORT = qw(runprepar);

my $G_USAGE = "
Usage: bastos2 prepar [options]

Options:
    --o        [STR]        name prefix of output directory [prepar]
    --r        [FILE]       reference genome (fasta format) [Malus_domestica.fa]
    --gtf      [FILE]       GTF file of genes (GTF2 format; Not required if GFF file is provided) [Malus_domestica.gtf]
    --gff      [FILE]       GFF file of genes (GFF3 format; Not required if GTF file is provided) [Malus_domestica.gff3]
    --pf1      [FILE]       paired-end1 fastq file from pollen parent   (FASTQ format; Not required if pre-aligned BAM files is provided)
    --pf2      [FILE]       paired-end2 fastq file from pollen parent   (FASTQ format; Not required if pre-aligned BAM files is provided)
    --mf1      [FILE]       paired-end1 fastq file from maternal parent (FASTQ format; Not required if pre-aligned BAM files is provided)
    --mf2      [FILE]       paired-end2 fastq file from maternal parent (FASTQ format; Not required if pre-aligned BAM files is provided)
    --pb       [FILE]       pre-aligned bam file from pollen parent     (Not required if --pf1 & --pf2 is provided)
    --mb       [FILE]       pre-aligned bam file from maternal parent   (Not required if --mf1 & --mf2 is provided)
    --log      [FILE]       prepar module log file [prepar.log]
    --db       [STR]        database directory of .fa & GTF & GFF files [Malus_domestica]

Aligment Options:
    --t        [INT]        number of threads [1]

SNV Calling Options:
    --aq       [INT]        skip alignments with mapQ smaller than [10]

SV Calling Options:
    --vq       [INT]        skip alignments with mapQ smaller than [10]

SNV filtering Options:
    --d        [INT]        skip SNVs with reads depth smaller than [10]
    --sq       [INT]        skip SNVs with phred-scaled quality smaller than [10]

SV filtering Options:
    --dp       [INT]        support reads number including paired-reads and split reads [10]
    --mq       [INT]        average mapQ [10]
    --l        [INT]        minimum SV length [1000]
    --precise  [STR]        only kept precise SVs [yes]

Outputs:
    prefix_dir   [DIR]
        P_M.p    [FILE]     genotype of the markers are homozygous in maternal parent but are heterozygous in pollen parent
        P_M.m    [FILE]     genotype of the markers are homozygous in pollen parent but are heterozygous in maternal parent
        P_M.pm   [FILE]     genotype of the markers are both heterozygous in maternal parent and pollen parent
        P_M.snv  [FILE]     SNVs vcf file of two parents
        P_M.sv   [FILE]     SVs vcf file of two parents

    prefix_dir/anno   [DIR]
        AT_refGene.txt          [FILE]      GenePred file
        AT_refGeneMrna.fa       [FILE]      transcript FASTA file
        snv.AT_multianno.txt    [FILE]      SNVs multianno file
        snv.AT_multianno.vcf    [FILE]      SNVs annotated VCF file
        snv.avinput             [FILE]      SNVs input file
        sv.AT_multianno.txt     [FILE]      SVs multianno file
        sv.AT_multianno.vcf     [FILE]      SVs annotated VCF file
        sv.avinput              [FILE]      SVs input file

Example:
    1) Use paired-end fastq files to run BSATOS2
        bsatos2 prepar --o prepar --pf1 P_1.fastq --pf2 P_2.fastq --mf1 M_1.fastq --mf2 M_2.fastq --r Malus_domestica.fa --gtf Malus_domestica.gtf
    2) Use pre-aligned BAM files to run BSATOS2
        bsatos2 prepar --o prepar --pb P.bam --mb M.bam --r Malus_domestica.fa --gtf Malus_domestica.gtf
";


sub runprepar {
    my $pf1 = undef;        my $pf2 = undef;         my $mf1 = undef;        my $mf2 = undef;       my $gff = undef;
    my $genome = undef;     my $header = undef;      my $pb = undef;         my $mb = undef;        my $l   = undef;
    my $help = undef;       my $gtf = undef;         my $s = undef;          my $t = undef;         my $dp  = undef;
    my $aq = undef;         my $vq = undef;          my $dep = undef;        my $sq = undef;        my $mq  = undef;
    my $gff = undef;        my $precise = undef;     my $log = undef;        my $db = undef;        my $prefix = undef;

    GetOptions (
        "pf1=s" => \$pf1,   "pf2=s" => \$pf2,    "mf1=s" => \$mf1,   "mf2=s" => \$mf2,
        "pb=s" => \$pb,     "mb=s" => \$mb,      "r=s" => \$genome,  "o=s" => \$prefix,
        "gtf=s" => \$gtf,   "gff=s" => \$gff,    "s=s" => \$s,       "t=s" => \$t,
        "aq=i" => \$aq,     "vq=i" => \$vq,      "d=i" => \$dep,     "sq=i" => \$sq,
        "log=s" => \$log,   "dp=i" => \$dp,      "mq=i" => \$mq,     "l=i" => \$l,
        "db=s" => \$db,     "precise=s" => \$precise,   "help" => \$help)
    or die("$G_USAGE");
    die "$G_USAGE" if ($help);
    die "$G_USAGE" if (!defined $prefix);

    my $para_prepar_file = "$FindBin::Bin/Python/src/para_prepar_file";
    if (-e $para_prepar_file){
        open PARAPREPARFILE, "$para_prepar_file" or die $!;
        while(<PARAPREPARFILE>){
            chomp;
            my @para = split /\t/, $_;
            if (@para[0] =~ /^o\b/ && @para[1] =~ /\w/) {$prefix = @para[1];}
            if (@para[0] =~ /^r\b/ && @para[1] =~ /\w/) {$genome = @para[1];}
            if (@para[0] =~ /^gtf\b/ && @para[1] =~ /\w/) {$gtf = @para[1];}
            if (@para[0] =~ /^gff\b/ && @para[1] =~ /\w/) {$gff = @para[1];}
            if (@para[0] =~ /^pf1\b/ && @para[1] =~ /\w/) {$pf1 = @para[1];}
            if (@para[0] =~ /^pf2\b/ && @para[1] =~ /\w/) {$pf2 = @para[1];}
            if (@para[0] =~ /^mf1\b/ && @para[1] =~ /\w/) {$mf1 = @para[1];}
            if (@para[0] =~ /^mf2\b/ && @para[1] =~ /\w/) {$mf2 = @para[1];}
            if (@para[0] =~ /^pb\b/ && @para[1] =~ /\w/) {$pb = @para[1];}
            if (@para[0] =~ /^mb\b/ && @para[1] =~ /\w/) {$mb = @para[1];}
            if (@para[0] =~ /^t\b/ && @para[1] =~ /\w/) {$t = @para[1];}
            if (@para[0] =~ /^aq\b/ && @para[1] =~ /\w/) {$aq = @para[1];}
            if (@para[0] =~ /^dp\b/ && @para[1] =~ /\w/) {$dp = @para[1];}
            if (@para[0] =~ /^vq\b/ && @para[1] =~ /\w/) {$vq = @para[1];}
            if (@para[0] =~ /^mq\b/ && @para[1] =~ /\w/) {$mq = @para[1];}
            if (@para[0] =~ /^sq\b/ && @para[1] =~ /\w/) {$sq = @para[1];}
            if (@para[0] =~ /^l\b/ && @para[1] =~ /\w/) {$l = @para[1];}
            if (@para[0] =~ /^d\b/ && @para[1] =~ /\w/) {$dep = @para[1];}
            if (@para[0] =~ /^precise\b/ && @para[1] =~ /\w/) {$precise = @para[1];}
            if (@para[0] =~ /^db\b/ && @para[1] =~ /\w/) {$db = @para[1];}
        }
        close PARAPREPARFILE;
    }

    unless(defined($dp)) {$dp = 10;}
    unless(defined($mq)) {$mq = 10;}
    unless(defined($l)) {$l = 1000;}
    unless(defined($t)) {$t = 1;}
    unless(defined($aq)) {$aq = 10;}
    unless(defined($vq)) {$vq = 10;}
    unless(defined($dep)) {$dep = 10;}
    unless(defined($sq)) {$sq = 10;}
    unless(defined($prefix)) {$prefix = "prepar";}
    unless(defined($precise)) {$precise = "yes";}
    unless(defined($header)) {$header = "yes";}

    my $delly = "delly";
    my $gffread = "gffread";
    my $minimap2 = "minimap2";
    my $gtfpred = "gtfToGenePred";
    my $bwa_mem2 = "bwa-mem2";
    my $rg = "$FindBin::Bin/tools/bin/rg";                                      # /home/user/BSATOS2/tools/bin/rg
    my $samtools = "$FindBin::Bin/tools/bin/samtools";                          # /home/user/BSATOS2/tools/bin/samtools
    my $bcftools = "$FindBin::Bin/tools/bin/bcftools";                          # /home/user/BSATOS2/tools/bin/bcftools
    my $work_dir = getcwd;                                                      # /home/user/bsa
    my $dir = $work_dir."/".$prefix."_dir";                                     # /home/user/bsa/prepar_dir
    my $P_M = $dir."/P_M";                                                      # /home/user/bsa/prepar_dir/P_M
    my $pb_bai = $pb.".bai";                                                    # /home/user/bsa/prepar_dir/P.bam.bai
    my $mb_bai = $mb.".bai";                                                    # /home/user/bsa/prepar_dir/M.bam.bai
    my $pb_flagstat = $pb.".flagstat";                                          # /home/user/bsa/prepar_dir/P.bam.flagstat
    my $mb_flagstat = $mb.".flagstat";                                          # /home/user/bsa/prepar_dir/M.bam.flagstat
    my $P_sam = $dir."/".$prefix."_P.sam";                                      # /home/user/bsa/prepar_dir/prepar_P.sam
    my $P_bam = $dir."/".$prefix."_P.bam";                                      # /home/user/bsa/prepar_dir/prepar_P.bam
    my $P_srt_bam = $dir."/".$prefix."_P_srt.bam";                              # /home/user/bsa/prepar_dir/prepar_P_srt.bam
    my $P_rm_bam = $dir."/".$prefix."_P_rm.bam";                                # /home/user/bsa/prepar_dir/prepar_P_rm.bam
    my $P_rm_bam_bai = $dir."/".$prefix."_P_rm.bam.bai";                        # /home/user/bsa/prepar_dir/prepar_P_rm.bam.bai
    my $P_rm_bam_flagstat = $dir."/".$prefix."_P_rm.bam.flagstat";              # /home/user/bsa/prepar_dir/prepar_P_rm.bam.flagstat
    my $M_sam = $dir."/".$prefix."_M.sam";                                      # /home/user/bsa/prepar_dir/prepar_M.sam
    my $M_bam = $dir."/".$prefix."_M.bam";                                      # /home/user/bsa/prepar_dir/prepar_M.bam
    my $M_srt_bam = $dir."/".$prefix."_M_srt.bam";                              # /home/user/bsa/prepar_dir/prepar_M_srt.bam
    my $M_rm_bam = $dir."/".$prefix."_M_rm.bam";                                # /home/user/bsa/prepar_dir/prepar_M_rm.bam
    my $M_rm_bam_bai = $dir."/".$prefix."_M_rm.bam.bai";                        # /home/user/bsa/prepar_dir/prepar_M_rm.bam.bai
    my $M_rm_bam_flagstat = $dir."/".$prefix."_M_rm.bam.flagstat";              # /home/user/bsa/prepar_dir/prepar_M_rm.bam.flagstat
    my $P_M_snv = $dir."/P_M.snv";                                              # /home/user/bsa/prepar_dir/P_M.snv
    my $P_M_snv_unfilter = $dir."/P_M.snv.unfilter";                            # /home/user/bsa/prepar_dir/P_M.snv.unfilter
    my $P_M_del_bcf = $dir."/P_M.del.bcf";                                      # /home/user/bsa/prepar_dir/P_M.del.bcf
    my $P_M_del_bcf_csi = $dir."/P_M.del.bcf.csi";                              # /home/user/bsa/prepar_dir/P_M.del.bcf.csi
    my $P_M_del_vcf = $dir."/P_M.del.vcf";                                      # /home/user/bsa/prepar_dir/P_M.del.vcf
    my $P_M_ins_bcf = $dir."/P_M.ins.bcf";                                      # /home/user/bsa/prepar_dir/P_M.ins.bcf
    my $P_M_ins_bcf_csi = $dir."/P_M.ins.bcf.csi";                              # /home/user/bsa/prepar_dir/P_M.ins.bcf.csi
    my $P_M_ins_vcf = $dir."/P_M.ins.vcf";                                      # /home/user/bsa/prepar_dir/P_M.ins.vcf
    my $P_M_sv = $dir."/P_M.sv";                                                # /home/user/bsa/prepar_dir/P_M.sv
    my $sum = $dir."/summary";                                                  # /home/user/bsa/prepar_dir/summary
    my $anno_dir = $dir."/anno";                                                # /home/user/bsa/prepar_dir/anno
    my $out_snv = $anno_dir."/snv";                                             # /home/user/bsa/prepar_dir/anno/snv
    my $out_sv = $anno_dir."/sv";                                               # /home/user/bsa/prepar_dir/anno/sv
    my $filter_get_genotype = "$FindBin::Bin/scripts/filter_get_genotype.pl";   # /home/user/BSATOS2/scripts/filter_get_genotype.pl
    my $filter_raw_sv = "$FindBin::Bin/scripts/filter_raw_sv.pl";               # /home/user/BSATOS2/scripts/filter_raw_sv.pl
    my $table_annovar = "$FindBin::Bin/scripts/table_annovar.pl";               # /home/user/BSATOS2/scripts/table_annovar.pl
    my $retrieve = "$FindBin::Bin/scripts/retrieve_seq_from_fasta.pl";          # /home/user/BSATOS2/scripts/retrieve_seq_from_fasta.pl

    unless(defined($log)) {$log = $work_dir."/".$prefix.".log";}                # /home/user/bsa/prepar.log
    unless(defined($db)) {$db = "Malus_domestica";}
    unless(defined($gtf)) {$gtf = "$FindBin::Bin/database/$db/$db.gtf";}        # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf
    unless(defined($genome)) {
        if (-e "$FindBin::Bin/database/$db/$db.fa"){$genome = "$FindBin::Bin/database/$db/$db.fa";}         # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa
        if (-e "$FindBin::Bin/database/$db/$db.fasta"){$genome = "$FindBin::Bin/database/$db/$db.fasta";}   # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fasta
    }
    unless(defined($gff)) {
        if (-e "$FindBin::Bin/database/$db/$db.gff"){$gff = "$FindBin::Bin/database/$db/$db.gff";}       # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff
        if (-e "$FindBin::Bin/database/$db/$db.gff3"){$gff = "$FindBin::Bin/database/$db/$db.gff3";}     # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff3
    }

#############################################################

    my %hlog = ();
    if(-e $log){
        open LOD, "<$log";
        my $count = 0;
        while(<LOD>){
            chomp;
            my @sp = split /\t/, $_;
            if($sp[2] eq "\#done") {$hlog{$sp[0]} = 1;}
            if($sp[2] eq "\#die") {$hlog{$sp[0]} = 0;}
            if($_ =~ /finish/) {$count++;}
        }
        if($count != 0){
            print "
                Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
                Version: 2.0
                The first module: prepar
                prepar      prepare the parents data\n\n
                This module has been finished!!!!!\n\n";
            exit(0);
        }
    }
    open (my $lo, ">>$log") or die $!;
    open SS, ">>$sum";

    print STDOUT "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The first module: prepar
        prepar      prepare the parents data\n\n
        ██████╗ ███████╗ █████╗ ████████╗ ██████╗ ███████╗██████╗
        ██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██╔═══██╗██╔════╝╚════██╗
        ██████╔╝███████╗███████║   ██║   ██║   ██║███████╗ █████╔╝
        ██╔══██╗╚════██║██╔══██║   ██║   ██║   ██║╚════██║██╔═══╝
        ██████╔╝███████║██║  ██║   ██║   ╚██████╔╝███████║███████╗
        ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝╚══════╝\n\n";

    print $lo "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The first module: prepar
        prepar      prepare the parents data\n\n";

#############################################################

    my $mmi = $genome.".mmi";   # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.mmi
    my $fai = $genome.".fai";   # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fai

    # print STDOUT "setup OMP threads number\n";
    # print $lo "setup OMP threads number:\nCMD0:\t";
    # unless(defined($hlog{"CMD0:"})) {$hlog{"CMD0:"} = 0;}
    # my $mx0 = $hlog{"CMD0:"};
    # my $cd0 = "export OMP_NUM_THREADS=$t";
    # &process_cmd($cd0, $lo, $mx0);

    if(!defined($gtf) && defined($gff)){
        $gtf = (split /\./, basename($gff))[0] . ".gtf";
        print STDOUT "convert GFF to GTF file:\n";
        print $lo "convert GFF to GTF file:\nCMD1:\t";
        unless(defined($hlog{"CMD1:"})) {$hlog{"CMD1:"} = 0;}
        my $mx1 = $hlog{"CMD1:"};
        my $cd1 = "$gffread $gff -T -o $gtf";
        # gffread /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff -T -o /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf
        &process_cmd($cd1, $lo, $mx1);
    }

    unless(-f $fai){
        print STDOUT "samtools index reference genome\n";
        print $lo "samtools index reference genome\nCMD2:\t";
        unless(defined($hlog{"CMD2:"})) {$hlog{"CMD2:"} = 0;}
        my $mx2 = $hlog{"CMD2:"};
        my $cd2 = "$samtools faidx $genome";
        # samtools faidx /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa
        &process_cmd($cd2, $lo, $mx2);
    }

    unless(-e $dir){
        print STDOUT "create result dir\n";
        print $lo "create result dir\nCMD3:\t";
        unless(defined($hlog{"CMD3:"})) {$hlog{"CMD3:"} = 0;}
        my $mx3 = $hlog{"CMD3:"};
        my $cd3 = "mkdir $dir";   # mkdir /home/user/bsa/prepar_dir
        &process_cmd($cd3, $lo, $mx3);
    }

    unless(defined($pb) || defined($mb)){
        unless(-f $mmi){
            print STDOUT "minimap index reference genome\n";
            print $lo "minimap index reference genome\nCMD4:\t";
            unless(defined($hlog{"CMD4:"})) {$hlog{"CMD4:"} = 0;}
            my $mx4 = $hlog{"CMD4:"};
            my $cd4 = "$minimap2 -d $mmi $genome";
            # minimap2 -d /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.mmi /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa
            &process_cmd($cd4, $lo, $mx4);
        }

        print STDOUT "align pollen parent reads to reference genome and generate BAM file\n";
        print $lo "align pollen parent reads to reference genome and generate BAM file\nCMD5:\t";
        unless(defined($hlog{"CMD5:"})) {$hlog{"CMD5:"} = 0;}
        my $mx5 = $hlog{"CMD5:"};
        my $cd5 = "$minimap2 -ax sr -t $t -R \"\@RG\\tID:P\\tSM:P\" $mmi $pf1 $pf2 -o $P_sam && $samtools view -@ $t -q $mq -bS -o $P_bam $P_sam && rm $P_sam";
        # minimap2 -ax sr -t 1 -R "@RG\tID:P\tSM:P" /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.mmi P_1.fastq P_2.fastq -o /home/user/bsa/prepar_dir/prepar_P.sam &&
        # samtools view -@ 1 -q 10 -bS -o /home/user/bsa/prepar_dir/prepar_P.bam /home/user/bsa/prepar_dir/prepar_P.sam && rm /home/user/bsa/prepar_dir/prepar_P.sam
        ## my $cd5 = "$bwa_mem2 index $genome && $bwa_mem2 mem -t $t $genome $pf1 $pf2 -o $P_sam && $samtools view -@ $t -q $mq -bS -o $P_bam $P_sam && rm $P_sam";
        ## bwa-mem2 index /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa && bwa-mem2 mem -t 1 /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa P_1.fastq P_2.fastq -o /home/user/bsa/prepar_dir/prepar_P.sam
        ## samtools view -@ 1 -q 10 -bS -o /home/user/bsa/prepar_dir/prepar_P.bam /home/user/bsa/prepar_dir/prepar_P.sam && rm /home/user/bsa/prepar_dir/prepar_P.sam
        &process_cmd($cd5, $lo, $mx5);

        print STDOUT "align maternal parent reads to reference genome and generate BAM file\n";
        print $lo "align maternal parent reads to reference genome and generate BAM file\nCMD6:\t";
        unless(defined($hlog{"CMD6:"})) {$hlog{"CMD6:"} = 0;}
        my $mx6 = $hlog{"CMD6:"};
        my $cd6 = "$minimap2 -ax sr -t $t -R \"\@RG\\tID:M\\tSM:M\" $mmi $mf1 $mf2 -o $M_sam && $samtools view -@ $t -q $mq -bS -o $M_bam $M_sam && rm $M_sam";
        # minimap2 -ax sr -t 1 -R "@RG\tID:M\tSM:M" /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa.mmi M_1.fastq M_2.fastq -o /home/user/bsa/prepar_dir/prepar_M.sam
        # samtools view -@ 1 -q 10 -bS -o /home/user/bsa/prepar_dir/prepar_M.bam /home/user/bsa/prepar_dir/prepar_M.sam && rm /home/user/bsa/prepar_dir/prepar_M.sam
        &process_cmd($cd6, $lo, $mx6);

        print STDOUT "sort pollen parent BAM file\n";
        print $lo "sort pollen parent BAM file\nCMD7:\t";
        unless(defined($hlog{"CMD7:"})) {$hlog{"CMD7:"} = 0;}
        my $mx7 = $hlog{"CMD7:"};
        my $cd7 = "$samtools sort -@ $t -o $P_srt_bam $P_bam && rm $P_bam";
        # samtools sort -@ 1 -o /home/user/bsa/prepar_dir/prepar_P_srt.bam /home/user/bsa/prepar_dir/prepar_P.bam && rm /home/user/bsa/prepar_dir/prepar_P.bam
        &process_cmd($cd7, $lo, $mx7);

        print STDOUT "sort maternal parent BAM file\n";
        print $lo "sort maternal parent BAM file\nCMD8:\t";
        unless(defined($hlog{"CMD8:"})) {$hlog{"CMD8:"} = 0;}
        my $mx8 = $hlog{"CMD8:"};
        my $cd8 = "$samtools sort -@ $t -o $M_srt_bam $M_bam && rm $M_bam";
        # samtools sort -@ 1 -o /home/user/bsa/prepar_dir/prepar_M_srt.bam /home/user/bsa/prepar_dir/prepar_M.bam && rm /home/user/bsa/prepar_dir/prepar_M.bam
        &process_cmd($cd8, $lo, $mx8);

        print STDOUT "remove duplicates pollen parent BAM file\n";
        print $lo "remove duplicates pollen parent BAM file\nCMD9:\t";
        unless(defined($hlog{"CMD9:"})) {$hlog{"CMD9:"} = 0;}
        my $mx9 = $hlog{"CMD9:"};
        my $cd9 = "$samtools rmdup $P_srt_bam $P_rm_bam && rm $P_srt_bam";
        # samtools rmdup /home/user/bsa/prepar_dir/prepar_P_srt.bam /home/user/bsa/prepar_dir/prepar_P_rm.bam && rm /home/user/bsa/prepar_dir/prepar_P_srt.bam
        &process_cmd($cd9, $lo, $mx9);

        print STDOUT "remove duplicates maternal parent BAM file\n";
        print $lo "remove duplicates maternal parent BAM file\nCMD10:\t";
        unless(defined($hlog{"CMD10:"})) {$hlog{"CMD10:"} = 0;}
        my $mx10 = $hlog{"CMD10:"};
        my $cd10 = "$samtools rmdup $M_srt_bam $M_rm_bam && rm $M_srt_bam";
        # samtools rmdup /home/user/bsa/prepar_dir/prepar_M_srt.bam /home/user/bsa/prepar_dir/prepar_M_rm.bam && /home/user/bsa/prepar_dir/prepar_M_srt.bam
        &process_cmd($cd10, $lo, $mx10);

        unless(-e $P_rm_bam_bai){
            print STDOUT "samtools index pollen parent rmdup BAM file\n";
            print $lo "samtools index pollen parent rmdup BAM file\nCMD11:\t";
            unless(defined($hlog{"CMD11:"})) {$hlog{"CMD11:"} = 0;}
            my $mx11 = $hlog{"CMD11:"};
            my $cd11 = "$samtools index -@ $t $P_rm_bam";
            # samtools index -@ 1 /home/user/bsa/prepar_dir/prepar_P_rm.bam
            &process_cmd($cd11, $lo, $mx11);
        }

        print STDOUT "summary the alignment result of pollen parent\n";
        print $lo "summary the alignment result of pollen parent\nCMDs1:\t";
        print SS "summary the alignment result of pollen parent:\n";
        unless(defined($hlog{"CMDs1:"})) {$hlog{"CMDs1:"} = 0;}
        my $name_s1 = $hlog{"CMDs1:"};
        my $cd_s1 = "$samtools flagstat -@ $t $P_rm_bam >> $sum";
        # samtools flagstat -@ 1 /home/user/bsa/prepar_dir/prepar_P_rm.bam >> /home/user/bsa/prepar_dir/summary
        # &process_cmd($cd_s1, $lo, $name_s1);

        unless(-e $M_rm_bam_bai){
            print STDOUT "samtools index maternal parent rmdup BAM file\n";
            print $lo "samtools index maternal parent rmdup BAM file\nCMD12:\t";
            unless(defined($hlog{"CMD12:"})) {$hlog{"CMD12:"} = 0;}
            my $mx12 = $hlog{"CMD12:"};
            my $cd12 = "$samtools index -@ $t $M_rm_bam";
            # samtools index -@ 1 /home/user/bsa/prepar_dir/prepar_M_rm.bam
            &process_cmd($cd12, $lo, $mx12);
        }

        print STDOUT "summary the alignment result of maternal parent\n";
        print $lo "summary the alignment result of maternal parent\nCMDs2:\t";
        print SS "summary the alignment result of maternal parent:\n";
        unless(defined($hlog{"CMDs2:"})) {$hlog{"CMDs2:"} = 0;}
        my $name_s2 = $hlog{"CMDs2:"};
        my $cd_s2 = "$samtools flagstat -@ $t $M_rm_bam >> $sum";
        # samtools flagstat -@ 1 /home/user/bsa/prepar_dir/prepar_M_rm.bam >> /home/user/bsa/prepar_dir/summary
        # &process_cmd($cd_s2, $lo, $name_s2);

        print STDOUT "parent SNV calling\n";
        print $lo "parent SNV calling\nCMD13:\t";
        unless(defined($hlog{"CMD13:"})) {$hlog{"CMD13:"} = 0;}
        my $mx13 = $hlog{"CMD13:"};
        my $cd13 = "$samtools mpileup -ugf $genome -q $aq $P_rm_bam $M_rm_bam | $bcftools call --threads $t -mv -> $P_M_snv_unfilter";
        # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 /home/user/bsa/prepar_dir/prepar_P_rm.bam /home/user/bsa/prepar_dir/prepar_M_rm.bam | bcftools call --threads 1 -mv -> /home/user/bsa/prepar_dir/P_M.snv.unfilter
        &process_cmd($cd13, $lo, $mx13);

        print STDOUT  "DEL SV calling\n";
        print $lo "DEL SV calling\nCMD14:\t";
        unless(defined($hlog{"CMD14:"})) {$hlog{"CMD14:"} = 0;}
        my $mx14 = $hlog{"CMD14:"};
        my $cd14 = "$delly call -q $vq -g $genome -t DEL -o $P_M_del_bcf $P_rm_bam $M_rm_bam";
        # delly call -q 10 -g /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -t DEL -o /home/user/bsa/prepar_dir/P_M.del.bcf /home/user/bsa/prepar_dir/prepar_P_rm.bam /home/user/bsa/prepar_dir/prepar_M_rm.bam
        &process_cmd($cd14, $lo, $mx14);

        print STDOUT "convert DEL BCF file to DEL VCF file\n";
        print $lo "convert DEL BCF file to DEL VCF file\nCMD15:\t";
        unless(defined($hlog{"CMD15:"})) {$hlog{"CMD15:"} = 0;}
        my $mx15 = $hlog{"CMD15:"};
        my $cd15 = "$bcftools view --threads $t $P_M_del_bcf | $filter_raw_sv --d $dp --mq $mq --l $l --precise $precise --header $header -> $P_M_del_vcf";
        # bcftools view --threads 1 /home/user/bsa/prepar_dir/P_M.del.bcf | /home/user/BSATOS2/scripts/filter_raw_sv.pl --d 10 --mq 10 --l 1000 --precise yes --header yes -> /home/user/bsa/prepar_dir/P_M.del.vcf
        &process_cmd($cd15, $lo, $mx15);

        print STDOUT "INS SV calling\n";
        print $lo "INS SV calling\nCMD16:\t";
        unless(defined($hlog{"CMD16:"})) {$hlog{"CMD16:"} = 0;}
        my $mx16 = $hlog{"CMD16:"};
        my $cd16 = "$delly call -q $vq -g $genome -t INS -o $P_M_ins_bcf $P_rm_bam $M_rm_bam";
        # delly call -q 10 -g /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -t INS -o /home/user/bsa/prepar_dir/P_M.ins.bcf /home/user/bsa/prepar_dir/prepar_P_rm.bam /home/user/bsa/prepar_dir/prepar_M_rm.bam
        &process_cmd($cd16, $lo, $mx16);

        print STDOUT "convert INS BCF file to VCF file\n";
        print $lo "convert INS BCF file to VCF file\nCMD17:\t";
        unless(defined($hlog{"CMD17:"})) {$hlog{"CMD17:"} = 0;}
        my $mx17 = $hlog{"CMD17:"};
        my $cd17 = "$bcftools view --threads $t $P_M_ins_bcf | $filter_raw_sv --d $dp --mq $mq --l $l --precise $precise --header $header -> $P_M_ins_vcf";
        # bcftools view --threads 1 /home/user/bsa/prepar_dir/P_M.ins.bcf | /home/user/BSATOS2/scripts/filter_raw_sv.pl --d 10 --mq 10 --l 1000 --precise yes --header yes -> /home/user/bsa/prepar_dir/P_M.ins.vcf
        &process_cmd($cd17, $lo, $mx17);

        print STDOUT "convert INS BCF file to VCF file\n";
        print $lo "merge and filter SV\nCMD18:\t";
        unless(defined($hlog{"CMD18:"})) {$hlog{"CMD18:"} = 0;}
        my $mx18 = $hlog{"CMD18:"};
        my $cd18 = "cat $P_M_del_vcf $P_M_ins_vcf | $rg -v \"#\" > $P_M_sv";
        # cat /home/user/bsa/prepar_dir/P_M.del.vcf /home/user/bsa/prepar_dir/P_M.ins.vcf | /home/user/BSATOS2/tools/bin/rg -v "#" > /home/user/bsa/prepar_dir/P_M.sv
        &process_cmd($cd18, $lo, $mx18);

        # if(-e $P_M_del_bcf){
        #     print STDOUT "remove files\n";
        #     print $lo "remove files\nCMD19:\t";
        #     unless(defined($hlog{"CMD19:"})) {$hlog{"CMD19:"} = 0;}
        #     my $mx19 = $hlog{"CMD19:"};
        #     my $cd19 = "rm $P_M_del_bcf $P_M_ins_bcf $P_M_del_bcf_csi $P_M_ins_bcf_csi";
        #     # rm /home/user/bsa/prepar_dir/P_M.del.bcf /home/user/bsa/prepar_dir/P_M.ins.bcf /home/user/bsa/prepar_dir/P_M.del.bcf.csi /home/user/bsa/prepar_dir/P_M.ins.bcf.csi
        #     &process_cmd($cd19, $lo, $mx19);
        # }

    }else{

        unless(-e $pb_bai){
            print STDOUT "samtools index pollen parent BAM file\n";
            print $lo "samtools index pollen parent BAM file\nCMD20:\t";
            unless(defined($hlog{"CMD20:"})) {$hlog{"CMD20:"} = 0;}
            my $mx20 = $hlog{"CMD20:"};
            my $cd20 = "$samtools index -@ $t $pb";   # samtools index -@ 1 P.bam
            &process_cmd($cd20, $lo, $mx20);
        }

        print STDOUT "summary the alignment result of pollen parent\n";
        print $lo "summary the alignment result of pollen parent\nCMDs1:\t";
        print SS "summary the alignment result of pollen parent\n";
        unless(defined($hlog{"CMDs1:"})) {$hlog{"CMDs1:"} = 0;}
        my $name_s1 = $hlog{"CMDs1:"};
        if(-e $pb_flagstat){
            my $cd_s1 = "cat $pb_flagstat >> $sum";
            # cat P.bam.flagstat >> /home/user/bsa/prepar_dir/summary
            # &process_cmd($cd_s1, $lo, $name_s1);
        }else{
            my $cd_s1 = "$samtools flagstat -@ $t $pb > $pb_flagstat && cat $pb_flagstat >> $sum";
            # samtools flagstat P.bam > P.bam.flagstat && cat P.bam.flagstat >> /home/user/bsa/prepar_dir/summary
            # &process_cmd($cd_s1, $lo, $name_s1);
        }

        unless(-e $mb_bai){
            print STDOUT "samtools index maternal parent BAM file\n";
            print $lo "samtools index maternal parent BAM file\nCMD21:\t";
            unless(defined($hlog{"CMD21:"})) {$hlog{"CMD21:"} = 0;}
            my $mx21 = $hlog{"CMD21:"};
            my $cd21 = "$samtools index -@ $t $mb";   # samtools index -@ 1 M.bam
            &process_cmd($cd21, $lo, $mx21);
        }

        print STDOUT "summary the alignment result of maternal parent\n";
        print $lo "summary the alignment result of maternal parent\nCMDs2:\t";
        print SS "summary the alignment result of maternal parent\n";
        unless(defined($hlog{"CMDs2:"})) {$hlog{"CMDs2:"} = 0;}
        my $name_s2 = $hlog{"CMDs2:"};
        if(-e $mb_flagstat){
            my $cd_s2 = "cat $mb_flagstat >> $sum";
            # cat M.bam.flagstat >> /home/user/bsa/prepar_dir/summary
            # &process_cmd($cd_s2, $lo, $name_s2);
        }else{
            my $cd_s2 = "$samtools flagstat -@ $t $mb > $mb_flagstat && cat $mb_flagstat >> $sum";
            # samtools flagstat -@ 1 M.bam > M.bam.flagstat && cat M.bam.flagstat >> /home/user/bsa/prepar_dir/summary
            # &process_cmd($cd_s2, $lo, $name_s2);
        }

        unless(-e $P_M_snv_unfilter){
            print STDOUT "SNV calling\n";
            print $lo "SNV calling\nCMD22:\t";
            unless(defined($hlog{"CMD22:"})) {$hlog{"CMD22:"} = 0;}
            my $mx22 = $hlog{"CMD22:"};
            my $cd22 = "$samtools mpileup -ugf $genome -q $aq $pb $mb | $bcftools call --threads $t -mv -> $P_M_snv_unfilter";
            # samtools mpileup -ugf /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -q 10 P.bam M.bam | bcftools call --threads 1 -mv -> /home/user/bsa/prepar_dir/P_M.snv.unfilter
            &process_cmd($cd22, $lo, $mx22);
        }

        unless(-e $P_M_del_bcf){
            print STDOUT "DEL SV calling\n";
            print $lo "DEL SV calling\nCMD23:\t";
            unless(defined($hlog{"CMD23:"})) {$hlog{"CMD23:"} = 0;}
            my $mx23 = $hlog{"CMD23:"};
            my $cd23 = "$delly call -q $vq -g $genome -t DEL -o $P_M_del_bcf $pb $mb";
            # delly call -q 10 -g /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -t DEL -o /home/user/bsa/prepar_dir/P_M.del.bcf P.bam M.bam
            &process_cmd($cd23, $lo, $mx23);
        }

        print STDOUT "convert DEL BCF to VCF\n";
        print $lo "convert DEL BCF to VCF\nCMD24:\t";
        unless(defined($hlog{"CMD24:"})) {$hlog{"CMD24:"} = 0;}
        my $mx24 = $hlog{"CMD24:"};
        my $cd24 = "$bcftools view --threads $t $P_M_del_bcf | $filter_raw_sv --d $dp --mq $mq --l $l --precise $precise --header $header -> $P_M_del_vcf";
        # bcftools view --threads 1 /home/user/bsa/prepar_dir/P_M.del.bcf | /home/user/BSATOS2/scripts/filter_raw_sv.pl --d 10 --mq 10 --l 1000 --precise yes --header yes -> /home/user/bsa/prepar_dir/P_M.del.vcf
        &process_cmd($cd24, $lo, $mx24);

        unless(-e $P_M_ins_bcf){
            print STDOUT "INS SV calling\n";
            print $lo "INS SV calling\nCMD25:\t";
            unless(defined($hlog{"CMD25:"})) {$hlog{"CMD25:"} = 0;}
            my $mx25 = $hlog{"CMD25:"};
            my $cd25 = "$delly call -q $vq -g $genome -t INS -o $P_M_ins_bcf $pb $mb";
            # delly call -q 10 -g /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa -t DEL -o /home/user/bsa/prepar_dir/P_M.ins.bcf P.bam M.bam
            &process_cmd($cd25, $lo, $mx25);
        }

        print STDOUT "convert INS BCF to VCF\n";
        print $lo "convert INS BCF to VCF\nCMD26:\t";
        unless(defined($hlog{"CMD26:"})) {$hlog{"CMD26:"} = 0;}
        my $mx26 = $hlog{"CMD26:"};
        my $cd26 = "$bcftools view --threads $t $P_M_ins_bcf | $filter_raw_sv --d $dp --mq $mq --l $l --precise $precise --header $header -> $P_M_ins_vcf";
        # bcftools view --threads 1 /home/user/bsa/prepar_dir/P_M.ins.bcf | /home/user/BSATOS2/scripts/filter_raw_sv.pl --d 10 --mq 10 --l 1000 --precise yes --header yes -> /home/user/bsa/prepar_dir/P_M.ins.vcf
        &process_cmd($cd26, $lo, $mx26);

        print STDOUT "merge and filter SV file\n";
        print $lo "merge and filter SV file\nCMD27:\t";
        unless(defined($hlog{"CMD27:"})) {$hlog{"CMD27:"} = 0;}
        my $mx27 = $hlog{"CMD27:"};
        my $cd27 = "head -n 100 $P_M_del_vcf | $rg \"##\" > $dir/indel_header && head -n 100 $P_M_snv_unfilter | $rg \"##\" > $dir/snv_header && cat $P_M_del_vcf $P_M_ins_vcf | $rg -v \"#\" > $P_M_sv";
        # head -n 100 /home/user/bsa/prepar_dir/P_M.del.vcf | /home/user/BSATOS2/tools/bin/rg "##" > /home/user/bsa/prepar_dir/indel_header
        # head -n 100 /home/user/bsa/prepar_dir/P_M.snv.unfilter | /home/user/BSATOS2/tools/bin/rg "##" > /home/user/bsa/prepar_dir/snv_header
        # cat /home/user/bsa/prepar_dir/P_M.del.vcf /home/user/bsa/prepar_dir/P_M.ins.vcf | /home/user/BSATOS2/tools/bin/rg -v "#" > /home/user/bsa/prepar_dir/P_M.sv
        &process_cmd($cd27, $lo, $mx27);

        # if(-e $P_M_del_bcf){
        #     print STDOUT "remove SV files\n";
        #     print $lo "remove SV files\nCMD28:\t";
        #     unless(defined($hlog{"CMD28:"})) {$hlog{"CMD28:"} = 0;}
        #     my $mx28 = $hlog{"CMD28:"};
        #     my $cd28 = "rm $P_M_del_bcf $P_M_ins_bcf $P_M_del_bcf_csi $P_M_ins_bcf_csi $P_M_del_vcf $P_M_ins_vcf";
        #     # rm /home/user/bsa/prepar_dir/P_M.del.bcf /home/user/bsa/prepar_dir/P_M.ins.bcf /home/user/bsa/prepar_dir/P_M.del.bcf.csi /home/user/bsa/prepar_dir/P_M.ins.bcf.csi /home/user/bsa/prepar_dir/P_M.del.vcf /home/user/bsa/prepar_dir/P_M.ins.vcf
        #     &process_cmd($cd28, $lo);
        # }
    }

    print STDOUT "filter SNV based on reads depth, phred-scaled quality and biparent genotypes\n";
    print $lo "filter SNV based on reads depth, phred-scaled quality and biparent genotypes\nCMD29:\t";
    unless(defined($hlog{"CMD29:"})) {$hlog{"CMD29:"} = 0;}
    my $mx29 = $hlog{"CMD29:"};
    my $cd29 = "$filter_get_genotype -d $dep -q $sq -o $P_M $P_M_snv_unfilter >> $sum && perl -ne \"print unless(/\\.\\/\\.:/)\" $P_M_snv_unfilter > $P_M_snv";
    # /home/user/BSATOS2/scripts/filter_get_genotype.pl -d 10 -q 10 -o /home/user/bsa/prepar_dir/P_M /home/user/bsa/prepar_dir/P_M.snv.unfilter >> /home/user/bsa/prepar_dir/summary
    # perl -ne "print unless(/\.\/\.:/)" /home/user/bsa/prepar_dir/P_M.snv.unfilter > /home/user/bsa/prepar_dir/P_M.snv
    &process_cmd($cd29, $lo, $mx29);

    unless(-e $anno_dir) {
        print STDOUT "create SNV and SV annotation dir\n";
        print $lo "create SNV and SV annotation dir\nCMD30:\t";
        unless(defined($hlog{"CMD30:"})) {$hlog{"CMD30:"} = 0;}
        my $mx30 = $hlog{"CMD30:"};
        my $cd30 = "mkdir $anno_dir";
        # mkdir /home/user/bsa/prepar_dir/anno
        &process_cmd($cd30, $lo, $mx30);
    }

    print STDOUT "create genome annotation database step1\n";
    print $lo "create genome annotation database step1\nCMD31:\t";
    unless(defined($hlog{"CMD31:"})) {$hlog{"CMD31:"} = 0;}
    my $mx31 = $hlog{"CMD31:"};
    my $cd31 = "$gtfpred -genePredExt $gtf $anno_dir/AT_refGene.txt";
    # gtfToGenePred -genePredExt /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf /home/user/bsa/prepar_dir/anno/AT_refGene.txt
    &process_cmd($cd31, $lo, $mx31);

    print STDOUT "create genome annotation database step2\n";
    print $lo "create genome annotation database step2\nCMD32:\t";
    unless(defined($hlog{"CMD32:"})) {$hlog{"CMD32:"} = 0;}
    my $mx32 = $hlog{"CMD32:"};
    my $cd32 = "perl $retrieve --format refGene --seqfile $genome $anno_dir/AT_refGene.txt --outfile $anno_dir/AT_refGeneMrna.fa";
    # perl /home/user/BSATOS2/scripts/retrieve_seq_from_fasta.pl --format refGene --seqfile /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa /home/user/bsa/prepar_dir/anno/AT_refGene.txt --outfile /home/user/bsa/prepar_dir/anno/AT_refGeneMrna.fa
    &process_cmd($cd32, $lo, $mx32);

    print STDOUT "annotate SNV file\n";
    print $lo "annotate SNV file\nCMD33:\t";
    unless(defined($hlog{"CMD33:"})) {$hlog{"CMD33:"} = 0;}
    my $mx33 = $hlog{"CMD33:"};
    my $cd33 = "perl $table_annovar -thread $t $P_M_snv $anno_dir -buildver AT -out $out_snv -protocol refGene -operation g -nastring . -vcfinput";
    # perl /home/user/BSATOS2/scripts/table_annovar.pl -thread 1 /home/user/bsa/prepar_dir/P_M.snv /home/user/bsa/prepar_dir/anno -buildver AT -out /home/user/bsa/prepar_dir/anno/snv -protocol refGene -operation g -nastring . -vcfinput
    &process_cmd($cd33, $lo, $mx33);
    # (convert2annovar.pl -includeinfo -allsample -withfreq -format vcf4 /home/user/bsa/prepar_dir/P_M.snv > /home/user/bsa/prepar_dir/anno/snv.avinput)
    # (table_annovar.pl -thread 4 /home/user/bsa/prepar_dir/anno/snv.avinput /home/user/bsa/prepar_dir/anno -buildver AT -outfile /home/user/bsa/prepar_dir/anno/snv -protocol refGene -operation g -nastring . -otherinfo)
    # (annotate_variation.pl -geneanno -buildver AT -dbtype refGene -outfile /home/user/bsa/prepar_dir/anno/snv.refGene -exonsort /home/user/bsa/prepar_dir/anno/snv.avinput /home/user/bsa/prepar_dir/anno -thread 4)

    print STDOUT "annotate SV file\n";
    print $lo "annotate SV file\nCMD34:\t";
    unless(defined($hlog{"CMD34:"})) {$hlog{"CMD34:"} = 0;}
    my $mx34 = $hlog{"CMD34:"};
    my $cd34 = "perl $table_annovar -thread $t $P_M_sv $anno_dir -buildver AT -out $out_sv -protocol refGene -operation g -nastring . -vcfinput";
    # perl /home/user/BSATOS2/scripts/table_annovar.pl -thread 1 /home/user/bsa/prepar_dir/P_M.sv /home/user/bsa/prepar_dir/anno -buildver AT -out /home/user/bsa/prepar_dir/anno/sv -protocol refGene -operation g -nastring . -vcfinput
    &process_cmd($cd34, $lo, $mx34);

    print STDOUT "\nprepar module finished!!!";
    print $lo "\nprepar module finished!!!";
    exit(0);
}

#############################################################

 sub process_cmd {
    my ($cmd, $lk, $hg) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t...............................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t...............................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime = "[".localtime()."]";
        if($ret){
           print $lk "\#die\t$starttime ... $endtime\n";
           print STDOUT "\#die\t$starttime ... $endtime\n";
           die "Error, cmd: < $cmd > died with return value $ret";
        }else{
            print $lk "\#done\t$starttime ... $endtime\n";
            print STDOUT "\#done\t$starttime ... $endtime\n";
        }
    return;
    }
}
