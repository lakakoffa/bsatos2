#!/usr/bin/env perl
package igv;
BEGIN {my $VERSION = '2.0';}

use Cwd;
use strict;
use Getopt::Long;
use File::Basename;
use base 'Exporter';
use FindBin qw($Bin);
our @EXPORT = qw(runigv);

my $G_USAGE = "
Usage: bastos2 igv [options]

Options:
    --o           [STR]     name prefix of output directory [igv]
    --r           [FILE]    reference genome (fasta format) [Malus_domestica.fa]
    --gtf         [FILE]    GTF file of genes (GTF2 format; Not required if GFF file is provided) [Malus_domestica.gtf]
    --gff         [FILE]    GFF file of genes (GFF3 format; Not required if GTF file is provided) [Malus_domestica.gff3]
    --prepar      [DIR]     result DIR from prepar module [prepar_dir]
    --hap         [DIR]     result DIR from haplotype module [haplotype_dir]
    --qtl         [DIR]     result DIR from qtl_pick module [qtl_pick_dir]
    --po          [DIR]     result DIR from polish module [polish_dir]
    --log         [FILE]    igv module log file [igv.log]
    --db          [STR]     database directory of .fa & GTF & GFF files [Malus_domestica]

Example:
    bsatos2 igv --o igv --r Malus_domestica.fa --gtf Malus_domestica.gtf --prepar prepar_dir --hap haplotype_dir --qtl qtl_pick_dir --po polish_dir
";


##################################################################

sub runigv {
    my $genome = undef;   my $o = undef;      my $help = undef;   my $prepar = undef;      my $s = undef;
    my $prep = undef;     my $hap = undef;    my $gtf = undef;    my $gff = undef;         my $db = undef;
    my $qtl = undef;      my $po = undef;     my $log = undef;

    GetOptions (
        "r=s" => \$genome,    "gtf=s" => \$gtf,   "gff=s" => \$gff,     "prepar=s" => \$prepar,
        "prep=s" => \$prep,   "o=s" => \$o,       "hap=s" => \$hap,     "s=s" => \$s,
        "qtl=s" => \$qtl,     "po=s" => \$po,     "log=s" => \$log,     "db=s" => \$db,   "help" => \$help)
    or die("$G_USAGE");
    die "$G_USAGE" if ($help);
    die "$G_USAGE" if (!defined $o);

    my $para_igv_file = "$FindBin::Bin/Python/src/para_igv_file";
    if (-e $para_igv_file){
        open PARAIGVFILE, "$para_igv_file" or die $!;
        while(<PARAIGVFILE>){
            chomp;
            my @para = split /\t/, $_;
            if (@para[0] =~ /^o\b/ && @para[1] =~ /\w/) {$o = @para[1];}
            if (@para[0] =~ /^r\b/ && @para[1] =~ /\w/) {$genome = @para[1];}
            if (@para[0] =~ /^gtf\b/ && @para[1] =~ /\w/) {$gtf = @para[1];}
            if (@para[0] =~ /^gff\b/ && @para[1] =~ /\w/) {$gff = @para[1];}
            if (@para[0] =~ /^prepar\b/ && @para[1] =~ /\w/) {$prepar = @para[1];}
            if (@para[0] =~ /^hap\b/ && @para[1] =~ /\w/) {$hap = @para[1];}
            if (@para[0] =~ /^qtl\b/ && @para[1] =~ /\w/) {$qtl = @para[1];}
            if (@para[0] =~ /^po\b/ && @para[1] =~ /\w/) {$po = @para[1];}
            if (@para[0] =~ /^db\b/ && @para[1] =~ /\w/) {$db = @para[1];}
        }
        close PARAIGVFILE;
    }


    unless(defined($o)) {$o = "igv";}

    my $gffread = "gffread";
    my $work_dir = getcwd;                                                # /home/user/bsa
    my $dir = $work_dir."/".$o."_dir";                                    # /home/user/bsa/igv_dir
    my $ref = $dir."/$db.fasta";                                          # /home/user/bsa/igv_dir/Malus_domestica.fasta
    my $gene_gff = $dir."/$db.gff";                                       # /home/user/bsa/igv_dir/Malus_domestica.gff
    my $gene_gtf = $dir."/$db.gtf";                                       # /home/user/bsa/igv_dir/Malus_domestica.gtf
    my $snv_vcf = $dir."/snv.vcf";                                        # /home/user/bsa/igv_dir/snv.vcf
    my $snv_maf = $dir."/snv.maf";                                        # /home/user/bsa/igv_dir/snv.maf
    my $sv_vcf = $dir."/sv.vcf";                                          # /home/user/bsa/igv_dir/sv.vcf
    my $sv_maf = $dir."/sv.maf";                                          # /home/user/bsa/igv_dir/sv.maf
    my $H_enriched_bed = $dir."/H_enriched.bed";                          # /home/user/bsa/igv_dir/H_enriched.bed
    my $L_enriched_bed = $dir."/L_enriched.bed";                          # /home/user/bsa/igv_dir/L_enriched.bed
    my $H_enriched_vcf = $dir."/H_enriched.vcf";                          # /home/user/bsa/igv_dir/H_enriched.vcf
    my $L_enriched_vcf = $dir."/L_enriched.vcf";                          # /home/user/bsa/igv_dir/L_enriched.vcf
    my $P_igv = $dir."/P.igv";                                            # /home/user/bsa/igv_dir/P.igv
    my $M_igv = $dir."/M.igv";                                            # /home/user/bsa/igv_dir/M.igv
    my $H_igv = $dir."/PM.igv";                                           # /home/user/bsa/igv_dir/PM.igv

    unless(defined($log)) {$log = $work_dir."/".$o.".log";}               # /home/user/bsa/igv.log
    unless(defined($prepar)) {$prepar = $work_dir."/prepar_dir";}         # /home/user/bsa/prepar_dir
    unless(defined($hap)) {$hap = $work_dir."/haplotype_dir";}            # /home/user/bsa/haplotype_dir
    unless(defined($qtl)) {$qtl = $work_dir."/qtl_pick_dir";}             # /home/user/bsa/qtl_pick_dir
    unless(defined($db)) {$db = "Malus_domestica";}
    unless(defined($gtf)) {$gtf = "$FindBin::Bin/database/$db/$db.gtf";}        # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf
    unless(defined($genome)) {
        if (-e "$FindBin::Bin/database/$db/$db.fa"){$genome = "$FindBin::Bin/database/$db/$db.fa";}         # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa
        if (-e "$FindBin::Bin/database/$db/$db.fasta"){$genome = "$FindBin::Bin/database/$db/$db.fasta";}   # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fasta
    }
    unless(defined($gff)) {
        if (-e "$FindBin::Bin/database/$db/$db.gff"){$gff = "$FindBin::Bin/database/$db/$db.gff";}       # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff
        if (-e "$FindBin::Bin/database/$db/$db.gff3"){$gff = "$FindBin::Bin/database/$db/$db.gff3";}     # /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff3
    }

##############################################################

    my %hlog = ();
    if(-e $log){
        open LOD, "<$log";
        my $count = 0;
        while(<LOD>){
            chomp;
            my @sp = split /\t/, $_;
            if($sp[2] eq "\#done") {$hlog{$sp[0]} = 1;}
            if($sp[2] eq "\#die") {$hlog{$sp[0]} = 0;}
            if($_ =~ /finished/) {$count++;}
        }
        if($count != 0){
            print "
                Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
                Version: 2.0
                The last module: igv
                igv         generate files for Integrative Genomics Viewer\n\n
                This module has been finished!!!!!\n\n";
            exit(0);
        }
    }
    open (my $lo, ">>$log") or die $!;

    print STDOUT "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The last module: igv
        igv         generate files for Integrative Genomics Viewer\n\n
        ██████╗ ███████╗ █████╗ ████████╗ ██████╗ ███████╗██████╗
        ██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██╔═══██╗██╔════╝╚════██╗
        ██████╔╝███████╗███████║   ██║   ██║   ██║███████╗ █████╔╝
        ██╔══██╗╚════██║██╔══██║   ██║   ██║   ██║╚════██║██╔═══╝
        ██████╔╝███████║██║  ██║   ██║   ╚██████╔╝███████║███████╗
        ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝╚══════╝\n\n";

    print $lo "
        Program: BSATOS2 (Bulked Segregant Analysis Tool for Outbreeding Species 2)
        Version: 2.0
        The last module: igv
        igv         generate files for Integrative Genomics Viewer\n\n";

##############################################################

    if(defined($gff)){
        unless(undifined($gtf)) {$gtf = (split /\./, basename($gff))[0] . ".gtf";}
        print STDOUT "convert GFF to GTF file:\n";
        print $lo "convert GFF to GTF file:\nCMD0:\t";
        unless(defined($hlog{"CMD0:"})) {$hlog{"CMD0:"} = 0;}
        my $mx0 = $hlog{"CMD0:"};
        my $cd0 = "$gffread $gff -T -o $gtf";
        # gffread /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff -T -o /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gtf
        &process_cmd($cd0, $lo, $mx0);
    }

    unless(-e $dir){
        print STDOUT "create igv result dir\n";
        print $lo "create igv result dir\nCMD1:\t";
        unless(defined($hlog{"CMD1:"})) {$hlog{"CMD1:"} = 0;}
        my $mx1 = $hlog{"CMD1:"};
        my $cd1 = "mkdir $dir";
        # mkdir /home/user/bsa/igv_dir
        &process_cmd($cd1, $lo, $mx1);
    }

    print STDOUT "create igv genome file\n";
    print $lo "create igv genome file\nCMD2:\t";
    unless(defined($hlog{"CMD2:"})) {$hlog{"CMD2:"} = 0;}
    my $mx2 = $hlog{"CMD2:"};
    my $cd2 ="cp $genome $ref";
    # cp /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.fa /home/user/bsa/igv_dir/Malus_domestica.fasta
    &process_cmd($cd2, $lo, $mx2);

    if(defined($gtf)){
        print STDOUT "create igv genome gtf\n";
        print $lo "create igv genome gtf\nCMD3:\t";
        unless(defined($hlog{"CMD3:"})) {$hlog{"CMD3:"} = 0;}
        my $mx3 = $hlog{"CMD3:"};
        my $cd3 = "cp $gtf $gene_gtf";
        # cp /home/user/bsa/igv_dir/Malus_domestica.gtf /home/user/bsa/igv_dir/Malus_domestica.gtf
        &process_cmd($cd3, $lo, $mx3);
    }

    if(defined($gff)){
        print STDOUT "create igv genome gff\n";
        print $lo "create igv genome gff\nCMD4:\t";
        unless(defined($hlog{"CMD4:"})) {$hlog{"CMD4:"} = 0;}
        my $mx4 = $hlog{"CMD4:"};
        my $cd4 = "cp $gff $gene_gff";
        # cp /home/user/BSATOS2/database/Malus_domestica/Malus_domestica.gff3 /home/user/bsa/igv_dir/Malus_domestica.gff
        &process_cmd($cd4, $lo, $mx4);
    }

    print STDOUT "copy pollen parent profiles\n";
    print $lo "copy pollen parent profiles\nCMD4:\t";
    unless(defined($hlog{"CMD5:"})) {$hlog{"CMD5:"} = 0;}
    my $mx5 = $hlog{"CMD5:"};
    my $cd5 = "cp $po/p.igv $P_igv";
    # cp /home/user/bsa/polish_dir/p.igv /home/user/bsa/igv_dir/P.igv
    &process_cmd($cd5, $lo, $mx5);

    print STDOUT "copy maternal parent profiles\n";
    print $lo "copy maternal parent profiles\nCMD6:\t";
    unless(defined($hlog{"CMD6:"})) {$hlog{"CMD6:"} = 0;}
    my $mx6 = $hlog{"CMD6:"};
    my $cd6 = "cp $po/m.igv $M_igv";
    # cp /home/user/bsa/polish_dir/m.igv /home/user/bsa/igv_dir/M.igv
    &process_cmd($cd6, $lo, $mx6);

    print STDOUT "copy pollen and maternal parent profiles\n";
    print $lo "copy pollen and maternal parent profiles\nCMD7:\t";
    unless(defined($hlog{"CMD7:"})) {$hlog{"CMD7:"} = 0;}
    my $mx7 = $hlog{"CMD7:"};
    my $cd7 = "cp $po/pm.igv $H_igv";
    # cp /home/user/bsa/polish_dir/pm.igv /home/user/bsa/igv_dir/PM.igv
    &process_cmd($cd7, $lo, $mx7);

    print STDOUT "move and merge IGV files form qtl_pick_dir\n";
    print $lo "move and merge IGV files from qtl_pick_dir\nCMD8:\t";
    unless(defined($hlog{"CMD8:"})) {$hlog{"CMD8:"} = 0;}
    my $mx8 = $hlog{"CMD8:"};
    my $cd8 = "mv $qtl/*igv* $dir";
    # mv /home/user/bsa/qtl_pick_dir/*igv* /home/user/bsa/igv_dir
    &process_cmd($cd8, $lo, $mx8);

    print STDOUT "merge IGV files form qtl_pick_dir\n";
    print $lo "merge IGV files from qtl_pick_dir\nCMD9:\t";
    unless(defined($hlog{"CMD9:"})) {$hlog{"CMD9:"} = 0;}
    my $mx9 = $hlog{"CMD9:"};
    my $cd9 = "cat $dir/*snv.igv.vcf >> $snv_vcf";
    # cat /home/user/bsa/igv_dir/*snv.igv.vcf >> /home/user/bsa/igv_dir/snv.vcf
    &process_cmd($cd9, $lo, $mx9);

    print STDOUT "merge IGV files form qtl_pick_dir\n";
    print $lo "merge IGV files from qtl_pick_dir\nCMD10:\t";
    unless(defined($hlog{"CMD10:"})) {$hlog{"CMD10:"} = 0;}
    my $mx10 = $hlog{"CMD10:"};
    my $cd10 = "cat $dir/*sv.igv.vcf >> $sv_vcf";
    # cat /home/user/bsa/igv_dir/*sv.igv.vcf >>/home/user/bsa/igv_dir/sv.vcf
    &process_cmd($cd10, $lo, $mx10);

    print STDOUT "merge IGV files form qtl_pick_dir\n";
    print $lo "merge IGV files from qtl_pick_dir\nCMD11:\t";
    unless(defined($hlog{"CMD11:"})) {$hlog{"CMD11:"} = 0;}
    my $mx11 = $hlog{"CMD11:"};
    my $cd11 = "cat $dir/*sv.igv.mut >> $sv_maf";
    # cat /home/user/bsa/igv_dir/*sv.igv.mut >>/home/user/bsa/igv_dir/sv.maf
    &process_cmd($cd11, $lo, $mx11);

    print STDOUT "merge IGV files form qtl_pick_dir\n";
    print $lo "merge IGV files from qtl_pick_dir\nCMD12:\t";
    unless(defined($hlog{"CMD12:"})) {$hlog{"CMD12:"} = 0;}
    my $mx12 = $hlog{"CMD12:"};
    my $cd12 = "cat $dir/*snv.igv.mut >> $snv_maf\n";
    # cat /home/user/bsa/igv_dir/*snv.igv.mut >>/home/user/bsa/igv_dir/snv.maf
    &process_cmd($cd12, $lo, $mx12);

    print STDOUT "\nigv module finished!!!!\n";
    print $lo "\nigv module finished!!!!\n";
    exit(0);
}


##################################################################

sub process_cmd {
    my ($cmd, $lk, $hg) = @_;
    my $starttime = "[".localtime()."]";
    if($hg == 1){
        print STDOUT "this command has been processed last time\n$cmd\t................................\#done\t$starttime\n";
        print $lk "$cmd\t\#done\t$starttime\n";
    }else{
        print STDOUT "$cmd\t................................";
        print $lk "$cmd\t";
        my $ret = system($cmd);
        my $endtime = "[".localtime()."]";
        if($ret){
            print $lk "\#die\t$starttime ... $endtime\n";
            print STDOUT "\#die\t$starttime ... $endtime\n";
            die "Error, cmd: < $cmd > died with return value $ret";
        }else{
            print $lk "\#done\t$starttime ... $endtime\n";
            print STDOUT "\#done\t$starttime ... $endtime\n";
        }
        return;
    }
}
