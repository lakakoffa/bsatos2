bwa-mem2 mem -t 4 BASTOS2/database/Malus_domestica/Malus_domestica.fa P_1.fq P_2.fq -o P.sam
samtools view -bS -@ 4 -T BASTOS2/database/Malus_domestica/Malus_domestica.fa P.sam -o P.unsrt.bam
samtools sort -n -@ 4 P.unsrt.bam -o P.nsrt.bam
samtools fixmate -@ 4 -m P.nsrt.bam P.fixmate.bam
samtools sort -@ 4 P.fixmate.bam -o P.srt.bam
samtools markdup -r -@ 4 P.srt.bam P.bam
samtools index -@ 4 P.bam
samtools phase P.bam > P.bam.phase
samtools flagstat -@ 4 P.bam > P.bam.flagstat
