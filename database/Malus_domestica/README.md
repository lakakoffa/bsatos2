The reference genome `Malus_domestica.fa` (GDDH13 v1.1) of apple is 687Mb, and because of the limitation that Github cannot upload files larger than 100Mb, the whole reference genome for Malus_domestica.fa was not uploaded to this directory.

If you need to use these files, you will need to run the scripts to regenerate the file manually.

On the one hand, you could run `concatenate.ps1` on Windows platform and `concatenate.sh` on UNIX/Linux/MacOS platform.

On the other hand, you can also download it by the link below (GDR) and rename it to `Malus_domestica.fa`.

https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica-genome_GDDH13_v1.1/assembly/GDDH13_1-1_formatted.fasta.gz