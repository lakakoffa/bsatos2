Write-Host "Changing directory to GDDH13v1.1" -ForegroundColor Green
Set-Location GDDH13v1.1
Write-Host "Reading sequences from all chromosomes" -ForegroundColor Green
$content = Get-Content Chr00.txt, Chr01.txt, Chr02.txt, Chr03.txt, Chr04.txt, Chr05.txt, Chr06.txt, Chr07.txt, Chr08.txt, Chr09.txt, Chr10.txt, Chr11.txt, Chr12.txt, Chr13.txt, Chr14.txt, Chr15.txt, Chr16.txt, Chr17.txt
Write-Host "Concatenating sequences to Malus_domestica" -ForegroundColor Green
$content | Out-File -FilePath ..\Malus_domestica.fa
Write-Host "Concatenating completed" -ForegroundColor Green
