#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Latest reviewed date: 2023-12-22

import os
import re
import sys
import subprocess
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QFileDialog
import ufunc
# sys.path.append(".")

times_new_roman_10 = QtGui.QFont("Times New Roman", 10)
times_new_roman_15 = QtGui.QFont("Times New Roman", 15)
os_path_dirname_file = os.path.dirname(__file__)

class Ui_MainWindow(object):
    from Python.SetupUI import setupUi
    def welcome_UI(self):
        self.welcomelabel1 = QtWidgets.QLabel(self.centralwidget)                                   # 标签控件
        self.welcomelabel1.setGeometry(QtCore.QRect(0, 50, 1920, 400))                              # 设置矩形框大小，左上角坐标为（0，50），宽度为1920，高度为400
        self.welcomelabel1.setText("Bulked Segregant Analysis Tool\nfor Out-crossing Species 2")
        self.welcomelabel1.setFont(QtGui.QFont("Times New Roman", 40))                              # 设置字体
        self.welcomelabel1.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignBottom)             # 水平居中对齐，顶端对齐
        self.welcomelabel1.setWordWrap(True)                                                        # 文字自动换行

        self.welcomelabel2 = QtWidgets.QLabel(self.centralwidget)                                   # 标签控件
        self.welcomelabel2.setGeometry(QtCore.QRect(0, 400, 1920, 400))                             # 设置矩形框大小，左上角坐标为（0，400），宽度为1920，高度为400
        self.welcomelabel2.setText("\n\nWelcome to BSATOS2!")
        self.welcomelabel2.setFont(QtGui.QFont("Times New Roman", 30))                              # 设置字体
        self.welcomelabel2.setAlignment(QtCore.Qt.AlignHCenter)                                     # 水平居中对齐
        self.welcomelabel2.setWordWrap(True)                                                        # 文字自动换行

        self.welcomelabel3 = QtWidgets.QLabel(self.centralwidget)                                   # 标签控件
        self.welcomelabel3.setGeometry(QtCore.QRect(0, 700, 1920, 400))                             # 设置矩形框大小，左上角坐标为（0，700），宽度为1920，高度为400
        self.welcomelabel3.setText("Version: 2.0")
        self.welcomelabel3.setFont(QtGui.QFont("Times New Roman", 20))                              # 设置字体
        self.welcomelabel3.setAlignment(QtCore.Qt.AlignHCenter)                                     # 水平居中对齐
        self.welcomelabel3.setWordWrap(True)                                                        # 文字自动换行

        self.imagelabel = QtWidgets.QLabel(self.centralwidget)                                      # 标签控件
        self.imagelabel.resize(200, 200)                                                            # 设置大小
        self.imagelabel.move(850, 50)                                                               # 设置坐标为（850，50）
        self.imagelabel.setAlignment(QtCore.Qt.AlignHCenter)                                        # 水平居中对齐
        self.pix = QtGui.QPixmap("cau.png")                                                         # 显示图片
        self.imagelabel.setPixmap(self.pix)
        self.imagelabel.setScaledContents(True)



    def menu_functions_intro_all(self):
        # self.centralwidget.hide()
        self.menu_func_widget = QtWidgets.QWidget(MainWindow)
        self.menu_func_widget.setObjectName("menu_func_widget")
        MainWindow.setCentralWidget(self.menu_func_widget)
        self.menu_functions_all_textedit = QtWidgets.QTextEdit(self.menu_func_widget)
        self.menu_functions_all_textedit.setReadOnly(True)
        self.menu_functions_all_textedit.setGeometry(QtCore.QRect(0, 0, 1915, 1060))
        # from Python.functions import func_all
        # self.menu_functions_all_textedit.setPlainText(func_all.funcusage_all_txt)
        self.menu_functions_all_textedit.setHtml(ufunc.funcusage_all_html)
        self.menu_func_widget.show()

    def menu_functions_intro_prepar(self):
        self.menu_func_widget = QtWidgets.QWidget(MainWindow)
        self.menu_func_widget.setObjectName("menu_func_widget")
        MainWindow.setCentralWidget(self.menu_func_widget)
        self.menu_functions_prepar_textedit = QtWidgets.QTextEdit(self.menu_func_widget)
        self.menu_functions_prepar_textedit.setReadOnly(True)
        self.menu_functions_prepar_textedit.setGeometry(QtCore.QRect(0, 0, 1915, 1060))
        # from Python.functions import func_prepar
        # self.menu_functions_prepar_textedit.setPlainText(func_prepar.funcusage_prepar_txt)
        self.menu_functions_prepar_textedit.setHtml(ufunc.funcusage_prepar_html)
        self.menu_func_widget.show()

    def menu_functions_intro_prep(self):
        self.menu_func_widget = QtWidgets.QWidget(MainWindow)
        self.menu_func_widget.setObjectName("menu_func_widget")
        MainWindow.setCentralWidget(self.menu_func_widget)
        self.menu_functions_prep_textedit = QtWidgets.QTextEdit(self.menu_func_widget)
        self.menu_functions_prep_textedit.setReadOnly(True)
        self.menu_functions_prep_textedit.setGeometry(QtCore.QRect(0, 0, 1915, 1060))
        # from Python.functions import func_prep
        # self.menu_functions_prep_textedit.setPlainText(func_prep.funcusage_prep_txt)
        self.menu_functions_prep_textedit.setHtml(ufunc.funcusage_prep_html)
        self.menu_func_widget.show()

    def menu_functions_intro_haplotype(self):
        self.menu_func_widget = QtWidgets.QWidget(MainWindow)
        self.menu_func_widget.setObjectName("menu_func_widget")
        MainWindow.setCentralWidget(self.menu_func_widget)
        self.menu_functions_haplotype_textedit = QtWidgets.QTextEdit(self.menu_func_widget)
        self.menu_functions_haplotype_textedit.setReadOnly(True)
        self.menu_functions_haplotype_textedit.setGeometry(QtCore.QRect(0, 0, 1915, 1060))
        # from Python.functions import func_haplotype
        # self.menu_functions_haplotype_textedit.setPlainText(func_haplotype.funcusage_haplotype_txt)
        self.menu_functions_haplotype_textedit.setHtml(ufunc.funcusage_haplotype_html)
        self.menu_func_widget.show()

    def menu_functions_intro_afd(self):
        self.menu_func_widget = QtWidgets.QWidget(MainWindow)
        self.menu_func_widget.setObjectName("menu_func_widget")
        MainWindow.setCentralWidget(self.menu_func_widget)
        self.menu_functions_afd_textedit = QtWidgets.QTextEdit(self.menu_func_widget)
        self.menu_functions_afd_textedit.setReadOnly(True)
        self.menu_functions_afd_textedit.setGeometry(QtCore.QRect(0, 0, 1915, 1060))
        # from Python.functions import func_afd
        # self.menu_functions_afd_textedit.setPlainText(func_afd.funcusage_afd_txt)
        self.menu_functions_afd_textedit.setHtml(ufunc.funcusage_afd_html)
        self.menu_func_widget.show()

    def menu_functions_intro_polish(self):
        self.menu_func_widget = QtWidgets.QWidget(MainWindow)
        self.menu_func_widget.setObjectName("menu_func_widget")
        MainWindow.setCentralWidget(self.menu_func_widget)
        self.menu_functions_polish_textedit = QtWidgets.QTextEdit(self.menu_func_widget)
        self.menu_functions_polish_textedit.setReadOnly(True)
        self.menu_functions_polish_textedit.setGeometry(QtCore.QRect(0, 0, 1915, 1060))
        # from Python.functions import func_polish
        # self.menu_functions_polish_textedit.setPlainText(func_polish.funcusage_polish_txt)
        self.menu_functions_polish_textedit.setHtml(ufunc.funcusage_polish_html)
        self.menu_func_widget.show()

    def menu_functions_intro_qtlpick(self):
        self.menu_func_widget = QtWidgets.QWidget(MainWindow)
        self.menu_func_widget.setObjectName("menu_func_widget")
        MainWindow.setCentralWidget(self.menu_func_widget)
        self.menu_functions_qtlpick_textedit = QtWidgets.QTextEdit(self.menu_func_widget)
        self.menu_functions_qtlpick_textedit.setReadOnly(True)
        self.menu_functions_qtlpick_textedit.setGeometry(QtCore.QRect(0, 0, 1915, 1060))
        # from Python.functions import func_qtlpick
        # self.menu_functions_qtlpick_textedit.setPlainText(func_qtlpick.funcusage_qtlpick_txt)
        self.menu_functions_qtlpick_textedit.setHtml(ufunc.funcusage_qtlpick_html)
        self.menu_func_widget.show()

    def menu_functions_intro_igv(self):
        self.menu_func_widget = QtWidgets.QWidget(MainWindow)
        self.menu_func_widget.setObjectName("menu_func_widget")
        MainWindow.setCentralWidget(self.menu_func_widget)
        self.menu_functions_igv_textedit = QtWidgets.QTextEdit(self.menu_func_widget)
        self.menu_functions_igv_textedit.setReadOnly(True)
        self.menu_functions_igv_textedit.setGeometry(QtCore.QRect(0, 0, 1915, 1060))
        # from Python.functions import func_igv
        # self.menu_functions_igv_textedit.setPlainText(func_igv.funcusage_igv_txt)
        self.menu_functions_igv_textedit.setHtml(ufunc.funcusage_igv_html)
        self.menu_func_widget.show()



    def menu_parameters_all_paraset(self):
        self.menu_parameters_widget = QtWidgets.QWidget(MainWindow)
        self.menu_parameters_widget.setObjectName("menu_parameters_widget")
        MainWindow.setCentralWidget(self.menu_parameters_widget)
        self.form_all = QtWidgets.QFormLayout(self.menu_parameters_widget)
        self.para_all_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_label.setText("Example:\n1) Use paired-end fastq files to run BSATOS2\n    bsatos2 all --o result --r Malus_domestica.fa --gtf Malus_domestica.gtf --pf1 P_1.fastq --pf2 P_2.fastq --mf1 M_1.fastq --mf2 M_2.fastq --hf1 H_1.fastq --hf2 H_2.fastq --lf1 L_1.fastq --lf2 L_2.fastq\n2) Use pre-aligned BAM files to run BSATOS2\n    bsatos2 all --o result --r Malus_domestica.fa --gtf Malus_domestica.gtf --pb P.bam --mb M.bam --hb H.bam --lb L.bam\n")
        self.para_all_label.setFont(times_new_roman_10)
        self.para_all_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_all_label.setWordWrap(True)
        self.para_all_radiobutton_reads = QtWidgets.QRadioButton(self.menu_parameters_widget)
        self.para_all_radiobutton_reads.setObjectName("para_all_radiobutton_reads")
        self.para_all_radiobutton_reads.setChecked(False)
        self.para_all_radiobutton_reads.setText("Use paired-end fastq files")
        self.para_all_radiobutton_reads.setFont(times_new_roman_10)
        self.para_all_radiobutton_bam = QtWidgets.QRadioButton(self.menu_parameters_widget)
        self.para_all_radiobutton_bam.setObjectName("para_all_radiobutton_bam")
        self.para_all_radiobutton_bam.setChecked(False)
        self.para_all_radiobutton_bam.setText("Use pre-aligned BAMs files")
        self.para_all_radiobutton_bam.setFont(times_new_roman_10)
        self.para_all_separator_label1 = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_options_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_options_label.setText("Options (Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly. Default values will apply to unseted arguments.)")
        self.para_all_options_label.setWordWrap(True)
        self.para_all_options_label.setFont(times_new_roman_10)
        self.para_all_options_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_all_o_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_o_label.setText("--o   STR")
        self.para_all_o_label.setFont(times_new_roman_10)
        self.para_all_o_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_o_text.setMaximumWidth(1000)                                                      # 设置文本框最大宽度
        self.para_all_o_text.setPlaceholderText("output dir name prefix [all] [not required]")          # 设置文本框浮显文字
        self.para_all_o_text.setToolTip("output dir name prefix [all] [not required]")                  # 设置鼠标悬停提示框
        self.para_all_o_text.setFont(times_new_roman_10)
        self.para_all_r_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_r_label.setText("--r   FILE")
        self.para_all_r_label.setFont(times_new_roman_10)
        self.para_all_r_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_r_text.setMaximumWidth(1000)
        self.para_all_r_text.setPlaceholderText("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_all_r_text.setToolTip("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_all_r_text.setFont(times_new_roman_10)
        self.para_all_db_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_db_label.setText("--db   STR")
        self.para_all_db_label.setFont(times_new_roman_10)
        self.para_all_db_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_db_text.setMaximumWidth(1000)
        self.para_all_db_text.setPlaceholderText("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_all_db_text.setToolTip("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_all_db_text.setFont(times_new_roman_10)
        self.para_all_gtf_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_gtf_label.setText("--gtf   FILE")
        self.para_all_gtf_label.setFont(times_new_roman_10)
        self.para_all_gtf_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_gtf_text.setMaximumWidth(1000)
        self.para_all_gtf_text.setPlaceholderText("GTF file of genes (GTF2 format; not required if GFF file is provided) [Malus_domestica.gtf]")
        self.para_all_gtf_text.setToolTip("GTF file of genes (GTF2 format; not required if GFF file is provided) [Malus_domestica.gtf]")
        self.para_all_gtf_text.setFont(times_new_roman_10)
        self.para_all_gff_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_gff_label.setText("--gff   FILE")
        self.para_all_gff_label.setFont(times_new_roman_10)
        self.para_all_gff_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_gff_text.setMaximumWidth(1000)
        self.para_all_gff_text.setPlaceholderText("GFF file of genes (GFF3 format; not required if GTF file is provided) [Malus_domestica.gff3]")
        self.para_all_gff_text.setToolTip("GFF file of genes (GFF3 format; not required if GTF file is provided) [Malus_domestica.gff3]")
        self.para_all_gff_text.setFont(times_new_roman_10)
        self.para_all_pf1_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_pf1_label.setText("--pf1   FILE")
        self.para_all_pf1_label.setFont(times_new_roman_10)
        self.para_all_pf1_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_pf1_text.setMaximumWidth(1000)
        self.para_all_pf1_text.setPlaceholderText("paired-end1 fastq file of pollen parent [not required if pre-aligned BAM files is provided]")
        self.para_all_pf1_text.setToolTip("paired-end1 fastq file of pollen parent [not required if pre-aligned BAM files is provided]")
        self.para_all_pf1_text.setFont(times_new_roman_10)
        self.para_all_pf2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_pf2_label.setText("--pf2   FILE")
        self.para_all_pf2_label.setFont(times_new_roman_10)
        self.para_all_pf2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_pf2_text.setMaximumWidth(1000)
        self.para_all_pf2_text.setPlaceholderText("paired-end2 fastq file of pollen parent [not required if pre-aligned BAM files is provided]")
        self.para_all_pf2_text.setToolTip("paired-end2 fastq file of pollen parent [not required if pre-aligned BAM files is provided]")
        self.para_all_pf2_text.setFont(times_new_roman_10)
        self.para_all_mf1_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_mf1_label.setText("--mf1   FILE")
        self.para_all_mf1_label.setFont(times_new_roman_10)
        self.para_all_mf1_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_mf1_text.setMaximumWidth(1000)
        self.para_all_mf1_text.setPlaceholderText("paired-end1 fastq file of maternal parent [not required if pre-aligned BAM files is provided]")
        self.para_all_mf1_text.setToolTip("paired-end1 fastq file of maternal parent [not required if pre-aligned BAM files is provided]")
        self.para_all_mf1_text.setFont(times_new_roman_10)
        self.para_all_mf2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_mf2_label.setText("--mf2   FILE")
        self.para_all_mf2_label.setFont(times_new_roman_10)
        self.para_all_mf2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_mf2_text.setMaximumWidth(1000)
        self.para_all_mf2_text.setPlaceholderText("paired-end2 fastq file of maternal parent [not required if pre-aligned BAM files is provided]")
        self.para_all_mf2_text.setToolTip("paired-end2 fastq file of maternal parent [not required if pre-aligned BAM files is provided]")
        self.para_all_mf2_text.setFont(times_new_roman_10)
        self.para_all_hf1_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_hf1_label.setText("--hf1   FILE")
        self.para_all_hf1_label.setFont(times_new_roman_10)
        self.para_all_hf1_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_hf1_text.setMaximumWidth(1000)
        self.para_all_hf1_text.setPlaceholderText("paired-end1 fastq file of H pool reads [not required if pre-aligned BAM files is provided]")
        self.para_all_hf1_text.setToolTip("paired-end1 fastq file of H pool reads [not required if pre-aligned BAM files is provided]")
        self.para_all_hf1_text.setFont(times_new_roman_10)
        self.para_all_hf2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_hf2_label.setText("--hf2   FILE")
        self.para_all_hf2_label.setFont(times_new_roman_10)
        self.para_all_hf2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_hf2_text.setMaximumWidth(1000)
        self.para_all_hf2_text.setPlaceholderText("paired-end2 fastq file of H pool reads [not required if pre-aligned BAM files is provided]")
        self.para_all_hf2_text.setToolTip("paired-end2 fastq file of H pool reads [not required if pre-aligned BAM files is provided]")
        self.para_all_hf2_text.setFont(times_new_roman_10)
        self.para_all_lf1_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_lf1_label.setText("--lf1   FILE")
        self.para_all_lf1_label.setFont(times_new_roman_10)
        self.para_all_lf1_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_lf1_text.setMaximumWidth(1000)
        self.para_all_lf1_text.setPlaceholderText("paired-end1 fastq file of L pool reads [not required if pre-aligned BAM files is provided]")
        self.para_all_lf1_text.setToolTip("paired-end1 fastq file of L pool reads [not required if pre-aligned BAM files is provided]")
        self.para_all_lf1_text.setFont(times_new_roman_10)
        self.para_all_lf2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_lf2_label.setText("--lf2   FILE")
        self.para_all_lf2_label.setFont(times_new_roman_10)
        self.para_all_lf2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_lf2_text.setMaximumWidth(1000)
        self.para_all_lf2_text.setPlaceholderText("paired-end2 fastq file of L pool reads [not required if pre-aligned BAM files is provided]")
        self.para_all_lf2_text.setToolTip("paired-end2 fastq file of L pool reads [not required if pre-aligned BAM files is provided]")
        self.para_all_lf2_text.setFont(times_new_roman_10)
        self.para_all_separator_label2 = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_pb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_pb_label.setText("--pb   FILE")
        self.para_all_pb_label.setFont(times_new_roman_10)
        self.para_all_pb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_pb_text.setMaximumWidth(1000)
        self.para_all_pb_text.setPlaceholderText("BAM file of pollen parent [not required if --pf1 & --pf2 is provided]")
        self.para_all_pb_text.setToolTip("BAM file of pollen parent [not required if --pf1 & --pf2 is provided]")
        self.para_all_pb_text.setFont(times_new_roman_10)
        self.para_all_mb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_mb_label.setText("--mb   FILE")
        self.para_all_mb_label.setFont(times_new_roman_10)
        self.para_all_mb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_mb_text.setMaximumWidth(1000)
        self.para_all_mb_text.setPlaceholderText("BAM file of maternal parent [not required if --mf1 & --mf2 is provided]")
        self.para_all_mb_text.setToolTip("BAM file of maternal parent [not required if --mf1 & --mf2 is provided]")
        self.para_all_mb_text.setFont(times_new_roman_10)
        self.para_all_hb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_hb_label.setText("--hb   FILE")
        self.para_all_hb_label.setFont(times_new_roman_10)
        self.para_all_hb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_hb_text.setMaximumWidth(1000)
        self.para_all_hb_text.setPlaceholderText("BAM file of H pool [not required if --hf1 & --hf2 is provided]")
        self.para_all_hb_text.setToolTip("BAM file of H pool [not required if --hf1 & --hf2 is provided]")
        self.para_all_hb_text.setFont(times_new_roman_10)
        self.para_all_lb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_all_lb_label.setText("--lb   FILE")
        self.para_all_lb_label.setFont(times_new_roman_10)
        self.para_all_lb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_all_lb_text.setMaximumWidth(1000)
        self.para_all_lb_text.setPlaceholderText("BAM file of L pool [not required if --lf1 & --lf2 is provided]")
        self.para_all_lb_text.setToolTip("BAM file of L pool [not required if --lf1 & --lf2 is provided]")
        self.para_all_lb_text.setFont(times_new_roman_10)
        self.para_all_button = QtWidgets.QPushButton("OK", clicked = lambda:para_all_pushbutton_slot())
        self.para_all_button.setMaximumWidth(1270)
        self.para_all_button.setMinimumWidth(1270)
        self.para_all_button.setMaximumHeight(50)
        self.para_all_button.setMinimumHeight(50)
        self.para_all_button.setFont(times_new_roman_10)     # QtGui.QFont('Arial', 10, QtGui.QFont.Bold)
        self.form_all.addRow(self.para_all_label)
        self.form_all.addRow(self.para_all_radiobutton_reads, self.para_all_radiobutton_bam)
        self.form_all.addRow(self.para_all_separator_label1)
        self.form_all.addRow(self.para_all_options_label)
        self.form_all.addRow(self.para_all_o_label, self.para_all_o_text)
        self.form_all.addRow(self.para_all_r_label, self.para_all_r_text)
        self.form_all.addRow(self.para_all_db_label, self.para_all_db_text)
        self.form_all.addRow(self.para_all_gtf_label, self.para_all_gtf_text)
        self.form_all.addRow(self.para_all_gff_label, self.para_all_gff_text)
        self.form_all.addRow(self.para_all_pf1_label, self.para_all_pf1_text)
        self.form_all.addRow(self.para_all_pf2_label, self.para_all_pf2_text)
        self.form_all.addRow(self.para_all_mf1_label, self.para_all_mf1_text)
        self.form_all.addRow(self.para_all_mf2_label, self.para_all_mf2_text)
        self.form_all.addRow(self.para_all_hf1_label, self.para_all_hf1_text)
        self.form_all.addRow(self.para_all_hf2_label, self.para_all_hf2_text)
        self.form_all.addRow(self.para_all_lf1_label, self.para_all_lf1_text)
        self.form_all.addRow(self.para_all_lf2_label, self.para_all_lf2_text)
        self.form_all.addRow(self.para_all_separator_label2)
        self.form_all.addRow(self.para_all_pb_label, self.para_all_pb_text)
        self.form_all.addRow(self.para_all_mb_label, self.para_all_mb_text)
        self.form_all.addRow(self.para_all_hb_label, self.para_all_hb_text)
        self.form_all.addRow(self.para_all_lb_label, self.para_all_lb_text)
        self.form_all.addRow(self.para_all_button)
        self.menu_parameters_widget.setLayout(self.form_all)

        def para_all_pushbutton_slot():
            with open(os.path.join(sys.path[0], "Python", "src", "para_all_file"), 'w') as para_all_file:
                # QMessageBox.information(None, "Attention", "Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly.\nParameters unseted will apply to default values.", QMessageBox.Ok)
                if self.para_all_o_text.text() == '':
                    para_all_file.write("o\tall\n")
                else:
                    para_all_file.write("o\t" + self.para_all_o_text.text() + "\n")
                if self.para_all_radiobutton_reads.isChecked():
                    if (self.para_all_pf1_text.text() == '') or (self.para_all_pf2_text.text() == '') or (self.para_all_mf1_text.text() == '') or (self.para_all_mf2_text.text() == '') or (self.para_all_hf1_text.text() == '') or (self.para_all_hf2_text.text() == '') or (self.para_all_lf1_text.text() == '') or (self.para_all_lf2_text.text() == ''):
                        QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--pf1\n--pf2\n--mf1\n--mf2\n--hf1\n--hf2\n--lf1\n--lf2", QMessageBox.Ok)
                    else:
                        para_all_file.write("pf1\t" + self.para_all_pf1_text.text() + "\n")
                        para_all_file.write("pf2\t" + self.para_all_pf2_text.text() + "\n")
                        para_all_file.write("mf1\t" + self.para_all_mf1_text.text() + "\n")
                        para_all_file.write("mf2\t" + self.para_all_mf2_text.text() + "\n")
                        para_all_file.write("hf1\t" + self.para_all_hf1_text.text() + "\n")
                        para_all_file.write("hf2\t" + self.para_all_hf2_text.text() + "\n")
                        para_all_file.write("lf1\t" + self.para_all_lf1_text.text() + "\n")
                        para_all_file.write("lf2\t" + self.para_all_lf2_text.text() + "\n")
                if self.para_all_radiobutton_bam.isChecked():
                    if (self.para_all_pb_text.text() == '') or (self.para_all_mb_text.text() == '') or (self.para_all_hb_text.text() == '') or (self.para_all_lb_text.text() == ''):
                        QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--pb\n--mb\n--hb\n--lb", QMessageBox.Ok)
                    else:
                        para_all_file.write("pb\t" + self.para_all_pb_text.text() + "\n")
                        para_all_file.write("mb\t" + self.para_all_mb_text.text() + "\n")
                        para_all_file.write("hb\t" + self.para_all_hb_text.text() + "\n")
                        para_all_file.write("lb\t" + self.para_all_lb_text.text() + "\n")
                if (self.para_all_gtf_text.text() != '') and (self.para_all_gff_text.text() != ''):
                    QMessageBox.warning(None, "Warning", "Please ONLY provide a GTF or GFF file.\nGFF file is not required if GTF file is provided.\nGTF file is not required if GFF file is provided.\nPlease input again.", QMessageBox.Ok)
                if (self.para_all_gtf_text.text() == '') and (self.para_all_gff_text.text() == ''):
                    # QMessageBox.warning(None, "Warning", "You didn't provide a GTF or GFF file.\nGFF file is not required if GTF file is provided.\nGTF file is not required if GFF file is provided.\nPlease input again.", QMessageBox.Ok)
                    para_all_file.write("gtf\ttMalus_domestica.gtf\n")
                if (self.para_all_gtf_text.text() != '') and (self.para_all_gff_text.text() == ''):
                    para_all_file.write("gtf\t" + self.para_all_gtf_text.text() + "\n")
                if (self.para_all_gtf_text.text() == '') and (self.para_all_gff_text.text() != ''):
                    para_all_file.write("gff\t" + self.para_all_gff_text.text() + "\n")
                if (self.para_all_r_text.text() == ''):
                    # QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--r", QMessageBox.Ok)
                    para_all_file.write("r\tMalus_domestica.fa\n")
                else:
                    para_all_file.write("r\t" + self.para_all_r_text.text() + "\n")
                if (self.para_all_db_text.text() == ''):
                    para_all_file.write("db\tMalus_domestica\n")
                else:
                    para_all_file.write("db\t" + self.para_all_db_text.text() + "\n")
                if not (self.para_all_radiobutton_reads.isChecked() | self.para_all_radiobutton_bam.isChecked()):
                    QMessageBox.information(None, "Select Filetype", "Please click a button to select the filetype of the input files.", QMessageBox.Ok)
                else:
                    QMessageBox.information(None, "Setup", "Saving arguments to file succeeded.", QMessageBox.Ok)



    def menu_parameters_prepar_paraset(self):
        self.menu_parameters_widget = QtWidgets.QWidget(MainWindow)
        self.menu_parameters_widget.setObjectName("menu_parameters_widget")
        MainWindow.setCentralWidget(self.menu_parameters_widget)
        self.form_prepar = QtWidgets.QFormLayout(self.menu_parameters_widget)
        self.para_prepar_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_label.setText("Example:\n1) Use paired-end fastq files to run BSATOS2\n    bsatos2 prepar --o prepar --pf1 P_1.fastq --pf2 P_2.fastq --mf1 M_1.fastq --mf2 M_2.fastq --r Malus_domestica.fa --gtf Malus_domestica.gtf\n2) Use pre-aligned BAM files to run BSATOS2\n    bsatos2 prepar --o prepar --pb P.bam --mb M.bam --r Malus_domestica.fa --gtf Malus_domestica.gtf")
        self.para_prepar_label.setFont(times_new_roman_10)
        self.para_prepar_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prepar_label.setWordWrap(True)
        self.para_prepar_radiobutton_reads = QtWidgets.QRadioButton(self.menu_parameters_widget)
        self.para_prepar_radiobutton_reads.setObjectName("para_prepar_radiobutton_reads")
        self.para_prepar_radiobutton_reads.setChecked(False)
        self.para_prepar_radiobutton_reads.setText("Use paired-end fastq files")
        self.para_prepar_radiobutton_reads.setFont(times_new_roman_10)
        self.para_prepar_radiobutton_bam = QtWidgets.QRadioButton(self.menu_parameters_widget)
        self.para_prepar_radiobutton_bam.setObjectName("para_prepar_radiobutton_bam")
        self.para_prepar_radiobutton_bam.setChecked(False)
        self.para_prepar_radiobutton_bam.setText("Use pre-aligned BAMs files")
        self.para_prepar_radiobutton_bam.setFont(times_new_roman_10)
        self.para_prepar_options_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_options_label.setText("Options (Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly. Default values will apply to unseted arguments.)")
        self.para_prepar_options_label.setWordWrap(True)
        self.para_prepar_options_label.setFont(times_new_roman_10)
        self.para_prepar_options_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prepar_o_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_o_label.setText("--o   STR")
        self.para_prepar_o_label.setFont(times_new_roman_10)
        self.para_prepar_o_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_o_text.setMaximumWidth(1000)
        self.para_prepar_o_text.setPlaceholderText("output dir name prefix [prepar] [not required]")
        self.para_prepar_o_text.setToolTip("output dir name prefix [prepar] [not required]")
        self.para_prepar_o_text.setFont(times_new_roman_10)
        self.para_prepar_r_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_r_label.setText("--r   FILE")
        self.para_prepar_r_label.setFont(times_new_roman_10)
        self.para_prepar_r_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_r_text.setMaximumWidth(1000)
        self.para_prepar_r_text.setPlaceholderText("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_prepar_r_text.setToolTip("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_prepar_r_text.setFont(times_new_roman_10)
        self.para_prepar_db_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_db_label.setText("--db   STR")
        self.para_prepar_db_label.setFont(times_new_roman_10)
        self.para_prepar_db_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_db_text.setMaximumWidth(1000)
        self.para_prepar_db_text.setPlaceholderText("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_prepar_db_text.setToolTip("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_prepar_db_text.setFont(times_new_roman_10)
        self.para_prepar_gtf_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_gtf_label.setText("--gtf   FILE")
        self.para_prepar_gtf_label.setFont(times_new_roman_10)
        self.para_prepar_gtf_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_gtf_text.setMaximumWidth(1000)
        self.para_prepar_gtf_text.setPlaceholderText("GTF file of genes (GTF2 format; not required if GFF file is provided) [Malus_domestica.gtf]")
        self.para_prepar_gtf_text.setToolTip("GTF file of genes (GTF2 format; not required if GFF file is provided) [Malus_domestica.gtf]")
        self.para_prepar_gtf_text.setFont(times_new_roman_10)
        self.para_prepar_gff_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_gff_label.setText("--gff   FILE")
        self.para_prepar_gff_label.setFont(times_new_roman_10)
        self.para_prepar_gff_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_gff_text.setMaximumWidth(1000)
        self.para_prepar_gff_text.setPlaceholderText("GFF file of genes (GFF3 format; not required if GTF file is provided) [Malus_domestica.gff3]")
        self.para_prepar_gff_text.setToolTip("GFF file of genes (GFF3 format; not required if GTF file is provided) [Malus_domestica.gff3]")
        self.para_prepar_gff_text.setFont(times_new_roman_10)
        self.para_prepar_pf1_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_pf1_label.setText("--pf1   FILE")
        self.para_prepar_pf1_label.setFont(times_new_roman_10)
        self.para_prepar_pf1_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_pf1_text.setMaximumWidth(1000)
        self.para_prepar_pf1_text.setPlaceholderText("paired-end1 fastq file from pollen parent (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prepar_pf1_text.setToolTip("paired-end1 fastq file from pollen parent (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prepar_pf1_text.setFont(times_new_roman_10)
        self.para_prepar_pf2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_pf2_label.setText("--pf2   FILE")
        self.para_prepar_pf2_label.setFont(times_new_roman_10)
        self.para_prepar_pf2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_pf2_text.setMaximumWidth(1000)
        self.para_prepar_pf2_text.setPlaceholderText("paired-end2 fastq file from pollen parent (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prepar_pf2_text.setToolTip("paired-end2 fastq file from pollen parent (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prepar_pf2_text.setFont(times_new_roman_10)
        self.para_prepar_mf1_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_mf1_label.setText("--mf1   FILE")
        self.para_prepar_mf1_label.setFont(times_new_roman_10)
        self.para_prepar_mf1_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_mf1_text.setMaximumWidth(1000)
        self.para_prepar_mf1_text.setPlaceholderText("paired-end1 fastq file from maternal parent (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prepar_mf1_text.setToolTip("paired-end1 fastq file from maternal parent (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prepar_mf1_text.setFont(times_new_roman_10)
        self.para_prepar_mf2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_mf2_label.setText("--mf2   FILE")
        self.para_prepar_mf2_label.setFont(times_new_roman_10)
        self.para_prepar_mf2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_mf2_text.setMaximumWidth(1000)
        self.para_prepar_mf2_text.setPlaceholderText("paired-end2 fastq file from maternal parent (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prepar_mf2_text.setToolTip("paired-end2 fastq file from maternal parent (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prepar_mf2_text.setFont(times_new_roman_10)
        #self.para_prepar_separator_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_pb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_pb_label.setText("--pb   FILE")
        self.para_prepar_pb_label.setFont(times_new_roman_10)
        self.para_prepar_pb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_pb_text.setMaximumWidth(1000)
        self.para_prepar_pb_text.setPlaceholderText("pre-aligned bam file from pollen parent [not required if --pf1 & --pf2 is provided]")
        self.para_prepar_pb_text.setToolTip("pre-aligned bam file from pollen parent [not required if --pf1 & --pf2 is provided]")
        self.para_prepar_pb_text.setFont(times_new_roman_10)
        self.para_prepar_mb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_mb_label.setText("--mb   FILE")
        self.para_prepar_mb_label.setFont(times_new_roman_10)
        self.para_prepar_mb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_mb_text.setMaximumWidth(1000)
        self.para_prepar_mb_text.setPlaceholderText("pre-aligned bam file from maternal parent [not required if --mf1 & --mf2 is provided]")
        self.para_prepar_mb_text.setToolTip("pre-aligned bam file from maternal parent [not required if --mf1 & --mf2 is provided]")
        self.para_prepar_mb_text.setFont(times_new_roman_10)
        self.para_prepar_alignmentoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_alignmentoptions_label.setText("Alignment Options")
        self.para_prepar_alignmentoptions_label.setFont(times_new_roman_10)
        self.para_prepar_alignmentoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prepar_t_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_t_label.setText("--t   INT")
        self.para_prepar_t_label.setFont(times_new_roman_10)
        self.para_prepar_t_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_t_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prepar_t_text.setMaximumWidth(1000)
        self.para_prepar_t_text.setPlaceholderText("number of threads [1] [not required]")
        self.para_prepar_t_text.setToolTip("number of threads [1] [not required]")
        self.para_prepar_t_text.setFont(times_new_roman_10)
        self.para_prepar_snvcallingoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_snvcallingoptions_label.setText("SNV Calling Options")
        self.para_prepar_snvcallingoptions_label.setFont(times_new_roman_10)
        self.para_prepar_snvcallingoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prepar_aq_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_aq_label.setText("--aq   INT")
        self.para_prepar_aq_label.setFont(times_new_roman_10)
        self.para_prepar_aq_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_aq_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prepar_aq_text.setMaximumWidth(1000)
        self.para_prepar_aq_text.setPlaceholderText("skip alignments with mapQ smaller than INT [10] [not required]")
        self.para_prepar_aq_text.setToolTip("skip alignments with mapQ smaller than INT [10] [not required]")
        self.para_prepar_aq_text.setFont(times_new_roman_10)
        self.para_prepar_svcallingoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_svcallingoptions_label.setText("SV Calling Options")
        self.para_prepar_svcallingoptions_label.setFont(times_new_roman_10)
        self.para_prepar_svcallingoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prepar_vq_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_vq_label.setText("--vq   INT")
        self.para_prepar_vq_label.setFont(times_new_roman_10)
        self.para_prepar_vq_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_vq_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prepar_vq_text.setMaximumWidth(1000)
        self.para_prepar_vq_text.setPlaceholderText("skip alignments with mapQ smaller than INT [10] [not required]")
        self.para_prepar_vq_text.setToolTip("skip alignments with mapQ smaller than INT [10] [not required]")
        self.para_prepar_vq_text.setFont(times_new_roman_10)
        self.para_prepar_svfilteringoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_svfilteringoptions_label.setText("SV Filtering Options")
        self.para_prepar_svfilteringoptions_label.setFont(times_new_roman_10)
        self.para_prepar_svfilteringoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prepar_dp_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_dp_label.setText("--dp   INT")
        self.para_prepar_dp_label.setFont(times_new_roman_10)
        self.para_prepar_dp_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_dp_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prepar_dp_text.setMaximumWidth(1000)
        self.para_prepar_dp_text.setPlaceholderText("support reads number including paired-reads and split reads [10] [not required]")
        self.para_prepar_dp_text.setToolTip("support reads number including paired-reads and split reads [10] [not required]")
        self.para_prepar_dp_text.setFont(times_new_roman_10)
        self.para_prepar_mq_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_mq_label.setText("--mq   INT")
        self.para_prepar_mq_label.setFont(times_new_roman_10)
        self.para_prepar_mq_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_mq_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prepar_mq_text.setMaximumWidth(1000)
        self.para_prepar_mq_text.setPlaceholderText("average mapQ [10] [not required]")
        self.para_prepar_mq_text.setToolTip("average mapQ [10] [not required]")
        self.para_prepar_mq_text.setFont(times_new_roman_10)
        self.para_prepar_l_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_l_label.setText("--l   INT")
        self.para_prepar_l_label.setFont(times_new_roman_10)
        self.para_prepar_l_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_l_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prepar_l_text.setMaximumWidth(1000)
        self.para_prepar_l_text.setPlaceholderText("minimum SV length [1000] [not required]")
        self.para_prepar_l_text.setToolTip("minimum SV length [1000] [not required]")
        self.para_prepar_l_text.setFont(times_new_roman_10)
        self.para_prepar_precise_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_precise_label.setText("--precise   STR")
        self.para_prepar_precise_label.setFont(times_new_roman_10)
        self.para_prepar_precise_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_precise_text.setMaximumWidth(1000)
        self.para_prepar_precise_text.setPlaceholderText("only kept precise SVs [yes] [not required]")
        self.para_prepar_precise_text.setToolTip("only kept precise SVs [yes] [not required]")
        self.para_prepar_precise_text.setFont(times_new_roman_10)
        self.para_prepar_snvfilteringoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_snvfilteringoptions_label.setText("SNV Filtering Options")
        self.para_prepar_snvfilteringoptions_label.setFont(times_new_roman_10)
        self.para_prepar_snvfilteringoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prepar_d_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_d_label.setText("--d   INT")
        self.para_prepar_d_label.setFont(times_new_roman_10)
        self.para_prepar_d_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_d_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prepar_d_text.setMaximumWidth(1000)
        self.para_prepar_d_text.setPlaceholderText("skip SNVs with reads depth smaller than INT [10] [not required]")
        self.para_prepar_d_text.setToolTip("skip SNVs with reads depth smaller than INT [10] [not required]")
        self.para_prepar_d_text.setFont(times_new_roman_10)
        self.para_prepar_sq_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prepar_sq_label.setText("--sq   INT")
        self.para_prepar_sq_label.setFont(times_new_roman_10)
        self.para_prepar_sq_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prepar_sq_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prepar_sq_text.setMaximumWidth(1000)
        self.para_prepar_sq_text.setPlaceholderText("skip SNVs with phred-scaled quality smaller than [10] [not required]")
        self.para_prepar_sq_text.setToolTip("skip SNVs with phred-scaled quality smaller than [10] [not required]")
        self.para_prepar_sq_text.setFont(times_new_roman_10)
        self.para_prepar_button = QtWidgets.QPushButton("OK", clicked = lambda:para_prepar_pushbutton_slot())
        self.para_prepar_button.setMaximumWidth(1270)
        self.para_prepar_button.setMinimumWidth(1270)
        self.para_prepar_button.setMaximumHeight(50)
        self.para_prepar_button.setMinimumHeight(50)
        self.para_prepar_button.setFont(times_new_roman_10)     # QtGui.QFont('Arial', 10, QtGui.QFont.Bold)
        self.form_prepar.addRow(self.para_prepar_label)
        self.form_prepar.addRow(self.para_prepar_radiobutton_reads, self.para_prepar_radiobutton_bam)
        self.form_prepar.addRow(self.para_prepar_options_label)
        self.form_prepar.addRow(self.para_prepar_o_label, self.para_prepar_o_text)
        self.form_prepar.addRow(self.para_prepar_r_label, self.para_prepar_r_text)
        self.form_prepar.addRow(self.para_prepar_db_label, self.para_prepar_db_text)
        self.form_prepar.addRow(self.para_prepar_gtf_label, self.para_prepar_gtf_text)
        self.form_prepar.addRow(self.para_prepar_gff_label, self.para_prepar_gff_text)
        self.form_prepar.addRow(self.para_prepar_pf1_label, self.para_prepar_pf1_text)
        self.form_prepar.addRow(self.para_prepar_pf2_label, self.para_prepar_pf2_text)
        self.form_prepar.addRow(self.para_prepar_mf1_label, self.para_prepar_mf1_text)
        self.form_prepar.addRow(self.para_prepar_mf2_label, self.para_prepar_mf2_text)
        #self.form_prepar.addRow(self.para_prepar_separator_label)
        self.form_prepar.addRow(self.para_prepar_pb_label, self.para_prepar_pb_text)
        self.form_prepar.addRow(self.para_prepar_mb_label, self.para_prepar_mb_text)
        self.form_prepar.addRow(self.para_prepar_alignmentoptions_label)
        self.form_prepar.addRow(self.para_prepar_t_label, self.para_prepar_t_text)
        self.form_prepar.addRow(self.para_prepar_snvcallingoptions_label)
        self.form_prepar.addRow(self.para_prepar_aq_label, self.para_prepar_aq_text)
        self.form_prepar.addRow(self.para_prepar_svcallingoptions_label)
        self.form_prepar.addRow(self.para_prepar_vq_label, self.para_prepar_vq_text)
        self.form_prepar.addRow(self.para_prepar_svfilteringoptions_label)
        self.form_prepar.addRow(self.para_prepar_dp_label, self.para_prepar_dp_text)
        self.form_prepar.addRow(self.para_prepar_mq_label, self.para_prepar_mq_text)
        self.form_prepar.addRow(self.para_prepar_l_label, self.para_prepar_l_text)
        self.form_prepar.addRow(self.para_prepar_precise_label, self.para_prepar_precise_text)
        self.form_prepar.addRow(self.para_prepar_snvfilteringoptions_label)
        self.form_prepar.addRow(self.para_prepar_d_label, self.para_prepar_d_text)
        self.form_prepar.addRow(self.para_prepar_sq_label, self.para_prepar_sq_text)
        self.form_prepar.addRow(self.para_prepar_button)
        self.menu_parameters_widget.setLayout(self.form_prepar)

        def para_prepar_pushbutton_slot():
            with open(os.path.join(sys.path[0], "Python", "src", "para_prepar_file"), 'w') as para_prepar_file:
                # QMessageBox.information(None, "Attention", "Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly.\nParameters unseted will apply to default values.", QMessageBox.Ok)
                if self.para_prepar_o_text.text() == '':
                    para_prepar_file.write("o\tprepar\n")
                else:
                    para_prepar_file.write("o\t" + self.para_prepar_o_text.text() + "\n")
                if self.para_prepar_radiobutton_reads.isChecked():
                    if (self.para_prepar_pf1_text.text() == '') or (self.para_prepar_pf2_text.text() == '') or (self.para_prepar_mf1_text.text() == '') or (self.para_prepar_mf2_text.text() == ''):
                        QMessageBox.warning(None, "Warning", "Required arguments are prepar necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--pf1\n--pf2\n--mf1\n--mf2", QMessageBox.Ok)
                    else:
                        para_prepar_file.write("pf1\t" + self.para_prepar_pf1_text.text() + "\n")
                        para_prepar_file.write("pf2\t" + self.para_prepar_pf2_text.text() + "\n")
                        para_prepar_file.write("mf1\t" + self.para_prepar_mf1_text.text() + "\n")
                        para_prepar_file.write("mf2\t" + self.para_prepar_mf2_text.text() + "\n")
                if self.para_prepar_radiobutton_bam.isChecked():
                    if (self.para_prepar_pb_text.text() == '') or (self.para_prepar_mb_text.text() == ''):
                        QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--pb\n--mb", QMessageBox.Ok)
                    else:
                        para_prepar_file.write("pb\t" + self.para_prepar_pb_text.text() + "\n")
                        para_prepar_file.write("mb\t" + self.para_prepar_mb_text.text() + "\n")
                if self.para_prepar_t_text.text() == '':
                    para_prepar_file.write("t\t1\n")
                else:
                    para_prepar_file.write("t\t" + self.para_prepar_t_text.text() + "\n")
                if self.para_prepar_aq_text.text() == '':
                    para_prepar_file.write("aq\t10\n")
                else:
                    para_prepar_file.write("aq\t" + self.para_prepar_aq_text.text() + "\n")
                if self.para_prepar_vq_text.text() == '':
                    para_prepar_file.write("vq\t10\n")
                else:
                    para_prepar_file.write("vq\t" + self.para_prepar_vq_text.text() + "\n")
                if self.para_prepar_dp_text.text() == '':
                    para_prepar_file.write("dp\t10\n")
                else:
                    para_prepar_file.write("dp\t" + self.para_prepar_dp_text.text() + "\n")
                if self.para_prepar_mq_text.text() == '':
                    para_prepar_file.write("mq\t10\n")
                else:
                    para_prepar_file.write("mq\t" + self.para_prepar_mq_text.text() + "\n")
                if self.para_prepar_l_text.text() == '':
                    para_prepar_file.write("l\t1000\n")
                else:
                    para_prepar_file.write("l\t" + self.para_prepar_l_text.text() + "\n")
                if self.para_prepar_precise_text.text() == '':
                    para_prepar_file.write("precise\tyes\n")
                else:
                    para_prepar_file.write("precise\t" + self.para_prepar_precise_text.text() + "\n")
                if self.para_prepar_d_text.text() == '':
                    para_prepar_file.write("d\t10\n")
                else:
                    para_prepar_file.write("d\t" + self.para_prepar_d_text.text() + "\n")
                if self.para_prepar_sq_text.text() == '':
                    para_prepar_file.write("sq\t10\n")
                else:
                    para_prepar_file.write("sq\t" + self.para_prepar_sq_text.text() + "\n")
                if (self.para_prepar_gtf_text.text() != '') and (self.para_prepar_gff_text.text() != ''):
                    QMessageBox.warning(None, "Warning", "Please ONLY provide a GTF or GFF file.\nGFF file is not required if GTF file is provided.\nGTF file is not required if GFF file is provided.\nPlease input again.", QMessageBox.Ok)
                if (self.para_prepar_gtf_text.text() == '') and (self.para_prepar_gff_text.text() == ''):
                    para_prepar_file.write("gtf\tMalus_domestica.gtf\n")
                    # QMessageBox.warning(None, "Warning", "You didn't provide a GTF or GFF file.\nGFF file is not required if GTF file is provided.\nGTF file is not required if GFF file is provided.\nPlease input again.", QMessageBox.Ok)
                if (self.para_prepar_gtf_text.text() != '') and (self.para_prepar_gff_text.text() == ''):
                    para_prepar_file.write("gtf\t" + self.para_prepar_gtf_text.text() + "\n")
                if (self.para_prepar_gtf_text.text() == '') and (self.para_prepar_gff_text.text() != ''):
                    para_prepar_file.write("gff\t" + self.para_prepar_gff_text.text() + "\n")
                if (self.para_prepar_r_text.text() == ''):
                    para_prepar_file.write("r\tMalus_domestica.fa\n")
                    # QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--r", QMessageBox.Ok)
                else:
                    para_prepar_file.write("r\t" + self.para_prepar_r_text.text() + "\n")
                if (self.para_prepar_db_text.text() == ''):
                    para_prepar_file.write("db\tMalus_domestica\n")
                else:
                    para_prepar_file.write("db\t" + self.para_prepar_db_text.text() + "\n")
                if not (self.para_prepar_radiobutton_reads.isChecked() | self.para_prepar_radiobutton_bam.isChecked()):
                    QMessageBox.information(None, "Select Filetype", "Please click a button to select the filetype of the input files.", QMessageBox.Ok)
                else:
                    QMessageBox.information(None, "Setup", "Saving arguments to file succeeded.", QMessageBox.Ok)



    def menu_parameters_prep_paraset(self):
        self.menu_parameters_widget = QtWidgets.QWidget(MainWindow)
        self.menu_parameters_widget.setObjectName("menu_parameters_widget")
        MainWindow.setCentralWidget(self.menu_parameters_widget)
        self.form_prep = QtWidgets.QFormLayout(self.menu_parameters_widget)
        self.para_prep_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_label.setText("Example:\n1) Use paired-end fastq files to run BSATOS2\n    bsatos2 prep --o prep --hf1 H_1.fastq --hf2 H_2.fastq --lf1 L_1.fastq --lf2 L_2.fastq --r Malus_domestica.fa --gp P_M.p --gm P_M.m --g3 P_M.pm\n2) Use pre-aligned BAM files to run BSATOS2\n    bsatos2 prep --o prep --hb H.bam --lb L.bam --r Malus_domestica.fa --gp P_M.p --gm P_M.m --gpm P_M.pm\n")
        self.para_prep_label.setFont(times_new_roman_10)
        self.para_prep_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prep_label.setWordWrap(True)
        self.para_prep_radiobutton_reads = QtWidgets.QRadioButton(self.menu_parameters_widget)
        self.para_prep_radiobutton_reads.setObjectName("para_prepar_radiobutton_reads")
        self.para_prep_radiobutton_reads.setChecked(False)
        self.para_prep_radiobutton_reads.setText("Use paired-end fastq files")
        self.para_prep_radiobutton_reads.setFont(times_new_roman_10)
        self.para_prep_radiobutton_bam = QtWidgets.QRadioButton(self.menu_parameters_widget)
        self.para_prep_radiobutton_bam.setObjectName("para_prepar_radiobutton_bam")
        self.para_prep_radiobutton_bam.setChecked(False)
        self.para_prep_radiobutton_bam.setText("Use pre-aligned BAMs files")
        self.para_prep_radiobutton_bam.setFont(times_new_roman_10)
        self.para_prep_options_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_options_label.setText("Options (Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly. Default values will apply to unseted arguments.)")
        self.para_prep_options_label.setWordWrap(True)
        self.para_prep_options_label.setFont(times_new_roman_10)
        self.para_prep_options_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prep_o_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_o_label.setText("--o   STR")
        self.para_prep_o_label.setFont(times_new_roman_10)
        self.para_prep_o_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_o_text.setMaximumWidth(1000)
        self.para_prep_o_text.setPlaceholderText("output dir name prefix [prep] [not required]")
        self.para_prep_o_text.setToolTip("output dir name prefix [prep] [not required]")
        self.para_prep_o_text.setFont(times_new_roman_10)
        self.para_prep_r_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_r_label.setText("--r   FILE")
        self.para_prep_r_label.setFont(times_new_roman_10)
        self.para_prep_r_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_r_text.setMaximumWidth(1000)
        self.para_prep_r_text.setPlaceholderText("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_prep_r_text.setToolTip("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_prep_r_text.setFont(times_new_roman_10)
        self.para_prep_db_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_db_label.setText("--db   STR")
        self.para_prep_db_label.setFont(times_new_roman_10)
        self.para_prep_db_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_db_text.setMaximumWidth(1000)
        self.para_prep_db_text.setPlaceholderText("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_prep_db_text.setToolTip("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_prep_db_text.setFont(times_new_roman_10)
        self.para_prep_hf1_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_hf1_label.setText("--hf1   FILE")
        self.para_prep_hf1_label.setFont(times_new_roman_10)
        self.para_prep_hf1_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_hf1_text.setMaximumWidth(1000)
        self.para_prep_hf1_text.setPlaceholderText("paired-end1 fastq file from high extreme pool (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prep_hf1_text.setToolTip("paired-end1 fastq file from high extreme pool (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prep_hf1_text.setFont(times_new_roman_10)
        self.para_prep_hf2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_hf2_label.setText("--hf2   FILE")
        self.para_prep_hf2_label.setFont(times_new_roman_10)
        self.para_prep_hf2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_hf2_text.setMaximumWidth(1000)
        self.para_prep_hf2_text.setPlaceholderText("paired-end2 fastq file from high extreme pool (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prep_hf2_text.setToolTip("paired-end2 fastq file from high extreme pool (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prep_hf2_text.setFont(times_new_roman_10)
        self.para_prep_lf1_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_lf1_label.setText("--lf1   FILE")
        self.para_prep_lf1_label.setFont(times_new_roman_10)
        self.para_prep_lf1_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_lf1_text.setMaximumWidth(1000)
        self.para_prep_lf1_text.setPlaceholderText("paired-end1 fastq file from low extreme pool (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prep_lf1_text.setToolTip("paired-end1 fastq file from low extreme pool (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prep_lf1_text.setFont(times_new_roman_10)
        self.para_prep_lf2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_lf2_label.setText("--lf2   FILE")
        self.para_prep_lf2_label.setFont(times_new_roman_10)
        self.para_prep_lf2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_lf2_text.setMaximumWidth(1000)
        self.para_prep_lf2_text.setPlaceholderText("paired-end2 fastq file from low extreme pool (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prep_lf2_text.setToolTip("paired-end2 fastq file from low extreme pool (FASTQ format; not required if pre-aligned BAM files is provided)")
        self.para_prep_lf2_text.setFont(times_new_roman_10)
        self.para_prep_hb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_hb_label.setText("--hb   FILE")
        self.para_prep_hb_label.setFont(times_new_roman_10)
        self.para_prep_hb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_hb_text.setMaximumWidth(1000)
        self.para_prep_hb_text.setPlaceholderText("pre-aligned bam file from high extreme pool [not required if --hf1 & --hf2 is provided]")
        self.para_prep_hb_text.setToolTip("pre-aligned bam file from high extreme pool [not required if --hf1 & --hf2 is provided]")
        self.para_prep_hb_text.setFont(times_new_roman_10)
        self.para_prep_lb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_lb_label.setText("--lb   FILE")
        self.para_prep_lb_label.setFont(times_new_roman_10)
        self.para_prep_lb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_lb_text.setMaximumWidth(1000)
        self.para_prep_lb_text.setPlaceholderText("pre-aligned bam file from low extreme pool [not required if --lf1 & --lf2 is provided]")
        self.para_prep_lb_text.setToolTip("pre-aligned bam file from low extreme pool [not required if --lf1 & --lf2 is provided]")
        self.para_prep_lb_text.setFont(times_new_roman_10)
        self.para_prep_gp_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_gp_label.setText("--gp   FILE")
        self.para_prep_gp_label.setFont(times_new_roman_10)
        self.para_prep_gp_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_gp_text.setMaximumWidth(1000)
        self.para_prep_gp_text.setPlaceholderText("P file from prepar step [required]")
        self.para_prep_gp_text.setToolTip("P file from prepar step [required]")
        self.para_prep_gp_text.setFont(times_new_roman_10)
        self.para_prep_gm_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_gm_label.setText("--gm   FILE")
        self.para_prep_gm_label.setFont(times_new_roman_10)
        self.para_prep_gm_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_gm_text.setMaximumWidth(1000)
        self.para_prep_gm_text.setPlaceholderText("M file from prepar step [required]")
        self.para_prep_gm_text.setToolTip("M file from prepar step [required]")
        self.para_prep_gm_text.setFont(times_new_roman_10)
        self.para_prep_gpm_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_gpm_label.setText("--gpm   FILE")
        self.para_prep_gpm_label.setFont(times_new_roman_10)
        self.para_prep_gpm_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_gpm_text.setMaximumWidth(1000)
        self.para_prep_gpm_text.setPlaceholderText("PM file from prepar step [required]")
        self.para_prep_gpm_text.setToolTip("PM file from prepar step [required]")
        self.para_prep_gpm_text.setFont(times_new_roman_10)
        self.para_prep_alignmentoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_alignmentoptions_label.setText("Alignment Options")
        self.para_prep_alignmentoptions_label.setFont(times_new_roman_10)
        self.para_prep_alignmentoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prep_t2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_t2_label.setText("--t2   INT")
        self.para_prep_t2_label.setFont(times_new_roman_10)
        self.para_prep_t2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_t2_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prep_t2_text.setMaximumWidth(1000)
        self.para_prep_t2_text.setPlaceholderText("number of threads [1] [not required]")
        self.para_prep_t2_text.setToolTip("number of threads [1] [not required]")
        self.para_prep_t2_text.setFont(times_new_roman_10)
        self.para_prep_snvcallingoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_snvcallingoptions_label.setText("SNV Calling Options")
        self.para_prep_snvcallingoptions_label.setFont(times_new_roman_10)
        self.para_prep_snvcallingoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prep_aq2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_aq2_label.setText("--aq2   INT")
        self.para_prep_aq2_label.setFont(times_new_roman_10)
        self.para_prep_aq2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_aq2_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prep_aq2_text.setMaximumWidth(1000)
        self.para_prep_aq2_text.setPlaceholderText("skip alignments with mapQ smaller than INT [10] [not required]")
        self.para_prep_aq2_text.setToolTip("skip alignments with mapQ smaller than INT [10] [not required]")
        self.para_prep_aq2_text.setFont(times_new_roman_10)
        self.para_prep_snvfilteringoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_snvfilteringoptions_label.setText("SNV Filtering Options")
        self.para_prep_snvfilteringoptions_label.setFont(times_new_roman_10)
        self.para_prep_snvfilteringoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_prep_d2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_d2_label.setText("--d2    INT")
        self.para_prep_d2_label.setFont(times_new_roman_10)
        self.para_prep_d2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_d2_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prep_d2_text.setMaximumWidth(1000)
        self.para_prep_d2_text.setPlaceholderText("average sequencing coverage [10] [not required]")
        self.para_prep_d2_text.setToolTip("average sequencing coverage [10] [not required]")
        self.para_prep_d2_text.setFont(times_new_roman_10)
        self.para_prep_sq2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_sq2_label.setText("--sq2   INT")
        self.para_prep_sq2_label.setFont(times_new_roman_10)
        self.para_prep_sq2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_sq2_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prep_sq2_text.setMaximumWidth(1000)
        self.para_prep_sq2_text.setPlaceholderText("skip SNVs with phred-scaled quality smaller than [10] [not required]")
        self.para_prep_sq2_text.setToolTip("skip SNVs with phred-scaled quality smaller than [10] [not required]")
        self.para_prep_sq2_text.setFont(times_new_roman_10)
        self.para_prep_pn_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_prep_pn_label.setText("--pn   INT")
        self.para_prep_pn_label.setFont(times_new_roman_10)
        self.para_prep_pn_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_prep_pn_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_prep_pn_text.setMaximumWidth(1000)
        self.para_prep_pn_text.setPlaceholderText("reads counts of minor allele should greater than INT [3] [not required]")
        self.para_prep_pn_text.setToolTip("reads counts of minor allele should greater than INT [3] [not required]")
        self.para_prep_pn_text.setFont(times_new_roman_10)
        self.para_prep_button = QtWidgets.QPushButton("OK", clicked = lambda:para_prep_pushbutton_slot())
        self.para_prep_button.setMaximumWidth(1270)
        self.para_prep_button.setMinimumWidth(1270)
        self.para_prep_button.setMaximumHeight(50)
        self.para_prep_button.setMinimumHeight(50)
        self.para_prep_button.setFont(times_new_roman_10)     # QtGui.QFont('Arial', 10, QtGui.QFont.Bold)
        self.form_prep.addRow(self.para_prep_label)
        self.form_prep.addRow(self.para_prep_radiobutton_reads, self.para_prep_radiobutton_bam)
        self.form_prep.addRow(self.para_prep_options_label)
        self.form_prep.addRow(self.para_prep_o_label, self.para_prep_o_text)
        self.form_prep.addRow(self.para_prep_r_label, self.para_prep_r_text)
        self.form_prep.addRow(self.para_prep_db_label, self.para_prep_db_text)
        self.form_prep.addRow(self.para_prep_hf1_label, self.para_prep_hf1_text)
        self.form_prep.addRow(self.para_prep_hf2_label, self.para_prep_hf2_text)
        self.form_prep.addRow(self.para_prep_lf1_label, self.para_prep_lf1_text)
        self.form_prep.addRow(self.para_prep_lf2_label, self.para_prep_lf2_text)
        self.form_prep.addRow(self.para_prep_hb_label, self.para_prep_hb_text)
        self.form_prep.addRow(self.para_prep_lb_label, self.para_prep_lb_text)
        self.form_prep.addRow(self.para_prep_gp_label, self.para_prep_gp_text)
        self.form_prep.addRow(self.para_prep_gm_label, self.para_prep_gm_text)
        self.form_prep.addRow(self.para_prep_gpm_label, self.para_prep_gpm_text)
        self.form_prep.addRow(self.para_prep_alignmentoptions_label)
        self.form_prep.addRow(self.para_prep_t2_label, self.para_prep_t2_text)
        self.form_prep.addRow(self.para_prep_snvcallingoptions_label)
        self.form_prep.addRow(self.para_prep_aq2_label, self.para_prep_aq2_text)
        self.form_prep.addRow(self.para_prep_snvfilteringoptions_label)
        self.form_prep.addRow(self.para_prep_d2_label, self.para_prep_d2_text)
        self.form_prep.addRow(self.para_prep_sq2_label, self.para_prep_sq2_text)
        self.form_prep.addRow(self.para_prep_pn_label, self.para_prep_pn_text)
        self.form_prep.addRow(self.para_prep_button)
        self.menu_parameters_widget.setLayout(self.form_prep)

        def para_prep_pushbutton_slot():
            with open(os.path.join(sys.path[0], "Python", "src", "para_prep_file"), 'w') as para_prep_file:
                # QMessageBox.information(None, "Attention", "Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly.\nParameters unseted will apply to default values.", QMessageBox.Ok)
                if self.para_prep_o_text.text() == '':
                    para_prep_file.write("o\tprep\n")
                else:
                    para_prep_file.write("o\t" + self.para_prep_o_text.text() + "\n")
                if self.para_prep_radiobutton_reads.isChecked():
                    if (self.para_prep_hf1_text.text() == '') or (self.para_prep_hf2_text.text() == '') or (self.para_prep_lf1_text.text() == '') or (self.para_prep_lf2_text.text() == ''):
                        QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--hf1\n--hf2\n--lf1\n--lf2", QMessageBox.Ok)
                    else:
                        para_prep_file.write("hf1\t" + self.para_prep_hf1_text.text() + "\n")
                        para_prep_file.write("hf2\t" + self.para_prep_hf2_text.text() + "\n")
                        para_prep_file.write("lf1\t" + self.para_prep_lf1_text.text() + "\n")
                        para_prep_file.write("lf2\t" + self.para_prep_lf2_text.text() + "\n")
                if self.para_prep_radiobutton_bam.isChecked():
                    if (self.para_prep_hb_text.text() == '') or (self.para_prep_lb_text.text() == ''):
                        QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--hb\n--lb", QMessageBox.Ok)
                    else:
                        para_prep_file.write("hb\t" + self.para_prep_hb_text.text() + "\n")
                        para_prep_file.write("lb\t" + self.para_prep_lb_text.text() + "\n")
                if self.para_prep_t2_text.text() == '':
                    para_prep_file.write("t2\t1\n")
                else:
                    para_prep_file.write("t2\t" + self.para_prep_t2_text.text() + "\n")
                if self.para_prep_aq2_text.text() == '':
                    para_prep_file.write("aq2\t10\n")
                else:
                    para_prep_file.write("aq2\t" + self.para_prep_aq2_text.text() + "\n")
                if self.para_prep_d2_text.text() == '':
                    para_prep_file.write("d2\t10\n")
                else:
                    para_prep_file.write("d2\t" + self.para_prep_d2_text.text() + "\n")
                if self.para_prep_sq2_text.text() == '':
                    para_prep_file.write("sq2\t10\n")
                else:
                    para_prep_file.write("sq2\t" + self.para_prep_sq2_text.text() + "\n")
                if self.para_prep_pn_text.text() == '':
                    para_prep_file.write("pn\t3\n")
                else:
                    para_prep_file.write("pn\t" + self.para_prep_pn_text.text() + "\n")
                if self.para_prep_r_text.text() == '':
                    para_prep_file.write("r\tMalus_domestica.fa\n")
                else:
                    para_prep_file.write("r\t" + self.para_prep_r_text.text() + "\n")
                if (self.para_prep_db_text.text() == ''):
                    para_prep_file.write("db\tMalus_domestica\n")
                else:
                    para_prep_file.write("db\t" + self.para_prep_db_text.text() + "\n")
                if not (self.para_prep_radiobutton_reads.isChecked() | self.para_prep_radiobutton_bam.isChecked()):
                    QMessageBox.information(None, "Select Filetype", "Please click a button to select the filetype of the input files.", QMessageBox.Ok)
                else:
                    if (self.para_prep_gp_text.text() == '') or (self.para_prep_gm_text.text() == '') or (self.para_prep_gpm_text.text() == ''):
                        QMessageBox.warning(None, "Warning", "Required parameters are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--r\n--gp\n--gm\n--gpm", QMessageBox.Ok)
                    else:
                        para_prep_file.write("gp\t" + self.para_prep_gp_text.text() + "\n")
                        para_prep_file.write("gm\t" + self.para_prep_gm_text.text() + "\n")
                        para_prep_file.write("gpm\t" + self.para_prep_gpm_text.text() + "\n")
                        QMessageBox.information(None, "Setup", "Saving arguments to file succeeded.", QMessageBox.Ok)



    def menu_parameters_haplotype_paraset(self):
        self.menu_parameters_widget = QtWidgets.QWidget(MainWindow)
        self.menu_parameters_widget.setObjectName("menu_parameters_widget")
        MainWindow.setCentralWidget(self.menu_parameters_widget)
        self.form_haplotype = QtWidgets.QFormLayout(self.menu_parameters_widget)
        self.para_haplotype_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_label.setText("Example:\n    bsatos2 haplotype --o haplotype --pb P.bam --mb M.bam --hb H.bam --lb L.bam --r Malus_domestica.fa\n")
        self.para_haplotype_label.setFont(times_new_roman_10)
        self.para_haplotype_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_haplotype_label.setWordWrap(True)
        self.para_haplotype_options_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_options_label.setText("Options (Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly. Default values will apply to unseted arguments.)")
        self.para_haplotype_options_label.setWordWrap(True)
        self.para_haplotype_options_label.setFont(times_new_roman_10)
        self.para_haplotype_options_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_haplotype_o_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_o_label.setText("--o   STR")
        self.para_haplotype_o_label.setFont(times_new_roman_10)
        self.para_haplotype_o_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_o_text.setMaximumWidth(1000)
        self.para_haplotype_o_text.setPlaceholderText("output dir name prefix [haplotype] [not required]")
        self.para_haplotype_o_text.setToolTip("output dir name prefix [haplotype] [not required]")
        self.para_haplotype_o_text.setFont(times_new_roman_10)
        self.para_haplotype_r_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_r_label.setText("--r   FILE")
        self.para_haplotype_r_label.setFont(times_new_roman_10)
        self.para_haplotype_r_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_r_text.setMaximumWidth(1000)
        self.para_haplotype_r_text.setPlaceholderText("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_haplotype_r_text.setToolTip("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_haplotype_r_text.setFont(times_new_roman_10)
        self.para_haplotype_db_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_db_label.setText("--db   STR")
        self.para_haplotype_db_label.setFont(times_new_roman_10)
        self.para_haplotype_db_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_db_text.setMaximumWidth(1000)
        self.para_haplotype_db_text.setPlaceholderText("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_haplotype_db_text.setToolTip("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_haplotype_db_text.setFont(times_new_roman_10)
        self.para_haplotype_hb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_hb_label.setText("--hb   FILE")
        self.para_haplotype_hb_label.setFont(times_new_roman_10)
        self.para_haplotype_hb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_hb_text.setMaximumWidth(1000)
        self.para_haplotype_hb_text.setPlaceholderText("pre-aligned bam file from high extreme pool [required]")
        self.para_haplotype_hb_text.setToolTip("pre-aligned bam file from high extreme pool [required]")
        self.para_haplotype_hb_text.setFont(times_new_roman_10)
        self.para_haplotype_lb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_lb_label.setText("--lb   FILE")
        self.para_haplotype_lb_label.setFont(times_new_roman_10)
        self.para_haplotype_lb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_lb_text.setMaximumWidth(1000)
        self.para_haplotype_lb_text.setPlaceholderText("pre-aligned bam file from low extreme pool [required]")
        self.para_haplotype_lb_text.setToolTip("pre-aligned bam file from low extreme pool [required]")
        self.para_haplotype_lb_text.setFont(times_new_roman_10)
        self.para_haplotype_pb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_pb_label.setText("--pb   FILE")
        self.para_haplotype_pb_label.setFont(times_new_roman_10)
        self.para_haplotype_pb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_pb_text.setMaximumWidth(1000)
        self.para_haplotype_pb_text.setPlaceholderText("pre-aligned bam file from pollen parent [required]")
        self.para_haplotype_pb_text.setToolTip("pre-aligned bam file from pollen parent [required]")
        self.para_haplotype_pb_text.setFont(times_new_roman_10)
        self.para_haplotype_mb_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_mb_label.setText("--mb   FILE")
        self.para_haplotype_mb_label.setFont(times_new_roman_10)
        self.para_haplotype_mb_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_mb_text.setMaximumWidth(1000)
        self.para_haplotype_mb_text.setPlaceholderText("pre-aligned bam file from maternal parent [required]")
        self.para_haplotype_mb_text.setToolTip("pre-aligned bam file from maternal parent [required]")
        self.para_haplotype_mb_text.setFont(times_new_roman_10)
        self.para_haplotype_hapcut2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_hapcut2_label.setText("--hapcut2   STR")
        self.para_haplotype_hapcut2_label.setFont(times_new_roman_10)
        self.para_haplotype_hapcut2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_hapcut2_text.setMaximumWidth(1000)
        self.para_haplotype_hapcut2_text.setPlaceholderText("use samtools algorithm or HAPCUT2 algorithm to assembly haplotype [NO] [not required]")
        self.para_haplotype_hapcut2_text.setToolTip("use samtools algorithm or HAPCUT2 algorithm to assembly haplotype [NO] [not required]")
        self.para_haplotype_hapcut2_text.setFont(times_new_roman_10)
        self.para_haplotype_snvgenotypingoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_snvgenotypingoptions_label.setText("SNV Genotyping Options")
        self.para_haplotype_snvgenotypingoptions_label.setFont(times_new_roman_10)
        self.para_haplotype_snvgenotypingoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_haplotype_t3_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_t3_label.setText("--t3   INT")
        self.para_haplotype_t3_label.setFont(times_new_roman_10)
        self.para_haplotype_t3_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_t3_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_haplotype_t3_text.setMaximumWidth(1000)
        self.para_haplotype_t3_text.setPlaceholderText("number of threads [1] [not required]")
        self.para_haplotype_t3_text.setToolTip("number of threads [1] [not required]")
        self.para_haplotype_t3_text.setFont(times_new_roman_10)
        self.para_haplotype_dep_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_dep_label.setText("--dep   INT")
        self.para_haplotype_dep_label.setFont(times_new_roman_10)
        self.para_haplotype_dep_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_dep_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_haplotype_dep_text.setMaximumWidth(1000)
        self.para_haplotype_dep_text.setPlaceholderText("skip SNPs with read depth smaller than INT [10] [not required]")
        self.para_haplotype_dep_text.setToolTip("skip SNPs with read depth smaller than INT [10] [not required]")
        self.para_haplotype_dep_text.setFont(times_new_roman_10)
        self.para_haplotype_aq3_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_aq3_label.setText("--aq3   INT")
        self.para_haplotype_aq3_label.setFont(times_new_roman_10)
        self.para_haplotype_aq3_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_aq3_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_haplotype_aq3_text.setMaximumWidth(1000)
        self.para_haplotype_aq3_text.setPlaceholderText("skip alignment with mapQ smaller than INT [10] [not required]")
        self.para_haplotype_aq3_text.setToolTip("skip alignment with mapQ smaller than INT [10] [not required]")
        self.para_haplotype_aq3_text.setFont(times_new_roman_10)
        self.para_haplotype_vq2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_haplotype_vq2_label.setText("--vq2   INT")
        self.para_haplotype_vq2_label.setFont(times_new_roman_10)
        self.para_haplotype_vq2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_haplotype_vq2_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_haplotype_vq2_text.setMaximumWidth(1000)
        self.para_haplotype_vq2_text.setPlaceholderText("skip SNPs with phred-scaled quality smaller than INT [10] [not required]")
        self.para_haplotype_vq2_text.setToolTip("skip SNPs with phred-scaled quality smaller than INT [10] [not required]")
        self.para_haplotype_vq2_text.setFont(times_new_roman_10)
        self.para_haplotype_button = QtWidgets.QPushButton("OK", clicked = lambda:para_haplotype_pushbutton_slot())
        self.para_haplotype_button.setMaximumWidth(1150)
        self.para_haplotype_button.setMinimumWidth(1150)
        self.para_haplotype_button.setMaximumHeight(50)
        self.para_haplotype_button.setMinimumHeight(50)
        self.para_haplotype_button.setFont(times_new_roman_10)     # QtGui.QFont('Arial', 10, QtGui.QFont.Bold)
        self.form_haplotype.addRow(self.para_haplotype_label)
        self.form_haplotype.addRow(self.para_haplotype_options_label)
        self.form_haplotype.addRow(self.para_haplotype_o_label, self.para_haplotype_o_text)
        self.form_haplotype.addRow(self.para_haplotype_r_label, self.para_haplotype_r_text)
        self.form_haplotype.addRow(self.para_haplotype_t3_label, self.para_haplotype_t3_text)
        self.form_haplotype.addRow(self.para_haplotype_db_label, self.para_haplotype_db_text)
        self.form_haplotype.addRow(self.para_haplotype_hb_label, self.para_haplotype_hb_text)
        self.form_haplotype.addRow(self.para_haplotype_lb_label, self.para_haplotype_lb_text)
        self.form_haplotype.addRow(self.para_haplotype_pb_label, self.para_haplotype_pb_text)
        self.form_haplotype.addRow(self.para_haplotype_mb_label, self.para_haplotype_mb_text)
        self.form_haplotype.addRow(self.para_haplotype_hapcut2_label, self.para_haplotype_hapcut2_text)
        self.form_haplotype.addRow(self.para_haplotype_snvgenotypingoptions_label)
        self.form_haplotype.addRow(self.para_haplotype_dep_label, self.para_haplotype_dep_text)
        self.form_haplotype.addRow(self.para_haplotype_aq3_label, self.para_haplotype_aq3_text)
        self.form_haplotype.addRow(self.para_haplotype_vq2_label, self.para_haplotype_vq2_text)
        self.form_haplotype.addRow(self.para_haplotype_button)
        self.menu_parameters_widget.setLayout(self.form_haplotype)

        def para_haplotype_pushbutton_slot():
            with open(os.path.join(sys.path[0], "Python", "src", "para_haplotype_file"), 'w') as para_haplotype_file:
                # QMessageBox.information(None, "Attention", "Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly.\nParameters unseted will apply to default values.", QMessageBox.Ok)
                if self.para_haplotype_o_text.text() == '':
                    para_haplotype_file.write("o\thaplotype\n")
                else:
                    para_haplotype_file.write("o\t" + self.para_haplotype_o_text.text() + "\n")
                if self.para_haplotype_t3_text.text() == '':
                    para_haplotype_file.write("t3\t1\n")
                else:
                    para_haplotype_file.write("t3\t" + self.para_haplotype_t3_text.text() + "\n")
                if self.para_haplotype_hapcut2_text.text() == '':
                    para_haplotype_file.write("hapcut2\tNO\n")
                else:
                    para_haplotype_file.write("hapcut2\t" + self.para_haplotype_hapcut2_text.text() + "\n")
                if self.para_haplotype_dep_text.text() == '':
                    para_haplotype_file.write("dep\t10\n")
                else:
                    para_haplotype_file.write("dep\t" + self.para_haplotype_dep_text.text() + "\n")
                if self.para_haplotype_aq3_text.text() == '':
                    para_haplotype_file.write("aq3\t10\n")
                else:
                    para_haplotype_file.write("aq3\t" + self.para_haplotype_aq3_text.text() + "\n")
                if self.para_haplotype_vq2_text.text() == '':
                    para_haplotype_file.write("vq2\t40\n")
                else:
                    para_haplotype_file.write("vq2\t" + self.para_haplotype_vq2_text.text() + "\n")
                if (self.para_haplotype_r_text.text() == ''):
                    para_haplotype_file.write("r\tMalus_domestica.fa\n")
                    # QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--r", QMessageBox.Ok)
                else:
                    para_haplotype_file.write("r\t" + self.para_haplotype_r_text.text() + "\n")
                if (self.para_haplotype_db_text.text() == ''):
                    para_haplotype_file.write("db\tMalus_domestica\n")
                else:
                    para_haplotype_file.write("db\t" + self.para_haplotype_db_text.text() + "\n")
                if (self.para_haplotype_hb_text.text() == '') or (self.para_haplotype_lb_text.text() == '') or (self.para_haplotype_pb_text.text() == '') or (self.para_haplotype_mb_text.text() == ''):
                    QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--hb\n--lb\n--pb\n--mb", QMessageBox.Ok)
                else:
                    para_haplotype_file.write("hb\t" + self.para_haplotype_hb_text.text() + "\n")
                    para_haplotype_file.write("lb\t" + self.para_haplotype_lb_text.text() + "\n")
                    para_haplotype_file.write("pb\t" + self.para_haplotype_pb_text.text() + "\n")
                    para_haplotype_file.write("mb\t" + self.para_haplotype_mb_text.text() + "\n")
                    QMessageBox.information(None, "Setup", "Saving arguments to file succeeded.", QMessageBox.Ok)



    def menu_parameters_afd_paraset(self):
        self.menu_parameters_widget = QtWidgets.QWidget(MainWindow)
        self.menu_parameters_widget.setObjectName("menu_parameters_widget")
        MainWindow.setCentralWidget(self.menu_parameters_widget)
        self.form_afd = QtWidgets.QFormLayout(self.menu_parameters_widget)
        self.para_afd_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_label.setText("Example:\n    bsatos2 afd --o afd --sd g --w 1000000 --pc P.counts --mc M.counts --pmc PM.counts --hap haplotype.block\n")
        self.para_afd_label.setFont(times_new_roman_10)
        self.para_afd_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_afd_label.setWordWrap(True)
        self.para_afd_options_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_options_label.setText("Options (Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly. Default values will apply to unseted arguments.)")
        self.para_afd_options_label.setWordWrap(True)
        self.para_afd_options_label.setFont(times_new_roman_10)
        self.para_afd_options_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_afd_o_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_o_label.setText("--o   STR")
        self.para_afd_o_label.setFont(times_new_roman_10)
        self.para_afd_o_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_afd_o_text.setMaximumWidth(1000)
        self.para_afd_o_text.setPlaceholderText("output dir name prefix [afd] [not required]")
        self.para_afd_o_text.setToolTip("output dir name prefix [afd] [not required]")
        self.para_afd_o_text.setFont(times_new_roman_10)
        self.para_afd_pc_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_pc_label.setText("--pc   FILE")
        self.para_afd_pc_label.setFont(times_new_roman_10)
        self.para_afd_pc_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_afd_pc_text.setMaximumWidth(1000)
        self.para_afd_pc_text.setPlaceholderText("read counts with different alleles from H & L pools in G1 type loci from prep module [required]")
        self.para_afd_pc_text.setToolTip("read counts with different alleles from H & L pools in G1 type loci from prep module [required]")
        self.para_afd_pc_text.setFont(times_new_roman_10)
        self.para_afd_mc_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_mc_label.setText("--mc   FILE")
        self.para_afd_mc_label.setFont(times_new_roman_10)
        self.para_afd_mc_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_afd_mc_text.setMaximumWidth(1000)
        self.para_afd_mc_text.setPlaceholderText("read counts with different alleles from H & L pools in G2 type loci from prep module [required]")
        self.para_afd_mc_text.setToolTip("read counts with different alleles from H & L pools in G2 type loci from prep module [required]")
        self.para_afd_mc_text.setFont(times_new_roman_10)
        self.para_afd_pmc_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_pmc_label.setText("--pmc   FILE")
        self.para_afd_pmc_label.setFont(times_new_roman_10)
        self.para_afd_pmc_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_afd_pmc_text.setMaximumWidth(1000)
        self.para_afd_pmc_text.setPlaceholderText("read counts with different alleles from H & L pools in G3 type loci from prep module [required]")
        self.para_afd_pmc_text.setToolTip("read counts with different alleles from H & L pools in G3 type loci from prep module [required]")
        self.para_afd_pmc_text.setFont(times_new_roman_10)
        self.para_afd_hap_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_hap_label.setText("--hap   FILE")
        self.para_afd_hap_label.setFont(times_new_roman_10)
        self.para_afd_hap_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_afd_hap_text.setMaximumWidth(1000)
        self.para_afd_hap_text.setPlaceholderText("merged, corrented and patched haplotype block file of two parents and two pools from haplotype module [required]")
        self.para_afd_hap_text.setToolTip("merged, corrented and patched haplotype block file of two parents and two pools from haplotype module [required]")
        self.para_afd_hap_text.setFont(times_new_roman_10)
        self.para_afd_statisticsoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_statisticsoptions_label.setText("Statistics Options")
        self.para_afd_statisticsoptions_label.setFont(times_new_roman_10)
        self.para_afd_statisticsoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_afd_sd_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_sd_label.setText("--sd   STR")
        self.para_afd_sd_label.setFont(times_new_roman_10)
        self.para_afd_sd_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_afd_sd_text.setMaximumWidth(1000)
        self.para_afd_sd_text.setPlaceholderText("statistic method: ed/g/abs [g] [not required]")
        self.para_afd_sd_text.setToolTip("statistic method: ed/g/abs [g] [not required]")
        self.para_afd_sd_text.setFont(times_new_roman_10)
        self.para_afd_w_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_w_label.setText("--w   INT")
        self.para_afd_w_label.setFont(times_new_roman_10)
        self.para_afd_w_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_afd_w_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_afd_w_text.setMaximumWidth(1000)
        self.para_afd_w_text.setPlaceholderText("sliding window size [1000000] [not required]")
        self.para_afd_w_text.setToolTip("sliding window size [1000000] [not required]")
        self.para_afd_w_text.setFont(times_new_roman_10)
        self.para_afd_fn_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_afd_fn_label.setText("--fn   INT")
        self.para_afd_fn_label.setFont(times_new_roman_10)
        self.para_afd_fn_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_afd_fn_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_afd_fn_text.setMaximumWidth(1000)
        self.para_afd_fn_text.setPlaceholderText("batches for smoothing; the smaller the faster, but more memory needed [10] [not required]")
        self.para_afd_fn_text.setToolTip("batches for smoothing; the smaller the faster, but more memory needed [10] [not required]")
        self.para_afd_fn_text.setFont(times_new_roman_10)
        self.para_afd_button = QtWidgets.QPushButton("OK", clicked = lambda:para_afd_pushbutton_slot())
        self.para_afd_button.setMaximumWidth(1120)
        self.para_afd_button.setMinimumWidth(1120)
        self.para_afd_button.setMaximumHeight(50)
        self.para_afd_button.setMinimumHeight(50)
        self.para_afd_button.setFont(times_new_roman_10)     # QtGui.QFont('Arial', 10, QtGui.QFont.Bold)
        self.form_afd.addRow(self.para_afd_label)
        self.form_afd.addRow(self.para_afd_options_label)
        self.form_afd.addRow(self.para_afd_o_label, self.para_afd_o_text)
        self.form_afd.addRow(self.para_afd_pc_label, self.para_afd_pc_text)
        self.form_afd.addRow(self.para_afd_mc_label, self.para_afd_mc_text)
        self.form_afd.addRow(self.para_afd_pmc_label, self.para_afd_pmc_text)
        self.form_afd.addRow(self.para_afd_hap_label, self.para_afd_hap_text)
        self.form_afd.addRow(self.para_afd_statisticsoptions_label)
        self.form_afd.addRow(self.para_afd_sd_label, self.para_afd_sd_text)
        self.form_afd.addRow(self.para_afd_w_label, self.para_afd_w_text)
        self.form_afd.addRow(self.para_afd_fn_label, self.para_afd_fn_text)
        self.form_afd.addRow(self.para_afd_button)
        self.menu_parameters_widget.setLayout(self.form_afd)

        def para_afd_pushbutton_slot():
            with open(os.path.join(sys.path[0], "Python", "src", "para_afd_file"), 'w') as para_afd_file:
                # QMessageBox.information(None, "Attention", "Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly.\nParameters unseted will apply to default values.", QMessageBox.Ok)
                if self.para_afd_o_text.text() == '':
                    para_afd_file.write("o\tafd\n")
                else:
                    para_afd_file.write("o\t" + self.para_afd_o_text.text() + "\n")
                if self.para_afd_sd_text.text() == '':
                    para_afd_file.write("sd\tg\n")
                else:
                    para_afd_file.write("sd\t" + self.para_afd_sd_text.text() + "\n")
                if self.para_afd_w_text.text() == '':
                    para_afd_file.write("w\t1000000\n")
                else:
                    para_afd_file.write("w\t" + self.para_afd_w_text.text() + "\n")
                if self.para_afd_fn_text.text() == '':
                    para_afd_file.write("fn\t10\n")
                else:
                    para_afd_file.write("fn\t" + self.para_afd_fn_text.text() + "\n")
                if (self.para_afd_pc_text.text() == '') or (self.para_afd_mc_text.text() == '') or (self.para_afd_pmc_text.text() == '') or (self.para_afd_hap_text.text() == ''):
                    QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--pc\n--mc\n--pmc\n--hap", QMessageBox.Ok)
                else:
                    para_afd_file.write("pc\t" + self.para_afd_pc_text.text() + "\n")
                    para_afd_file.write("mc\t" + self.para_afd_mc_text.text() + "\n")
                    para_afd_file.write("pmc\t" + self.para_afd_pmc_text.text() + "\n")
                    para_afd_file.write("hap\t" + self.para_afd_hap_text.text() + "\n")
                    QMessageBox.information(None, "Setup", "Saving arguments to file succeeded.", QMessageBox.Ok)



    def menu_parameters_polish_paraset(self):
        self.menu_parameters_widget = QtWidgets.QWidget(MainWindow)
        self.menu_parameters_widget.setObjectName("menu_parameters_widget")
        MainWindow.setCentralWidget(self.menu_parameters_widget)
        self.form_polish = QtWidgets.QFormLayout(self.menu_parameters_widget)
        self.para_polish_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_label.setText("Example:\n    bsatos2 polish --o polish --sd2 g --p P.AFD --m M.AFD --pm PM.AFD --hap haplotype.block\n")
        self.para_polish_label.setFont(times_new_roman_10)
        self.para_polish_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_polish_label.setWordWrap(True)
        self.para_polish_options_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_options_label.setText("Options (Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly. Default values will apply to unseted arguments.)")
        self.para_polish_options_label.setWordWrap(True)
        self.para_polish_options_label.setFont(times_new_roman_10)
        self.para_polish_options_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_polish_o_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_o_label.setText("--o   STR")
        self.para_polish_o_label.setFont(times_new_roman_10)
        self.para_polish_o_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_polish_o_text.setMaximumWidth(1000)
        self.para_polish_o_text.setPlaceholderText("output dir name prefix [polish] [not required]")
        self.para_polish_o_text.setToolTip("output dir name prefix [polish] [not required]")
        self.para_polish_o_text.setFont(times_new_roman_10)
        self.para_polish_p_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_p_label.setText("--p   FILE")
        self.para_polish_p_label.setFont(times_new_roman_10)
        self.para_polish_p_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_polish_p_text.setMaximumWidth(1000)
        self.para_polish_p_text.setPlaceholderText("smoothed curve base on P type loci across genome with haplotype information from afd module [required]")
        self.para_polish_p_text.setToolTip("smoothed curve base on P type loci across genome with haplotype information from afd module [required]")
        self.para_polish_p_text.setFont(times_new_roman_10)
        self.para_polish_m_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_m_label.setText("--m   FILE")
        self.para_polish_m_label.setFont(times_new_roman_10)
        self.para_polish_m_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_polish_m_text.setMaximumWidth(1000)
        self.para_polish_m_text.setPlaceholderText("smoothed curve base on M type loci across genome with haplotype information from afd module [required]")
        self.para_polish_m_text.setToolTip("smoothed curve base on M type loci across genome with haplotype information from afd module [required]")
        self.para_polish_m_text.setFont(times_new_roman_10)
        self.para_polish_pm_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_pm_label.setText("--pm   FILE")
        self.para_polish_pm_label.setFont(times_new_roman_10)
        self.para_polish_pm_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_polish_pm_text.setMaximumWidth(1000)
        self.para_polish_pm_text.setPlaceholderText("smoothed curve base on PM type loci across genome with haplotype information from afd module [required]")
        self.para_polish_pm_text.setToolTip("smoothed curve base on PM type loci across genome with haplotype information from afd module [required]")
        self.para_polish_pm_text.setFont(times_new_roman_10)
        self.para_polish_hap_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_hap_label.setText("--hap   FILE")
        self.para_polish_hap_label.setFont(times_new_roman_10)
        self.para_polish_hap_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_polish_hap_text.setMaximumWidth(1000)
        self.para_polish_hap_text.setPlaceholderText("merged haplotype block file from haplotype module [required]")
        self.para_polish_hap_text.setToolTip("merged haplotype block file from haplotype module [required]")
        self.para_polish_hap_text.setFont(times_new_roman_10)
        self.para_polish_fdr_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_fdr_label.setText("--fdr   FLOAT")
        self.para_polish_fdr_label.setFont(times_new_roman_10)
        self.para_polish_fdr_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_polish_fdr_text.setValidator(QtGui.QDoubleValidator(0.0, 1.0, 2))
        self.para_polish_fdr_text.setMaximumWidth(1000)
        self.para_polish_fdr_text.setPlaceholderText("FDR threshold for polishing [0.01] [not required]")
        self.para_polish_fdr_text.setToolTip("FDR threshold for polishing [0.01] [not required]")
        self.para_polish_fdr_text.setFont(times_new_roman_10)
        self.para_polish_fdr_text.setToolTip("FDR threshold for polishing [0.01] [not required]")
        self.para_polish_fdr_text.setFont(times_new_roman_10)
        self.para_polish_statisticsoptions_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_statisticsoptions_label.setText("Statistics Options")
        self.para_polish_statisticsoptions_label.setFont(times_new_roman_10)
        self.para_polish_statisticsoptions_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_polish_sd2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_sd2_label.setText("--sd2   STR")
        self.para_polish_sd2_label.setFont(times_new_roman_10)
        self.para_polish_sd2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_polish_sd2_text.setMaximumWidth(1000)
        self.para_polish_sd2_text.setPlaceholderText("statistic method: ed/g/abs [g] [not required]")
        self.para_polish_sd2_text.setToolTip("statistic method: ed/g/abs [g] [not required]")
        self.para_polish_sd2_text.setFont(times_new_roman_10)
        self.para_polish_w2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_w2_label.setText("--w2   INT")
        self.para_polish_w2_label.setFont(times_new_roman_10)
        self.para_polish_w2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_polish_w2_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_polish_w2_text.setMaximumWidth(1000)
        self.para_polish_w2_text.setPlaceholderText("sliding window size [1000000] [not required]")
        self.para_polish_w2_text.setToolTip("sliding window size [1000000] [not required]")
        self.para_polish_w2_text.setFont(times_new_roman_10)
        self.para_polish_fn2_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_polish_fn2_label.setText("--fn2   INT")
        self.para_polish_fn2_label.setFont(times_new_roman_10)
        self.para_polish_fn2_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_polish_fn2_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_polish_fn2_text.setMaximumWidth(1000)
        self.para_polish_fn2_text.setPlaceholderText("batches for smoothing; the smaller the faster but more memory needed [10] [not required]")
        self.para_polish_fn2_text.setToolTip("batches for smoothing; the smaller the faster but more memory needed [10] [not required]")
        self.para_polish_fn2_text.setFont(times_new_roman_10)
        self.para_polish_button = QtWidgets.QPushButton("OK", clicked = lambda:para_polish_pushbutton_slot())
        self.para_polish_button.setMaximumWidth(1120)
        self.para_polish_button.setMinimumWidth(1120)
        self.para_polish_button.setMaximumHeight(50)
        self.para_polish_button.setMinimumHeight(50)
        self.para_polish_button.setFont(times_new_roman_10)     # QtGui.QFont('Arial', 10, QtGui.QFont.Bold)
        self.form_polish.addRow(self.para_polish_label)
        self.form_polish.addRow(self.para_polish_options_label)
        self.form_polish.addRow(self.para_polish_o_label, self.para_polish_o_text)
        self.form_polish.addRow(self.para_polish_p_label, self.para_polish_p_text)
        self.form_polish.addRow(self.para_polish_m_label, self.para_polish_m_text)
        self.form_polish.addRow(self.para_polish_pm_label, self.para_polish_pm_text)
        self.form_polish.addRow(self.para_polish_hap_label, self.para_polish_hap_text)
        self.form_polish.addRow(self.para_polish_fdr_label, self.para_polish_fdr_text)
        self.form_polish.addRow(self.para_polish_statisticsoptions_label)
        self.form_polish.addRow(self.para_polish_sd2_label, self.para_polish_sd2_text)
        self.form_polish.addRow(self.para_polish_w2_label, self.para_polish_w2_text)
        self.form_polish.addRow(self.para_polish_fn2_label, self.para_polish_fn2_text)
        self.form_polish.addRow(self.para_polish_button)
        self.menu_parameters_widget.setLayout(self.form_polish)

        def para_polish_pushbutton_slot():
            with open(os.path.join(sys.path[0], "Python", "src", "para_polish_file"), 'w') as para_polish_file:
                # QMessageBox.information(None, "Attention", "Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly.\nParameters unseted will apply to default values.", QMessageBox.Ok)
                if self.para_polish_o_text.text() == '':
                    para_polish_file.write("o\tpolish\n")
                else:
                    para_polish_file.write("o\t" + self.para_polish_o_text.text() + "\n")
                if self.para_polish_fdr_text.text() == '':
                    para_polish_file.write("fdr\t0.01\n")
                else:
                    para_polish_file.write("fdr\t" + self.para_polish_fdr_text.text() + "\n")
                if self.para_polish_sd2_text.text() == '':
                    para_polish_file.write("sd2\tg\n")
                else:
                    para_polish_file.write("sd2\t" + self.para_polish_sd2_text.text() + "\n")
                if self.para_polish_w2_text.text() == '':
                    para_polish_file.write("w2\t1000000\n")
                else:
                    para_polish_file.write("w2\t" + self.para_polish_w2_text.text() + "\n")
                if self.para_polish_fn2_text.text() == '':
                    para_polish_file.write("fn2\t10\n")
                else:
                    para_polish_file.write("fn2\t" + self.para_polish_fn2_text.text() + "\n")
                if (self.para_polish_p_text.text() == '') or (self.para_polish_m_text.text() == '') or (self.para_polish_pm_text.text() == '') or (self.para_polish_hap_text.text() == ''):
                    QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--p\n--m\n--pm\n--hap", QMessageBox.Ok)
                else:
                    para_polish_file.write("p\t" + self.para_polish_p_text.text() + "\n")
                    para_polish_file.write("m\t" + self.para_polish_m_text.text() + "\n")
                    para_polish_file.write("pm\t" + self.para_polish_pm_text.text() + "\n")
                    para_polish_file.write("hap\t" + self.para_polish_hap_text.text() + "\n")
                    QMessageBox.information(None, "Setup", "Saving arguments to file succeeded.", QMessageBox.Ok)



    def menu_parameters_qtlpick_paraset(self):
        self.menu_parameters_widget = QtWidgets.QWidget(MainWindow)
        self.menu_parameters_widget.setObjectName("menu_parameters_widget")
        MainWindow.setCentralWidget(self.menu_parameters_widget)
        self.form_qtlpick = QtWidgets.QFormLayout(self.menu_parameters_widget)
        self.para_qtlpick_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_label.setText("Example:\n    bsatos2 qtl_pick --o qtl_pick --gtf Malus_domestica.gtf --gp P.polished.afd --gm M.polished.afd --gpm PM.polished.afd --snv snv.AT_multianno.txt --sv sv.AT_multianno.txt --hap haplotyle.block --win 1000000\n")
        self.para_qtlpick_label.setFont(times_new_roman_10)
        self.para_qtlpick_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_qtlpick_label.setWordWrap(True)
        self.para_qtlpick_options_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_options_label.setText("Options (Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly. Default values will apply to unseted arguments.)")
        self.para_qtlpick_options_label.setWordWrap(True)
        self.para_qtlpick_options_label.setFont(times_new_roman_10)
        self.para_qtlpick_options_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_qtlpick_o_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_o_label.setText("--o   STR")
        self.para_qtlpick_o_label.setFont(times_new_roman_10)
        self.para_qtlpick_o_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_o_text.setMaximumWidth(1000)
        self.para_qtlpick_o_text.setPlaceholderText("output dir name prefix [qtl_pick] [not required]")
        self.para_qtlpick_o_text.setToolTip("output dir name prefix [qtl_pick] [not required]")
        self.para_qtlpick_o_text.setFont(times_new_roman_10)
        self.para_qtlpick_db_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_db_label.setText("--db   STR")
        self.para_qtlpick_db_label.setFont(times_new_roman_10)
        self.para_qtlpick_db_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_db_text.setMaximumWidth(1000)
        self.para_qtlpick_db_text.setPlaceholderText("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_qtlpick_db_text.setToolTip("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_qtlpick_db_text.setFont(times_new_roman_10)
        self.para_qtlpick_gtf_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_gtf_label.setText("--gtf   FILE")
        self.para_qtlpick_gtf_label.setFont(times_new_roman_10)
        self.para_qtlpick_gtf_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_gtf_text.setMaximumWidth(1000)
        self.para_qtlpick_gtf_text.setPlaceholderText("GTF file of genes (GTF2 format; not required if GFF file is provided) [Malus_domestica.gtf]")
        self.para_qtlpick_gtf_text.setToolTip("GTF file of genes (GTF2 format; not required if GFF file is provided) [Malus_domestica.gtf]")
        self.para_qtlpick_gtf_text.setFont(times_new_roman_10)
        self.para_qtlpick_gff_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_gff_label.setText("--gff   FILE")
        self.para_qtlpick_gff_label.setFont(times_new_roman_10)
        self.para_qtlpick_gff_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_gff_text.setMaximumWidth(1000)
        self.para_qtlpick_gff_text.setPlaceholderText("GFF file of genes (GFF3 format; not required if GTF file is provided) [Malus_domestica.gff3]")
        self.para_qtlpick_gff_text.setToolTip("GFF file of genes (GFF3 format; not required if GTF file is provided) [Malus_domestica.gff3]")
        self.para_qtlpick_gff_text.setFont(times_new_roman_10)
        self.para_qtlpick_gp_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_gp_label.setText("--gp   FILE")
        self.para_qtlpick_gp_label.setFont(times_new_roman_10)
        self.para_qtlpick_gp_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_gp_text.setMaximumWidth(1000)
        self.para_qtlpick_gp_text.setPlaceholderText("smoothed curve base on P type loci across genome with haplotype information from polish module [required]")
        self.para_qtlpick_gp_text.setToolTip("smoothed curve base on P type loci across genome with haplotype information from polish module [required]")
        self.para_qtlpick_gp_text.setFont(times_new_roman_10)
        self.para_qtlpick_gm_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_gm_label.setText("--gm   FILE")
        self.para_qtlpick_gm_label.setFont(times_new_roman_10)
        self.para_qtlpick_gm_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_gm_text.setMaximumWidth(1000)
        self.para_qtlpick_gm_text.setPlaceholderText("smoothed curve base on M type loci across genome with haplotype information from polish module [required]")
        self.para_qtlpick_gm_text.setToolTip("smoothed curve base on M type loci across genome with haplotype information from polish module [required]")
        self.para_qtlpick_gm_text.setFont(times_new_roman_10)
        self.para_qtlpick_gpm_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_gpm_label.setText("--gpm   FILE")
        self.para_qtlpick_gpm_label.setFont(times_new_roman_10)
        self.para_qtlpick_gpm_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_gpm_text.setMaximumWidth(1000)
        self.para_qtlpick_gpm_text.setPlaceholderText("smoothed curve base on PM type loci across genome with haplotype information from polish module [required]")
        self.para_qtlpick_gpm_text.setToolTip("smoothed curve base on PM type loci across genome with haplotype information from polish module [required]")
        self.para_qtlpick_gpm_text.setFont(times_new_roman_10)
        self.para_qtlpick_snv_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_snv_label.setText("--snv   FILE")
        self.para_qtlpick_snv_label.setFont(times_new_roman_10)
        self.para_qtlpick_snv_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_snv_text.setMaximumWidth(1000)
        self.para_qtlpick_snv_text.setPlaceholderText("annotated SNVs file from prepar step [required]")
        self.para_qtlpick_snv_text.setToolTip("annotated SNVs file from prepar step [required]")
        self.para_qtlpick_snv_text.setFont(times_new_roman_10)
        self.para_qtlpick_sv_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_sv_label.setText("--sv   FILE")
        self.para_qtlpick_sv_label.setFont(times_new_roman_10)
        self.para_qtlpick_sv_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_sv_text.setMaximumWidth(1000)
        self.para_qtlpick_sv_text.setPlaceholderText("annotated SVs file from prepar step [required]")
        self.para_qtlpick_sv_text.setToolTip("annotated SVs file from prepar step [required]")
        self.para_qtlpick_sv_text.setFont(times_new_roman_10)
        self.para_qtlpick_hap_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_hap_label.setText("--hap   FILE")
        self.para_qtlpick_hap_label.setFont(times_new_roman_10)
        self.para_qtlpick_hap_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_hap_text.setMaximumWidth(1000)
        self.para_qtlpick_hap_text.setPlaceholderText("haplotye file from haplotype step [required]")
        self.para_qtlpick_hap_text.setToolTip("haplotye file from haplotype step [required]")
        self.para_qtlpick_hap_text.setFont(times_new_roman_10)
        self.para_qtlpick_q_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_q_label.setText("--q   INT")
        self.para_qtlpick_q_label.setFont(times_new_roman_10)
        self.para_qtlpick_q_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_q_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_qtlpick_q_text.setMaximumWidth(1000)
        self.para_qtlpick_q_text.setPlaceholderText("mininum phred-scaled quality score [10] [not required]")
        self.para_qtlpick_q_text.setToolTip("mininum phred-scaled quality score [10] [not required]")
        self.para_qtlpick_q_text.setFont(times_new_roman_10)
        self.para_qtlpick_pr_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_pr_label.setText("--pr   INT")
        self.para_qtlpick_pr_label.setFont(times_new_roman_10)
        self.para_qtlpick_pr_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_pr_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_qtlpick_pr_text.setMaximumWidth(1000)
        self.para_qtlpick_pr_text.setPlaceholderText("promoter region [2000] [not required]")
        self.para_qtlpick_pr_text.setToolTip("promoter region [2000] [not required]")
        self.para_qtlpick_pr_text.setFont(times_new_roman_10)
        self.para_qtlpick_per_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_per_label.setText("--per   FLOAT")
        self.para_qtlpick_per_label.setFont(times_new_roman_10)
        self.para_qtlpick_per_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_per_text.setValidator(QtGui.QDoubleValidator(0.0, 1.0, 2))
        self.para_qtlpick_per_text.setMaximumWidth(1000)
        self.para_qtlpick_per_text.setPlaceholderText("minimum percentage of blocks in QTLs region [0.7] [not required]")
        self.para_qtlpick_per_text.setToolTip("minimum percentage of blocks in QTLs region [0.7] [not required]")
        self.para_qtlpick_per_text.setFont(times_new_roman_10)
        self.para_qtlpick_bl_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_bl_label.setText("--bl   INT")
        self.para_qtlpick_bl_label.setFont(times_new_roman_10)
        self.para_qtlpick_bl_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_bl_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_qtlpick_bl_text.setMaximumWidth(1000)
        self.para_qtlpick_bl_text.setPlaceholderText("minimum enriched block length [2] [not required]")
        self.para_qtlpick_bl_text.setToolTip("minimum enriched block length [2] [not required]")
        self.para_qtlpick_bl_text.setFont(times_new_roman_10)
        self.para_qtlpick_fdr_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_fdr_label.setText("--fdr   FLOAT")
        self.para_qtlpick_fdr_label.setFont(times_new_roman_10)
        self.para_qtlpick_fdr_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_fdr_text.setValidator(QtGui.QDoubleValidator(0.0, 1.0, 2))
        self.para_qtlpick_fdr_text.setMaximumWidth(1000)
        self.para_qtlpick_fdr_text.setPlaceholderText("FDR threshold for QTLs [0.01] [not required]")
        self.para_qtlpick_fdr_text.setToolTip("FDR threshold for QTLs [0.01] [not required]")
        self.para_qtlpick_fdr_text.setFont(times_new_roman_10)
        self.para_qtlpick_win_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_qtlpick_win_label.setText("--win   INT")
        self.para_qtlpick_win_label.setFont(times_new_roman_10)
        self.para_qtlpick_win_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_qtlpick_win_text.setValidator(QtGui.QIntValidator(0, 100000000))
        self.para_qtlpick_win_text.setMaximumWidth(1000)
        self.para_qtlpick_win_text.setPlaceholderText("sliding window size [1000000] [not required]")
        self.para_qtlpick_win_text.setToolTip("sliding window size [1000000] [not required]")
        self.para_qtlpick_win_text.setFont(times_new_roman_10)
        self.para_qtlpick_button = QtWidgets.QPushButton("OK", clicked = lambda:para_qtlpick_pushbutton_slot())
        self.para_qtlpick_button.setMaximumWidth(1120)
        self.para_qtlpick_button.setMinimumWidth(1120)
        self.para_qtlpick_button.setMaximumHeight(50)
        self.para_qtlpick_button.setMinimumHeight(50)
        self.para_qtlpick_button.setFont(times_new_roman_10)     # QtGui.QFont('Arial', 10, QtGui.QFont.Bold)
        self.form_qtlpick.addRow(self.para_qtlpick_label)
        self.form_qtlpick.addRow(self.para_qtlpick_options_label)
        self.form_qtlpick.addRow(self.para_qtlpick_o_label, self.para_qtlpick_o_text)
        self.form_qtlpick.addRow(self.para_qtlpick_db_label, self.para_qtlpick_db_text)
        self.form_qtlpick.addRow(self.para_qtlpick_gtf_label, self.para_qtlpick_gtf_text)
        self.form_qtlpick.addRow(self.para_qtlpick_gff_label, self.para_qtlpick_gff_text)
        self.form_qtlpick.addRow(self.para_qtlpick_gp_label, self.para_qtlpick_gp_text)
        self.form_qtlpick.addRow(self.para_qtlpick_gm_label, self.para_qtlpick_gm_text)
        self.form_qtlpick.addRow(self.para_qtlpick_gpm_label, self.para_qtlpick_gpm_text)
        self.form_qtlpick.addRow(self.para_qtlpick_snv_label, self.para_qtlpick_snv_text)
        self.form_qtlpick.addRow(self.para_qtlpick_sv_label, self.para_qtlpick_sv_text)
        self.form_qtlpick.addRow(self.para_qtlpick_hap_label, self.para_qtlpick_hap_text)
        self.form_qtlpick.addRow(self.para_qtlpick_q_label, self.para_qtlpick_q_text)
        self.form_qtlpick.addRow(self.para_qtlpick_pr_label, self.para_qtlpick_pr_text)
        self.form_qtlpick.addRow(self.para_qtlpick_per_label, self.para_qtlpick_per_text)
        self.form_qtlpick.addRow(self.para_qtlpick_bl_label, self.para_qtlpick_bl_text)
        self.form_qtlpick.addRow(self.para_qtlpick_fdr_label, self.para_qtlpick_fdr_text)
        self.form_qtlpick.addRow(self.para_qtlpick_win_label, self.para_qtlpick_win_text)
        self.form_qtlpick.addRow(self.para_qtlpick_button)
        self.menu_parameters_widget.setLayout(self.form_qtlpick)

        def para_qtlpick_pushbutton_slot():
            with open(os.path.join(sys.path[0], "Python", "src", "para_qtlpick_file"), 'w') as para_qtlpick_file:
                # QMessageBox.information(None, "Attention", "Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly.\nParameters unseted will apply to default values.", QMessageBox.Ok)
                if self.para_qtlpick_o_text.text() == '':
                    para_qtlpick_file.write("o\tqtl_pick\n")
                else:
                    para_qtlpick_file.write("o\t" + self.para_qtlpick_o_text.text() + "\n")
                if self.para_qtlpick_q_text.text() == '':
                    para_qtlpick_file.write("q\t20\n")
                else:
                    para_qtlpick_file.write("q\t" + self.para_qtlpick_q_text.text() + "\n")
                if self.para_qtlpick_pr_text.text() == '':
                    para_qtlpick_file.write("pr\t2000\n")
                else:
                    para_qtlpick_file.write("pr\t" + self.para_qtlpick_pr_text.text() + "\n")
                if self.para_qtlpick_per_text.text() == '':
                    para_qtlpick_file.write("per\t0.7\n")
                else:
                    para_qtlpick_file.write("per\t" + self.para_qtlpick_per_text.text() + "\n")
                if self.para_qtlpick_bl_text.text() == '':
                    para_qtlpick_file.write("bl\t2\n")
                else:
                    para_qtlpick_file.write("bl\t" + self.para_qtlpick_bl_text.text() + "\n")
                if self.para_qtlpick_fdr_text.text() == '':
                    para_qtlpick_file.write("fdr\t0.01\n")
                else:
                    para_qtlpick_file.write("fdr\t" + self.para_qtlpick_fdr_text.text() + "\n")
                if self.para_qtlpick_win_text.text() == '':
                    para_qtlpick_file.write("win\t1000000\n")
                else:
                    para_qtlpick_file.write("win\t" + self.para_qtlpick_win_text.text() + "\n")
                if (self.para_qtlpick_gtf_text.text() != '') and (self.para_qtlpick_gff_text.text() != ''):
                    QMessageBox.warning(None, "Warning", "Please ONLY provide a GTF or GFF file.\nGFF file is not required if GTF file is provided.\nGTF file is not required if GFF file is provided.\nPlease input again.", QMessageBox.Ok)
                if (self.para_qtlpick_gtf_text.text() == '') and (self.para_qtlpick_gff_text.text() == ''):
                    para_qtlpick_file.write("gtf\tMalus_domestica.gtf\n")
                    # QMessageBox.warning(None, "Warning", "You didn't provide a GTF or GFF file.\nGFF file is not required if GTF file is provided.\nGTF file is not required if GFF file is provided.\nPlease input again.", QMessageBox.Ok)
                if (self.para_qtlpick_gtf_text.text() != '') and (self.para_qtlpick_gff_text.text() == ''):
                    para_qtlpick_file.write("gtf\t" + self.para_qtlpick_gtf_text.text() + "\n")
                if (self.para_qtlpick_gtf_text.text() == '') and (self.para_qtlpick_gff_text.text() != ''):
                    para_qtlpick_file.write("gff\t" + self.para_qtlpick_gff_text.text() + "\n")
                if (self.para_qtlpick_db_text.text() == ''):
                    para_qtlpick_file.write("db\tMalus_domestica\n")
                else:
                    para_qtlpick_file.write("db\t" + self.para_qtlpick_db_text.text() + "\n")
                if (self.para_qtlpick_gp_text.text() == '') or (self.para_qtlpick_gm_text.text() == '') or (self.para_qtlpick_gpm_text.text() == '') or (self.para_qtlpick_snv_text.text() == '') or (self.para_qtlpick_sv_text.text() == '') or (self.para_qtlpick_hap_text.text() == ''):
                    QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--gp\n--gm\n--gpm\n--v\n--sv\n--hap", QMessageBox.Ok)
                else:
                    para_qtlpick_file.write("gp\t" + self.para_qtlpick_gp_text.text() + "\n")
                    para_qtlpick_file.write("gm\t" + self.para_qtlpick_gm_text.text() + "\n")
                    para_qtlpick_file.write("gpm\t" + self.para_qtlpick_gpm_text.text() + "\n")
                    para_qtlpick_file.write("snv\t" + self.para_qtlpick_snv_text.text() + "\n")
                    para_qtlpick_file.write("sv\t" + self.para_qtlpick_sv_text.text() + "\n")
                    para_qtlpick_file.write("hap\t" + self.para_qtlpick_hap_text.text() + "\n")
                    QMessageBox.information(None, "Setup", "Saving arguments to file succeeded.", QMessageBox.Ok)



    def menu_parameters_igv_paraset(self):
        self.menu_parameters_widget = QtWidgets.QWidget(MainWindow)
        self.menu_parameters_widget.setObjectName("menu_parameters_widget")
        MainWindow.setCentralWidget(self.menu_parameters_widget)
        self.form_igv = QtWidgets.QFormLayout(self.menu_parameters_widget)
        self.para_igv_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_label.setText("Example:\n    bsatos2 igv --o igv --r Malus_domestica.fa --gtf Malus_domestica.gtf --prepar prepar_dir --hap haplotype_dir --qtl qtl_pick_dir --po polish_dir\n")
        self.para_igv_label.setFont(times_new_roman_10)
        self.para_igv_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_igv_label.setWordWrap(True)
        self.para_igv_options_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_options_label.setText("Options (Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly. Default values will apply to unseted arguments.)")
        self.para_igv_options_label.setWordWrap(True)
        self.para_igv_options_label.setFont(times_new_roman_10)
        self.para_igv_options_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.para_igv_o_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_o_label.setText("--o   STR")
        self.para_igv_o_label.setFont(times_new_roman_10)
        self.para_igv_o_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_igv_o_text.setMaximumWidth(1000)
        self.para_igv_o_text.setPlaceholderText("output dir name prefix [igv] [not required]")
        self.para_igv_o_text.setToolTip("output dir name prefix [igv] [not required]")
        self.para_igv_o_text.setFont(times_new_roman_10)
        self.para_igv_r_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_r_label.setText("--r   FILE")
        self.para_igv_r_label.setFont(times_new_roman_10)
        self.para_igv_r_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_igv_r_text.setMaximumWidth(1000)
        self.para_igv_r_text.setPlaceholderText("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_igv_r_text.setToolTip("reference genome (fasta format) [Malus_domestica.fa]")
        self.para_igv_r_text.setFont(times_new_roman_10)
        self.para_igv_db_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_db_label.setText("--db   STR")
        self.para_igv_db_label.setFont(times_new_roman_10)
        self.para_igv_db_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_igv_db_text.setMaximumWidth(1000)
        self.para_igv_db_text.setPlaceholderText("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_igv_db_text.setToolTip("database directory of .fa & GTF & GFF files [Malus_domestica]")
        self.para_igv_db_text.setFont(times_new_roman_10)
        self.para_igv_gtf_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_gtf_label.setText("--gtf   FILE")
        self.para_igv_gtf_label.setFont(times_new_roman_10)
        self.para_igv_gtf_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_igv_gtf_text.setMaximumWidth(1000)
        self.para_igv_gtf_text.setPlaceholderText("GTF file of genes (GTF2 format; Not required if GTF file is provided) [Malus_domestica.gtf]")
        self.para_igv_gtf_text.setToolTip("GTF file of genes (GTF2 format; Not required if GTF file is provided) [Malus_domestica.gtf]")
        self.para_igv_gtf_text.setFont(times_new_roman_10)
        self.para_igv_gff_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_gff_label.setText("--gff   FILE")
        self.para_igv_gff_label.setFont(times_new_roman_10)
        self.para_igv_gff_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_igv_gff_text.setMaximumWidth(1000)
        self.para_igv_gff_text.setPlaceholderText("GFF file of genes (GFF3 format; Not required if GTF file is provided) [Malus_domestica.gff3]")
        self.para_igv_gff_text.setToolTip("GFF file of genes (GFF3 format; Not required if GTF file is provided) [Malus_domestica.gff3]")
        self.para_igv_gff_text.setFont(times_new_roman_10)
        self.para_igv_prepar_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_prepar_label.setText("--prepar   DIR")
        self.para_igv_prepar_label.setFont(times_new_roman_10)
        self.para_igv_prepar_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_igv_prepar_text.setMaximumWidth(1000)
        self.para_igv_prepar_text.setPlaceholderText("result DIR from prepar module [prepar_dir] [required]")
        self.para_igv_prepar_text.setToolTip("result DIR from prepar module [prepar_dir] [required]")
        self.para_igv_prepar_text.setFont(times_new_roman_10)
        self.para_igv_hap_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_hap_label.setText("--hap   DIR")
        self.para_igv_hap_label.setFont(times_new_roman_10)
        self.para_igv_hap_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_igv_hap_text.setMaximumWidth(1000)
        self.para_igv_hap_text.setPlaceholderText("result DIR from haplotype module [haplotype_dir] [required]")
        self.para_igv_hap_text.setToolTip("result DIR from haplotype module [haplotype_dir] [required]")
        self.para_igv_hap_text.setFont(times_new_roman_10)
        self.para_igv_qtl_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_qtl_label.setText("--qtl   DIR")
        self.para_igv_qtl_label.setFont(times_new_roman_10)
        self.para_igv_qtl_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_igv_qtl_text.setMaximumWidth(1000)
        self.para_igv_qtl_text.setPlaceholderText("result DIR from qtl_pick module [qtl_pick_dir] [required]")
        self.para_igv_qtl_text.setToolTip("result DIR from qtl_pick module [qtl_pick_dir] [required]")
        self.para_igv_qtl_text.setFont(times_new_roman_10)
        self.para_igv_po_label = QtWidgets.QLabel(self.menu_parameters_widget)
        self.para_igv_po_label.setText("--po   DIR")
        self.para_igv_po_label.setFont(times_new_roman_10)
        self.para_igv_po_text = QtWidgets.QLineEdit(self.menu_parameters_widget)
        self.para_igv_po_text.setMaximumWidth(1000)
        self.para_igv_po_text.setPlaceholderText("result DIR from polish module [polish_dir] [required]")
        self.para_igv_po_text.setToolTip("result DIR from polish module [polish_dir] [required]")
        self.para_igv_po_text.setFont(times_new_roman_10)
        self.para_igv_button = QtWidgets.QPushButton("OK", clicked = lambda:para_igv_pushbutton_slot())
        self.para_igv_button.setMaximumWidth(1120)
        self.para_igv_button.setMinimumWidth(1120)
        self.para_igv_button.setMaximumHeight(50)
        self.para_igv_button.setMinimumHeight(50)
        self.para_igv_button.setFont(times_new_roman_10)     # QtGui.QFont('Arial', 10, QtGui.QFont.Bold)
        self.form_igv.addRow(self.para_igv_label)
        self.form_igv.addRow(self.para_igv_options_label)
        self.form_igv.addRow(self.para_igv_o_label, self.para_igv_o_text)
        self.form_igv.addRow(self.para_igv_r_label, self.para_igv_r_text)
        self.form_igv.addRow(self.para_igv_db_label, self.para_igv_db_text)
        self.form_igv.addRow(self.para_igv_gtf_label, self.para_igv_gtf_text)
        self.form_igv.addRow(self.para_igv_gff_label, self.para_igv_gff_text)
        self.form_igv.addRow(self.para_igv_prepar_label, self.para_igv_prepar_text)
        self.form_igv.addRow(self.para_igv_hap_label, self.para_igv_hap_text)
        self.form_igv.addRow(self.para_igv_qtl_label, self.para_igv_qtl_text)
        self.form_igv.addRow(self.para_igv_po_label, self.para_igv_po_text)
        self.form_igv.addRow(self.para_igv_button)
        self.menu_parameters_widget.setLayout(self.form_igv)

        def para_igv_pushbutton_slot():
            with open(os.path.join(sys.path[0], "Python", "src", "para_igv_file"), 'w') as para_igv_file:
                # QMessageBox.information(None, "Attention", "Please input the FULL PATH of the required input files, otherwise BSATOS2 may not work properly.\nParameters unseted will apply to default values.", QMessageBox.Ok)
                if self.para_igv_o_text.text() == '':
                    para_igv_file.write("o\tigv\n")
                else:
                    para_igv_file.write("o\t" + self.para_igv_o_text.text() + "\n")
                if (self.para_igv_db_text.text() == ''):
                    para_igv_file.write("db\tMalus_domestica\n")
                else:
                    para_igv_file.write("db\t" + self.para_igv_db_text.text() + "\n")
                if (self.para_igv_gtf_text.text() == '') and (self.para_igv_gff_text.text() == ''):
                    para_igv_file.write("gtf\tMalus_domestica.gtf\n")
                    # QMessageBox.warning(None, "Warning", "You didn't provide a GTF or GFF file.\nGFF file is not required if GTF file is provided.\nGTF file is not required if GFF file is provided.\nPlease input again.", QMessageBox.Ok)
                if (self.para_igv_gtf_text.text() != '') and (self.para_igv_gff_text.text() == ''):
                    para_igv_file.write("gtf\t" + self.para_igv_gtf_text.text() + "\n")
                if (self.para_igv_gtf_text.text() == '') and (self.para_igv_gff_text.text() != ''):
                    para_igv_file.write("gff\t" + self.para_igv_gff_text.text() + "\n")
                if self.para_igv_r_text.text() == '':
                    para_igv_file.write("r\tMalus_domestica.fa\n")
                else:
                    para_igv_file.write("r\t" + self.para_igv_r_text.text() + "\n")
                if self.para_igv_prepar_text.text() == '':
                    para_igv_file.write("prepar\tprepar_dir\n")
                else:
                    para_igv_file.write("prepar\t" + self.para_igv_prepar_text.text() + "\n")
                if self.para_igv_hap_text.text() == '':
                    para_igv_file.write("hap\thaplotype_dir\n")
                else:
                    para_igv_file.write("hap\t" + self.para_igv_hap_text.text() + "\n")
                if self.para_igv_qtl_text.text() == '':
                    para_igv_file.write("qtl\tqtl_pick_dir\n")
                else:
                    para_igv_file.write("qtl\t" + self.para_igv_qtl_text.text() + "\n")
                if self.para_igv_po_text.text() == '':
                    para_igv_file.write("po\tpolish_dir\n")
                else:
                    para_igv_file.write("po\t" + self.para_igv_po_text.text() + "\n")
                # QMessageBox.warning(None, "Warning", "Required arguments are all necessary to run BSATOS2.\nBSATOS2 may not work properly if the following arguments were left blank.\nPlease check it out.\n\n--r\n--prepar\n--hap\n--qtl\n--po", QMessageBox.Ok)
                QMessageBox.information(None, "Setup", "Saving arguments to file succeeded.", QMessageBox.Ok)



    def menu_file_opendir_slot(self):
        self.menu_file_opendir_widget = QtWidgets.QWidget(MainWindow)
        self.menu_file_opendir_widget.setObjectName("menu_file_opendir_widget")
        MainWindow.setCentralWidget(self.menu_file_opendir_widget)
        self.select_directory = QMessageBox.information(self.menu_file_opendir_widget, "Open files diretory", "Please select the files directory to open to run BSATOS2", QMessageBox.Open | QMessageBox.Cancel)
        if self.select_directory == QMessageBox.Open:                                                               # 判断是否单击了“Open”按钮
            self.select_open_directory = QFileDialog()                                                              # 文件对话框
            self.select_open_directory.setFileMode(QFileDialog.DirectoryOnly)                                       # 选择时只能选中文件夹
            self.select_open_directory.setDirectory(os.getcwd())                                                    # 设置初始路径
            if self.select_open_directory.exec_():                                                                  # 判断是否选择了文件夹
                self.directory_name_list = self.select_open_directory.selectedFiles()                               # 获取选择的多个文件夹或文件夹名字
                self.para_all_outdir_text.setText(self.directory_name_list[0])

            self.file_model = QtWidgets.QFileSystemModel()                                                          # 创建存储文件系统的模型
            self.file_model.setRootPath(self.select_open_directory)
            self.menu_file_opendir_treeview = QtWidgets.QTreeView(self.menu_file_opendir_widget)                    # 创建树视图
            self.menu_file_opendir_treeview.setGeometry(QtCore.QRect(0, 0, 1920, 1020))
            self.menu_file_opendir_treeview.setObjectName("menu_file_opendir_treeview")
            self.menu_file_opendir_treeview.setAlternatingRowColors(True)                                         # 设置行背景色(颜色)交替
            self.menu_file_opendir_treeview.header().setSortIndicator(0, QtCore.Qt.AscendingOrder)                  # 设置按第一列升序排序
            self.menu_file_opendir_treeview.header().setDefaultSectionSize(400)                                     # 设置默认标题列宽大小
            self.menu_file_opendir_treeview.header().resizeSection(0, 100)
            self.menu_file_opendir_treeview.setColumnWidth(0, 100)
            self.menu_file_opendir_treeview.header().setStretchLastSection(True)                                    # 设置最后一列自适应宽度
            self.menu_file_opendir_treeview.expandAll()                                                             # 设置展开所有节点
            self.menu_file_opendir_treeview.setAnimated(True)                                                       # 设置动画效果
            self.menu_file_opendir_treeview.setStyle(QStyleFactory.create('windows'))                               # 设置为虚线连接风格
            self.menu_file_opendir_treeview.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)                 # 设置垂直滚动条为按需显示
            self.menu_file_opendir_treeview.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)               # 设置水平滚动条为按需显示
            self.menu_file_opendir_treeview.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)           # 设置树节点为单选
            self.menu_file_opendir_treeview.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)            # 设置选中节点时为整行选中
            self.menu_file_opendir_treeview.setItemsExpandable(True)                                                # 设置是否可以展开项
            self.menu_file_opendir_treeview.setSortingEnabled(True)                                                 # 设置单击头部可排序
            self.menu_file_opendir_treeview.setWordWrap(True)                                                       # 设置文字自动换行
            self.menu_file_opendir_treeview.setHeaderHidden(False)                                                  # 设置不隐藏头部
            self.menu_file_opendir_treeview.setExpandsOnDoubleClick(True)                                           # 设置双击可以展开节点
            self.menu_file_opendir_treeview.header().setVisible(True)                                               # 设置显示头部
            self.menu_file_opendir_treeview.setModel(self.file_model)                                               # 为树视图创建数据模型
            self.menu_file_opendir_treeview.setRootIndex(self.file_model.index(self.select_open_directory))         # 设置根索引
            self.menu_file_opendir_treeview.show()



    def menu_run_all_slot(self):
        para_all_file_path = os.path.join(sys.path[0], "Python", "src", "para_all_file")
        if not os.path.exists(para_all_file_path):
            QMessageBox.warning(None, "Warning", "Arguments profile NOT found! \n Please setup arguments again in \'Parameter\' module.", QMessageBox.Ok)
        else:
            with open(para_all_file_path, mode = "r") as para_all_file:
                lines = para_all_file.readlines()
            for line in lines:
                line = line.strip()
                paras = line.split("\t")
                if (re.match(r'^o\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    o = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^r\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    r = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gtf\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gtf = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gff\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gff = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pf1\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pf1 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pf2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pf2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^mf1\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    mf1 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^mf2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    mf2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hf1\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hf1 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hf2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hf2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^lf1\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    lf1 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^lf2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    lf2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^mb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    mb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^lb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    lb = re.match('[ _.:/\\w\-]+', paras[1]).group()

            self.menu_run_all_widget = QtWidgets.QWidget(MainWindow)
            self.menu_run_all_widget.setObjectName("menu_run_all_widget")
            MainWindow.setCentralWidget(self.menu_run_all_widget)
            self.menu_run_all_label = QtWidgets.QLabel(self.menu_run_all_widget)                             # 标签控件
            self.menu_run_all_label.setGeometry(QtCore.QRect(600, 100, 1920, 800))                           # 设置矩形框大小，左上角坐标为（600，100），宽度为1920，高度为800
            self.menu_run_all_label.setText("Arguments profile found:\n\n\t" + "\t".join(lines) + "\nStarted running.")
            self.menu_run_all_label.setFont(times_new_roman_15)
            self.menu_run_all_label.setAlignment(QtCore.Qt.AlignLeft)                                        # 水平居中对齐
            self.menu_run_all_label.setWordWrap(True)                                                        # 文字自动换行

            bsatos_all_sh = os.path.join(os_path_dirname_file, "bsatos_all.sh")
            with open(bsatos_all_sh, "w") as f:
                if (('pb' and 'mb' and 'hb' and 'lb') in dir()) and ((pb and mb and hb and lb) != ''):
                    f.write(os_path_dirname_file + "/bsatos all --o " + o + " --r " + r + " --pb " + pb + " --mb " + mb + " --hb " + hb + " --lb " + lb)
                    # f.write("{0}/bsatos all --o {1} --r {2} --pb {3} --mb {4} --hb {5} --lb {6}".format(os_path_dirname_file, o, r, pb, mb, hb, lb))
                    if ('gtf' in dir()) and (gtf != ''):
                        f.write(" --gtf " + gtf)
                    if ('gff' in dir()) and (gff != ''):
                        f.write(" --gff " + gff)
                if (('pf1' and 'pf2' and 'mf1' and 'mf2' and 'hf1' and 'hf2' and 'lf1' and 'lf2') in dir()) and ((pf1 and pf2 and mf1 and mf2 and hf1 and hf2 and lf1 and lf2) != ''):
                    f.write(os_path_dirname_file + "/bsatos all --o " + o + " --r " + r + " --pf1 " + pf1 + " --pf2 " + pf2 + " --mf1 " + mf1 + " --mf2 " + mf2 + " --hf1 " + hf1 + " --hf2 " + hf2 + " --lf1 " + lf1 + " --lf2 " + lf2)
                    # f.write("{0}/bsatos all --o {1} --r {2} --pf1 {3} --pf2 {4} --mf1 {5} --mf2 {6} --hf1 {7} --hf2 {8} --lf1 {9} --lf2 {10}".format(os_path_dirname_file, o, r, pf1, pf2, mf1, mf2, hf1, hf2, lf1, lf2))
                    if ('gtf' in dir()) and (gtf != ''):
                        f.write(" --gtf " + gtf)
                    if ('gff' in dir()) and (gff != ''):
                        f.write(" --gff " + gff)
            run_all_cmd = ["sh", bsatos_all_sh, "&"]
            ret = subprocess.run(run_all_cmd, check = True, shell = True, stderr = subprocess.STDOUT, text = True)
            if ret.returncode == 0:
                QMessageBox.information(None, "Run ALL module", "Starting to run module ALL.", QMessageBox.Ok)
            else:
                QMessageBox.warning(None, "Warning", "Error: runtime errors occured.\nPlease check it out.", QMessageBox.Ok)



    def menu_run_prepar_slot(self):
        para_prepar_file_path = os.path.join(sys.path[0], "Python", "src", "para_prepar_file")
        if not os.path.exists(para_prepar_file_path):
            QMessageBox.warning(None, "Warning", "Arguments profile NOT found! \n Please setup arguments again in \'Parameter\' module.", QMessageBox.Ok)
        else:
            with open(para_prepar_file_path, mode = "r") as para_prepar_file:
                lines = para_prepar_file.readlines()
            for line in lines:
                line = line.strip()
                paras = line.split("\t")
                if (re.match(r'^o\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    o = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^r\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    r = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gtf\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gtf = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gff\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gff = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pf1\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pf1 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pf2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pf2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^mf1\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    mf1 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^mf2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    mf2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^mb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    mb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^aq\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    aq = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    aq = 10
                if (re.match(r'^vq\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    vq = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    vq = 10
                if (re.match(r'^mq\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    mq = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    mq = 10
                if (re.match(r'^sq\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    sq = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    sq = 10
                if (re.match(r'^t\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    t = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    t = 1
                if (re.match(r'^d\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    d = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    d = 10

            self.menu_run_prepar_widget = QtWidgets.QWidget(MainWindow)
            self.menu_run_prepar_widget.setObjectName("menu_run_prepar_widget")
            MainWindow.setCentralWidget(self.menu_run_prepar_widget)
            self.menu_run_prepar_label = QtWidgets.QLabel(self.menu_run_prepar_widget)                          # 标签控件
            self.menu_run_prepar_label.setGeometry(QtCore.QRect(600, 100, 1920, 800))                           # 设置矩形框大小，左上角坐标为（600，100），宽度为1920，高度为800
            self.menu_run_prepar_label.setText("Arguments profile found:\n\n\t" + "\t".join(lines) + "\nStarted running.")
            self.menu_run_prepar_label.setFont(times_new_roman_15)
            self.menu_run_prepar_label.setAlignment(QtCore.Qt.AlignLeft)                                        # 水平居中对齐
            self.menu_run_prepar_label.setWordWrap(True)                                                        # 文字自动换行

        bsatos_prepar_sh = os.path.join(os_path_dirname_file, "bsatos_prepar.sh")
        with open(bsatos_prepar_sh, "w") as f:
            if (('pb' and 'mb') in dir()) and ((pb and mb) != ''):
                f.write(os_path_dirname_file + "/bsatos prepar --o " + o + " --r " + r + " --pb " + pb + " --mb " + mb + " --aq " + str(aq) + " --vq " + str(vq) + " --sq " + str(sq) + " --t " + str(t) + " --d " + str(d))
                if ('gtf' in dir()) and (gtf != ''):
                    f.write(" --gtf " + gtf)
                if ('gff' in dir()) and (gff != ''):
                    f.write(" --gff " + gff)
            if (('pf1' and 'pf2' and 'mf1' and 'mf2') in dir()) and ((pf1 and pf2 and mf1 and mf2) != ''):
                f.write(os_path_dirname_file + "/bsatos prepar --o " + o + " --r " + r + "--pf1 " + pf1 + " --pf2 " + pf2 + " --mf1 " + mf1 + " --mf2 " + mf2 + " --aq " + str(aq) + " --vq " + str(vq) + " --sq " + str(sq) + " --t " + str(t) + " --d " + str(d))
                if ('gtf' in dir()) and (gtf != ''):
                    f.write(" --gtf " + gtf)
                if ('gff' in dir()) and (gff != ''):
                    f.write(" --gff " + gff)
        run_prepar_cmd = ["sh", bsatos_prepar_sh, "&"]
        ret = subprocess.run(run_prepar_cmd, check = True, shell = True, stderr = subprocess.STDOUT, text = True)
        if ret.returncode == 0:
            QMessageBox.information(None, "Run PREPAR module", "Starting to run module PREPAR.", QMessageBox.Ok)
        else:
            QMessageBox.warning(None, "Warning", "Error: runtime errors occured.\nPlease check it out.", QMessageBox.Ok)



    def menu_run_prep_slot(self):
        para_prep_file_path = os.path.join(sys.path[0], "Python", "src", "para_prep_file")
        if not os.path.exists(para_prep_file_path):
            QMessageBox.warning(None, "Warning", "Arguments profile NOT found! \n Please setup arguments again in \'Parameter\' module.", QMessageBox.Ok)
        else:
            with open(para_prep_file_path, mode = "r") as para_prep_file:
                lines = para_prep_file.readlines()
            for line in lines:
                line = line.strip()
                paras = line.split("\t")
                if (re.match(r'^o\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    o = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^r\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    r = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hf1\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hf1 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hf2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hf2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^lf1\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    lf1 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^lf2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    lf2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^lb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    lb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^aq2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    aq2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    aq2 = 10
                if (re.match(r'^sq2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    sq2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    sq2 = 10
                if (re.match(r'^gp\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gp = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gm\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gm = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gpm\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gpm = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^t2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    t2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    t2 = 1
                if (re.match(r'^d2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    d2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    d2 = 10
                if (re.match(r'^pn\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pn = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    pn = 3

            self.menu_run_prep_widget = QtWidgets.QWidget(MainWindow)
            self.menu_run_prep_widget.setObjectName("menu_run_prep_widget")
            MainWindow.setCentralWidget(self.menu_run_prep_widget)
            self.menu_run_prep_label = QtWidgets.QLabel(self.menu_run_prep_widget)                            # 标签控件
            self.menu_run_prep_label.setGeometry(QtCore.QRect(600, 100, 1920, 800))                           # 设置矩形框大小，左上角坐标为（600，100），宽度为1920，高度为800
            self.menu_run_prep_label.setText("Arguments profile found:\n\n\t" + "\t".join(lines) + "\nStarted running.")
            self.menu_run_prep_label.setFont(times_new_roman_15)
            self.menu_run_prep_label.setAlignment(QtCore.Qt.AlignLeft)                                        # 水平居中对齐
            self.menu_run_prep_label.setWordWrap(True)                                                        # 文字自动换行

        bsatos_prep_sh = os.path.join(os_path_dirname_file, "bsatos_prep.sh")
        with open(bsatos_prep_sh, "w") as f:
            if (('hb' and 'lb' and 'gp' and 'gm' and 'gpm') in dir()) and ((hb and lb and gp and gm and gpm) != ''):
                f.write(os_path_dirname_file + "/bsatos prep --o " + o + " --r " + r + " --hb " + hb + " --lb " + lb + " --gp " + "prepar_dir/P_M.p" + " --gm " + "prepar_dir/P_M.m" + " --gpm " + "prepar_dir/P_M.pm" + " --t2 " + str(t2) + " --aq2 " + str(aq2) + " --sq2 " + str(sq2) + " --d2 " + str(d2) + " --pn " + str(pn))
            if (('hf1' and 'hf2' and 'lf1' and 'lf2' and 'gp' and 'gm' and 'gpm') in dir()) and ((hf1 and hf2 and lf1 and lf2 and gp and gm and gpm) != ''):
                f.write(os_path_dirname_file + "/bsatos prep --o " + o + " --r " + r + " --hf1 " + hf1 + " --hf2 " + hf2 + " --lf1 " + lf1 + " --lf2 " + lf2 + " --gp " + "prepar_dir/P_M.p" + " --gm " + "prepar_dir/P_M.m" + " --gpm " + "prepar_dir/P_M.pm" + " --t2 " + str(t2) + " --aq2 " + str(aq2) + " --sq2 " + str(sq2) + " --d2 " + str(d2) + " --pn " + str(pn))
        run_prep_cmd = ["sh", bsatos_prep_sh, "&"]
        ret = subprocess.run(run_prep_cmd, check = True, shell = True, stderr = subprocess.STDOUT, text = True)
        if ret.returncode == 0:
            QMessageBox.information(None, "Run PREP module", "Starting to run module PREP.", QMessageBox.Ok)
        else:
            QMessageBox.warning(None, "Warning", "Error: runtime errors occured.\nPlease check it out.", QMessageBox.Ok)



    def menu_run_haplotype_slot(self):
        para_haplotype_file_path = os.path.join(sys.path[0], "Python", "src", "para_haplotype_file")
        if not os.path.exists(para_haplotype_file_path):
            QMessageBox.warning(None, "Warning", "Arguments profile NOT found! \n Please setup arguments again in \'Parameter\' module.", QMessageBox.Ok)
        else:
            with open(para_haplotype_file_path, mode = "r") as para_haplotype_file:
                lines = para_haplotype_file.readlines()
            for line in lines:
                line = line.strip()
                paras = line.split("\t")
                if (re.match(r'^o\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    o = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^r\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    r = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^lb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    lb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^mb\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    mb = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hapcut2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hapcut2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    hapcut2 = "NO"
                if (re.match(r'^dep\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    dep = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    dep = 10
                if (re.match(r'^aq3\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    aq3 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    aq3 = 10
                if (re.match(r'^vq2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    vq2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    vq2 = 10
                if (re.match(r'^var\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    var = re.match('[ _.:/\\w\-]+', paras[1]).group()

            self.menu_run_haplotype_widget = QtWidgets.QWidget(MainWindow)
            self.menu_run_haplotype_widget.setObjectName("menu_run_haplotype_widget")
            MainWindow.setCentralWidget(self.menu_run_haplotype_widget)
            self.menu_run_haplotype_label = QtWidgets.QLabel(self.menu_run_haplotype_widget)                       # 标签控件
            self.menu_run_haplotype_label.setGeometry(QtCore.QRect(600, 100, 1920, 800))                           # 设置矩形框大小，左上角坐标为（600，100），宽度为1920，高度为800
            self.menu_run_haplotype_label.setText("Arguments profile found:\n\n\t" + "\t".join(lines) + "\nStarted running.")
            self.menu_run_haplotype_label.setFont(times_new_roman_15)
            self.menu_run_haplotype_label.setAlignment(QtCore.Qt.AlignLeft)                                        # 水平居中对齐
            self.menu_run_haplotype_label.setWordWrap(True)                                                        # 文字自动换行

        bsatos_haplotype_sh = os.path.join(os_path_dirname_file, "bsatos_haplotype.sh")
        with open(bsatos_haplotype_sh, "w") as f:
            if (('hb' and 'lb' and 'pb' and 'mb') in dir()) and ((hb and lb and pb and mb) != ''):
                f.write(os_path_dirname_file + "/bsatos haplotype --o " + o + " --r " + r + " --hb " + hb + " --lb " + lb + " --pb " + pb + " --mb " + mb + " --t3 " + str(t3) + " --hapcut2 " + hapcut2 + " --dep " + str(dep) + " --aq3 " + str(aq3) + " --vq2 " + str(vq2) + "--var " + "prepar_dir/P_M.snv")
        run_haplotype_cmd = ["sh", bsatos_haplotype_sh, "&"]
        ret = subprocess.run(run_haplotype_cmd, check = True, shell = True, stderr = subprocess.STDOUT, text = True)
        if ret.returncode == 0:
            QMessageBox.information(None, "Run HAPLOTYPE module", "Starting to run module HAPLOTYPE.", QMessageBox.Ok)
        else:
            QMessageBox.warning(None, "Warning", "Error: runtime errors occured.\nPlease check it out.", QMessageBox.Ok)



    def menu_run_afd_slot(self):
        para_afd_file_path = os.path.join(sys.path[0], "Python", "src", "para_afd_file")
        if not os.path.exists(para_afd_file_path):
            QMessageBox.warning(None, "Warning", "Arguments profile NOT found! \n Please setup arguments again in \'Parameter\' module.", QMessageBox.Ok)
        else:
            with open(para_afd_file_path, mode = "r") as para_afd_file:
                lines = para_afd_file.readlines()
            for line in lines:
                line = line.strip()
                paras = line.split("\t")
                if (re.match(r'^o\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    o = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pc\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pc = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^mc\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    mc = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pmc\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pmc = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hap\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hap = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^sd\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    sd = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    sd = "g"
                if (re.match(r'^w\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    w = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    w = 1000000
                if (re.match(r'^th\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    th = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    th = "N"
                if (re.match(r'^m\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    m = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    m = "N"

            self.menu_run_afd_widget = QtWidgets.QWidget(MainWindow)
            self.menu_run_afd_widget.setObjectName("menu_run_afd_widget")
            MainWindow.setCentralWidget(self.menu_run_afd_widget)
            self.menu_run_afd_label = QtWidgets.QLabel(self.menu_run_afd_widget)                             # 标签控件
            self.menu_run_afd_label.setGeometry(QtCore.QRect(600, 100, 1920, 800))                           # 设置矩形框大小，左上角坐标为（600，100），宽度为1920，高度为800
            self.menu_run_afd_label.setText("Arguments profile found:\n\n\t" + "\t".join(lines) + "\nStarted running.")
            self.menu_run_afd_label.setFont(times_new_roman_15)
            self.menu_run_afd_label.setAlignment(QtCore.Qt.AlignLeft)                                        # 水平居中对齐
            self.menu_run_afd_label.setWordWrap(True)                                                        # 文字自动换行

        bsatos_afd_sh = os.path.join(os_path_dirname_file, "bsatos_afd.sh")
        with open(bsatos_afd_sh, "w") as f:
            if (('pc' and 'mc' and 'pmc' and 'hap') in dir()) and ((pc and mc and pmc and hap) != ''):
                f.write(os_path_dirname_file + "/bsatos afd --o " + o + " --pc " + "prepar_dir/P.counts" + " --mc " + "prepar_dir/M.counts" + " --pmc " + "prepar_dir/PM.counts" + " --hap " + "haplotype_dir/haplotype.block" + " --sd " + sd + " --w " + str(w) + " --th " + th + " --m " + m)
        run_afd_cmd = ["sh", bsatos_afd_sh, "&"]
        ret = subprocess.run(run_afd_cmd, check = True, shell = True, stderr = subprocess.STDOUT, text = True)
        if ret.returncode == 0:
            QMessageBox.information(None, "Run AFD module", "Starting to run module AFD.", QMessageBox.Ok)
        else:
            QMessageBox.warning(None, "Warning", "Error: runtime errors occured.\nPlease check it out.", QMessageBox.Ok)



    def menu_run_polish_slot(self):
        para_polish_file_path = os.path.join(sys.path[0], "Python", "src", "para_polish_file")
        if not os.path.exists(para_polish_file_path):
            QMessageBox.warning(None, "Warning", "Arguments profile NOT found! \n Please setup arguments again in \'Parameter\' module.", QMessageBox.Ok)
        else:
            with open(para_polish_file_path, mode = "r") as para_polish_file:
                lines = para_polish_file.readlines()
            for line in lines:
                line = line.strip()
                paras = line.split("\t")
                if (re.match(r'^o\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    o = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^p\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    p = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^m\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    m = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^pm\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pm = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hap\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hap = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^sd2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    sd2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    sd2 = "g"
                if (re.match(r'^w2\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    w2 = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    w2 = 1000000
                if (re.match(r'^th\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    th = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    th = "N"
                if (re.match(r'^m\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    m = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    m = "N"

            self.menu_run_polish_widget = QtWidgets.QWidget(MainWindow)
            self.menu_run_polish_widget.setObjectName("menu_run_polish_widget")
            MainWindow.setCentralWidget(self.menu_run_polish_widget)
            self.menu_run_polish_label = QtWidgets.QLabel(self.menu_run_polish_widget)                          # 标签控件
            self.menu_run_polish_label.setGeometry(QtCore.QRect(600, 100, 1920, 800))                           # 设置矩形框大小，左上角坐标为（600，100），宽度为1920，高度为800
            self.menu_run_polish_label.setText("Arguments profile found:\n\n\t" + "\t".join(lines) + "\nStarted running.")
            self.menu_run_polish_label.setFont(times_new_roman_15)
            self.menu_run_polish_label.setAlignment(QtCore.Qt.AlignLeft)                                        # 水平居中对齐
            self.menu_run_polish_label.setWordWrap(True)                                                        # 文字自动换行

        bsatos_polish_sh = os.path.join(os_path_dirname_file, "bsatos_polish.sh")
        with open(bsatos_polish_sh, "w") as f:
            if (('p' and 'm' and 'pm' and 'hap') in dir()) and ((p and p and pm and hap) != ''):
                f.write(os_path_dirname_file + "/bsatos polish --o " + o + " --p " + "afd_dir/P.AFD" + " --m " + "afd_dir/M.AFD" + " --pm " + "afd_dir/PM.AFD" + " --hap " + "haplotype_dir/haplotype.block" + " --sd2 " + sd2 + " --w2 " + str(w2) + " --th " + th + " --m " + m)
        run_polish_cmd = ["sh", bsatos_polish_sh, "&"]
        ret = subprocess.run(run_polish_cmd, check = True, shell = True, stderr = subprocess.STDOUT, text = True)
        if ret.returncode == 0:
            QMessageBox.information(None, "Run POLISH module", "Starting to run module POLISH.", QMessageBox.Ok)
        else:
            QMessageBox.warning(None, "Warning", "Error: runtime errors occured.\nPlease check it out.", QMessageBox.Ok)



    def menu_run_qtlpick_slot(self):
        para_qtlpick_file_path = os.path.join(sys.path[0], "Python", "src", "para_qtlpick_file")
        if not os.path.exists(para_qtlpick_file_path):
            QMessageBox.warning(None, "Warning", "Arguments profile NOT found! \n Please setup arguments again in \'Parameter\' module.", QMessageBox.Ok)
        else:
            with open(para_qtlpick_file_path, mode = "r") as para_qtlpick_file:
                lines = para_qtlpick_file.readlines()
            for line in lines:
                line = line.strip()
                paras = line.split("\t")
                if (re.match(r'^o\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    o = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gtf\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gtf = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gff\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gff = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gp\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gp = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gm\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gm = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gpm\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gpm = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hap\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hap = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^snv\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    snv = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^sv\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    sv = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^q\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    q = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    q = 10
                if (re.match(r'^pr\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    pr = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    pr = 2000
                if (re.match(r'^win\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    win = re.match('[ _.:/\\w\-]+', paras[1]).group()
                else:
                    win = 1000000

            self.menu_run_qtlpick_widget = QtWidgets.QWidget(MainWindow)
            self.menu_run_qtlpick_widget.setObjectName("menu_run_qtlpick_widget")
            MainWindow.setCentralWidget(self.menu_run_qtlpick_widget)
            self.menu_run_qtlpick_label = QtWidgets.QLabel(self.menu_run_qtlpick_widget)                         # 标签控件
            self.menu_run_qtlpick_label.setGeometry(QtCore.QRect(600, 100, 1920, 800))                           # 设置矩形框大小，左上角坐标为（600，100），宽度为1920，高度为800
            self.menu_run_qtlpick_label.setText("Arguments profile found:\n\n\t" + "\t".join(lines) + "\nStarted running.")
            self.menu_run_qtlpick_label.setFont(times_new_roman_15)
            self.menu_run_qtlpick_label.setAlignment(QtCore.Qt.AlignLeft)                                        # 水平居中对齐
            self.menu_run_qtlpick_label.setWordWrap(True)                                                        # 文字自动换行

        bsatos_qtlpick_sh = os.path.join(os_path_dirname_file, "bsatos_qtlpick.sh")
        with open(bsatos_qtlpick_sh, "w") as f:
            if (('gp' and 'gm' and 'gpm' and 'hap' and 'snv' and 'sv') in dir()) and ((gp and gm and gpm and hap and snv and sv) != ''):
                f.write(os_path_dirname_file + "/bsatos qtl_pick --o " + o + " --gp " + "polish_dir/P.polished.afd" + " --gm " + "polish_dir/M.polished.afd" + " --gpm " + "polish_dir/PM.polished.afd" + " --hap " + "haplotype_dir/haplotype.block" + " --snv " + "prepar_dir/anno/snv.AT_multianno.txt" + " --sv " + "prepar_dir/anno/sv.AT_multianno.txt" + " --q " + str(q) + " --pr " + str(pr) + " --win " + str(win))
                if ('gtf' in dir()) and (gtf != ''):
                    f.write(" --gtf " + gtf)
                if ('gff' in dir()) and (gff != ''):
                    f.write(" --gff " + gff)
        run_qtlpick_cmd = ["sh", bsatos_qtlpick_sh, "&"]
        ret = subprocess.run(run_qtlpick_cmd, check = True, shell = True, stderr = subprocess.STDOUT, text = True)
        if ret.returncode == 0:
            QMessageBox.information(None, "Run QTL_PICK module", "Starting to run module QTL_PICK.", QMessageBox.Ok)
        else:
            QMessageBox.warning(None, "Warning", "Error: runtime errors occured.\nPlease check it out.", QMessageBox.Ok)



    def menu_run_igv_slot(self):
        para_igv_file_path = os.path.join(sys.path[0], "Python", "src", "para_igv_file")
        if not os.path.exists(para_igv_file_path):
            QMessageBox.warning(None, "Warning", "Arguments profile NOT found! \n Please setup arguments again in \'Parameter\' module.", QMessageBox.Ok)
        else:
            with open(para_igv_file_path, mode = "r") as para_igv_file:
                lines = para_igv_file.readlines()
            for line in lines:
                line = line.strip()
                paras = line.split("\t")
                if (re.match(r'^o\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    o = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^r\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    r = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gtf\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gtf = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^gff\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    gff = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^prepar\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    prepar = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^hap\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    hap = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^qtl\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    qtl = re.match('[ _.:/\\w\-]+', paras[1]).group()
                if (re.match(r'^po\b', paras[0]) and re.match('[ _.:/\\w\-]+', paras[1])):
                    po = re.match('[ _.:/\\w\-]+', paras[1]).group()

            self.menu_run_igv_widget = QtWidgets.QWidget(MainWindow)
            self.menu_run_igv_widget.setObjectName("menu_run_igv_widget")
            MainWindow.setCentralWidget(self.menu_run_igv_widget)
            self.menu_run_igv_label = QtWidgets.QLabel(self.menu_run_igv_widget)                             # 标签控件
            self.menu_run_igv_label.setGeometry(QtCore.QRect(600, 100, 1920, 800))                           # 设置矩形框大小，左上角坐标为（600，100），宽度为1920，高度为800
            self.menu_run_igv_label.setText("Arguments profile found:\n\n\t" + "\t".join(lines) + "\nStarted running.")
            self.menu_run_igv_label.setFont(times_new_roman_15)
            self.menu_run_igv_label.setAlignment(QtCore.Qt.AlignLeft)                                        # 水平居中对齐
            self.menu_run_igv_label.setWordWrap(True)                                                        # 文字自动换行

        bsatos_igv_sh = os.path.join(os_path_dirname_file, "bsatos_igv.sh")
        with open(bsatos_igv_sh, "w") as f:
            f.write(os_path_dirname_file + "/bsatos igv --o " + o + " --r " + r + " --prepar " + prepar + " --hap " + hap + " --qtl " + qtl + " --po " + po)
            if ('gtf' in dir()) and (gtf != ''):
                f.write(" --gtf " + gtf)
            if ('gff' in dir()) and (gff != ''):
                f.write(" --gff " + gff)
        run_igv_cmd = ["sh", bsatos_igv_sh, "&"]
        ret = subprocess.run(run_igv_cmd, check = True, shell = True, stderr = subprocess.STDOUT, text = True)
        if ret.returncode == 0:
            QMessageBox.information(None, "Run IGV module", "Starting to run module IGV.", QMessageBox.Ok)
        else:
            QMessageBox.warning(None, "Warning", "Error: runtime errors occured.\nPlease check it out.", QMessageBox.Ok)



    def menu_help_info_slot(self):
        self.menu_help_help_widget = QtWidgets.QWidget(MainWindow)
        self.menu_help_help_widget.setObjectName("menu_help_help_widget")
        MainWindow.setCentralWidget(self.menu_help_help_widget)
        self.menu_help_help_label = QtWidgets.QLabel(self.menu_help_help_widget)
        self.menu_help_help_label.setGeometry(QtCore.QRect(0, 0, 1920, 1080))
        self.menu_help_help_label.setText(ufunc.help_info)
        self.menu_help_help_label.setFont(times_new_roman_10)
        self.menu_help_help_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
        self.menu_help_help_label.setWordWrap(True)


    def menu_help_notes_slot(self):
        # self.centralwidget.hide()
        self.menu_help_notes_widget = QtWidgets.QWidget(MainWindow)
        self.menu_help_notes_widget.setObjectName("menu_help_notes_widget")
        MainWindow.setCentralWidget(self.menu_help_notes_widget)
        self.menu_help_notes_textedit = QtWidgets.QTextEdit(self.menu_help_notes_widget)
        self.menu_help_notes_textedit.setReadOnly(True)
        self.menu_help_notes_textedit.setFont(times_new_roman_10)
        # self.menu_help_notes_textedit.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.menu_help_notes_textedit.setGeometry(QtCore.QRect(0, 0, 1915, 1060))
        self.menu_help_notes_textedit.setHtml(ufunc.notes_info_html)
        self.menu_help_notes_widget.show()


    def menu_help_about_slot(self):
        self.menu_help_about_widget = QtWidgets.QWidget(MainWindow)
        self.menu_help_about_widget.setObjectName("menu_help_about_widget")
        MainWindow.setCentralWidget(self.menu_help_about_widget)
        self.menu_help_about_label = QtWidgets.QLabel(self.menu_help_about_widget)
        self.menu_help_about_label.setGeometry(QtCore.QRect(0, 0, 1920, 1080))
        self.menu_help_about_label.setText(ufunc.about_info)
        self.menu_help_about_label.setFont(times_new_roman_15)
        self.menu_help_about_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.menu_help_about_label.setWordWrap(True)


    def menu_exit_script_slot(self):
        self.exit_info = QMessageBox.information(None, "Exit", "Exiting right now.\nSee you next time.\nBest wishes to you.", QMessageBox.Ok)
        sys.exit()


    def setStatusBar_menu_module_all(self):
        self.statusbar_label.setText(self.menu_module_all.text())
    def setStatusBar_menu_module_prepar(self):
        self.statusbar_label.setText(self.menu_module_prepar.text())
    def setStatusBar_menu_module_prep(self):
        self.statusbar_label.setText(self.menu_module_prep.text())
    def setStatusBar_menu_module_haplotype(self):
        self.statusbar_label.setText(self.menu_module_haplotype.text())
    def setStatusBar_menu_module_afd(self):
        self.statusbar_label.setText(self.menu_module_afd.text())
    def setStatusBar_menu_module_polish(self):
        self.statusbar_label.setText(self.menu_module_polish.text())
    def setStatusBar_menu_module_qtlpick(self):
        self.statusbar_label.setText(self.menu_module_qtlpick.text())
    def setStatusBar_menu_module_igv(self):
        self.statusbar_label.setText(self.menu_module_igv.text())


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "BSATOS2"))



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    ui.welcome_UI()
    MainWindow.show()
    sys.exit(app.exec_())
