#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

parser = argparse.ArgumentParser(description = "integrate variant infomation with QTLs")
parser.add_argument("-qtl", "--qtl", type = argparse.FileType("r"), required = True, help = "QTL input file of qtl_pick_dir")
parser.add_argument("-out", "--out", type = str, required = True, help = "QTL output file of qtl_pick_dir")
parser.add_argument("-int", "--int", type = int, required = True, help = "interval length to be specified of QTL")
args = parser.parse_args()  # 解析所有参数

out = open(args.out, "w")
for qtlline in args.qtl:
    # if qtlline.startswith("#Origin"):
    if "Accession" in qtlline:
        out.write(qtlline)
    else:
        # 0-Origin 1-Chrom 2-QTL_Start 3-QTL_End 4-Peak_Pos 5-Peak 6-Accession
        qtl_line = qtlline.split("\t")
        qtl_start = int(qtl_line[2])
        qtl_end = int(qtl_line[3])
        if (qtl_end - qtl_start) >= (args.int * 1000):
            out.write(qtlline)
out.close()
