#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys
import argparse

parser = argparse.ArgumentParser(description = "filter SVs based on the QTL type", usage = "python filter_sv_based_on_qtl.py --i <QTL type> --d <promoter region> <annotated SNVs file from ANNOVAR>")
parser.add_argument("-d", "--d", type = int, default = 2000, help = "promoter region")
parser.add_argument("-i", "--i", type = str, required = True, help = "QTL type")
args = parser.parse_args()  # 解析所有参数

print("#Chr\tStart\tEnd\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tOtherinfo\tQual\t-\tChr\tStart\t-\tRef\tAlt\tQual\t-\tInfo\tFormat\tPollen Parent\tMaternal Parent\tChr\tEnd\tRef\tAlt\tP_HAP1\tP_HAP2\tP_NAME\tP_EN\tP_G\tM_HAP1\tM_HAP2\tM_NAME\tM_EN\tM_G")

for vcfline in sys.stdin.readlines():
    line = vcfline.strip().split("\t")
    # 0-Chr03  1-20440548  2-20441352  3-0  4--  5-upstream  6-gene:MD03G1163900  7-dist=451  8-.  9-.  10-0.5  11-3726  12-.  13-Chr03  14-20440548  15-DEL00042776  16-A  17-<DEL>  18-3726  19-PASS
    # 20-PRECISE;SVTYPE=DEL;SVMETHOD=EMBL.DELLYv1.1.6;END=20441352;PE=46;MAPQ=60;CT=3to5;CIPOS=-9,9;CIEND=-9,9;SRMAPQ=60;INSLEN=0;HOMLEN=8;SR=17;SRQ=0.983957;CONSENSUS=TACAATCTATGAATTTCTGAT;CE=1.85025
    # 21-GT:GL:GQ:FT:RCL:RC:RCR:RDCN:DR:DV:RR:RV  22-0/1:-41.7728,0,-47.272:10000:PASS:19745:19901:18833:1:41:41:15:13  23-0/1:-46.3787,0,-25.8789:10000:PASS:16267:19399:18816:1:5:6:8:13
    if line[19] == "PASS":
        if re.findall("(\d/\d):", line[22]):
            parent1 = re.findall("(\d/\d):", line[22])[0]
        if re.findall("(\d/\d):", line[23]):
            parent2 = re.findall("(\d/\d):", line[23])[0]
        if args.i == "P":
            if (parent1 == "0/1") and ((parent2 == "0/0") or (parent2 == "1/1")):
                if re.findall("dist=(\d+);dist=(\d+)", vcfline):
                    dist_list = re.findall("dist=(\d+);dist=(\d+)", vcfline)
                    upstream_dist, downstream_dist = int(dist_list[0][0]), int(dist_list[0][1])
                    if (upstream_dist > args.d) and (downstream_dist > args.d):
                        continue
                if not (("\tsynonymous SNV" in vcfline) or ("intronic" in vcfline) or ("downstream" in vcfline)):
                    print(vcfline.strip() + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t")
        if args.i == "M":
            if (parent2 == "0/1") and ((parent1 == "0/0") or (parent1 == "1/1")):
                if re.findall("dist=(\d+);dist=(\d+)", vcfline):
                    dist_list = re.findall("dist=(\d+);dist=(\d+)", vcfline)
                    upstream_dist, downstream_dist = int(dist_list[0][0]), int(dist_list[0][1])
                    if (upstream_dist > args.d) and (downstream_dist > args.d):
                        continue
                if not (("\tsynonymous SNV" in vcfline) or ("intronic" in vcfline) or ("downstream" in vcfline)):
                    print(vcfline.strip() + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t")
        if args.i == "H":
            if (parent1 == "0/1") and (parent2 == "0/1"):
                if re.findall("dist=(\d+);dist=(\d+)", vcfline):
                    dist_list = re.findall("dist=(\d+);dist=(\d+)", vcfline)
                    upstream_dist, downstream_dist = int(dist_list[0][0]), int(dist_list[0][1])
                    if (upstream_dist > args.d) and (downstream_dist > args.d):
                        continue
                if not (("\tsynonymous SNV" in vcfline) or ("intronic" in vcfline) or ("downstream" in vcfline)):
                    print(vcfline.strip() + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t")
