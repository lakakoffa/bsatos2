#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtWidgets

def setupUi(self, MainWindow):
    MainWindow.setObjectName("MainWindow")
    MainWindow.resize(1920, 1080)
    self.centralwidget = QtWidgets.QWidget(MainWindow)
    self.centralwidget.setObjectName("centralwidget")
    MainWindow.setCentralWidget(self.centralwidget)
    _translate = QtCore.QCoreApplication.translate

    self.menubar = QtWidgets.QMenuBar(MainWindow)                                               # 添加菜单栏
    self.menubar.setGeometry(QtCore.QRect(0, 0, 80, 10))
    self.menubar.setObjectName("menubar")
    # self.menu_module = QtWidgets.QMenu(self.menubar)                                            # 添加“模块”菜单
    # self.menu_module.setObjectName("menu_module")
    # self.menu_module.setTitle(_translate("MainWindow", "Module"))
    self.menu_functions = QtWidgets.QMenu(self.menubar)                                         # 添加“功能”菜单
    self.menu_functions.setObjectName("menu_functions")
    self.menu_functions.setTitle(_translate("MainWindow", "Function"))
    self.menu_parameters = QtWidgets.QMenu(self.menubar)                                        # 添加“参数”菜单
    self.menu_parameters.setObjectName("menu_parameters")
    self.menu_parameters.setTitle(_translate("MainWindow", "Parameter"))
    self.menu_file = QtWidgets.QMenu(self.menubar)                                              # 添加“文件”菜单
    self.menu_file.setObjectName("menu_file")
    self.menu_file.setTitle(_translate("MainWindow", "File"))
    self.menu_run = QtWidgets.QMenu(self.menubar)                                               # 添加“运行”菜单
    self.menu_run.setObjectName("menu_run")
    self.menu_run.setTitle(_translate("MainWindow", "Run"))
    # self.menu_review = QtWidgets.QMenu(self.menubar)                                            # 添加“查看”菜单
    # self.menu_review.setObjectName("menu_review")
    # self.menu_review.setTitle(_translate("MainWindow", "View"))
    self.menu_help = QtWidgets.QMenu(self.menubar)                                              # 添加“帮助”菜单
    self.menu_help.setObjectName("menu_help")
    self.menu_help.setTitle(_translate("MainWindow", "Help"))
    self.menu_exit = QtWidgets.QMenu(self.menubar)                                              # 添加“退出”菜单
    self.menu_exit.setObjectName("menu_exit")
    self.menu_exit.setTitle(_translate("MainWindow", "Exit"))
    MainWindow.setMenuBar(self.menubar)

    # self.menu_module_all = QtWidgets.QAction(MainWindow)                                        # 添加“模块”-->“ALL”菜单项
    # self.menu_module_all.setEnabled(True)                                                       # 设置菜单项可用
    # self.menu_module_all.setText("ALL")
    # self.menu_module.addAction(self.menu_module_all)                                            # 在“模块”菜单中添加“ALL”菜单项
    # self.menu_module_all.triggered.connect(self.setStatusBar_menu_module_all)                   # 为“ALL”菜单项绑定信号
    # self.menu_module.addSeparator()                                                             # 添加菜单项分隔线
    # # self.menu_module_script = QtWidgets.QAction(MainWindow)                                     # 添加“模块”-->“SCRIPT”菜单项
    # # self.menu_module_script.setEnabled(True)                                                    # 设置菜单项可用
    # # self.menu_module_script.setText("SCRIPT")
    # # self.menu_module.addAction(self.menu_module_script)                                         # 在“模块”菜单中添加“SCRIPT”菜单项
    # # self.menu_module_script.triggered.connect(self.setStatusBar_menu_module_script)             # 为“SCRIPT”菜单项绑定信号
    # # self.menu_module.addSeparator()                                                             # 添加菜单项分隔线
    # self.menu_module_prepar = QtWidgets.QAction(MainWindow)                                     # 添加“模块”-->“PREPAR”菜单项
    # self.menu_module_prepar.setEnabled(True)                                                    # 设置菜单项可用
    # self.menu_module_prepar.setText("PREPAR")
    # self.menu_module.addAction(self.menu_module_prepar)                                         # 在“模块”菜单中添加“PREPAR”菜单项
    # self.menu_module_prepar.triggered.connect(self.setStatusBar_menu_module_prepar)             # 为“PREPAR”菜单项绑定信号
    # self.menu_module_prep = QtWidgets.QAction(MainWindow)                                       # 添加“模块”-->“PREP”菜单项
    # self.menu_module_prep.setEnabled(True)                                                      # 设置菜单项可用
    # self.menu_module_prep.setText("PREP")
    # self.menu_module.addAction(self.menu_module_prep)                                           # 在“模块”菜单中添加“PREP”菜单项
    # self.menu_module_prep.triggered.connect(self.setStatusBar_menu_module_prep)                 # 为“PREP”菜单项绑定信号
    # self.menu_module_haplotype = QtWidgets.QAction(MainWindow)                                  # 添加“模块”-->“HAPLOTYPE”菜单项
    # self.menu_module_haplotype.setEnabled(True)                                                 # 设置菜单项可用
    # self.menu_module_haplotype.setText("HAPLOTYPE")
    # self.menu_module.addAction(self.menu_module_haplotype)                                      # 在“模块”菜单中添加“HAPLOTYPE”菜单项
    # self.menu_module_haplotype.triggered.connect(self.setStatusBar_menu_module_haplotype)       # 为“HAPLOTYPE”菜单项绑定信号
    # self.menu_module_afd = QtWidgets.QAction(MainWindow)                                        # 添加“模块”-->“AFD”菜单项
    # self.menu_module_afd.setEnabled(True)                                                       # 设置菜单项可用
    # self.menu_module_afd.setText("AFD")
    # self.menu_module.addAction(self.menu_module_afd)                                            # 在“模块”菜单中添加“AFD”菜单项
    # self.menu_module_afd.triggered.connect(self.setStatusBar_menu_module_afd)                   # 为“AFD”菜单项绑定信号
    # self.menu_module_polish = QtWidgets.QAction(MainWindow)                                     # 添加“模块”-->“POLISH”菜单项
    # self.menu_module_polish.setEnabled(True)                                                    # 设置菜单项可用
    # self.menu_module_polish.setText("POLISH")
    # self.menu_module.addAction(self.menu_module_polish)                                         # 在“模块”菜单中添加“POLISH”菜单项
    # self.menu_module_polish.triggered.connect(self.setStatusBar_menu_module_polish)             # 为“POLISH”菜单项绑定信号
    # self.menu_module_qtlpick = QtWidgets.QAction(MainWindow)                                    # 添加“模块”-->“QTL_PICK”菜单项
    # self.menu_module_qtlpick.setEnabled(True)                                                   # 设置菜单项可用
    # self.menu_module_qtlpick.setText("QTL_PICK")
    # self.menu_module.addAction(self.menu_module_qtlpick)                                        # 在“模块”菜单中添加“QTL_PICK”菜单项
    # self.menu_module_qtlpick.triggered.connect(self.setStatusBar_menu_module_qtlpick)           # 为“QTL_PICK”菜单项绑定信号
    # self.menu_module_igv = QtWidgets.QAction(MainWindow)                                        # 添加“模块”-->“IGV”菜单项
    # self.menu_module_igv.setEnabled(True)                                                       # 设置菜单项可用
    # self.menu_module_igv.setText("IGV")
    # self.menu_module.addAction(self.menu_module_igv)                                            # 在“模块”菜单中添加“IGV”菜单项
    # self.menu_module_igv.triggered.connect(self.setStatusBar_menu_module_igv)                   # 为“IGV”菜单项绑定信号
    # self.menubar.addAction(self.menu_module.menuAction())                                       # 将“模块”菜单的菜单项添加到菜单栏中

    self.menu_functions_all = QtWidgets.QAction(MainWindow)                                     # 添加“功能”-->“ALL”菜单项
    self.menu_functions_all.setEnabled(True)                                                    # 设置菜单项可用
    self.menu_functions_all.setText("ALL")
    self.menu_functions.addAction(self.menu_functions_all)                                      # 在“功能”菜单中添加“ALL”菜单项
    self.menu_functions_all.triggered.connect(self.menu_functions_intro_all)                    # 为“ALL”菜单项绑定信号
    self.menu_functions.addSeparator()                                                          # 添加菜单项分隔线
    # self.menu_functions_script = QtWidgets.QAction(MainWindow)                                  # 添加“功能”-->“SCRIPT”菜单项
    # self.menu_functions_script.setEnabled(True)                                                 # 设置菜单项可用
    # self.menu_functions_script.setText("SCRIPT")
    # self.menu_functions.addAction(self.menu_functions_script)                                   # 在“功能”菜单中添加“SCRIPT”菜单项
    # self.menu_functions_script.triggered.connect(self.menu_functions_intro_script)              # 为“SCRIPT”菜单项绑定信号
    # self.menu_functions.addSeparator()                                                          # 添加菜单项分隔线
    self.menu_functions_prepar = QtWidgets.QAction(MainWindow)                                  # 添加“功能”-->“PREPAR”菜单项
    self.menu_functions_prepar.setEnabled(True)                                                 # 设置菜单项可用
    self.menu_functions_prepar.setText("PREPAR")
    self.menu_functions.addAction(self.menu_functions_prepar)                                   # 在“功能”菜单中添加“PREPAR”菜单项
    self.menu_functions_prepar.triggered.connect(self.menu_functions_intro_prepar)              # 为“PREPAR”菜单项绑定信号
    self.menu_functions_prep = QtWidgets.QAction(MainWindow)                                    # 添加“功能”-->“PREP”菜单项
    self.menu_functions_prep.setEnabled(True)                                                   # 设置菜单项可用
    self.menu_functions_prep.setText("PREP")
    self.menu_functions.addAction(self.menu_functions_prep)                                     # 在“功能”菜单中添加“PREP”菜单项
    self.menu_functions_prep.triggered.connect(self.menu_functions_intro_prep)                  # 为“PREP”菜单项绑定信号
    self.menu_functions_haplotype = QtWidgets.QAction(MainWindow)                               # 添加“功能”-->“HAPLOTYPE”菜单项
    self.menu_functions_haplotype.setEnabled(True)                                              # 设置菜单项可用
    self.menu_functions_haplotype.setText("HAPLOTYPE")
    self.menu_functions.addAction(self.menu_functions_haplotype)                                # 在“功能”菜单中添加“HAPLOTYPE”菜单项
    self.menu_functions_haplotype.triggered.connect(self.menu_functions_intro_haplotype)        # 为“HAPLOTYPE”菜单项绑定信号
    self.menu_functions_afd = QtWidgets.QAction(MainWindow)                                     # 添加“功能”-->“AFD”菜单项
    self.menu_functions_afd.setEnabled(True)                                                    # 设置菜单项可用
    self.menu_functions_afd.setText("AFD")
    self.menu_functions.addAction(self.menu_functions_afd)                                      # 在“功能”菜单中添加“AFD”菜单项
    self.menu_functions_afd.triggered.connect(self.menu_functions_intro_afd)                    # 为“AFD”菜单项绑定信号
    self.menu_functions_polish = QtWidgets.QAction(MainWindow)                                  # 添加“功能”-->“POLISH”菜单项
    self.menu_functions_polish.setEnabled(True)                                                 # 设置菜单项可用
    self.menu_functions_polish.setText("POLISH")
    self.menu_functions.addAction(self.menu_functions_polish)                                   # 在“功能”菜单中添加“POLISH”菜单项
    self.menu_functions_polish.triggered.connect(self.menu_functions_intro_polish)              # 为“POLISH”菜单项绑定信号
    self.menu_functions_qtlpick = QtWidgets.QAction(MainWindow)                                 # 添加“功能”-->“QTL_PICK”菜单项
    self.menu_functions_qtlpick.setEnabled(True)                                                # 设置菜单项可用
    self.menu_functions_qtlpick.setText("QTL_PICK")
    self.menu_functions.addAction(self.menu_functions_qtlpick)                                  # 在“功能”菜单中添加“QTL_PICK”菜单项
    self.menu_functions_qtlpick.triggered.connect(self.menu_functions_intro_qtlpick)            # 为“QTL_PICK”菜单项绑定信号
    self.menu_functions_igv = QtWidgets.QAction(MainWindow)                                     # 添加“功能”-->“IGV”菜单项
    self.menu_functions_igv.setEnabled(True)                                                    # 设置菜单项可用
    self.menu_functions_igv.setText("IGV")
    self.menu_functions.addAction(self.menu_functions_igv)                                      # 在“功能”菜单中添加“IGV”菜单项
    self.menu_functions_igv.triggered.connect(self.menu_functions_intro_igv)                    # 为“IGV”菜单项绑定信号
    self.menubar.addAction(self.menu_functions.menuAction())                                    # 将“功能”菜单的菜单项添加到菜单栏中

    self.menu_parameters_all = QtWidgets.QAction(MainWindow)                                    # 添加“参数”-->“ALL”菜单项
    self.menu_parameters_all.setEnabled(True)                                                   # 设置菜单项可用
    self.menu_parameters_all.setText("ALL")
    self.menu_parameters.addAction(self.menu_parameters_all)                                    # 在“参数”菜单中添加“ALL”菜单项
    # from src.functions.menu_parameters_paraset import menu_parameters_all_paraset
    self.menu_parameters_all.triggered.connect(self.menu_parameters_all_paraset)                # 为“ALL”菜单项绑定信号
    self.menu_parameters.addSeparator()                                                         # 添加菜单项分隔线
    # self.menu_parameters_script = QtWidgets.QAction(MainWindow)                                 # 添加“参数”-->“SCRIPT”菜单项
    # self.menu_parameters_script.setEnabled(True)                                                # 设置菜单项可用
    # self.menu_parameters_script.setText("SCRIPT")
    # self.menu_parameters.addAction(self.menu_parameters_script)                                 # 在“参数”菜单中添加“SCRIPT”菜单项
    # self.menu_parameters_script.triggered.connect(self.menu_parameters_script_paraset)          # 为“SCRIPT”菜单项绑定信号
    # self.menu_parameters.addSeparator()                                                         # 添加菜单项分隔线
    self.menu_parameters_prepar = QtWidgets.QAction(MainWindow)                                 # 添加“参数”-->“PREPAR”菜单项
    self.menu_parameters_prepar.setEnabled(True)                                                # 设置菜单项可用
    self.menu_parameters_prepar.setText("PREPAR")
    self.menu_parameters.addAction(self.menu_parameters_prepar)                                 # 在“参数”菜单中添加“PREPAR”菜单项
    self.menu_parameters_prepar.triggered.connect(self.menu_parameters_prepar_paraset)          # 为“PREPAR”菜单项绑定信号
    self.menu_parameters_prep = QtWidgets.QAction(MainWindow)                                   # 添加“参数”-->“PREP”菜单项
    self.menu_parameters_prep.setEnabled(True)                                                  # 设置菜单项可用
    self.menu_parameters_prep.setText("PREP")
    self.menu_parameters.addAction(self.menu_parameters_prep)                                   # 在“参数”菜单中添加“PREP”菜单项
    self.menu_parameters_prep.triggered.connect(self.menu_parameters_prep_paraset)              # 为“PREP”菜单项绑定信号
    self.menu_parameters_haplotype = QtWidgets.QAction(MainWindow)                              # 添加“参数”-->“HAPLOTYPE”菜单项
    self.menu_parameters_haplotype.setEnabled(True)                                             # 设置菜单项可用
    self.menu_parameters_haplotype.setText("HAPLOTYPE")
    self.menu_parameters.addAction(self.menu_parameters_haplotype)                              # 在“参数”菜单中添加“HAPLOTYPE”菜单项
    self.menu_parameters_haplotype.triggered.connect(self.menu_parameters_haplotype_paraset)    # 为“HAPLOTYPE”菜单项绑定信号
    self.menu_parameters_afd = QtWidgets.QAction(MainWindow)                                    # 添加“参数”-->“AFD”菜单项
    self.menu_parameters_afd.setEnabled(True)                                                   # 设置菜单项可用
    self.menu_parameters_afd.setText("AFD")
    self.menu_parameters.addAction(self.menu_parameters_afd)                                    # 在“参数”菜单中添加“AFD”菜单项
    self.menu_parameters_afd.triggered.connect(self.menu_parameters_afd_paraset)                # 为“AFD”菜单项绑定信号
    self.menu_parameters_polish = QtWidgets.QAction(MainWindow)                                 # 添加“参数”-->“POLISH”菜单项
    self.menu_parameters_polish.setEnabled(True)                                                # 设置菜单项可用
    self.menu_parameters_polish.setText("POLISH")
    self.menu_parameters.addAction(self.menu_parameters_polish)                                 # 在“参数”菜单中添加“POLISH”菜单项
    self.menu_parameters_polish.triggered.connect(self.menu_parameters_polish_paraset)          # 为“POLISH”菜单项绑定信号
    self.menu_parameters_qtlpick = QtWidgets.QAction(MainWindow)                                # 添加“参数”-->“QTL_PICK”菜单项
    self.menu_parameters_qtlpick.setEnabled(True)                                               # 设置菜单项可用
    self.menu_parameters_qtlpick.setText("QTL_PICK")
    self.menu_parameters.addAction(self.menu_parameters_qtlpick)                                # 在“参数”菜单中添加“QTL_PICK”菜单项
    self.menu_parameters_qtlpick.triggered.connect(self.menu_parameters_qtlpick_paraset)        # 为“QTL_PICK”菜单项绑定信号
    self.menu_parameters_igv = QtWidgets.QAction(MainWindow)                                    # 添加“参数”-->“IGV”菜单项
    self.menu_parameters_igv.setEnabled(True)                                                   # 设置菜单项可用
    self.menu_parameters_igv.setText("IGV")
    self.menu_parameters.addAction(self.menu_parameters_igv)                                    # 在“参数”菜单中添加“IGV”菜单项
    self.menu_parameters_igv.triggered.connect(self.menu_parameters_igv_paraset)                # 为“IGV”菜单项绑定信号
    self.menubar.addAction(self.menu_parameters.menuAction())                                   # 将“参数”菜单的菜单项添加到菜单栏中

    self.menu_file_opendir = QtWidgets.QAction(MainWindow)                                      # 添加“运行”-->“Open directory”菜单项
    self.menu_file_opendir.setEnabled(True)                                                     # 设置菜单项可用
    self.menu_file_opendir.setText("Open input files")
    self.menu_file.addAction(self.menu_file_opendir)                                            # 在“运行”菜单中添加“Open directory”菜单项
    self.menu_file_opendir.triggered.connect(self.menu_file_opendir_slot)                       # 为“Open directory”菜单项绑定信号
    self.menubar.addAction(self.menu_file.menuAction())                                         # 将“文件”菜单的菜单项添加到菜单栏中

    self.menu_run_all = QtWidgets.QAction(MainWindow)                                           # 添加“运行”-->“ALL”菜单项
    self.menu_run_all.setEnabled(True)                                                          # 设置菜单项可用
    self.menu_run_all.setText("ALL")
    self.menu_run.addAction(self.menu_run_all)                                                  # 在“运行”菜单中添加“ALL”菜单项
    self.menu_run_all.triggered.connect(self.menu_run_all_slot)                                 # 为“ALL”菜单项绑定信号
    self.menubar.addAction(self.menu_run.menuAction())                                          # 将“运行”菜单的菜单项添加到菜单栏中
    self.menu_run.addSeparator()                                                                # 添加菜单项分隔线
    self.menu_run_prepar = QtWidgets.QAction(MainWindow)                                        # 添加“运行”-->“PREPAR”菜单项
    self.menu_run_prepar.setEnabled(True)                                                       # 设置菜单项可用
    self.menu_run_prepar.setText("PREPAR")
    self.menu_run.addAction(self.menu_run_prepar)                                               # 在“运行”菜单中添加“PREPAR”菜单项
    self.menu_run_prepar.triggered.connect(self.menu_run_prepar_slot)                           # 为“PREPAR”菜单项绑定信号
    self.menubar.addAction(self.menu_run.menuAction())                                          # 将“运行”菜单的菜单项添加到菜单栏中
    self.menu_run_prep = QtWidgets.QAction(MainWindow)                                          # 添加“运行”-->“PREP”菜单项
    self.menu_run_prep.setEnabled(True)                                                         # 设置菜单项可用
    self.menu_run_prep.setText("PREP")
    self.menu_run.addAction(self.menu_run_prep)                                                 # 在“运行”菜单中添加“PREP”菜单项
    self.menu_run_prep.triggered.connect(self.menu_run_prep_slot)                               # 为“PREP”菜单项绑定信号
    self.menubar.addAction(self.menu_run.menuAction())                                          # 将“运行”菜单的菜单项添加到菜单栏中
    self.menu_run_haplotype = QtWidgets.QAction(MainWindow)                                     # 添加“运行”-->“HAPLOTYPE”菜单项
    self.menu_run_haplotype.setEnabled(True)                                                    # 设置菜单项可用
    self.menu_run_haplotype.setText("HAPLOTYPE")
    self.menu_run.addAction(self.menu_run_haplotype)                                            # 在“运行”菜单中添加“HAPLOTYPE”菜单项
    self.menu_run_haplotype.triggered.connect(self.menu_run_haplotype_slot)                     # 为“HAPLOTYPE”菜单项绑定信号
    self.menubar.addAction(self.menu_run.menuAction())                                          # 将“运行”菜单的菜单项添加到菜单栏中
    self.menu_run_afd = QtWidgets.QAction(MainWindow)                                           # 添加“运行”-->“AFD”菜单项
    self.menu_run_afd.setEnabled(True)                                                          # 设置菜单项可用
    self.menu_run_afd.setText("AFD")
    self.menu_run.addAction(self.menu_run_afd)                                                  # 在“运行”菜单中添加“AFD”菜单项
    self.menu_run_afd.triggered.connect(self.menu_run_afd_slot)                                 # 为“AFD”菜单项绑定信号
    self.menubar.addAction(self.menu_run.menuAction())                                          # 将“运行”菜单的菜单项添加到菜单栏中
    self.menu_run_polish = QtWidgets.QAction(MainWindow)                                        # 添加“运行”-->“POLISH”菜单项
    self.menu_run_polish.setEnabled(True)                                                       # 设置菜单项可用
    self.menu_run_polish.setText("POLISH")
    self.menu_run.addAction(self.menu_run_polish)                                               # 在“运行”菜单中添加“POLISH”菜单项
    self.menu_run_polish.triggered.connect(self.menu_run_polish_slot)                           # 为“POLISH”菜单项绑定信号
    self.menubar.addAction(self.menu_run.menuAction())                                          # 将“运行”菜单的菜单项添加到菜单栏中
    self.menu_run_qtlpick = QtWidgets.QAction(MainWindow)                                       # 添加“运行”-->“QTL_PICK”菜单项
    self.menu_run_qtlpick.setEnabled(True)                                                      # 设置菜单项可用
    self.menu_run_qtlpick.setText("QTL_PICK")
    self.menu_run.addAction(self.menu_run_qtlpick)                                              # 在“运行”菜单中添加“QTL_PICK”菜单项
    self.menu_run_qtlpick.triggered.connect(self.menu_run_qtlpick_slot)                         # 为“QTL_PICK”菜单项绑定信号
    self.menubar.addAction(self.menu_run.menuAction())                                          # 将“运行”菜单的菜单项添加到菜单栏中
    self.menu_run_igv = QtWidgets.QAction(MainWindow)                                           # 添加“运行”-->“IGV”菜单项
    self.menu_run_igv.setEnabled(True)                                                          # 设置菜单项可用
    self.menu_run_igv.setText("IGV")
    self.menu_run.addAction(self.menu_run_igv)                                                  # 在“运行”菜单中添加“IGV”菜单项
    self.menu_run_igv.triggered.connect(self.menu_run_igv_slot)                                 # 为“IGV”菜单项绑定信号
    self.menubar.addAction(self.menu_run.menuAction())                                          # 将“运行”菜单的菜单项添加到菜单栏中

    self.menu_help_info = QtWidgets.QAction(MainWindow)                                         # 添加“帮助”-->“Help”菜单项
    self.menu_help_info.setEnabled(True)                                                        # 设置菜单项可用
    self.menu_help_info.setText("Info")
    self.menu_help.addAction(self.menu_help_info)                                               # 在“帮助”菜单中添加“Info”菜单项
    self.menu_help_info.triggered.connect(self.menu_help_info_slot)                             # 为“Info”菜单项绑定信号
    self.menubar.addAction(self.menu_help.menuAction())                                         # 将“帮助”菜单的菜单项添加到菜单栏中
    self.menu_help_notes = QtWidgets.QAction(MainWindow)                                        # 添加“帮助”-->“Notes”菜单项
    self.menu_help_notes.setEnabled(True)                                                       # 设置菜单项可用
    self.menu_help_notes.setText("Notes")
    self.menu_help.addAction(self.menu_help_notes)                                              # 在“帮助”菜单中添加“Notes”菜单项
    self.menu_help_notes.triggered.connect(self.menu_help_notes_slot)                           # 为“Notes”菜单项绑定信号
    self.menubar.addAction(self.menu_help.menuAction())                                         # 将“帮助”菜单的菜单项添加到菜单栏中
    self.menu_help.addSeparator()                                                               # 添加菜单项分隔线
    self.menu_help_about = QtWidgets.QAction(MainWindow)                                        # 添加“帮助”-->“About”菜单项
    self.menu_help_about.setEnabled(True)                                                       # 设置菜单项可用
    self.menu_help_about.setText("About")
    self.menu_help.addAction(self.menu_help_about)                                              # 在“帮助”菜单中添加“About”菜单项
    self.menu_help_about.triggered.connect(self.menu_help_about_slot)                           # 为“About”菜单项绑定信号
    self.menubar.addAction(self.menu_help.menuAction())                                         # 将“帮助”菜单的菜单项添加到菜单栏中

    self.menu_exit_script = QtWidgets.QAction(MainWindow)                                       # 添加“退出”-->“Exit”菜单项
    self.menu_exit_script.setEnabled(True)                                                      # 设置菜单项可用
    self.menu_exit_script.setText("Exit")
    self.menu_exit.addAction(self.menu_exit_script)                                             # 在“退出”菜单中添加“Exit”菜单项
    self.menu_exit_script.triggered.connect(self.menu_exit_script_slot)                         # 为“Exit”菜单项绑定信号
    self.menubar.addAction(self.menu_exit.menuAction())                                         # 将“退出”菜单的菜单项添加到菜单栏中

    self.statusbar = QtWidgets.QStatusBar(MainWindow)                                           # 添加状态栏
    self.statusbar.setObjectName("statusbar")
    MainWindow.setStatusBar(self.statusbar)
    # self.statusbar_label = QtWidgets.QLabel(MainWindow)                                         # 添加状态栏标签
    # self.statusbar_label.setText("Please select a module to run")                               # 添加状态栏标签文字
    # self.statusbar.addWidget(self.statusbar_label)

    self.retranslateUi(MainWindow)
    QtCore.QMetaObject.connectSlotsByName(MainWindow)
