#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import pandas
import argparse

parser = argparse.ArgumentParser(description = "add function annotation and expression pattern information for genes in QTL")
parser.add_argument("-expanno", "--expanno", type = str, required = True, help = "directory of expression & annotation files")
parser.add_argument("-gene", "--gene", type = str, required = True, help = "QTL SNV & SV genes file of qtl_pick_dir")
parser.add_argument("-polish", "--polish", type = str, required = True, help = "P/M/H.polished.afd directory")
parser.add_argument("-out", "--out", type = str, required = True, help = "output file of genes")
parser.add_argument("-var", "--var", type = str, required = True, help = "variation type")
args = parser.parse_args() #解析所有参数

mdo_exp_data = pandas.read_table(args.expanno + "/MDO_Exp.tsv", sep = "\t")
gdr_anno_data = pandas.read_table(args.expanno + "/GDR_Anno.tsv", sep = "\t")
md_anno_data = pandas.read_table(args.expanno + "/md.anno", sep = "\t", header = None, names = ["Gene", "Protein", "Anno"])
fruit_exp_data = pandas.read_table(args.expanno + "/fruit.exp", sep = "\t", header = None, names = ["MD", "MDP", "EXP"])
root_exp_data = pandas.read_table(args.expanno + "/root.exp", sep = "\t", header = None, names = ["MD", "MDP", "EXP"])
loc_data = pandas.read_table(args.expanno + "/location.txt", sep = "\t", header = None, names = ["Chr", "Start", "End", "Gene"])

# header = ["CHROM", "POS", "H_REF", "H_ALT", "L_REF", "L_ALT", "H_REF_AF", "H_ALT_AF", "L_REF_AF", "L_ALT_AF", "GV", "G1M", "G750K", "G500K", "G250K"]
p_polished_afd = (pandas.read_table(args.polish + "/P.polished.afd", sep = "\t")).iloc[:,0:15]
m_polished_afd = (pandas.read_table(args.polish + "/M.polished.afd", sep = "\t")).iloc[:, 0:15]
pm_polished_afd = (pandas.read_table(args.polish + "/PM.polished.afd", sep = "\t")).iloc[:, 0:15]
# p_polished_afd.columns = header
# m_polished_afd.columns = header
# pm_polished_afd.columns = header

output = open(args.out, "w", encoding = "utf-8")

if args.var == "snv":
    headerline = "Chr\tQTL_Start\tQTL_End\tPeak_Pos\tPeak_Value\tAccession\tFunc.refGene\tGene_Start\tGene_End\tSNV_Start\tSNV_End\tRef\tAlt\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tPos\tRef\tAlt\tQual\tInfo\tFormat\tPollen Parent\tMaternal Parent"
if args.var == "sv":
    headerline = "Chr\tQTL_Start\tQTL_End\tPeak_Pos\tPeak_Value\tAccession\tFunc.refGene\tGene_Start\tGene_End\tSV_Start\tSV_End\tRef\tAlt\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tPos\tRef\tAlt\tQual\tInfo\tFormat\tPollen Parent\tMaternal Parent"
output.write(headerline + "\tFruit_Expression\tRoot_Expression\tProtein (Aradopsis thaliana)\tAnnotation (Aradopsis thaliana)\tGDR_Protein\tGDR_Annotation\tInterPro\tGprimer_1M\tGprimer_750K\tGprimer_500K\tGprimer_250K\twhole_seedling_SRR768136\tdormant_buds_0month_SRR5252888\tdormant_buds_0month_SRR5252889\tdormant_buds_1months_SRR5252886\tdormant_buds_1months_SRR5252887\tdormant_buds_2months_SRR5252884\tdormant_buds_3months_SRR5252882\tdormant_buds_3months_SRR5252883\tdormant_buds_4months_SRR5252880\tdormant_buds_4months_SRR5252881\tbud_break_SRR5252878\tbud_break_SRR5252879\tflower_SRR3744766\tflower_SRR3744767\tflower_SRR3744768\tstigmas_SRR6308190\tstyles_SRR6308181\tfilaments_SRR6308188\tanthers_SRR6308187\tpollen_SRR6308192\tpetals_SRR6308191\tsepals_SRR6308194\treceptacles_SRR6308193\tovaries_SRR6308189\tfruit_1WAFB_SRR3384922\tfruit_2WAFB_SRR3384921\tfruit_3WAFB_SRR3384920\tfruit_4WAFB_SRR3384919\tfruit_5WAFB_SRR3384923\tfruit_6WAFB_SRR3384918\tfruit_8WAFB_SRR3384917\tfruit_10WAFB_SRR3384916\tfruit_12WAFB_SRR3384915\tfruit_14WAFB_SRR3384914\tfruit_16WAFB_SRR3384913\tfruit_18WAFB_SRR3384912\tfruit_19WAFB_SRR3384911\tfruit_20WAFB_SRR3384910\twhole_fruit_25DPA_SRR768128\twhole_fruit_35DPA_SRR768129\twhole_fruit_60DPA_SRR768130\twhole_fruit_87DPA_SRR768131\timmature_fruit_flesh_SRR7343308\timmature_fruit_flesh_SRR7343309\tripe_fruit_flesh_SRR6320494\tripe_fruit_peel_SRR5405143\tripe_fruit_peel_SRR6320572\tfruit_control_uninfected_SRR7403504\tfruit_control_uninfected_SRR7403506\tfruit_control_uninfected_SRR7403507\tfruit_PhleoR_penicillium_expansum_SRR7403500\tfruit_PhleoR_penicillium_expansum_SRR7403501\tfruit_PhleoR_penicillium_expansum_SRR7403508\tfruit_creA_penicillium_expansum_SRR7403502\tfruit_creA_penicillium_expansum_SRR7403503\tfruit_creA_penicillium_expansum_SRR7403505\tCentral_seeds_SRR1613973\tCentral_seeds_SRR1613974\tCentral_seeds_SRR1613975\tLateral_seeds_SRR1613976\tLateral_seeds_SRR1613977\tLateral_seeds_SRR1613978\tstem_SRR3744786\tstem_SRR3744787\tstem_SRR3744772\tseedling_shoot_apex_SRX765691\ttree_shoot_apex_SRX765683\tleaf_uninfected_SRR1089478\tleaf_ASGV_infected_SRR1089477\tleaf_uninfected_SRR767660\tleaf_2days_post_Venturia_inaequalis_SRR767669\tleaf_4days_post_Venturia_inaequalis_SRR767670\tleaf_6days_post_Venturia_inaequalis_SRR767671\tleaf_8days_post_Venturia_inaequalis_SRR767672\tleaf_10days_post_Venturia_inaequalis_SRR767673\tleaf_12days_post_Venturia_inaequalis_SRR767674\tleaf_14days_post_Venturia_inaequalis_SRR768127\tyoung_leaves_SRR6308182\tleaf_SRR3744769\tleaf_SRR3744770\tleaf_SRR3744771\tleaf_SRR5405145\tleaf_SRR6320561\n")

pattern = re.compile("MD\d\dG\d+")

with open(args.gene, "r") as genefile:
    genelines = genefile.readlines()
for line in genelines:
    # if not re.findall("Accession", line):
    if not line.startswith("#"):
        line = line.rstrip("\n")
        fields = line.split("\t")
        chrom = fields[0]
        accession = fields[5]
        pos = fields[15]
        gene = pattern.findall(line)
        if gene != []:
            gene = gene[0]

            loc_data_line = loc_data[loc_data["Gene"] == gene]
            if loc_data_line.size != 0:
                gene_start = str(format(list(loc_data_line["Start"])[0], ","))   # str(loc_data.iloc[loc_data_line.index[0], 1])
                gene_end = str(format(list(loc_data_line["End"])[0], ","))   # str(loc_data.iloc[loc_data_line.index[0], 2])
            else:
                gene_start, gene_end = "-", "-"

            fruit_exp_line = fruit_exp_data[fruit_exp_data["MD"] == gene]
            if fruit_exp_line.size != 0:
                fruit_exp = str(round(list(fruit_exp_line["EXP"])[0], 2))   # str(fruit_exp_data.iloc[fruit_exp_line.index[0], 2])
            else:
                fruit_exp = "-"

            root_exp_line = root_exp_data[root_exp_data["MD"] == gene]
            if root_exp_line.size != 0:
                root_exp = str(round(list(root_exp_line["EXP"])[0], 2))   # str(root_exp_data.iloc[root_exp_line.index[0], 2])
            else:
                root_exp = "-"

            md_anno_line = md_anno_data[md_anno_data["Gene"] == gene]
            if md_anno_line.size != 0:
                md_protein = list(md_anno_line["Protein"])[0]   # str(md_anno_data.iloc[md_anno_line.index[0], 1])
                md_anno = list(md_anno_line["Anno"])[0]   # str(md_anno_data.iloc[md_anno_line.index[0], 2])
            else:
                md_protein, md_anno = "-", "-"

            gdr_anno_line = gdr_anno_data[gdr_anno_data["Name"] == gene]
            if gdr_anno_line.size == 0:
                gdr_annotation, gdr_protein, interpro = "-", "-", "-"
            else:
                gdr_annotation = list(gdr_anno_line["BLAST"])[0]   # (gdr_anno_data.iloc[gdr_anno_line.index[0]])["BLAST"]
                if type(gdr_annotation) == float:
                    gdr_annotation = str(gdr_annotation)
                interpro = list(gdr_anno_line["InterPro"])[0]   # (gdr_anno_data.iloc[gdr_anno_line.index[0]])["InterPro"]
                if type(interpro) == float:
                    interpro = str(interpro)
                gdr_protein_list = re.findall(r'(tr\|\w+\|\w+)', gdr_annotation)
                if gdr_protein_list != []:
                    gdr_protein = gdr_protein_list[0]
                else:
                    gdr_protein = "-"

            if accession[0] == "P":
                Gprimer_value_line = p_polished_afd[(p_polished_afd["POS"] == pos) & (p_polished_afd["#CHROM"] == chrom)]
                if Gprimer_value_line.size == 0:
                    Gprimer_1M, Gprimer_750K, Gprimer_500K, Gprimer_250K = "-", "-", "-", "-"
                else:
                    Gprimer_1M = str(round(list(Gprimer_value_line["G1M"])[0], 2))
                    Gprimer_750K = str(round(list(Gprimer_value_line["G750K"])[0], 2))
                    Gprimer_500K = str(round(list(Gprimer_value_line["G500K"])[0], 2))
                    Gprimer_250K = str(round(list(Gprimer_value_line["G250K"])[0], 2))

            if accession[0] == "M":
                Gprimer_value_line = m_polished_afd[(m_polished_afd["POS"] == pos) & (m_polished_afd["#CHROM"] == chrom)]
                if Gprimer_value_line.size == 0:
                    Gprimer_1M, Gprimer_750K, Gprimer_500K, Gprimer_250K = "-", "-", "-", "-"
                else:
                    Gprimer_1M = str(round(list(Gprimer_value_line["G1M"])[0], 2))
                    Gprimer_750K = str(round(list(Gprimer_value_line["G750K"])[0], 2))
                    Gprimer_500K = str(round(list(Gprimer_value_line["G500K"])[0], 2))
                    Gprimer_250K = str(round(list(Gprimer_value_line["G250K"])[0], 2))

            if accession[0] == "H":
                Gprimer_value_line = pm_polished_afd[(pm_polished_afd["POS"] == pos) & (pm_polished_afd["#CHROM"] == chrom)]
                if Gprimer_value_line.size == 0:
                    Gprimer_1M, Gprimer_750K, Gprimer_500K, Gprimer_250K = "-", "-", "-", "-"
                else:
                    Gprimer_1M = str(round(list(Gprimer_value_line["G1M"])[0], 2))
                    Gprimer_750K = str(round(list(Gprimer_value_line["G750K"])[0], 2))
                    Gprimer_500K = str(round(list(Gprimer_value_line["G500K"])[0], 2))
                    Gprimer_250K = str(round(list(Gprimer_value_line["G250K"])[0], 2))

            mdo_exp_line = mdo_exp_data[mdo_exp_data["gene"] == gene]
            if mdo_exp_line.size == 0:
                expression = "\t" * 82
            else:
                fpkm_list = list(mdo_exp_data.iloc[mdo_exp_line.index[0], 1:])
                expression = "\t".join([str(round(float(fpkm), 2)) if fpkm != "0" else fpkm for fpkm in fpkm_list])

            newline_list= []
            newline_list.extend(fields[0:6])
            newline_list.append(fields[11])
            newline_list.extend([gene_start, gene_end])
            newline_list.extend(fields[6:11])
            newline_list.extend(fields[12:])
            newline_list.extend([fruit_exp, root_exp, md_protein, md_anno, gdr_protein, gdr_annotation, interpro])
            newline_list.extend([Gprimer_1M, Gprimer_750K, Gprimer_500K, Gprimer_250K, expression])
            output.write("\t".join(newline_list) + "\n")
output.close()
