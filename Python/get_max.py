#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import pandas

# Usage: python get_max.py <parent data from polish step> chrom lower upper

file = sys.argv[1]
chrom = sys.argv[2]
lower = float(sys.argv[3])
upper = float(sys.argv[4])

def argmax(series):
    index = 0
    value = 0
    for i in series.index:
        if (series[i] > value):
            index = i
            value = series[i]
    return index

data = pandas.read_table(file, sep = "\t", header = 0, low_memory = False)
data.columns = ["CHROM", "POS", "H_REF", "H_ALT", "L_REF", "L_ALT", "H_REF_AF", "H_ALT_AF", "L_REF_AF", "L_ALT_AF", "GV", "G1M", "G750K", "G500K", "G250K", "CHR", "POSITION", "REF", "ALT", "P_HAP1", "P_HAP2", "P_NAME", "M_HAP1", "M_HAP2", "M_NAME", "H_HAP1", "H_HAP2", "H_NAME", "L_HAP1", "L_HAP2", "L_NAME"]
data = data.iloc[:, 0:15]
# subset = data[((data["POS"] >= lower) & (data["CHROM"] == chrom)) & ((data["POS"] <= upper) & (data["CHROM"] == chrom))]
subset = data[(data["POS"] >= lower) & (data["POS"] <= upper) & (data["CHROM"] == chrom)]
if subset.size != 0:
    max_index = argmax(subset["G1M"])
    print(str(round(subset.loc[max_index]["G1M"], 4)) + "\t" + str(subset.loc[max_index]["POS"]), end = "")
else:
    print("NA")
