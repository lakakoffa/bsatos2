#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import sys
import argparse

parser = argparse.ArgumentParser(description = "get reads counts data based on the SNP calling result", usage = "python get_counts.py --d <dep> --q <quality> <VCF file>")
parser.add_argument("-d", type = int, default = 10, help = "the theshold of coverage depth")
parser.add_argument("-q", type = int, default = 10, help = "the log-phred SNP/InDel quality")
parser.add_argument("-vcf", type = str, help = "VCF file")
args = parser.parse_args()  # 解析所有参数

dp4 = re.compile("DP4=(\d+),(\d+),(\d+),(\d+)")

with open(args.vcf, "r") as infile:
    for line in infile.readlines():
        line = line.strip()
        if not (line.startswith("#") or ("INDEL" in line)):     # 过滤开头注释行以及（在FILTER列中）标注INDEL的行
            # CHROM、POS、ID、REF、ALT、QUAL、FILTER、INFO、FORMAT、Parent1、Parent2
            fields = line.split("\t")
            chrom, pos = fields[0], fields[1]
            if (float(fields[5]) >= args.q) and not ("," in fields[4]):
                match = dp4.findall(fields[7])     # DP4: Number of high-quality ref-forward, ref-reverse, alt-forward and alt-reverse bases
                ref = int(match[0][0]) + int(match[0][1])
                alt = int(match[0][2]) + int(match[0][3])
                all = ref + alt
                # snp_index = alt / all
                if (all >= args.d):
                    print(chrom + "\t" + pos + "\t" + str(ref) + "\t" + str(alt))
