#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pandas
import argparse

parser = argparse.ArgumentParser(description = "covert QTL, Func_Anno and variation files to Excel")
parser.add_argument("-sv", "--sv", type = str, required = True, help = "SV genes file")
parser.add_argument("-snv", "--snv", type = str, required = True, help = "SNV genes file")
parser.add_argument("-xlsx", "--xlsx", type = str, required = True, help = "output Excel file")
args = parser.parse_args() #解析所有参数

header = ["Chr", "QTL_Start", "QTL_End", "Peak_Pos", "Peak_Value", "Accession", "Func.refGene", "Gene_Start", "Gene_End", "SNV_SV_Start", "SNV_SV_End", "Ref", "Alt",
    "Gene.refGene", "GeneDetail.refGene", "ExonicFunc.refGene", "AAChange.refGene", "Pos", "Ref", "Alt", "Qual", "Info", "Format", "Pollen Parent", "Maternal Parent",
    "Fruit_Expression", "Root_Expression", "Protein", "Annotation", "BLAST_Protein", "BLAST", "InterPro", "Gprimer_1M", "Gprimer_750K", "Gprimer_500K", "Gprimer_250K",
    "whole_seedling_SRR768136", "dormant_buds_0month_SRR5252888", "dormant_buds_0month_SRR5252889", "dormant_buds_1months_SRR5252886", "dormant_buds_1months_SRR5252887",
    "dormant_buds_2months_SRR5252884", "dormant_buds_3months_SRR5252882", "dormant_buds_3months_SRR5252883", "dormant_buds_4months_SRR5252880", "dormant_buds_4months_SRR5252881",
    "bud_break_SRR5252878", "bud_break_SRR5252879", "flower_SRR3744766", "flower_SRR3744767", "flower_SRR3744768", "stigmas_SRR6308190", "styles_SRR6308181", "filaments_SRR6308188",
    "anthers_SRR6308187", "pollen_SRR6308192", "petals_SRR6308191", "sepals_SRR6308194", "receptacles_SRR6308193", "ovaries_SRR6308189", "fruit_1WAFB_SRR3384922", "fruit_2WAFB_SRR3384921",
    "fruit_3WAFB_SRR3384920", "fruit_4WAFB_SRR3384919", "fruit_5WAFB_SRR3384923", "fruit_6WAFB_SRR3384918", "fruit_8WAFB_SRR3384917", "fruit_10WAFB_SRR3384916", "fruit_12WAFB_SRR3384915",
    "fruit_14WAFB_SRR3384914", "fruit_16WAFB_SRR3384913", "fruit_18WAFB_SRR3384912", "fruit_19WAFB_SRR3384911", "fruit_20WAFB_SRR3384910", "whole_fruit_25DPA_SRR768128",
    "whole_fruit_35DPA_SRR768129", "whole_fruit_60DPA_SRR768130", "whole_fruit_87DPA_SRR768131", "immature_fruit_flesh_SRR7343308", "immature_fruit_flesh_SRR7343309",
    "ripe_fruit_flesh_SRR6320494", "ripe_fruit_peel_SRR5405143", "ripe_fruit_peel_SRR6320572", "fruit_control_uninfected_SRR7403504", "fruit_control_uninfected_SRR7403506",
    "fruit_control_uninfected_SRR7403507", "fruit_PhleoR_penicillium_expansum_SRR7403500", "fruit_PhleoR_penicillium_expansum_SRR7403501", "fruit_PhleoR_penicillium_expansum_SRR7403508",
    "fruit_creA_penicillium_expansum_SRR7403502", "fruit_creA_penicillium_expansum_SRR7403503", "fruit_creA_penicillium_expansum_SRR7403505", "Central_seeds_SRR1613973",
    "Central_seeds_SRR1613974", "Central_seeds_SRR1613975", "Lateral_seeds_SRR1613976", "Lateral_seeds_SRR1613977", "Lateral_seeds_SRR1613978", "stem_SRR3744786", "stem_SRR3744787",
    "stem_SRR3744772", "seedling_shoot_apex_SRX765691", "tree_shoot_apex_SRX765683", "leaf_uninfected_SRR1089478", "leaf_ASGV_infected_SRR1089477", "leaf_uninfected_SRR767660",
    "leaf_2days_post_Venturia_inaequalis_SRR767669", "leaf_4days_post_Venturia_inaequalis_SRR767670", "leaf_6days_post_Venturia_inaequalis_SRR767671", "leaf_8days_post_Venturia_inaequalis_SRR767672",
    "leaf_10days_post_Venturia_inaequalis_SRR767673", "leaf_12days_post_Venturia_inaequalis_SRR767674", "leaf_14days_post_Venturia_inaequalis_SRR768127", "young_leaves_SRR6308182",
    "leaf_SRR3744769", "leaf_SRR3744770", "leaf_SRR3744771", "leaf_SRR5405145", "leaf_SRR6320561"]

try:
    sv_data = pandas.read_table(args.sv, sep = "\t", low_memory = False)
    snv_data = pandas.read_table(args.snv, sep = "\t", low_memory = False)
    sv_data.columns = header
    snv_data.columns = header
    snv_sv_data = pandas.concat([sv_data, snv_data], axis = 0)
    if not os.path.exists(args.xlsx):
        snv_sv_data.to_excel(args.xlsx, sheet_name = "SNV_SV", index = False)
    else:
        with pandas.ExcelWriter(args.xlsx) as writer:
            snv_sv_data.to_excel(writer, sheet_name = "SNV_SV", index = False)
except pandas.errors.ParserError:
    print("pandas.errors.ParserError: Error tokenizing data.")
except pandas.errors.EmptyDataError:
    print("pandas.errors.EmptyDataError: No columns to parse from file.")
