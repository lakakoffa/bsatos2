#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import argparse

parser = argparse.ArgumentParser(description = "integrate variant infomation with QTLs")
parser.add_argument("-qtl", "--qtl", type = str, required = True, help = "QTL file of qtl_pick_dir")
parser.add_argument("-snv", "--snv", type = str, required = True, help = "output SNV file of qtl_pick_dir")
parser.add_argument("-sv", "--sv", type = str, required = True, help = "output SV file 0f qtl_pick_dir")
parser.add_argument("-dir", "--dir", type = str, required = True, help = "SNV_SV directory")
args = parser.parse_args() #解析所有参数

snv = open(args.snv, "w")
sv = open(args.sv, "w")
snv.write("#Chr\tQTL_Start\tQTL_End\tPeak_Pos\tPeak_Value\tAccession\tSNV_Start\tSNV_End\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tPos\tRef\tAlt\tQual\tInfo\tFormat\tPollen Parent\tMaternal Parent\n")
sv.write("#Chr\tQTL_Start\tQTL_End\tPeak_Pos\tPeak_Value\tAccession\tSV_Start\tSV_End\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tPos\tRef\tAlt\tQual\tInfo\tFormat\tPollen Parent\tMaternal Parent\n")

with open(args.qtl, "r") as qtlfile:
    qtllines = qtlfile.readlines()
# total_number_len = len(str(len(qtllines) - 3))
pnum = len(str(len([1 for i in qtllines if i.startswith("P")])))
mnum = len(str(len([1 for i in qtllines if i.startswith("M")])))
hnum = len(str(len([1 for i in qtllines if i.startswith("H")])))

for qtlline in qtllines:
    # if not re.findall("Accession", qtlline):
    if not qtlline.startswith("#"):
        qtl_line = qtlline.strip().split("\t")   # 0-Origin 1-Chrom 2-QTL_Start 3-QTL_End 4-Peak_Pos 5-Peak 6-Accession
        # qtl_line[2] = format(int(qtl_line[2]), ",")   # QTL_Start 千分位符格式化
        # qtl_line[3] = format(int(qtl_line[3]), ",")   # QTL_End 千分位符格式化
        # qtl_line[4] = format(int(qtl_line[4]), ",")   # Peak_Pos 千分位符格式化
        qtl_line[5] = '{:.2f}'.format(float(qtl_line[5]))   # Peak_Pos 峰值保留两位小数   str(round(float(qtl_line[5]), 2))
        qtl_accession = qtl_line[6]
        snv_file = args.dir + "/" + qtl_accession + ".snv"
        sv_file = args.dir + "/" + qtl_accession + ".sv"
        accession_list = qtl_accession.split(".")
        if qtlline.startswith("P"):
            qtl_line[6] = '%s.%0*d' % (accession_list[0], pnum, int(accession_list[1]))
        if qtlline.startswith("M"):
            qtl_line[6] = '%s.%0*d' % (accession_list[0], mnum, int(accession_list[1]))
        if qtlline.startswith("H"):
            qtl_line[6] = '%s.%0*d' % (accession_list[0], hnum, int(accession_list[1]))
        newqtlline = str.join("\t", qtl_line[2:7])
        if os.path.exists(snv_file):
            with open(snv_file, "r") as snvfile:
                snvlines = snvfile.readlines()
                if len(snvlines) != 1:
                    for snvline in snvlines[1:]:
                        if re.findall("Chr\d*", snvline, re.IGNORECASE):
                            fields = snvline.split("\t")
                            fields[1] = format(int(fields[1]), ",")
                            fields[2] = format(int(fields[2]), ",")
                            snv.write(fields[0] + "\t" + newqtlline + "\t" + "\t".join(fields[1:10]) + "\t" + str(fields[14]) + "\t" + "\t".join(fields[16:19]) + "\t" + "\t".join(fields[20:24]) + "\n")
        if os.path.exists(sv_file):
            with open(sv_file, "r") as svfile:
                svlines = svfile.readlines()
                if len(svlines) != 1:
                    for svline in svlines[1:]:
                        if re.findall("Chr\d*", svline, re.IGNORECASE):
                            fields = svline.split("\t")
                            fields[1] = format(int(fields[1]), ",")
                            fields[2] = format(int(fields[2]), ",")
                            sv.write(fields[0] + "\t" + newqtlline + "\t" + "\t".join(fields[1:10]) + "\t" + str(fields[14]) + "\t" + "\t".join(fields[16:19]) + "\t" + "\t".join(fields[20:24]) + "\n")
snv.close()
sv.close()
