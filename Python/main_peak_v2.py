#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import pandas
import argparse

parser = argparse.ArgumentParser(description = "calculate main peak and output QTL file")
parser.add_argument("-th1", "--th1", type = float, required = True, help = "threshold value of P data")
parser.add_argument("-th2", "--th2", type = float, required = True, help = "threshold value of M data")
parser.add_argument("-th3", "--th3", type = float, required = True, help = "threshold value of H data")
parser.add_argument("-p", "--p", type = str, required = True, help = "the P data from polish step")
parser.add_argument("-m", "--m", type = str, required = True, help = "the M data from polish step")
parser.add_argument("-pm", "--pm", type = str, required = True, help = "the H data from polish step")
args = parser.parse_args() #解析所有参数

def argmax(series):
    index, value = -1, -1
    for i in series.index:
        if (series[i] > value):
            index = i
            value = series[i]
    return index


def get_peak(file, thres, origin):
    print("#Origin\tChrom\tQTL_Start\tQTL_End\tPeak_Pos\tPeak\tAccession")
    dataframe = pandas.read_table(file, sep = "\t", header = 0, low_memory = False)
    dataframe.columns = ["CHROM", "POS", "H_REF", "H_ALT", "L_REF", "L_ALT", "H_REF_AF", "H_ALT_AF", "L_REF_AF", "L_ALT_AF", "GV", "G1M", "G750K", "G500K", "G250K", "CHR", "POSITION", "REF", "ALT", "P_HAP1", "P_HAP2", "P_NAME", "M_HAP1", "M_HAP2", "M_NAME", "H_HAP1", "H_HAP2", "H_NAME", "L_HAP1", "L_HAP2", "L_NAME"]
    count = 0
    for chrom in pandas.unique(dataframe["CHROM"]):
        data = dataframe[(dataframe["CHROM"] == chrom)].iloc[:, 0:12]
        chr_pos, arr_g1m, arr_pos = [], [], []
        first_line = dataframe.iloc[(data.index)[0], 0:12]
        first_line_g1m = float(first_line["G1M"])
        if first_line_g1m > thres:
            arr_g1m.append(first_line_g1m)
            arr_pos.append(first_line["POS"])
            chr_pos.append("up")
            chr_pos.append(first_line["POS"])
        for index in data.index[1:]:
            line = (dataframe.iloc[index, 0:12]).copy()
            chrom = line["CHROM"]
            arr_g1m.append(line["G1M"])
            arr_pos.append(line["POS"])
            if (len(arr_g1m) == 1):
                continue
            if (len(arr_g1m) == 2):
                if ((arr_g1m[0] >= thres) and (arr_g1m[1] < thres)):
                    chr_pos.append("down")
                    chr_pos.append(arr_pos[0])
                if ((arr_g1m[0] < thres) and (arr_g1m[1] >= thres)):
                    chr_pos.append("up")
                    chr_pos.append(arr_pos[1])
                arr_g1m.pop(0)
                arr_pos.pop(0)
        last_line = dataframe.iloc[(data.index)[-1], 0:12]
        last_line_g1m = last_line["G1M"]
        if last_line_g1m > thres:
            arr_g1m.append(last_line_g1m)
            arr_pos.append(last_line["POS"])
            chr_pos.append("down")
            chr_pos.append(last_line["POS"])

        # tmp = []
        # num = 0
        # for i in chr_pos:
        #     num += 1
        #     tmp.append(i)
        #     if len(tmp) == 4:
        #         if ((chr_pos[0] == "up") and (chr_pos[2] == "down")):
        #             up, lower, down, upper = tmp[0], tmp[1], tmp[2], tmp[3]
        #             subset = data[(data["POS"] >= lower) & (data["POS"] <= upper)]
        #             if subset.size != 0:
        #                 max_index = argmax(subset["G1M"])
        #                 pos = subset.loc[max_index]["POS"]
        #                 G1M = subset.loc[max_index]["G1M"]
        #                 # if((upper - lower) >= 10000):   # 过滤区间长度小于10K的QTL
        #                 count += 1
        #                 chrom_num = re.findall("Chr(\d\d)", chrom, re.IGNORECASE)[0]
        #                 accession = origin + str(chrom_num) + "." + str(count)
        #                 # print(origin + "\t" + chrom + "\t" + format(lower, ",") + "\t" + format(upper, ",") + "\t" + format(pos, ",") + "\t" + str(round(G1M, 2)) + "\t" + accession)
        #                 print(origin + "\t" + chrom + "\t" + str(lower) + "\t" + str(upper) + "\t" + str(pos) + "\t" + str(round(G1M, 2)) + "\t" + accession)
        #         tmp = []
        #         num = 0

        tmp = []
        total_number = len(chr_pos) // 4
        for i in range(total_number):
            if ((chr_pos[i*4+0] == "up") and (chr_pos[i*4+2] == "down")):
                tmp.extend([chr_pos[i*4+0], chr_pos[i*4+1], chr_pos[i*4+2], chr_pos[i*4+3]])
                up, lower, down, upper = tmp[0], tmp[1], tmp[2], tmp[3]
                subset = data[(data["POS"] >= lower) & (data["POS"] <= upper)]
                if subset.size != 0:
                    max_index = argmax(subset["G1M"])
                    pos = subset.loc[max_index]["POS"]
                    G1M = subset.loc[max_index]["G1M"]
                    # if((upper - lower) >= 10000):   # 过滤区间长度小于10K的QTL
                    count += 1
                    chrom_num = re.findall("Chr(\d\d)", chrom, re.IGNORECASE)[0]
                    accession = origin + str(chrom_num) + "." + str(count)
                    # print(origin + "\t" + chrom + "\t" + format(lower, ",") + "\t" + format(upper, ",") + "\t" + format(pos, ",") + "\t" + str(round(G1M, 2)) + "\t" + accession)
                    print(origin + "\t" + chrom + "\t" + str(lower) + "\t" + str(upper) + "\t" + str(pos) + "\t" + str(round(G1M, 2)) + "\t" + accession)
            tmp = []

get_peak(args.p, round(float(args.th1), 2), "P")     # polish_dir/P.polished.afd 2.771678 P
get_peak(args.m, round(float(args.th2), 2), "M")     # polish_dir/M.polished.afd 7.303028 M
get_peak(args.pm, round(float(args.th3), 2), "H")    # polish_dir/PM.polished.afd 3.390983 H
