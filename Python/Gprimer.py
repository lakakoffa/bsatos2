#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import pandas
import argparse

parser = argparse.ArgumentParser(description = "Gprimer values")
parser.add_argument("-gene", "--gene", type = str, required = True, help = "QTL SNV & SV genes file of qtl_pick_dir")
parser.add_argument("-polish", "--polish", type = str, required = True, help = "P/M/H.polished.afd directory")
parser.add_argument("-out", "--out", type = str, required = True, help = "output file of genes")
args = parser.parse_args() #解析所有参数

p_polished_afd = (pandas.read_table(args.polish + "/P.polished.afd", sep = "\t")).iloc[:,0:15]
m_polished_afd = (pandas.read_table(args.polish + "/M.polished.afd", sep = "\t")).iloc[:, 0:15]
pm_polished_afd = (pandas.read_table(args.polish + "/PM.polished.afd", sep = "\t")).iloc[:, 0:15]

out =  open(args.out, "w", encoding = "utf-8")
out.write("Gprimer_1M\tGprimer_750K\tGprimer_500K\tGprimer_250K\n")

with open(args.gene, "r", encoding = "utf-8") as f:
    genelines = f.readlines()
for line in genelines:
    if not line.startswith("#"):
        line = line.rstrip("\n")
        fields = line.split("\t")
        chrom = fields[0]
        accession = fields[5]
        pos = fields[15]
        # print(chrom + "\t" + accession[0] + "\t" + pos)

        if accession[0] == "P":
            Gprimer_value_line = p_polished_afd[(p_polished_afd["POS"] == pos) & (p_polished_afd["#CHROM"] == chrom)]
            if Gprimer_value_line.size != 0:
                Gprimer_1M = str(round(list(Gprimer_value_line["G1M"])[0], 4))
                Gprimer_750K = str(round(list(Gprimer_value_line["G750K"])[0], 4))
                Gprimer_500K = str(round(list(Gprimer_value_line["G500K"])[0], 4))
                Gprimer_250K = str(round(list(Gprimer_value_line["G250K"])[0], 4))
            else:
                Gprimer_1M, Gprimer_750K, Gprimer_500K, Gprimer_250K = "-", "-", "-", "-"

        if accession[0] == "M":
            Gprimer_value_line = m_polished_afd[(m_polished_afd["POS"] == pos) & (m_polished_afd["#CHROM"] == chrom)]
            if Gprimer_value_line.size != 0:
                Gprimer_1M = str(round(list(Gprimer_value_line["G1M"])[0], 4))
                Gprimer_750K = str(round(list(Gprimer_value_line["G750K"])[0], 4))
                Gprimer_500K = str(round(list(Gprimer_value_line["G500K"])[0], 4))
                Gprimer_250K = str(round(list(Gprimer_value_line["G250K"])[0], 4))
            else:
                Gprimer_1M, Gprimer_750K, Gprimer_500K, Gprimer_250K = "-", "-", "-", "-"

        if accession[0] == "H":
            Gprimer_value_line = pm_polished_afd[(pm_polished_afd["POS"] == pos) & (pm_polished_afd["#CHROM"] == chrom)]
            if Gprimer_value_line.size != 0:
                Gprimer_1M = str(round(list(Gprimer_value_line["G1M"])[0], 4))
                Gprimer_750K = str(round(list(Gprimer_value_line["G750K"])[0], 4))
                Gprimer_500K = str(round(list(Gprimer_value_line["G500K"])[0], 4))
                Gprimer_250K = str(round(list(Gprimer_value_line["G250K"])[0], 4))
            else:
                Gprimer_1M, Gprimer_750K, Gprimer_500K, Gprimer_250K = "-", "-", "-", "-"

        outline = "\t".join([Gprimer_1M, Gprimer_750K, Gprimer_500K, Gprimer_250K])
        out.write(outline + "\n")
        if (Gprimer_1M != "-") or (Gprimer_750K != "-") or (Gprimer_500K != "-") or (Gprimer_250K != "-"):
            print(outline)
out.close()
