#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import pandas

# Usage: python get_interval.py <parent data from polish step> chrom

data = pandas.read_table(sys.argv[1], sep = "\t", header = 0, low_memory = False)
data.columns = ["CHROM", "POS", "H_REF", "H_ALT", "L_REF", "L_ALT", "H_REF_AF", "H_ALT_AF", "L_REF_AF", "L_ALT_AF", "GV", "G1M", "G750K", "G500K", "G250K", "CHR", "POSITION", "REF", "ALT", "P_HAP1", "P_HAP2", "P_NAME", "M_HAP1", "M_HAP2", "M_NAME", "H_HAP1", "H_HAP2", "H_NAME", "L_HAP1", "L_HAP2", "L_NAME"]
subset = data[(data["CHROM"] == sys.argv[2])]
if subset.shape[0] != 0:   # subset.size != 0
    start = subset.index[0]
    end = subset.index[-1]
    print(str(start) + "\t" + str(end), end = "")
else:
    print("NA")
