#!/usr/bin/env Rscript
rm(list = ls())
library(parallel)
# library(foreach)
# library(doParallel)

smooth <- function(x, w) {
    pos <- x[,2];
    g <- x[,11];
    len <- length(pos);
    vec <- t(matrix(rep(1, len)));
    A <- pos %*% vec;
    X <- abs(A - t(A));
    f <- matrix(rep(1, len^2), nrow = len, ncol = len);
    f[X <= w/2] = 1;
    f[X > w/2] = 0;
    D <- X * f / w;
    D <- ((1 - D^3)^3) * f;
    E <- apply(D, 1, sum) %*% vec;
    G <- g %*% vec;
    N <- apply(t(G) * (D/E), 1, sum);
    dat <- data.frame(x[,1], x[,2], N);
    return(dat);
}

################################################################################

ke <- function(x, w, fn) {
    ## fn bigger, slower, but lower memory
    if (fn == 1) {
        sm <- smooth(x, w);
        return(sm[,3]);
    } else {
        end <- x[nrow(x), 2];
        n <- ceiling(end/w);
        if (fn > n) {fn <- n} else {fn <- fn}
        thes <- n / (sqrt((end/w) - (1/4)));
        if (fn < thes) {fn <- 2*ceiling(ceiling(fn)/2)} else {fn <- fn}
        array <- c(seq(0, end, by = w * round(n/fn)), end);
        for (i in c(1:(length(array) - 1))) {
            k <- i + 1;
            if (i == 1) {
                s <- array[i];
                stim <- array[i];
            } else {
                s <- array[i] - w/2;
                stim <- array[i];
            }
            if (i == (length(array) - 1)) {
                e <- array[k];
                etrim <- array[k];
            } else {
                e = array[k] + w/2;
                etrim <- array[k];
            }
            sub <- x[(x[,2] >= s) & (x[,2] <= e),];
            sm <- smooth(sub, w);
            sm_s <- sm[(sm[,2] >= stim) & (sm[,2] <= etrim),];
            if (i == 1) {res <- sm_s} else {res <- rbind(res, sm_s)}
            if (i %% 1000 == 0)
                cat(i, "lines processed ......", as.character(Sys.time()), "\n");
        }
    }
    return(res[,3]);
}

################################################################################

# ke2 <- function(x, w) {
#     H <- c();
#     for (i in 1:nrow(x)) {
#         D <- c();
#         min <- x[i,2] - w/2;
#         max <- x[i,2] + w/2;
#         subset <- x[x[,2] > min & x[,2] < max,];
#         D <- abs(subset[2] - x[i,2]) / w;
#         D3 <- (1 - D^3)^3;
#         n <- subset[11] * (D3 / sum(D3));
#         H[i] <- sum(n);
#         if (i %% 1000 == 0)
#             cat(i, "lines processed ......", as.character(Sys.time()), "\n");
#     }
#     return(H);
# }

# ke2 <- function(x, w) {
#     H <- c();
#     kernel <- function(i) {
#         subset <- x[x[,2] > (x[i,2] - w/2) & x[,2] < (x[i,2] + w/2),];
#         D <- c();
#         D <- abs(subset[2] - x[i,2]) / w;
#         D3 <- (1 - D^3)^3;
#         n <- subset[11] * (D3 / sum(D3));
#         H <- c(H, sum(n));
#         return(H)
#     }
#     foreach(i = 1:nrow(data), .combine = c, .export = "H") %dopar% kernel(i)
# }

ke2 <- function(x, w) {
    H <- c();
    kernel <- function(i) {
        subset <- x[(x[,2] > (x[i,2] - w/2)) & (x[,2] < (x[i,2] + w/2)),];
        D3 <- c();
        D3 <- (1 - (abs(subset[2] - x[i,2]) / w)^3)^3;
        return(c(H, sum(subset[11] * (D3 / sum(D3)))))
    }
    parSapply(cluster, 1:nrow(data), function(i) kernel(i))
}

################################################################################

argv <- commandArgs(trailingOnly = T);
if (length(argv) != 6) {stop("the number of parameters must be 6!")}
cores <- detectCores(logical = F);
cluster <- makeCluster(floor(cores/2));
# registerDoParallel(cluster, cores = floor(cores/2));
data <- read.table(argv[1], header = F);
colnames(data) <- c("CHR", "POS", "H_REF", "H_ALT", "L_REF", "L_ALT");
chr <- argv[2];
size <- as.numeric(argv[4]);
fn <- as.numeric(argv[6]);
data <- data[data[1] == chr,];

data$H_REF_F <- (data$H_REF) / (data$H_REF + data$H_ALT);
data$H_ALT_F <- (data$H_ALT) / (data$H_REF + data$H_ALT);
data$L_REF_F <- (data$L_REF) / (data$L_REF + data$L_ALT);
data$L_ALT_F <- (data$L_ALT) / (data$L_REF + data$L_ALT);
data$abs <- abs(data$H_ALT_F - data$L_ALT_F);   # absolute distance
size1 <- 0.75 * size;
size2 <- 0.5 * size;
size3 <- 0.25 * size;

clusterExport(cluster, c("data", "size", "size1", "size2", "size3"));

if (as.integer(argv[5]) == 1) {
    cat("\nCalculating S1M of", argv[3], "......", as.character(Sys.time()), "\n");
    S1M <- ke(data, size, fn);
    cat("Calculating S750K of", argv[3], "......", as.character(Sys.time()), "\n");
    S750K <- ke(data, size1, fn);
    cat("Calculating S500K of", argv[3], "......", as.character(Sys.time()), "\n");
    S500K <- ke(data, size2, fn);
    cat("Calculating S250K of", argv[3], "......", as.character(Sys.time()), "\n");
    S250K <- ke(data, size3, fn);
    data <- data.frame(data, S1M, S750K, S500K, S250K);
} else {
    clusterExport(cl, c("data", "size", "size1", "size2", "size3"));
    cat("\nCalculating S1M of", argv[3], "......", as.character(Sys.time()), "\n");
    S1M <- ke2(data, size);
    cat("Calculating S750K of", argv[3], "......", as.character(Sys.time()), "\n");
    S750K <- ke2(data, size1);
    cat("Calculating S500K of", argv[3], "......", as.character(Sys.time()), "\n");
    S500K <- ke2(data, size2);
    cat("Calculating S250K of", argv[3], "......", as.character(Sys.time()), "\n");
    S250K <- ke2(data, size3);
    data <- data.frame(data, S1M, S750K, S500K, S250K);
}
stopCluster(cluster);

name <- paste(argv[2], argv[3], "afd", sep = "_");
write.table(data, file = name, quote = F, sep = "\t", row.names = F, col.names = F)
