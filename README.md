### BSATOS (Bulked segregant analysis tools for outbreeding species)

Welcome to the BSATOS wiki! The Bulked Segregant Analyis Tools for Outbreeding Species (BSATOS) 
is developed by the institute for horticultue plants, China Argicultural University and 
Edmund Mach Foundation, Italy. BSATOS is designed for NGS-based bulked segregant analysis 
for outbreeding species, includings fruits trees, such as apple, and cirtus.

To improve the gene mapping efficiency of next generation sequencing-based segregant analysis (BSA) 
in outbreeding species and realize the rapid candidate gene mining based on multi-omics data, 
bulked segregant analysis tools for outbreeding species (BSATOS) was developed. Different from 
the classic two-way pseudo-testcross (PT) strategy, local phased 3-way BSA mapping strategy was 
introduced to use three type of segregate makers unbiased. After comparing different statistical 
methods to evaluate the allele frequency difference, G value method was selected. Multi-round 
G' value screening and multi-omics data based gene mining were also integrated.


Introduction and Quick start: https://github.com/maypoleflyn/BSATOS/wiki

Installation: https://github.com/maypoleflyn/BSATOS/wiki/Installation

Commands: https://github.com/maypoleflyn/BSATOS/wiki/Commands

Examples: https://github.com/maypoleflyn/BSATOS/wiki/Examples

Documentation: https://drive.google.com/file/d/1gPjisojwkKkCgLNS0fu3uqafwUp_o7Vm/view?usp=sharing


Citation:
Fei Shen et al, A bulked segregant analysis tool for out-crossing species (BSATOS) 
and QTL-based genomics-assisted prediction of complex traits in apple, 
Journal of Advanced Research, 2022, 2090-1232, https://doi.org/10.1016/j.jare.2022.03.013.

Please contact **Fei Shen (shenf1028@gmail.com)** or **Luca Bianco (luca.bianco@fmach.it)**, if you have any question.


### (0) ###
The reference genome `Malus_domestica.fa` (GDDH13 v1.1) of apple is 687Mb, and because of the limitation that Github cannot upload files larger than 100Mb, the whole reference genome for `Malus_domestica.fa` was not uploaded to the directory `./database/Malus_domestica`.


If you need to use these files, you will need to run the scripts in `./database/Malus_domestica` to regenerate the file manually.

On the one hand, you could run `concatenate.ps1` on Windows platform and `concatenate.sh` on UNIX/Linux/MacOS platform.


On the other hand, you can also download it by the link below (GDR) and rename it to `Malus_domestica.fa`.

https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica-genome_GDDH13_v1.1/assembly/GDDH13_1-1_formatted.fasta.gz


### (1) ###
When you are using Qt GUI verion of BSATOS2, please input the FULL PATH of
the required input files to make sure these file could be found when you're in the diretory of BSATOS2, otherwise BSATOS2 may don't work properly.

If you're in the diretory of the required input files, FULL PATH is not necessary.

However, the path of `BSATOS2' should be added into your system PATH.
Arguments unseted will apply to default values.


### (2) ###
—————————— Miniforge3 & BSATOS2 Setup ——————————

wget https://mirrors.bfsu.edu.cn/miniforge/LatestRelease/Miniforge3-Linux-x86_64.sh

sh Miniforge3-Linux-x86_64.sh

source ~/.bashrc

cp BSATOS2/condarc.txt ~/.condarc

conda init bash

conda clean -i

conda install -y mamba

mamba init bash

source ~/.bashrc

chmod -R 777 BSATOS2

mamba env create -f BSATOS2/bsatos2.yml

BSATOS2/bsatos all --o BSA_result --r reference_genome.fa --gtf genes.gtf --pb P.bam --mb M.bam --hb H.bam --lb L.bam
